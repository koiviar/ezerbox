<?php

namespace App\Console;

use App\Cron\CancelNotNeededServers;
use App\Cron\CheckSpaceUsage;
use App\Cron\PayOverLimit;
use App\Cron\SaveDiskStats;
use App\Helpers\FileBalancer;
use App\Helpers\FileProviderCache;
use App\Helpers\HostingApi;
use App\Helpers\Memory;
use App\Helpers\Payment\Qiwi;
use App\Model\ApiLog;
use App\Model\BaseModel;
use App\Model\Dir;
use App\Model\DownloadLink;
use App\Model\File;
use App\Model\Miniature;
use App\Model\Server;
use App\Model\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Storage;
use function Clue\StreamFilter\fun;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->call(function () {
//            $files = File::where('disk', '!=', 'clo')->limit(1000)->orderByDesc('created_at')->get();
//            foreach ($files as $file) {
//                $file->moveRealFile('clo', false, 'low');
//            }
//        })->everyMinute();

        // удаление временных файлов, которые по ошибке остались
        $schedule->call(function () {
            $files = Storage::disk('local')->allFiles('tmp');
            foreach ($files as $file) {
                if (Storage::disk('local')->exists($file) &&
                    time() - Storage::disk('local')->lastModified($file) > 86400) {
                    Storage::disk('local')->delete($file);
                }
            }
        })->everyFifteenMinutes();

        // удаляем подписки просроченые
        $schedule->call(function () {
            \App\Model\DiskSubscription::dropOldSubscriptions();
        })->hourly();

        // удаление старых файлов из корзины
        $schedule->call(function () {
            File::deleteOldFiles();
            Dir::deleteOldDirs();
        })->everyMinute();

        // ежечасная фиксация превышения лимитов
        $schedule->call(function () {
            CheckSpaceUsage::run();
        })->hourly();

        // списываем средства за превышение лимита
        $schedule->call(function () {
            PayOverLimit::run();
        })->dailyAt('23:30')->timezone('Europe/Moscow');

        // удаляем просроченные ссылки на скачивание
        $schedule->call(function () {
            DownloadLink::deleteOldLinks();
        })->cron('*/10 * * * *');

        // обновление статистики использования серверов
        $schedule->call(function () {
//            HostingApi::updateServersStat();
        })->cron('0 * * * *');

        // перенос файлов на более актуальный сервер
        $schedule->call(function () {
//            FileBalancer::moveFromInactiveServers();
        })->cron('5 * * * *');

        // отмена неиспользуемых сервров
        $schedule->call(function () {
//            CancelNotNeededServers::run();
        })->dailyAt('23:35')->timezone('Europe/Moscow');

        // сохраняем статистику за день по диску
        $schedule->call(function () {
            SaveDiskStats::run();
        })->dailyAt('23:40')->timezone('Europe/Moscow');

        // проверка платежей qiwi
        $schedule->call(function () {
            Qiwi::checkOrders();
        })->everyFiveMinutes();

        // Удаление просточенных симлинков на cdn файлы
        $schedule->call(function () {
            $dir = public_path('p');
            $links = array_diff(scandir($dir), ['..', '.']);
            foreach ($links as $link) {
                $link = $dir . '/' . $link;
                if (!is_dir($link)) {
                    try {
                        if (time() - fileatime($link) > 86400 * 7) {
                            unlink($link);
                        }
                    } catch (\Throwable $exception) {
                        unlink($link);
                    }
                }
            }
        })->cron('0 * * * *');

        // удаление неиспользуемых миниатюр
        $schedule->call(function () {
            Miniature::checkATime();
            Miniature::deleteOld();
        })->cron('15 * * * *');

        // оптимизация файлов базы данных
//        $schedule->call(function () {
//            foreach (BaseModel::SQLITE as $db) {
//                $pdo = new \PDO("sqlite:" . database_path('db/' . $db . '.sqlite'));
//                $q = $pdo->prepare('VACUUM');
//                $q->execute();
//            }
//        })->weekly();

        // чистка пустых временных папок
        $schedule->call(function () {
            $dirs = \Illuminate\Support\Facades\File::directories(storage_path('app/tmp'));
            foreach ($dirs as $dir) {
                $inDirs = \Illuminate\Support\Facades\File::directories($dir);
                foreach ($inDirs as $inDir) {
                    @rmdir($inDir);
                }
                @rmdir($dir);
            }
        })->daily();

        $schedule->call(function () {
            Memory::updateDisksStat();
        })->everyMinute();

        // бэкапы базы данных
//        $schedule->call(function () {
//            foreach (BaseModel::SQLITE as $db) {
//                if (file_exists(database_path('db/' . $db . '.sqlite'))) {
//                    $pdo = new \PDO("sqlite:" . database_path('db/' . $db . '.sqlite'));
//                    $q = $pdo->prepare('VACUUM');
//                    $q->execute();
//                }
//            }
//
//            $fileName = 'save_' . date('Y-m-d') . '.zip';
//            $backupFile = storage_path('app') . '/' . $fileName;
//            exec('cd ' . database_path('db') . ' && zip ' . $backupFile . ' *');
//            Storage::disk('icedrive')->writeStream('backup/' . $fileName, Storage::disk('local')->readStream($fileName));
//            @unlink($backupFile);
//        })->cron('17 1 * * *');

        $schedule->call(function () {
            ApiLog::where('created_at', '<', date('Y-m-d 00:00:00', time() - 86400 * 30))->delete();

//            $pdo = new \PDO("sqlite:" . database_path('db/api_logs.sqlite'));
//            $q = $pdo->prepare('VACUUM');
//            $q->execute();
        })->cron('0 0 * * *');

        // снимаем лок на подвисшиые таски
//        $schedule->call(function () {
//            $db = new BaseModel;
//            $jobs = $db->table('jobs')->get();
//            foreach ($jobs as $job) {
//                if ($job->reserved_at <= time() - 3600) {
//                    $job->reserved_at = null;
//                    $job->attempts = 0;
//                    $job->timestamps = false;
//                    $job->save();
//                }
//            }
//        })->hourly();

        // чистка непонятного кэша в файлах
        $schedule->call(function () {
            $path = 'framework/cache/data/';
            Storage::disk('storage')->deleteDirectory($path);
            Storage::disk('storage')->makeDirectory($path);
        })->hourly();

        // чистка неактивных аккаунтов (без файлов и авктивностью менее 2 минут)
        $schedule->call(function () {
            $users = User::where('folder', '=', '')
                ->where('created_at', '<', date('Y-m-d H:i:s', time() - 86400 * 14))
                ->get();
            foreach ($users as $user) {
                if (strtotime($user->last_activity) - strtotime($user->created_at) < 120) {
                    $user->delete();
                }
            }
        })->daily();

        // перенос файлов в долгосрочное хранилище
        $schedule->call(function () {
            $files = File::where('disk', '=', 'ez1')
                ->where('deleted', '=', 0)
                ->where('created_at', '<', date('Y-m-d H:i:s', time() - 86400 * 14))
                ->get();
            /** @var File $file */
            foreach ($files as $file) {
                $file->moveRealFile(Server::getBestServer(), false, 'low');
            }
        })->daily();

        // чистка файлов сессий
        $schedule->call(function () {
            $files = Storage::disk('storage')->files('/framework/sessions');
            foreach ($files as $file) {
                if (time() - fileatime(storage_path($file)) > 86400) {
                    Storage::disk('storage')->delete($file);
                }
            }
        })->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
