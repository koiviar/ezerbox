<?php

namespace App\Cron;

use App\Helpers\HostingApi;
use App\Model\Server;

/**
 * Class CancelNotNeededServers
 * @package App\Cron
 */
class CancelNotNeededServers
{
    public static function run()
    {
        $servers = Server::where('using_status', '=', 'free')
            ->where('status', '=', 'Active')
            ->where('type', '=', Server::TYPE_NFS)
            ->where('status', '=', 'Active')
            ->get();

        /** @var Server $server */
        foreach ($servers as $server) {
            $days = round((strtotime($server->pay_date) - time()) / (60 * 60 * 24));
            if ($server->hosting_id && $days && $days <= Server::CANCEL_DAYS) {
                HostingApi::cancelServer($server->hosting_id);

                $server->delete();
            }
        }
    }
}
