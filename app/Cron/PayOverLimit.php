<?php

namespace App\Cron;

use App\Helpers\Payment;
use App\Model\BadUser;
use App\Model\MoneyOps;
use App\Model\SpaceUsage;
use App\Model\User;
use App\Model\UserPlan;
use App\Model\VpsUsage;

/**
 * Class PayOverLimit
 * @package App\Cron
 */
class PayOverLimit
{
    public static function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            /** @var UserPlan $plan */
            $plan = UserPlan::where('user_id', '=', $user->id)->first();

            if ($plan->type === 'elastic') {
                /** @var SpaceUsage $diskUsage */
                $diskUsage = SpaceUsage::where('user_id', '=', $user->id)->first();

                if ($diskUsage && $diskUsage->usage > 0) {
                    $needPay = $diskUsage->usage * $plan->price / 30 / 24;
                    $needPay = ceil($needPay * 10000) / 10000;

                    $user->money -= $needPay;
                    $user->save();

                    // фиксируем операцию списания
                    MoneyOps::addNewOp($user->id, $needPay, '-', [
                        'text' => 'money.getMoneyFromDisk',
                        'hours' => $diskUsage->usage,
                        'price' => $plan->price,
                    ]);

                    $diskUsage->delete();
                }
            } else {
                $needPay = $plan->price / 30;
                $needPay = ceil($needPay * 10000) / 10000;

                $user->money -= $needPay;
                $user->save();

                // фиксируем операцию списания
                MoneyOps::addNewOp($user->id, $needPay, '-', [
                    'text' => 'money.getMoneyForPlan',
                    'plan' => $plan->plan,
                    'price' => $plan->price,
                ]);
            }
        }

//            $vpsUsages = VpsUsage::where('user_id', '=', $user->id)->get();
//            /** @var VpsUsage $vpsUsage */
//            foreach ($vpsUsages as $vpsUsage) {
//                if ($vpsUsage && $vpsUsage->usage > 0) {
//                    $needPay = $vpsUsage->usage * $vpsUsage->price / 30 / 24;
//                    $needPay = ceil($needPay * 10000) / 10000;
//
//                    if ($user->money >= $needPay) {
//                        $user->money -= $needPay;
//                        $user->save();
//
                         // фиксируем операцию списания
//                        MoneyOps::addNewOp($user->id, $needPay, '-', [
//                            'text' => 'money.getMoneyFromNFS',
//                            'hours' => $vpsUsage->usage,
//                            'server' => $vpsUsage->server,
//                            'price' => $vpsUsage->price,
//                        ]);
//
//                        $vpsUsage->delete();
//
//                        if ($badUser) {
//                            $badUser->delete();
//                        }
//                    } else {
//                        if (!$badUser) {
//                            $badUser = new BadUser(['user_id' => $user->id]);
//                            $badUser->save();
//                        }
//                    }
//                } else {
//                    if ($badUser) {
//                        $badUser->delete();
//                    }
//                }
//            }
//        }
    }
}

