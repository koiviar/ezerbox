<?php

namespace App\Cron;

use App\Model\DiskUsageStat;
use App\Model\User;

/**
 * Class SaveDiskStats
 * @package App\Cron
 */
class SaveDiskStats
{
    public static function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            DiskUsageStat::saveStats($user->id);
        }
    }
}
