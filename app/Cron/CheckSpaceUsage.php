<?php

namespace App\Cron;

use App\Helpers\Payment;
use App\Model\File;
use App\Model\Nfs;
use App\Model\SpaceUsage;
use App\Model\User;
use App\Model\VpsUsage;

/**
 * Class CheckSpaceUsage
 * @package App\Cron
 */
class CheckSpaceUsage
{
    public static function run()
    {
        $users = User::all();

        // начисления за использование диска
        /** @var User $user */
        foreach ($users as $user) {
            /** @var SpaceUsage $spaceUsage */
            $spaceUsage = SpaceUsage::firstOrNew(['user_id' => $user->id]);

            if ($spaceUsage->checkTime()) {
                $used = File::convertByteToGb(File::getUsedSpace($user->id, false));
                $allSpace = File::convertByteToGb(File::getUserAllSpace($user->id));

                $overLimit = ceil($used - $allSpace);

                if ($overLimit > 0) {
                    $spaceUsage->usage = $spaceUsage->usage ? $spaceUsage->usage + $overLimit : $overLimit;
                    $spaceUsage->setCheckTime();
                    $spaceUsage->save();
                }
            }
        }

        // начисления за использование VPS
        /** @var User $user */
        foreach ($users as $user) {
            $nfss = Nfs::where('user_id', '=', $user->id)->get();

            /** @var Nfs $nfs */
            foreach ($nfss as $nfs) {
                /** @var VpsUsage $vpsUsage */
                $vpsUsage = VpsUsage::firstOrNew([
                    'user_id' => $user->id,
                    'server' => $nfs->server->name,
                    'price' => Payment::NFS_PRICE[$nfs->server->size],
                ]);

                if ($vpsUsage->checkTime()) {
                    $vpsUsage->usage++;
                    $vpsUsage->setCheckTime();
                    $vpsUsage->save();
                }
            }
        }
    }
}
