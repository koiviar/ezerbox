<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

/**
 * Class User
 * @package App
 *
 * @property $login
 * @property $avatar
 * @property $money
 * @property $folder
 * @method static create(array $array)
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'mongo';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvatar($size = 'orig')
    {
        return str_replace('###', $size, Auth::user()->avatar);
    }

    public function getMoney()
    {
        return number_format(Auth::user()->money, 2, '.', ' ');
    }

    public function getUserFolder()
    {
        return Auth::user()->folder;
    }
}
