<?php

namespace App\Libs;

use App\Model\User;
use Illuminate\Support\Facades\Auth;

class ViewsIndexer
{
    // подсчет юзеров в зависимости от заголовков запроса и ip
    public static function indexUser($request, $model, $id)
    {
        $server = $request->server->all();
        $header = $request->headers->all();
        if (Auth::user()) {
            $userId = Auth::id();
        } else {
            $userId = 0;
        }
        $userInfo = json_encode([$header['user-agent'], $server['REMOTE_ADDR'], $userId]);

        @mkdir(storage_path('app/userCounter'));
        $fileLink = storage_path('app/userCounter') . '/' . $model . '_' . $id;
        $data = unserialize(@file_get_contents($fileLink));
        $data[] = hash('crc32', $userInfo);
        $data = array_unique($data);
        file_put_contents($fileLink, serialize($data));

        return count($data);
    }

    public static function getCount($model, $id)
    {
        @mkdir(storage_path('app/userCounter'));
        $fileLink = storage_path('app/userCounter') . '/' . $model . '_' . $id;
        $data = unserialize(@file_get_contents($fileLink));
        return $data ? count($data) : 0;
    }

    public static function saveViews($model, $id)
    {
        $views = self::getCount($model, $id);
        $class = '\App\Model\\' . $model;
        $ent = $class::where('id', '=', $id)->first();
        if ($ent) {
            $ent->views += $views;
            $ent->save();
        }
        @unlink(storage_path('app/userCounter') . '/' . $model . '_' . $id);
        // так же добавляем просмотры юзеру для их оплаты
        $user = User::where('id', '=', $ent->user_id)->first();
        $user->views_to_pay += $views;
        $user->activity_index += $views;
        $user->save();

        return true;
    }

    public static function startSave()
    {
        $files = array_diff(scandir(storage_path('app/userCounter')), ['.', '..']);
        foreach ($files as $file) {
            $tmp = explode('_', $file);
            self::saveViews($tmp[0], $tmp[1]);
        }
        return true;
    }
}
