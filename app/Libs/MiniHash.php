<?php

namespace App\Libs;

class MiniHash
{
    private static $symbols = [
        'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o',
        'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k',
        'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', '1',
        '2', '3', '4', '5', '6', '7', '8', '9', '0'
    ];

    public static function gen($length = 10)
    {
        $str = '';

        for ($i = 0; $i <= $length; $i++) {
            $str .= self::$symbols[rand(0, count(self::$symbols) - 1)];
        }

        return $str;
        // return str_random($length);
    }

    public static function unic($object, $field, $mask = '###', $length = 8)
    {
        $hash = '';
        do {
            $hash = self::gen($length);
            $count = $object::where($field, 'like', str_replace('###', $hash, $mask))->count();
        } while ($count);
        return $hash;
    }
}
