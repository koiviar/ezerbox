<?php

namespace App\Libs;

class Js
{
    public static function get($link)
    {
//        $domain = $_SERVER['SERVER_NAME'] == 'ezerbox.com' ? 'cdn.'.$_SERVER['SERVER_NAME'] : $_SERVER['SERVER_NAME'];
        $domain = $_SERVER['SERVER_NAME'];

        if (substr_count($link, 'http')) {
            return '<script type="text/javascript" src="' . $link . '"></script>';
        }

        $fileTime = @filemtime(public_path() . $link);
        $newLink = $link . '?' . $fileTime;

        return '<script src="//' . $domain . $newLink . '"></script>';
    }
}
