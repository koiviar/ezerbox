<?php

namespace App\Libs;

class Css
{
    public static function get($link)
    {
//	    $domain = $_SERVER['SERVER_NAME'] == 'ezerbox.com' ? 'cdn.'.$_SERVER['SERVER_NAME'] : $_SERVER['SERVER_NAME'];
        $domain = $_SERVER['SERVER_NAME'];

        if (substr_count($link, 'http')) {
            return '<link rel="stylesheet" href="' . $link . '">';
        }

        $fileTime = @filemtime(public_path() . $link);
        $newLink = $link . '?' . $fileTime;

        return '<link rel="stylesheet" href="//' . $domain . $newLink . '">';
    }
}
