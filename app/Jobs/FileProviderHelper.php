<?php

namespace App\Jobs;

use App\Helpers\FileProviderCache;
use App\Model\File;
use App\Model\FileProviderCache as FileCache;
use App\Model\Server;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class FileProviderHelper implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var File $fileModel */
        $file = File::where('id', '=', $this->data['id'])->first();

        if (!$file) {
            throw new \Exception('No file ' . $this->data['id']);
        };

//        if (!Storage::disk(FileCache::PROVIDER_DISK_DRIVER)->exists($file->id . $file->extension)) {
//            $stream = Storage::disk($file->disk)->getDriver()->readStream($file->link);
//            Storage::disk(FileCache::PROVIDER_DISK_DRIVER)
//                ->put($file->id . $file->extension, $stream);
//        }

        // инициализация параметров
        $startHeight = $height = $this->data['height'];
        $startWidth = $width = $this->data['width'];
        $public = $this->data['public'];

        $filePath = FileProviderCache::getMiniaturePath($file, $startHeight, $startWidth);

        // если все параметры уже известны, проверяем наличие миниатюры
        if (!file_exists($filePath)) {
            $originalPath = FileProviderCache::getFile($file);

            if (!$originalPath) {
                throw new \Exception('No original path');
            };

            $img = Image::make($originalPath);

            if (!$height) {
                $height = round($img->height() / abs($img->width() / $width));
            }
            if (!$width) {
                $width = round($img->width() / abs($img->height() / $height));
            }

            $img->fit($width, $height)->encode('jpg', 90);
            $img->save($filePath);
        };


        /** @var FileCache $fileCache */
        $fileCache = FileCache::firstOrNew(['file_id' => $file->id, 'miniature_code' => $width . '/' . $height]);

        if (!$fileCache->link) {
            $link = '/mcodes/' . $width . 'x' . $height . '/' . strtolower(Str::random(2)) . '/' . $file->id . '_' . $width . '_' . $height . '.jpg';
            $server = Server::getBestServer();

            $providerPathO = $file->id . $file->extension;
            $providerPathM = $file->id . '_' . $width . '_' . $height . '.jpg';

            $stream = Storage::disk(FileProviderCache::PROVIDER_DISK_DRIVER)->readStream($providerPathM);
            Storage::disk($server . '_p')->put($link, $stream);
            Storage::disk(FileProviderCache::PROVIDER_DISK_DRIVER)->delete($providerPathM);
            Storage::disk(FileProviderCache::PROVIDER_DISK_DRIVER)->delete($providerPathO);

            $fileCache->disk = $server;
            $fileCache->link = $link;
        }

        $fileCache->expire_date = date('Y-m-d H:i:s', time() + FileProviderCache::MINIATURE_TIME_CACHE);
        $fileCache->miniature = 1;
        $fileCache->extension = $file->extension;
        $fileCache->save();

        $httpLink = 'https://' . $fileCache->disk . '.ezerbox.com' . $fileCache->link;

        Cache::add('miniature_cache_' . $file->id . $width . $height, $httpLink, 86400 * 7);
    }
}
