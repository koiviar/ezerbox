<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SiteMapGenerator implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sitemap = App::make('sitemap');
        // $sitemap->add(URL::to('/'), '2012-08-25T20:10:00+02:00', '1.0', 'daily');
        $sitemap->add(URL::to('/'), date('Y-m-dTH:i:s'), '1.0', 'daily');
        $sitemap->add(URL::to('/publicFile'), date('Y-m-dTH:i:s'), '0.9', 'daily');
        $sitemap->add(URL::to('/bucket'), date('Y-m-dTH:i:s'), '0.9', 'daily');
        $sitemap->add(URL::to('/note'), date('Y-m-dTH:i:s'), '0.9', 'daily');

        $publicFiles = \App\Model\File::where('public_id', '!=', '')->get();
        foreach ($publicFiles as $file) {
            $file->prepareToView();
            if ($file->ex_type == 'image') {
                $images = [
                    ['url' => 'https://' . $file->link, 'title' => $file->name, 'caption' => $file->name],
                ];
                $sitemap->add(URL::to('/publicFile/' . $file->public_id), date('Y-m-dTH:i:s', $file->updated_at->timestamp), '0.8', 'daily', $images);
            } else {
                $sitemap->add(URL::to('/publicFile/' . $file->public_id), date('Y-m-dTH:i:s', $file->updated_at->timestamp), '0.8', 'daily');
            }
        }

        $posts = \App\Model\Post::get();
        foreach ($posts as $post) {
            if ($post->files) {
                $files = explode('|', $post->files);
                $images = [];
                foreach ($files as $file) {
                    $images[] = [
                        'url' => 'https://' . $post->server . '.ezerbox.com' . $file,
                        'title' => trim(str_replace('#', ' ', $post->tags)),
                        'caption' => trim(str_replace('#', ' ', $post->tags))];
                }
                $sitemap->add(URL::to('/post/' . $post->id), date('Y-m-dTH:i:s', $post->updated_at->timestamp), '0.8', 'daily', $images);
            } else {
                $sitemap->add(URL::to('/post/' . $post->id), date('Y-m-dTH:i:s', $post->updated_at->timestamp), '0.8', 'daily');
            }
        }

        $sitemap->store('xml', 'sitemap', public_path());
    }
}
