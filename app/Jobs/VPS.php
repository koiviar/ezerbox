<?php

namespace App\Jobs;

use App\Helpers\HostingApi;
use App\Model\Server;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class VPS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public const ACTION_CREATE = 'create';
    public const ACTION_RELOAD = 'reload';
    public const ACTION_CHANGE_PASS = 'change_pass';
    public const ACTION_CANCEL = 'cancel';
    public const ACTION_REINSTALL = 'reinstall';

    protected $data;

    /**
     * VPS constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        $server_id = $this->data['server_id'];
        switch ($this->data['action']) {
            case VPS::ACTION_CREATE:
                Server::setProcess($server_id, Server::PROCESS_CREATE);

                $order = HostingApi::orderNew($this->data['product_id']);
                $serviceId = HostingApi::getServiceIdFromOrderNumber($order['order_num']);

                /** @var Server $server */
                $server = Server::where('id', '=', $server_id)->first();
                $server->hosting_id = $serviceId;
                $server->save();

                Server::setProcess($server_id, Server::PROCESS_INSTALL);

                do {
                    sleep(5);
                    $info = HostingApi::getServerInfo($server->hosting_id);
                } while (!(isset($info['status']) || (isset($info['status']) && $info['status'] == 'Active')));

                // обновляем модель сервера
                $server = Server::where('id', '=', $server_id)->first();
                HostingApi::updatePassword($server);

                // обновляем модель сервера
                $server = Server::where('id', '=', $server_id)->first();
                HostingApi::updateServerInfo($server);

                break;

            case VPS::ACTION_CHANGE_PASS:
                /** @var Server $server */
                $server = Server::find($server_id);
                HostingApi::updatePassword($server);
                break;

            case VPS::ACTION_REINSTALL:
                /** @var Server $server */
                $server = Server::find($server_id);
                // для безопасности
                if ($server->type != Server::TYPE_NFS) {
                    throw new \Exception('Попытка переустановки не NFS VPS!');
                }

                Server::setProcess($server_id, Server::PROCESS_REINSTALL);

                $task = HostingApi::reinstall($server->hosting_id, $this->data['os']);

                do {
                    sleep(5);
                    $taskInfo = HostingApi::taskResult($server->hosting_id, $task['task_id']);
                } while ($taskInfo['results'] == '');

                // обновляем модель сервера
                $server = Server::find($server_id);
                HostingApi::updatePassword($server);

                // обновляем модель сервера
                $server = Server::find($server_id);
                HostingApi::updateServerInfo($server);

                break;

            case VPS::ACTION_RELOAD:
                /** @var Server $server */
                $server = Server::find($server_id);

                Server::setProcess($server_id, Server::PROCESS_RELOAD);

                $task = HostingApi::reboot($server->hosting_id);

                do {
                    sleep(5);
                    $taskInfo = HostingApi::taskResult($server->hosting_id, $task['task_id']);
                } while ($taskInfo['results'] == '');

                Server::setProcess($server_id, null);

                break;
        }
    }
}
