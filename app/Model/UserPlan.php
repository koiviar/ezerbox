<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

/**
 * Class UserPlan
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $plan
 * @property $type
 * @property $space
 * @property $price
 * @property $created_at
 * @property $updated_at
 *
 * @mixin Builder
 */
class UserPlan extends BaseModel
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'user_plans';

    public function save(array $options = [])
    {
        Cache::forget('get_used_space_info_' . $this->user_id);

        return parent::save($options);
    }
}
