<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @package App\Model
 */
class Transaction extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'transactions';

    protected $fillable = [
        'id',
        'user_id',
        'type',
        'money',
        'direct',
        'created_at',
        'updated_at',
    ];

    public function fixTransaction($type, $money, $userId = 0, $comment = '')
    {
        if ($money <= 0 || empty($userId)) return false;
        if (in_array($type, ['bucketPay'])) {
            $this->direct = '-';
        } else {
            $this->direct = '+';
        }
        $this->money = $money;
        $this->user_id = $userId;
        $this->type = $type;
        $this->comment = $comment;
        $this->save();
    }
}
