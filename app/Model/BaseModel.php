<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class BaseModel
 * @package App\Model
 *
 * @mixin Builder
 */
class BaseModel extends Model
{
    const SQLITE = [
        'bad_users',
        'comments',
        'dirs',
        'disk_subscriptions',
        'disk_usage_stats',
        'download_links',
        'error_logs',
        'files',
        'file_provider_caches',
        'groups',
        'money_ops',
        'nfs',
        'orders',
        'servers',
        'space_usage',
        'tokens',
        'vps_usages',
        'notes',
        'api_logs',
        'miniatures',
        'ads_stats',
        'user_plans',
        'use_services',
    ];

    /**
     * BaseModel constructor.
     * @param array $attributes
     * @param null $table
     */
    public function __construct(array $attributes = [], $table = null)
    {
        $this->setConnection('mongo');
//        $this->setConnection(self::getCN($table ?? $this->table));
        parent::__construct($attributes);
    }

    /**
     * @param $type
     * @return $this
     */
    public function db($type)
    {
        $this->setConnection(self::getCN($this->table, $type));

        return $this;
    }

    public function table($table)
    {
        $this->table = $table;
        $this->setConnection(self::getCN($table));

        return $this;
    }

    /**
     * @param $table
     * @param null $type
     * @return mixed|string|null
     */
    public static function getCN($table, $type = null)
    {
        return 'mysql';
        return in_array($table, self::SQLITE) ? 'sqlite_' . $table : 'sqlite_main';
//        if (!$type) {
//            $type = env('DB_CONNECTION');
//        }
//        return $type == 'mongo' ? $type : (in_array($table, self::SQLITE) ? 'sqlite_' . $table : 'sqlite_main');
    }

    public function save(array $options = [])
    {
        if (!$this->id) {
            do {
                $count = Cache::remember($this->table . '_count', 300, function () {
                    return $this->count();
                });
                $count = $count < 10 ? 9 : $count;
                $this->id = rand(1, $count * 2);
            } while ($this->where('id', '=', $this->id)->count() != 0);
        }

        if (isset($this->user_id)) {
            $this->user_id = (int)$this->user_id;
        }

        return parent::save($options);
    }
}
