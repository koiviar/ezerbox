<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\User;

/**
 * Class DiskSubscription
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $expire_date
 * @property $comment
 * @property $space
 * @property $forever
 */
class DiskSubscription extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'disk_subscriptions';

    protected $fillable = [
        'id',
        'user_id',
        'expire_date',
        'comment',
        'space',
        'forever',
        'created_at',
        'updated_at',
    ];

    public static function create($userId, $space, $period, $comment = '')
    {
        $m = new DiskSubscription;
        $m->user_id = $userId;
        $m->space = $space;
        $m->comment = $comment;
        $m->expire_date = date('Y-m-d H:i:s', time() + $period * 86400 * 30);
        $m->save();
    }

    public static function dropOldSubscriptions()
    {
        $list = DiskSubscription::where('forever', '=', 0)->where('expire_date', '<', date('Y-m-d H:i:s'))->get();

        $userIds = [];

        /** @var DiskSubscription $item */
        foreach ($list as $item) {
            $userIds[] = $item->user_id;
            $item->delete();
        }
    }
}
