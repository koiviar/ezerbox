<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Builder;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UseService
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $type
 * @property $price
 * @property $info
 * @property $billed
 * @property $created_at
 * @property $updated_at
 *
 * @mixin Builder
 */
class UseServiceOld extends BaseModelOld
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'use_services';
}
