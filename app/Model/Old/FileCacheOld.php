<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class FileCacheOld extends BaseModelOld
{
    protected $primaryKey = 'id';

    public static function dropOldFiles()
    {
        $files = FileCacheOld::where('last_use', '<', time())->get();
        foreach ($files as $file) {
            Storage::disk('lc')->delete($file->link);
            $file->delete();
        }
    }
}
