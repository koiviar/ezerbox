<?php

namespace App\Model\Old;


use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class AdsStat
 * @package App\Model
 *
 * @property $id
 * @property $code
 * @property $desc
 * @property $views
 * @property $regs
 * @property $created_at
 * @property $updated_at
 *
 * @mixin Builder
 */
class AdsStatOld extends BaseModelOld
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'ads_stats';
}
