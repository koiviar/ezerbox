<?php

namespace App\Model\Old;

use App\Jobs\CashOperations;
use App\Libs\MiniHash;
use App\Model\File;
use App\Model\Transaction;
use App\Model\DiskSubscription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;


/**
 *
 * CREATE TABLE dummy (
 * @property $id
 * @property $name
 * @property $email
 * @property $email_verified_at
 * @property $password
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 * @property $free_space
 * @property $pay_space
 * @property $money
 * @property $avatar
 * @property $folder
 * @property $views_to_pay
 * @property $last_activity
 * @property $login
 * @property $activity_index
 *
 * @method static find(int|null $id)
 */
class UserOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    protected $table = 'users';

    protected $fillable = [
        'id',
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'free_space',
        'pay_space',
        'money',
        'avatar',
        'folder',
        'views_to_pay',
        'last_activity',
        'login',
        'activity_index',
        'created_at',
        'updated_at',
    ];

    // ежедневные операции с балансом по крону
    public function cashOperations()
    {
        // пополнение баланса за просмотры
        $payViews = $this->views_to_pay - ($this->views_to_pay % 10);
        $viewsInvoice = round($payViews / 1000, 2);
        $this->money += $viewsInvoice;
        $this->views_to_pay = $this->views_to_pay % 10;
        // фиксация операции
        $transaction = new Transaction;
        $transaction->fixTransaction('viewsInvoice', $viewsInvoice, $this->id, $payViews);

        // списывание за хранение файлов
        $bucketCost = round($this->pay_space / 30, 2);
        $this->money -= $bucketCost;
        // фиксация операции
        $transaction = new Transaction;
        $transaction->fixTransaction('bucketPay', $bucketCost, $this->id);
    }

    // отправка юзеров демону на обработку баланса
    public static function addToJobsCashOperation()
    {
        $countUsers = UserOld::count();
        for ($i = 0; $i < ceil($countUsers / 100); $i++) {
            dispatch((new CashOperations($i))->onQueue('low'));
        }
    }

    public static function getDir($userId)
    {
        $user = UserOld::find($userId);
        if (empty($user->folder)) {
            $user->folder = MiniHash::unic('App\Model\User', 'folder', '###', 8);
            $user->save();
        }
        return $user->folder;
    }

    public static function getUsedSpaceInfo()
    {
        $result = Cache::remember('get_used_space_info_' . Auth::id(), 300, function () {
            $result = [];
            $result['used'] = File::getUsedSpace(Auth::id());
            $result['percentUsed'] = File::checkUsedPercent(File::getUsedSpace(Auth::id(), false), UserOld::getAllSpace(Auth::id()));
            $result['spaceAvailable'] = File::humanSize(UserPlanOld::where('user_id', '=', Auth::id())->first()->space * 1024 * 1024 * 1024);
            return $result;
        });

        return $result;
    }

    public static function getAllSpace($userId)
    {
        return UserPlanOld::where('user_id', '=', $userId)->first()->space * 1024 * 1024 * 1024;
    }

    public static function getAvailableSpaceHuman()
    {
        return File::humanSize(UserOld::getAllSpace(Auth::id()));
    }

    public function checkAndDeleteUpperLimitFiles()
    {
        $allSpace = self::getAllSpace($this->id);
        $usedSpace = File::getUsedSpace($this->id, false);

        if ($allSpace < $usedSpace) {
            $sizeToDrop = $usedSpace - $allSpace;
            $files = File::where('user_id', '=', $this->id)->where('deleted', '=', 0)->orderBy('updated_at', 'ASC')->get();

            foreach ($files as $file) {
                if ($sizeToDrop > 0) {
                    $file->softDelete();
                    $sizeToDrop -= $file->size;
                }
            }
        }
    }

    public function delete()
    {
        // удаление файлов
        File::where('user_id', '=', $this->id)->update(['deleted' => 2]);
        DirOld::where('user_id', '=', $this->id)->update(['deleted' => 2]);

        BadUserOld::where('user_id', '=', $this->id)->delete();
        DiskSubscription::where('user_id', '=', $this->id)->delete();
        DiskUsageStatOld::where('user_id', '=', $this->id)->delete();
        NoteOld::where('user_id', '=', $this->id)->delete();
        GroupOld::where('user_id', '=', $this->id)->delete();
        OrderOld::where('user_id', '=', $this->id)->delete();
        TokenOld::where('user_id', '=', $this->id)->delete();
        UserPlanOld::where('user_id', '=', $this->id)->delete();
        Transaction::where('user_id', '=', $this->id)->delete();
        MoneyOpsOld::where('user_id', '=', $this->id)->delete();

        return parent::delete();
    }
}
