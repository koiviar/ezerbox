<?php

namespace App\Model\Old;

use Illuminate\Foundation\Exceptions\Handler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


/**
 * Class ErrorLog
 * @package App\Model
 *
 * @property $user_id
 * @property $code
 * @property $message
 * @property $file
 * @property $trace
 */
class ErrorLogOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    public $table = 'error_log';

    protected $fillable = [
        'id',
        'user_id',
        'code',
        'message',
        'file',
        'trace',
        'created_at',
        'updated_at',
    ];

    public static function saveError($exception)
    {
        $error = new ErrorLogOld;
        $error->user_id = Auth::id() ?? null;
        $error->code = $exception->getCode();
        $error->message = $exception->getMessage();
        $error->file = $exception->getFile() . ':' . $exception->getLine();
        $error->trace = $exception->getTraceAsString();
        $error->save();
    }
}
