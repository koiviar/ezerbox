<?php

namespace App\Model\Old;

use Laravel\Scout\Searchable;

/**
 * Class Note
 * @package App\Model
 *
 * @property $id
 * @property $title
 * @property $search_title
 * @property $content
 * @property $user_id
 * @property $group_id
 * @property $sort
 */
class NoteOld extends BaseModelOld
{
    use Searchable;

    protected $primaryKey = 'id';
    protected $table = 'notes';

    protected $fillable = [
        'id',
        'title',
        'search_title',
        'content',
        'user_id',
        'group_id',
        'sort',
        'created_at',
        'updated_at',
    ];

    public function searchableAs()
    {
        return 'notes_index';
    }

    public function toSearchableArray()
    {
        return array_filter($this->toArray(), function ($key) {
            return in_array($key, ['id', 'title']);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function __get($key)
    {
        $value = parent::__get($key);

        if ($key == 'content') {
            $value = decrypt($value);
        }

        return $value;
    }

    public function __set($key, $value)
    {
        if ($key == 'content') {
            $value = encrypt($value);
        }

        if ($key == 'title') {
            $this->search_title = mb_strtolower($value);
        }

        parent::__set($key, $value);
    }
}
