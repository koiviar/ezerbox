<?php

namespace App\Model\Old;

use App\Helpers\Cloudflare;
use App\Helpers\Disk;
use App\Helpers\Data;
use App\Jobs\FileDistributor;
use App\Libs\MiniHash;
use App\Model\PublicFileProviderCache;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Jobs\FileDropper;
use App\Helpers\FileMover;
use Illuminate\Support\Str;

/**
 * Class File
 * @package App\Model
 *
 * @method static find($id)
 * @method static where(string $string, string $string1, int|null $id)
 *
 * @property $id
 * @property $user_id
 * @property $name
 * @property $search_title
 * @property $dir
 * @property $link
 * @property $type
 * @property $size
 * @property $server
 * @property $ex_type
 * @property $deleted
 * @property $deleted_time
 * @property $bucket_dir
 * @property $code
 * @property $public_code
 * @property $extension
 * @property $disk
 * @property $created_at
 * @property $updated_at
 *
 * @mixin Builder
 */
class FileOld extends BaseModelOld
{

    const SYSTEM_FILE_FOLDER = 'ezerbox';
    const SYSTEM_LOCAL_FOLDER = 'tmp';
    const LOCAL_SERVER = 'local';
    const FILE_PROVIDER_FOLDER = 'provider_folder';

    // 1 неделя
    const DELETED_LIVE_TIME = 86400 * 7;
    const BACKUP_DISKS = [
        'b2'
    ];

    protected $primaryKey = 'id';
    protected $table = 'files';

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'search_title',
        'dir',
        'link',
        'type',
        'size',
        'server',
        'ex_type',
        'deleted',
        'deleted_time',
        'bucket_dir',
        'code',
        'public_code',
        'extension',
        'disk',
        'created_at',
        'updated_at',
    ];

    public function getFullFileName()
    {
        if (substr($this->name, -strlen($this->extension)) == $this->extension) {
            return $this->name;
        } else {
            return $this->name . $this->extension;
        }
    }

    public function inspectBaseParam()
    {
        $ex = explode('.', $this->link);
        $ex = last($ex);
        $exType = '';
        if (in_array($ex, ['jpg', 'jpeg', 'png', 'svg', 'gif', 'webp'])) {
            $exType = 'image';
        }
        $this->extension = '.' . $ex;
        $this->type = Storage::disk(FileOld::LOCAL_SERVER)->mimeType(FileOld::SYSTEM_LOCAL_FOLDER . '/' . $this->link);
        $this->size = Storage::disk(FileOld::LOCAL_SERVER)->size(FileOld::SYSTEM_LOCAL_FOLDER . '/' . $this->link);
        $tmp = explode('/', $this->type);
        $this->ex_type = reset($tmp);
    }

    public function prepareToView()
    {
        // подготовка размера
        $this->size = $this->humanSize($this->size);
        // подготовка типа
        // switch ($this->ex_type) {
        //     case 'image':
        //         $this->ex_typeH = trans('bucket.typeImage');
        //         break;

        //     default:
        //         $this->ex_typeH = trans('bucket.typeFile');
        //         break;
        // }
        $this->ex_typeH = $this->ex_type;
        if (!$this->ex_typeH) {
            $this->ex_typeH = 'file';
        }
        // подготовка времени
        $this->created_atH = date('H:i d.m.Y', $this->created_at->timestamp);
        $this->updated_atH = date('H:i d.m.Y', $this->updated_at->timestamp);

        $this->icon = $this->getIcon();

        $this->download_link = $this->getDownloadLink();

        $this->preview = $this->ex_type == 'image' ? ($_SERVER['SERVER_NAME'] == 'ezerbox.com' ? '//ezerbox.com/d/' : '/d/') . '255x140/' . $this->code : null;
    }

    public static function getUsedSpace($userId = 0, $human = true)
    {
        $size = FileOld::where('user_id', '=', $userId)->where('deleted', '=', 0)->sum('size');
        return $human ? self::humanSize($size) : $size;
    }

    public static function humanSize($size)
    {
        if ($size < 1024) {
            $result = $size . ' b';
        } elseif ($size < 1024 * 1024) {
            $result = round($size / 1024, 1) . ' Kb';
        } elseif ($size < 1024 * 1024 * 1024) {
            $result = round($size / (1024 * 1024), 1) . ' Mb';
        } else {
            $result = round($size / (1024 * 1024 * 1024), 1) . ' Gb';
        }
        return $result;
    }

    public static function checkUsedPercent($used, $all)
    {
        // $all в гигабайтах, переводим
        return round($used / ($all / 100), 2);
    }

    public function deleteFile()
    {
        $path = FileOld::SYSTEM_LOCAL_FOLDER . '/' . $this->link;
        Storage::disk(FileOld::LOCAL_SERVER)->delete($path);

        return !Storage::disk(FileOld::LOCAL_SERVER)->exists($path);
    }

    public static function getInnerFolders($newIds, $dirs = null)
    {
        if (empty($newIds)) {
            return $dirs;
        }

        $dirsNew = DirOld::whereIn('dir', $newIds)
            ->where('user_id', '=', Auth::id())
            ->where('deleted', '=', 0)->get();

        $newIds = [];
        foreach ($dirsNew as $dir) {
            $newIds[] = $dir->id;
        }

        $data = self::getInnerFolders($newIds, $dirsNew);
        if ($data) {
            if (!$dirs) {
                return self::getInnerFolders($newIds, $dirsNew);
            } else {
                return $dirs->merge(self::getInnerFolders($newIds, $dirsNew));
            }
        } else {
            return $dirsNew;
        }
    }

    public static function deleteFolderContent($dirId, $soft = true, $first = false)
    {
        $dirs = self::getInnerFolders([$dirId]);
        $dirIds = [];
        foreach ($dirs as $dir) {
            $dirIds[] = $dir->id;
        }

        DirOld::where('user_id', '=', Auth::id())->whereIn('id', $dirIds)->update([
            'deleted' => 1,
            'deleted_time' => date('Y-m-d H:i:s'),
            'bucket_dir' => 0
        ]);

        // поиск папок
//        $dirs = Dir::where('dir', '=', $dirId)
//            ->where('user_id', '=', Auth::id())
//            ->where('deleted', '=', 0)->get();
//        foreach ($dirs as $dir) {
//            $res = File::deleteFolderContent($dir->id, $soft);
//        }
        // удаление содержимого папок
//        $files = File::where('dir', '=', $dirId)
//            ->where('user_id', '=', Auth::id())
//            ->where('deleted', '=', 0)->get();

        $files = FileOld::whereIn('dir', array_merge($dirIds, [$dirId]))
            ->where('user_id', '=', Auth::id())
            ->where('deleted', '=', 0)->get();
        foreach ($files as $file) {
            $soft ? $file->softDelete(false) : $file->delete();
        }

        // удаление папки
        $dir = DirOld::where('id', '=', $dirId)
            ->where('user_id', '=', Auth::id())
            ->first();

        if ($soft) {
            $dir->deleted = 1;
            $dir->deleted_time = date('Y-m-d H:i:s');
            $dir->bucket_dir = $first ? 1 : 0;
            $dir->save();
        } else {
            $dir->delete();
        }
    }

    public static function restoreFolderContent($dirId)
    {
        // поиск папок
        $dirs = DirOld::where('dir', '=', $dirId)->get();
        foreach ($dirs as $dir) {
            FileOld::deleteFolderContent($dir->id);
        }
        // восстановление содержимого папок
        $files = FileOld::where('dir', '=', $dirId)->get();
        foreach ($files as $file) {
            // проверка на наличие места для восстановления файла
            if (self::getAvailableSpace(Auth::id()) < $file->size) {
                continue;
            }

            $file->deleted = 0;
            $file->deleted_time = null;
            $file->save();
        }

        // удаление папки
        $dir = DirOld::find($dirId);
        $dir->deleted = 0;
        $dir->deleted_time = null;
        $dir->save();
    }

    public static function getBucketNavigation($dir = 0)
    {
        $result = [];
        while ($dir != 0) {
            $dirM = DirOld::where('id', '=', $dir)
                ->where('user_id', '=', Auth::id())
                ->first();

            if ($dirM != null) {
                $data = [];
                $data['id'] = $dirM->id;
                $data['name'] = $dirM->name;
                $result[] = $data;
                $dir = $dirM->dir;
            } else {
                $dir = 0;
            }
        }
        return array_reverse($result);
    }

    public static function getFolderContentCount($dirId = 0)
    {
        $files = FileOld::where('dir', '=', $dirId)->where('user_id', '=', Auth::id())->where('deleted', '=', 0);
        $dirs = DirOld::where('dir', '=', $dirId)->where('user_id', '=', Auth::id())->where('deleted', '=', 0);

//        $result['size'] = self::humanSize(Dir::size($dirId));
        $result['size'] = self::humanSize(FileOld::where('dir', '=', $dirId)->where('user_id', '=', Auth::id())->where('deleted', '=', 0)->sum('size'));
        $result['files'] = $files->count();
        $result['dirs'] = $dirs->count();
        return $result;
    }

    public static function getTrashFolderContentCount($dirId = 0)
    {
        $files = FileOld::where('user_id', '=', Auth::id())->where('deleted', '=', 1);
        $dirs = DirOld::where('user_id', '=', Auth::id())->where('deleted', '=', 1);

        if (!$dirId) {
            $files->where(function ($qin) use ($dirId) {
                $qin->where('dir', '=', $dirId)
                    ->orWhere('bucket_dir', '=', 1);
            });
            $dirs->where(function ($qin) use ($dirId) {
                $qin->where('dir', '=', $dirId)
                    ->orWhere('bucket_dir', '=', 1);
            });
        } else {
            $files->where('dir', '=', $dirId);
            $dirs->where('dir', '=', $dirId);
        }

        $result['size'] = self::humanSize(DirOld::size($dirId, true));
        $result['files'] = $files->count();
        $result['dirs'] = $dirs->count();
        return $result;
    }

    public static function getAvailableSpace($userId = 0)
    {
        $usedSize = self::getUsedSpace($userId, false);
        $allSpace = UserOld::getAllSpace($userId);
        return $allSpace - $usedSize;
    }

    public static function getUserAllSpace($userId = 0)
    {
        return UserOld::getAllSpace($userId);
    }

    public static function getHierarhyDirList($userId = 0, $dirId = 0, $level = 0)
    {
        $levelPad = '';
        for ($i = 0; $i < $level; $i++) {
            $levelPad .= '-';
        }

        $dirs = DirOld::where('user_id', '=', $userId)
            ->where('deleted', '=', 0)
            ->orderBy('name')
            ->get();

        $list = [];
        /** @var DirOld $dir */
        foreach ($dirs as $dir) {
            $list[$dir->dir][] = $dir;
        }

        $data = self::getChildCategoryList(0, $list, 0);

//        $index = $data ? max(array_keys($data)) : 0;
        $index = 0;

        if ($level == 0) $result[0] = [
            'id' => 0,
            'name' => trans('bucket.bucket')
        ];
        foreach ($data as $key => $value) {
//            $result[$key] = $value;
            $result[++$index] = [
                'id' => $key,
                'name' => $value
            ];
        }

//        if ($level == 0) $result = '<select class="form-control selectDirMove"><option value="0">' . trans('bucket.bucket') . '</option>' . $result . '</select>';
        return $result;
    }

    protected static function getChildCategoryList($parentId, $list, $level)
    {
        $result = [];
        if (!isset($list[$parentId])) {
            return $result;
        }

        $index = $list ? max(array_keys($list)) : 0;

        foreach ($list[$parentId] as $item) {
            $result[$item->id] = str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $item->name;
//            $result .= '<option value="' . $item->id . '">' . str_repeat('-', $level) . $item->name . '</option>';
            $data = self::getChildCategoryList($item->id, $list, $level + 1);
//            if ($data) {
//                $result[$item->name] = $data;
//            }
            foreach ($data as $key => $value) {
                $result[$key] = $value;
//                $result[++$index] = [
//                    'id' => $key,
//                    'name' => $value
//                ];
            }
        }
        return $result;
    }

    public static function deleteEnt($data, $soft = true)
    {
        foreach ($data['ent'] as $item) {
            if ($item['type'] == 'dir') {
                // удаляем содержимое папки
                $res = FileOld::deleteFolderContent($item['id'], $soft, true);
            } else {
                /** @var FileOld $file */
                $file = FileOld::find($item['id']);
                if ($soft) {
                    $file->softDelete();
                } else {
                    $data = [];
                    $data['id'] = $file->id;
                    dispatch((new FileDropper($data))->onQueue('low'));

                    $file->deleted = 2;
                    $file->save();
                }
            }
        }
    }

    public function softDelete($bucketRootDir = true)
    {
        $this->deleted = 1;
        $this->deleted_time = date('Y-m-d H:i:s');
        $this->bucket_dir = $bucketRootDir ? 1 : 0;
        $this->save();

        if ($this->public_code) {
            @unlink(public_path('p') . '/' . $this->public_code . $this->extension);
            MiniatureOld::linksDelete($this->id);
        }

        Cache::forget('get_used_space_info_' . $this->user_id . '_0');

        // чистим кэш размера папок dir_size_
//        Cache::forget('dir_size_' . $this->dir . '_' . $this->user_id);
//        $lastDir = $this->dir;
//        do {
//            $dir = null;
//            /** @var Dir $lastDirM */
//            $lastDirM = Dir::find($lastDir);
//            if ($lastDirM) {
//                /** @var Dir $dir */
//                $dir = Dir::find($lastDirM->dir);
//                if ($dir) {
//                    $lastDir = $dir->dir;
//                    Cache::forget('dir_size_' . $dir->dir . '_' . $dir->user_id . '_0');
//                }
//            }
//        } while($dir);
    }

    public function delete()
    {
        // удаляем приватный кэш файлов
        $fileCashes = FileProviderCacheOld::where('file_id', '=', $this->id)->get();
        foreach ($fileCashes as $fileCashe) {
            if (!$fileCashe->delete()) return false;
        }

        if ($this->public_code) {
            @unlink(public_path('p') . '/' . $this->public_code . $this->extension);
        }

        $miniatures = MiniatureOld::where('file_id', '=', $this->id)->get();
        foreach ($miniatures as $miniature) {
            $miniature->delete();
        }

        // удаляем сам файл
        if (!$this->deleteFile()) {
            return false;
        }

        // удаляем файл в хранилище
        Storage::disk($this->disk)->delete($this->link);
        if (Storage::disk($this->disk)->exists($this->link)) {
            return false;
        }

        // удаляем закешированный файл на cdn
        if ($this->public_code) {
//            Cloudflare::flushFileCacheByUrl($this->public_code, $this->extension, true);
            Cache::forget('file_public_' . $this->public_code);
        }

        // удаляем комменты
//        Comment::where('zone_id', 'PublicFile/'.$this->id)->delete();

        return parent::delete();
    }

    public static function restoreEnt($data)
    {
        foreach ($data['ent'] as $item) {
            if ($item['type'] == 'dir') {
                // проверка наличия места для всего содержимого папки
                if (self::getAvailableSpace(Auth::id()) < DirOld::size($item['id'], true)) {
                    $dir = DirOld::find($item['id']);
                    // установка сообщения об ошибке
                    $data = Data::getData('filesRestoreMsg') ?? [];
//                    $data[] = 'Папка "' . $dir->name . '" не может быть восстановлена, недостаточно места (' . self::humanSize($space) . ')';
                    Data::setData('filesRestoreMsg', $data);

                    // установка сущноестей, которые не могут быть удалены из списка
                    $data = Data::getData('filesRestoreIds') ?? [];
                    $data[] = (int)$item['id'];
                    Data::setData('filesRestoreIds', $data);

                    continue;
                }

                // удаляем содержимое папки
                FileOld::restoreFolderContent($item['id']);
            } else {
                $file = FileOld::find($item['id']);

                // проверка на наличие места для восстановления файла
                if (self::getAvailableSpace(Auth::id()) < $file->size) {
                    // установка сообщения об ошибке
                    $data = Data::getData('filesRestoreMsg') ?? [];
                    $data[] = $file->name . ' - не может быть восстановлен, недостаточно места';
                    Data::setData('filesRestoreMsg', $data);

                    // установка сущноестей, которые не могут быть удалены из списка
                    $data = Data::getData('filesRestoreIds') ?? [];
                    $data[] = $file->id;
                    Data::setData('filesRestoreIds', $data);

                    continue;
                }

                $file->deleted = 0;
                $file->deleted_time = null;
                $file->save();
            }
        }
    }

    public function save($args = [])
    {
        if ($this->original &&
            (
                (isset($this->original['public_code']) && $this->original['public_code'] && !$this->public_code) ||
                (isset($this->original['deleted']) && $this->original['deleted'] == 0 && $this->deleted == 1 && $this->public_code)
            )
        ) {
            Cloudflare::flushFileCacheByUrl($this->original['public_code'], $this->extension, true);
            MiniatureOld::linksDelete($this->id);
        }

        // проверка имени файла, во избежании повтора в одной папке
        if ($this->deleted == 0 && isset($this->original['name']) && $this->original['name'] != $this->name) {
//            while (File::where('name', '=', $this->name)
//                ->where('deleted', '=', 0)->where('dir', '=', $this->dir)->count()) {
//                $this->name = str_replace($this->extension, '', $this->name) . '_1' . $this->extension;
//            }
        }

        if (!$this->user_id) {
            $this->user_id = Auth::id();
        }

        if (!$this->id) {
            Cache::forget('get_used_space_info_' . $this->user_id);
        }

        // чистим кэш размера папок dir_size_
        Cache::forget('dir_size_' . $this->dir . '_' . $this->user_id);
        $lastDir = $this->dir;
        do {
            $dir = null;
            /** @var DirOld $lastDirM */
            $lastDirM = DirOld::find($lastDir);
            if ($lastDirM) {
                /** @var DirOld $dir */
                $dir = DirOld::find($lastDirM->dir);
                if ($dir) {
                    $lastDir = $dir->dir;
                    Cache::forget('dir_size_' . $dir->dir . '_' . $dir->user_id);
                }
            }
        } while($dir);

        parent::save($args);
    }

    public static function deleteOldFiles()
    {
        FileOld::where('deleted', '=', 1)
            ->where('deleted_time', '<', date('Y-m-d H:i:s', time() - self::DELETED_LIVE_TIME))
            ->update(['deleted' => 2]);

        $files = FileOld::select(['id'])->where('deleted', '=', 2)->limit(10000)->get();

        foreach ($files as $file) {
            $data = [];
            $data['id'] = $file->id;
            dispatch((new FileDropper($data))->onQueue('low'));
        }
    }

    public static function convertByteToGb($size)
    {
        return $size / 1024 / 1024 / 1024;
    }

    public static function convertByteToMb($size)
    {
        return $size / 1024 / 1024;
    }

    public static function convertByteToKb($size)
    {
        return $size / 1024;
    }

    public function getIcon()
    {
        if ($this->ex_type == 'video') {
            return 'far fa-file-video';
        } else if (strpos($this->extension, 'zip') !== false) {
            return 'far fa-file-archive';
        } else if (strpos($this->extension, 'rar') !== false) {
            return 'far fa-file-archive';
        } else if (strpos($this->extension, 'doc') !== false) {
            return 'far fa-file-word';
        }  else if (strpos($this->extension, 'csv') !== false) {
            return 'far fa-file-excel';
        } else if ($this->ex_type == 'text') {
            return 'far fa-file-alt';
        }  else if ($this->ex_type == 'application') {
            return 'far fa-file-code';
        } else {
            return 'far fa-file';
        }
    }

    public function getPrivateLink()
    {
        return 'https://s.ezerbox.com/d/' . $this->code;
    }

    public function getCdnLink()
    {
        $user = Cache::remember('search_user_by_id_' . $this->user_id, 3600, function () {
            return UserOld::find($this->user_id);
        });
        return $this->public_code ? 'https://cdn.ezerbox.com/p/' . mb_strtolower($user->login) . '/' .
            Str::substr($this->public_code, 0, 2) . '/' . $this->public_code . $this->extension : null;
    }

    public function moveRealFile($disk = null, $sync = false, $queue = 'med')
    {
        $data = [];
        $data['id'] = $this->id;
//        $data['disk'] = 'icedrive'; //$disk ?? Server::getBestServer();
        $data['disk'] = $disk ?? ServerOld::getBestServer();

        if ($sync) {
            FileMover::move($data);
        } else {
            dispatch((new FileDistributor($data))->onQueue($queue));
        }
    }

    public function toColdStorage($sync = false)
    {
        $this->moveRealFile('b2', $sync);
    }

    public function toStandardStorage($sync = false)
    {
        if ($this->disk == 'b2') {
            $this->moveRealFile(null, $sync);
        }
    }

    public function generateCode()
    {
        $this->code = MiniHash::unic('App\Model\File', 'code', '###', 12);
    }

    public function generatePublicCode()
    {
        $this->public_code = MiniHash::unic('App\Model\File', 'public_code', '###', 12);
    }

    public function userFilter($userId = null)
    {
        return $this->where('user_id', '=', $userId ?? Auth::id());
    }

    public static function getUserFile($id)
    {
        $file = new FileOld;
        return $file->userFilter()
            ->where('id', '=', $id)
            ->first();
    }

    public function __set($key, $value)
    {
        if ($key == 'name') {
            $this->search_title = mb_strtolower($value);
        }

        $error = false;
        // заменяем ключ
        if ($key == 'folder_id') {
            $key = 'dir';
        }

        // проверяем сущесствования папки у юзера
        if ($key == 'dir' && $value != 0) {
            /** @var DirOld $dir */
            $dir = DirOld::where('user_id', '=', $this->user_id)
                ->where('id', '=', $value)
                ->first();

            if (!$dir) {
                $error = true;
            }
        }

        if (!$error) {
            parent::__set($key, $value);
        }
    }

    public function __get($key)
    {
        // заменяем ключ
        if ($key == 'folder_id') {
            $key = 'dir';
        }

        return parent::__get($key);
    }

    public function getDownloadLink()
    {
        return DownloadLinkOld::getLink($this);
    }

    /**
     * @param string $path
     * @param bool $create
     * @return false|int
     */
    public static function getDirId(string $path = '/', bool $create = false)
    {
        if (in_array($path, ['', '/'])) {
            return 0;
        }

        $dirs = explode('/', $path);
        foreach ($dirs as $key => $dir) {
            if ($dir === '') {
                unset($dirs[$key]);
            }
        }
        $lastDir = 0;
        foreach ($dirs as $dir) {
            /** @var DirOld $dirM */
            $dirM = DirOld::where('user_id', '=', Auth::id())
                ->where('dir', '=', $lastDir)
                ->where('deleted', '=', 0)
                ->where('name', '=', $dir)
                ->first();

            if (!$dirM && $create) {
                $dirM = new DirOld;
                $dirM->user_id = Auth::id();
                $dirM->name = $dir;
                $dirM->dir = $lastDir;
                $dirM->save();

                $lastDir = $dirM->id;
            } else if (!$dirM && !$create) {
                return false;
            } else if ($dirM) {
                $lastDir = $dirM->id;
            }
        }

        return $lastDir;
    }

    /**
     * @param string $path
     * @return FileOld|null
     */
    public static function getFileByPath(string $path) : ?FileOld
    {
        $fileName = last(explode('/', $path));
        $dir = FileOld::getDirId(str_replace($fileName, '', $path));

        return FileOld::where('user_id', '=', Auth::id())
            ->where('deleted', '=', 0)
            ->where('dir', '=', $dir)
            ->where('name', '=', $fileName)
            ->first();
    }

    /**
     * @param FileOld $file
     * @return string
     */
    public static function getFilePath(FileOld $file) : string
    {
        return self::getDirPath($file->dir) . '/' . $file->name;
    }

    /**
     * @param int $dirId
     * @return string
     */
    public static function getDirPath(int $dirId) : string
    {
        if ($dirId == 0) {
            return '';
        }

        /** @var DirOld $dir */
        $dir = DirOld::where('deleted', '=', 0)
            ->where('user_id', '=', Auth::id())
            ->where('id', '=', $dirId)
            ->first();

        if ($dir) {
            $path = '/' . $dir->name;

            while ($dir && $dir->dir !== 0) {
                $dir = DirOld::where('deleted', '=', 0)
                    ->where('user_id', '=', Auth::id())
                    ->where('id', '=', $dir->dir)
                    ->first();

                if ($dir) {
                    $path = '/' . $dir->name . $path;
                }
            }
        }

        return $path ?? '/';
    }
}
