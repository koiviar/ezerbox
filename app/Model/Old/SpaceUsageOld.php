<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SpaceUsage
 *
 * @property $id
 * @property $user_id
 * @property $usage
 * @property $last_check
 *
 * @package App\Model
 * @method static firstOrNew(array $array)
 */
class SpaceUsageOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    protected $table = 'space_usage';

    protected $fillable = [
        'id',
        'user_id',
        'usage',
        'last_check',
        'created_at',
        'updated_at',
    ];

    public function checkTime()
    {
        return date('Y-m-d H', strtotime($this->last_check)) != date('Y-m-d H');
    }

    public function setCheckTime()
    {
        $this->last_check = date('Y-m-d H:i:s');

        return $this;
    }
}
