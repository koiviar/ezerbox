<?php

namespace App\Model\Old;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * Class ApiLogs
 * @package App
 * @property $id
 * @property $user_id
 * @property $token_id
 * @property $request
 * @property $params
 * @property $created_at
 *
 * @mixin Builder
 */
class ApiLogOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    protected $table = 'api_logs';

    /**
     * @return mixed
     */
    public function getToken()
    {
        return Cache::remember('token_' . $this->token_id, 60, function() {
            return TokenOld::where('id', '=', $this->token_id)->first()->token ?? null;
        });
    }
}
