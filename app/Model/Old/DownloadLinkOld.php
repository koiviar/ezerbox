<?php

namespace App\Model\Old;

use App\Libs\MiniHash;
use App\Helpers\Cloudflare;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

/**
 * Class DownloadLink
 * @package App\Model
 *
 * @property $id
 * @property $server
 * @property $link
 * @property $dir_path
 * @property $expire_date
 * @property $static
 * @property $file_id
 *
 * @mixin Builder
 */
class DownloadLinkOld extends BaseModelOld
{
    const LINK_LIVE_TIME = 3600 * 3;
    const PUBLIC_POSTFIX = '_p';
    const BASE_DIR = 'd';

    protected $primaryKey = 'id';
    protected $table = 'download_links';

    protected $fillable = [
        'id',
        'server',
        'link',
        'dir_path',
        'expire_date',
        'static',
        'file_id',
        'created_at',
        'updated_at',
    ];

    public function save($args = [])
    {
        if (!$this->expire_date) {
            $this->expire_date = $this->getExpireDate();
        }

        parent::save($args);
    }

    public static function getLink(FileOld $file, $static = true)
    {
        if ($file->disk == 'tmp') {
            return null;
        }

//        $file->toStandardStorage(true);
        if (in_array($file->disk, ['co1', 'ez1'])) {
            return Cache::remember(__METHOD__ . $file->code . '-' . $file->id, self::LINK_LIVE_TIME, function () use ($file) {
                $link = Storage::disk($file->disk)->temporaryUrl($file->link, now()->addHours(3));
                return str_replace('co1.storage.yandexcloud.net', 'storage.ezerbox.com', $link);
            });
        }

        if ($static) {
            $link = Cache::remember('download_link_' . $file->id, 86400, function () use ($file) {
                /** @var DownloadLinkOld $link */
                $link = DownloadLinkOld::where('file_id', '=', $file->id)
                    ->where('server', '=', $file->disk)
                    ->where('static', '=', 'y')
                    ->first();

                if ($link) {
                    $link->updateExpireData();
                    return $link->link;
                }
                return null;
            });

            if ($link) {
                return $link;
            }
        }

        $downloadLink = new DownloadLinkOld;
        return $downloadLink->generateLink($file, $static);
    }

    /**
     * @param FileOld $file
     * @param bool $static
     * @return string
     */
    public function generateLink(FileOld $file, bool $static = true) : string
    {
        if ($file->disk == 'tmp') {
            return false;
        }
        // случайные пути папок
//        $dirPath = MiniHash::unic(new DownloadLink, 'dir_path', '%/###/', 64);

//        $filePath = Storage::disk($file->disk)->getDriver()->getAdapter()->getRoot() . $file->link;
//        $fileName = str_replace(' ', '_', $file->getFullFileName());
//        $fileName = str_replace(['#', '/', '\'', '"', '!', '?', '$', '\\', '*'], '', $fileName);
//        $newPath = Storage::disk($file->disk . DownloadLink::PUBLIC_POSTFIX)->getDriver()->getAdapter()->getRoot()
//            . DownloadLink::BASE_DIR . '/' . $dirPath . '/' . $fileName;
//
//        /** @var User $user */
//        $user = User::find($file->user_id);
//        $userFolder = Storage::disk($file->disk)->getDriver()->getAdapter()->getRoot() . $user->folder;
//
//        $linkDir = DownloadLink::BASE_DIR . '/' . $dirPath . '/';
//
//        $serverDomain = 'https://' . $file->disk . '.ezerbox.com';
//
//        $curl = new Client;
//        $result = $curl->post($serverDomain . '/storage-ctl.php', [
//            'form_params' => [
//                'target' => $filePath,
//                'link' => $newPath,
//                'folder' => $userFolder,
//                'create_folder' => $dirPath,
//                'hash' => md5('e' . $filePath . $newPath . 'box')
//            ]
//        ]);



//        $result = json_decode($result->getBody()->getContents(), true);

//        if (isset($result['status']) && $result['status'] == 'OK') {

        $fileName = str_replace(' ', '_', $file->getFullFileName());
        $fileName = str_replace(['#', '/', '\'', '"', '!', '?', '$', '\\', '*'], '', $fileName);
        $filePath = storage_path('disk/' . $file->disk . '/') . $file->link;

        if (file_exists($filePath)/* && filesize($filePath) == $file->size*/) {
            @mkdir(public_path('d'));
            $publicDir = Str::random(128);
            @mkdir(public_path('d/' . $publicDir));
            $publicPath = 'd/' . $publicDir . '/' . $fileName;
            $systemLink = public_path($publicPath);
            symlink($filePath, $systemLink);
        } else {
            return false;
        }

        $this->server = $file->disk;
        $this->link = 'https://ezerbox.com/' . $publicPath;
        $this->dir_path = $publicDir;
        $this->file_id = $file->id;
        $this->static = $static ? 'y' : 'n';
        $this->save();
//        } else {
//            abort(500);
//        }

        return $this->link;
    }

    public static function deleteOldLinks()
    {
        $links = DownloadLinkOld::where('expire_date', '<=', date('Y-m-d H:i:s'))->get();

        /** @var DownloadLinkOld $link */
        foreach ($links as $link) {
            $link->delete();
        }
    }

    public function delete()
    {
//        Cloudflare::deleteCdnLinkCache($this->link);

//        $files = Storage::disk($this->server . DownloadLink::PUBLIC_POSTFIX)->allFiles($this->dir_path);
//        Storage::disk($this->server . DownloadLink::PUBLIC_POSTFIX)->delete($files);

//        Storage::disk($this->server . DownloadLink::PUBLIC_POSTFIX)->deleteDirectory($this->dir_path);

//        if (Storage::disk($this->server . DownloadLink::PUBLIC_POSTFIX)->has($this->dir_path)) {
//            return false;
//        }

        try {
            $files = scandir(public_path('d/' . $this->dir_path));
            foreach ($files as $file) {
                @unlink(public_path('d/' . $this->dir_path . '/' . $file));
            }
        } catch (\Throwable $e) {};
        @rmdir(public_path('d/' . $this->dir_path));

        Cache::forget('download_link_' . $this->file_id);

        parent::delete();

        return $this;
    }

    public function updateExpireData()
    {
        $this->expire_date = $this->getExpireDate();
        $this->save();
    }

    private function getExpireDate()
    {
        return date('Y-m-d H:i:s', time() + DownloadLinkOld::LINK_LIVE_TIME);
    }
}
