<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileProviderCache
 * @package App\Model
 *
 * @property $id
 * @property $file_id
 * @property $expire_date
 * @property $miniature
 * @property $miniature_code
 * @property $extension
 * @property $views
 * @property $link
 * @property $disk
 */
class FileProviderCacheOld extends BaseModelOld
{
    const PROVIDER_DISK_DRIVER = 'provider';

    protected $primaryKey = 'id';
    protected $table = 'file_provider_caches';
    //
    protected $fillable = [
        'id',
        'file_id',
        'expire_date',
        'miniature',
        'miniature_code',
        'extension',
        'views',
        'link',
        'disk',
        'created_at',
        'updated_at',
    ];

    public function delete()
    {
        if (!$this->deleteFile()) {
            return false;
        }

        $size = explode('/', $this->miniature_code);
        Cache::flush('miniature_cache_' . $this->file_id . ($size[0] ?? '') . '_' . ($size[1] ?? ''));

        return parent::delete();
    }

    public function deleteFile()
    {
        if ($this->miniature) {
            $signature = explode('/', $this->miniature_code);
            $path = $this->file_id . '_' . $signature[0] . '_' . $signature[1] . $this->extension;

            /** @var FileOld $file */
            $file = FileOld::find($this->file_id);
            @unlink(public_path('p/' . str_replace('/', 'x', $this->miniature_code) . '/' . $file->public_code . $file->extension));
            @rmdir(public_path('p/' . str_replace('/', 'x', $this->miniature_code)));
        } else {
            $path = $this->file_id . $this->extension;
        }

        if ($this->link) {
            if (Storage::disk($this->disk . '_p')->exists($this->link)) {
                Storage::disk($this->disk . '_p')->delete($this->link);
            }
            if (Storage::disk($this->disk . '_p')->exists($this->link)) {
               return false;
            }
        }

        Storage::disk(self::PROVIDER_DISK_DRIVER)->delete($path);

        return !Storage::disk(self::PROVIDER_DISK_DRIVER)->exists($path);
    }
}
