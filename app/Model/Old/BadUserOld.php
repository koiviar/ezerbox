<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BadUser
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 */
class BadUserOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    protected $table = 'bad_users';

    protected $fillable = [
        'id',
        'user_id',
        'created_at',
        'updated_at',
    ];
}
