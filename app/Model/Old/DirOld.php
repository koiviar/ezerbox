<?php

namespace App\Model\Old;

use App\Model\File;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

/**
 * Class Dir
 * @package App\Model
 * @method static find($id)
 * @method static where(string $string, string $string1, int|null $id)
 *
 * @property $id
 * @property $user_id
 * @property $name
 * @property $search_title
 * @property $dir
 * @property $deleted
 * @property $deleted_time
 * @property $bucket_dir
 * @property $created_at
 * @property $updated_at
 *
 * @mixin Builder
 */
class DirOld extends BaseModelOld
{
    const DELETED_LIVE_TIME = 86400 * 7;

    protected $primaryKey = 'id';
    protected $table = 'dirs';

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'search_title',
        'dir',
        'deleted',
        'deleted_time',
        'bucket_dir',
        'created_at',
        'updated_at',
    ];

    public static function deleteOldDirs()
    {
        DirOld::where('deleted', '=', 1)
            ->where('deleted_time', '<', date('Y-m-d H:i:s', time() - self::DELETED_LIVE_TIME))
            ->update(['deleted' => 2]);

        $dirs = DirOld::where('deleted', '=', 2)->get();

        foreach ($dirs as $dir) {
            $dir->delete();
        }
    }

    public static function getInnerDirsIds($ids = [0], $deleted = false)
    {
        // получаем папки нужного уровня
        $dirs = self::whereIn('dir', $ids)->where('user_id', '=', Auth::id())->where('deleted', '=', $deleted ? 1 : 0)->get();

        $innerIds = [];
        foreach ($dirs as $dir) {
            $innerIds[] = $dir->id;
        }

        // получаем папки дочерние
        if ($innerIds) {
            $ids = array_merge($ids, self::getInnerDirsIds($innerIds));
        }

//        sort($ids);
        return $ids;
    }

    public static function size($dirId = 0, $deleted = false)
    {
        return Cache::remember('dir_size_' . $dirId . '_' . Auth::id() . '_' . (int)$deleted, 86400, function () use ($dirId, $deleted) {
            $dirsIds = self::getInnerDirsIds([$dirId], $deleted);

            $files = File::where('deleted', '=', $deleted ? 1 : 0)
                ->where('user_id', '=', Auth::id());

            if (!$dirId) {
                $files->where(function ($qin) use ($dirsIds) {
                    $qin->whereIn('dir', $dirsIds)
                        ->orWhere('bucket_dir', '=', 1);
                });
            } else {
                $files->whereIn('dir', $dirsIds);
            }

            return $files->sum('size');
        });
    }

    public function getSize($humanView = false)
    {
        $dirsIds = self::getInnerDirsIds([$this->id], $this->deleted);

        $files = File::where('deleted', '=', $this->deleted ? 1 : 0)
            ->where('user_id', '=', Auth::id());

        if (!$this->id) {
            $files->where(function ($qin) use ($dirsIds) {
                $qin->whereIn('dir', $dirsIds)
                    ->orWhere('bucket_dir', '=', 1);
            });
        } else {
            $files->whereIn('dir', $dirsIds);
        }

        $rSize = $files->sum('size');

        if ($humanView) {
            return File::humanSize($rSize);
        } else {
            return $rSize;
        }
    }

    public function userFilter($userId = null)
    {
        return $this->where('user_id', '=', $userId ?? Auth::id());
    }

    public static function getUserFolder($id)
    {
        $dir = new DirOld;
        return $dir->userFilter()
            ->where('id', '=', $id)
            ->first();
    }

    public function save(array $options = [])
    {
        if (!$this->user_id) {
            $this->user_id = Auth::id();
        }

        if (!DirOld::where('user_id', '=', $this->user_id)
            ->where('id', '=', $this->dir)
            ->count()) {
            $this->dir = 0;
        }

        return parent::save($options);
    }

    public function __set($key, $value)
    {
        if ($key == 'name') {
            $this->search_title = mb_strtolower($value);
        }

        parent::__set($key, $value);
    }
}
