<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Model;
use App\Model\DiskSubscription;

/**
 * Class Order
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $sum
 * @property $currency
 * @property $status
 * @property $type
 * @property $eur
 */
class OrderOld extends BaseModelOld
{
    const TYPE_SUB = 'subscription';
    const TYPE_COIN = 'coin';
    const TYPE_DISK = 'disk';
    const TYPE_NFS = 'nfs';

    const TYPE_BALANCE = 'balance';

    protected $primaryKey = 'id';
    protected $table = 'orders';

    protected $fillable = [
        'id',
        'user_id',
        'sum',
        'currency',
        'status',
        'type',
        'eur',
        'created_at',
        'updated_at',
    ];

    public function save($arg = [])
    {
        // зачисление валюты
        if (
            $this->status == 'complete' &&
            $this->type == OrderOld::TYPE_BALANCE &&
            isset($this->original['status']) &&
            $this->original['status'] == 'pending'
        ) {
            /** @var UserOld $user */
            $user = UserOld::find($this->user_id);
            $user->money += $this->eur;
            $user->save();

            MoneyOpsOld::addNewOp($this->user_id, $this->eur, '+', [
                'text' => 'money.fillUp',
                'sum' => $this->sum,
                'eur' => $this->eur,
            ]);
        }

        parent::save($arg);
    }
}
