<?php

namespace App\Model\Old;

use App\Helpers\HostingApi;
use App\Helpers\Payment;
use App\Helpers\ServerWorkers;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Class Nfs
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $server_id
 * @property $label
 * @property ServerOld $server
 */
class NfsOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    protected $table = 'nfs';

    protected $fillable = [
        'id',
        'user_id',
        'server_id',
        'label',
        'created_at',
        'updated_at',
    ];

    /** @var ServerOld */
    protected $server;

    /**
     * @param string $key
     * @return ServerOld|mixed
     */
    public function __get($key)
    {
        if ($key == 'server') {
            $this->loadServer();
            return $this->server;
        }

        return parent::__get($key);
    }

    public function unbind()
    {
        $this->loadServer();

        $this->server->using_status = ServerOld::USING_STATUS_FREE;
        $this->server->save();

        ServerWorkers::reinstallOs($this->server_id, HostingApi::OS_NFS);

        $this->delete();
    }

    public function reloadServer()
    {
        $this->loadServer();

        ServerWorkers::reloadServer($this->server_id);
    }

    protected function loadServer()
    {
        if (!$this->server && $this->server_id) {
            $this->server = ServerOld::find($this->server_id);
        }
    }

    public function getPrice()
    {
        return Payment::NFS_PRICE[$this->__get('server')->size];
    }

    /**
     * @param $userId
     * @param $space
     * @return NfsOld
     */
    public static function createNfs($userId, $space)
    {
        // создаем запись с NFS
        $nfs = new NfsOld;
        $nfs->user_id = $userId;
        $nfs->save();

        /** @var ServerOld $server */
        $server = ServerOld::where('using_status', '=', 'free')
            ->where('status', '=', 'Active')
            ->where('size', '=', $space)
            ->where('type', '=', ServerOld::TYPE_NFS)
            ->first();

        // если нашли сервер, то привязываем его
        if ($server) {
            $server->using_status = 'used';
            $server->name = 'NFS-' . $nfs->id;
            $server->save();
        } // если свободного сервера нет, то создаем новый
        else {
            $server = new ServerOld();
            $server->name = 'NFS-' . $nfs->id;
            $server->using_status = 'used';
            $server->size = $space;
            $server->type = ServerOld::TYPE_NFS;
            $server->save();

            ServerWorkers::createNew($server, HostingApi::PRODUCT_ID_STORAGE[$space]);
        }

        // привязываем сервер
        $nfs->server_id = $server->id;
        $nfs->save();

        return $nfs;
    }
}
