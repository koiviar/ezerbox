<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VpsUsage
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $usage
 * @property $server
 * @property $price
 * @property $last_check
 */
class VpsUsageOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    protected $table = 'vps_usages';

    protected $fillable = [
        'id',
        'user_id',
        'usage',
        'server',
        'price',
        'last_check',
        'created_at',
        'updated_at',
    ];

    public function checkTime()
    {
        return date('Y-m-d H', strtotime($this->last_check)) != date('Y-m-d H');
    }

    public function setCheckTime()
    {
        $this->last_check = date('Y-m-d H:i:s');

        return $this;
    }
}
