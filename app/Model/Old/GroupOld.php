<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Group
 * @package App\Model
 *
 * @property $id
 * @property $title
 * @property $user_id
 * @property $sort
 */
class GroupOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    protected $table = 'groups';

    protected $fillable = [
        'id',
        'title',
        'user_id',
        'sort',
        'created_at',
        'updated_at',
    ];
}
