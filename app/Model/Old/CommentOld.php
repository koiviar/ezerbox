<?php

namespace App\Model\Old;

use Illuminate\Database\Eloquent\Model;
use App\Model\User;

class CommentOld extends BaseModelOld
{
    protected $primaryKey = 'id';
    protected $table = 'comments';

    protected $fillable = [
        'id',
        'user_id',
        'message',
        'like',
        'dislike',
        'zone_id',
        'parent',
        'created_at',
        'updated_at',
    ];

    protected $comments;
    protected $zone;

    public function getCommentsTo($zone_id)
    {
        $this->zone = $zone_id;
        $this->comments = CommentOld::where('zone_id', '=', $zone_id)->get();
        $this->setUserInfo();
        $this->setHierarhy();
    }

    private function setUserInfo()
    {
        // достаем всех идентификаторы пользователей
        $userIds = [];
        foreach ($this->comments as $comment) {
            $userIds[] = $comment->user_id;
        }
        $userIds = array_unique($userIds);
        // достаем инфу о юзерах
        $users = User::select('avatar', 'name', 'id', 'login')->whereIn('id', $userIds)->get()->keyBy('id')->toArray();
        // приписываем необходимую информацию
        foreach ($this->comments as $key => $comment) {
            if (!isset($users[$comment->user_id])) {
                unset($this->comments[$key]);
                continue;
            }
            $this->comments[$key]->avatar = $users[$comment->user_id]['avatar'];
            $this->comments[$key]->name = $users[$comment->user_id]['name'];
        }
    }

    private function setHierarhy()
    {
        $this->comments = $this->findChild();
    }

    private function findChild($parent = 0)
    {
        $comments = [];
        foreach ($this->comments as $key => $comment) {
            if ($comment->parent == $parent) {
                $comment->child = $this->findChild($comment->id);
                $comments[] = $comment;
            }
        }
        return $comments;
    }

    public function getMainView()
    {
        return (string)view('comments.mainView', ['comments' => $this->comments, 'zone' => $this->zone]);
    }
}
