<?php

namespace App\Model;

use App\Helpers\Payment;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MoneyOps
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $sum
 * @property $cur
 * @property $dir
 * @property $info
 */
class MoneyOps extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'money_ops';

    protected $fillable = [
        'id',
        'user_id',
        'sum',
        'cur',
        'dir',
        'info',
        'created_at',
        'updated_at',
    ];

    /**
     * @param int $user_id
     * @param float $sum
     * @param string $dir
     * @param array $info
     * @param string $cur
     */
    public static function addNewOp(int $user_id, float $sum, string $dir = '-', array $info = [], $cur = Payment::CURRENCY_EUR)
    {
        $op = new MoneyOps;
        $op->user_id = $user_id;
        $op->sum = $sum;
        $op->cur = $cur;
        $op->dir = $dir;
        $op->info = $info;
        $op->save();
    }

    /**
     * @return string|null
     */
    public function getPreparedText()
    {
        return trans($this->info['text'], $this->info);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        $value = parent::__get($key);

        if ($key == 'info') {
            $value = json_decode($value, true);
        }

        return $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function __set($key, $value)
    {
        if ($key == 'info') {
            $value = json_encode($value);
        }

        parent::__set($key, $value);
    }
}
