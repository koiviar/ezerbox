<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Ramsey\Uuid\Type\Integer;

/**
 * Class Server
 * @package App\Model
 *
 * @property $id
 * @property $hosting_id
 * @property $using_status
 * @property $pay_date
 * @property $local_ip
 * @property $ip
 * @property $name
 * @property $document_root
 * @property $size
 * @property $status
 * @property $type
 * @property $space_used
 * @property $space_used_p
 * @property $bw_used_p
 * @property $pwd
 * @property $process
 * @property $task
 *
 * @mixin Builder
 */
class Server extends BaseModel
{
    const TYPE_STORAGE = 'storage';
    const TYPE_MAIN = 'main';
    const TYPE_NFS = 'nfs';

    const PROCESS_CREATE = 'creating';
    const PROCESS_PWD = 'change password';
    const PROCESS_INSTALL = 'install OS';
    const PROCESS_REINSTALL = 'reinstall OS';
    const PROCESS_RELOAD = 'reload';

    const USING_STATUS_USE = 'using';
    const USING_STATUS_FREE = 'free';

    const STORAGE_SIZES = [
        256,
        512,
        1024,
        2048,
    ];

    const CANCEL_DAYS = 8;

    protected $primaryKey = 'id';
    protected $table = 'servers';

    protected $fillable = [
        'id',
        'hosting_id',
        'using_status',
        'pay_date',
        'local_ip',
        'ip',
        'name',
        'document_root',
        'size',
        'status',
        'type',
        'space_used',
        'space_used_p',
        'bw_used_p',
        'pwd',
        'process',
        'task',
        'created_at',
        'updated_at',
    ];

    public static function getBestServer()
    {
//        $scaleBuckets = 1;
//        for ($i = 1; $i <= $scaleBuckets; $i++) {
//            $size = Cache::remember(__METHOD__, 30, function () use ($i) {
//                return File::where('disk', '=', "ez$i")->sum('size') / 1024 / 1024 / 1024;
//            });
//            if ($size < 50) {
//                return "ez$i";
//            }
//        }

//        return 'ms1';
//        return 'co1';
        return 'clo';
//        /** @var Server $server */
//        $server = Server::where('type', '=', Server::TYPE_STORAGE)->first();
//
//        return $server->name ?? 'n1';
    }

    /**
     * @param string $server
     * @return string|null
     */
    public static function getLocalIp(string $server)
    {
        $serverCacheKey = 'server_local_ip_' . $server;

        if (Cache::has($serverCacheKey)) {
            return Cache::get($serverCacheKey);
        }

        /** @var Server $server */
        $server = Server::where('name', '=', $server)->first();
        if (!$server) {
            return null;
        }

        Cache::put($serverCacheKey, $server->local_ip, 600);

        return $server->local_ip;
    }

    /**
     * @param int $id
     * @param string|null $process
     */
    public static function setProcess(int $id, ?string $process)
    {
        /** @var Server $server */
        $server = Server::where('id', '=', $id)->find();
        $server->process = $process;
        $server->save();
    }
}
