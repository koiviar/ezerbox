<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DiskUsageStat
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $date
 * @property $info
 * @property $created_at
 */
class DiskUsageStat extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'disk_usage_stats';

    protected $fillable = [
        'id',
        'user_id',
        'date',
        'info',
        'created_at',
        'updated_at',
    ];

    /**
     * @param int $userId
     */
    public static function saveStats(int $userId)
    {
        /** @var DiskUsageStat $stat */
        $stat = DiskUsageStat::where('user_id', '=', $userId)
            ->where('date', '=', date('Y-m-d 00:00:00'))
            ->first();

        if (!$stat) {
            $stat = new DiskUsageStat;
            $stat->user_id = $userId;
            $stat->date = date('Y-m-d 00:00:00');
        }

        $data['count'] = File::where('user_id', '=', $userId)
            ->where('deleted', '=', 0)
            ->count();
        $data['size'] = File::where('user_id', '=', $userId)
            ->where('deleted', '=', 0)
            ->sum('size');

        $stat->info = $data;
        $stat->save();
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        $value = parent::__get($key);

        if ($key == 'info') {
            $value = json_decode($value, true);
        }

        return $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function __set($key, $value)
    {
        if ($key == 'info') {
            $value = json_encode($value);
        }

        parent::__set($key, $value);
    }
}
