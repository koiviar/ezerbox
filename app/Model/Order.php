<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\DiskSubscription;

/**
 * Class Order
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $sum
 * @property $currency
 * @property $status
 * @property $type
 * @property $eur
 */
class Order extends BaseModel
{
    const TYPE_SUB = 'subscription';
    const TYPE_COIN = 'coin';
    const TYPE_DISK = 'disk';
    const TYPE_NFS = 'nfs';

    const TYPE_BALANCE = 'balance';

    protected $primaryKey = 'id';
    protected $table = 'orders';

    protected $fillable = [
        'id',
        'user_id',
        'sum',
        'currency',
        'status',
        'type',
        'eur',
        'created_at',
        'updated_at',
    ];

    public function save($arg = [])
    {
        // зачисление валюты
        if (
            $this->status == 'complete' &&
            $this->type == Order::TYPE_BALANCE &&
            isset($this->original['status']) &&
            $this->original['status'] == 'pending'
        ) {
            /** @var User $user */
            $user = User::where('id', '=', $this->user_id)->find();
            $user->money += $this->eur;
            $user->save();

            MoneyOps::addNewOp($this->user_id, $this->eur, '+', [
                'text' => 'money.fillUp',
                'sum' => $this->sum,
                'eur' => $this->eur,
            ]);
        }

        parent::save($arg);
    }
}
