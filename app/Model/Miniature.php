<?php

namespace App\Model;

use App\Jobs\CreateMiniature;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Exception\NotWritableException;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Miniature
 * @package App\Model
 *
 * @property $file_id
 * @property $width
 * @property $height
 * @property $disk
 * @property $path
 * @property $expire_at
 * @property $updated_at
 * @property $created_at
 *
 * @mixin Builder
 */
class Miniature extends BaseModel
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'miniatures';
    protected static $liveTime = 86400 * 30;

    const DISK = 'miniatures';

    /**
     * @param int $fileId
     * @param int|null $width
     * @param int|null $height
     * @return Miniature|Model|object|null
     */
    public static function findMiniature(int $fileId, int $width = null, int $height = null)
    {
        $miniature = Miniature::where('file_id', '=', $fileId);
        if ($width) {
            $miniature->where('width', '=', $width);
        } else {
            $miniature->whereNull('width');
        }
        if ($height) {
            $miniature->where('height', '=', $height);
        } else {
            $miniature->whereNull('height');
        }
        return $miniature->first();
    }

    /**
     * @param int $fileId
     * @param int|null $width
     * @param int|null $height
     * @param bool $secure
     * @return false|string
     */
    public static function createFile(int $fileId, int $width = null, int $height = null, bool $secure = false)
    {
        $file = File::where('id', '=', $fileId)->first();
        if (!in_array(strtolower(str_replace('.', '', $file->extension)), ['jpg', 'jpeg', 'png', 'gif', 'webp'])) {
            return false;
        }

        $user = Cache::remember('user_' . $file->user_id, 60, function () use ($file) {
            return User::where('id', '=', $file->user_id)->first();
        });
        $path = strtolower($user->login) . '/' . $width . 'x' . $height . '/' . Str::substr($file->code, 0, 2) . '/' . $file->code . '.jpg';

        if (!Storage::disk(self::DISK)->exists($path)) {
            $data = [
                'file_id' => $fileId,
                'width' => $width,
                'height' => $height,
                'path' => $path
            ];
            dispatch((new CreateMiniature($data))->onQueue('high'));

            do {
                if (Cache::has('miniature_error_' . $file->id)) {
                    Cache::forget('miniature_error_' . $file->id);
                    abort(500);
                }

                $ready = false;
                if (Cache::has('miniature_' . $file->id . "$width-$height")) {
                    $ready = true;
                } else {
                    usleep(300000);
                }
            } while (!$ready);
        }

        return Storage::disk(self::DISK)->url($path);
    }

    /**
     * @param int $fileId
     * @param int|null $width
     * @param int|null $height
     * @param $path
     */
    public static function createWorker(int $fileId, int $width = null, int $height = null, $path)
    {
        /** @var File $file */
        $file = File::where('id', '=', $fileId)->first();

        $miniaturePathDir = storage_path('app/miniatures');
        $miniaturePath = $miniaturePathDir . '/' . $fileId . '_' . $width . '_' . $height . '.jpg';
        if ($file->public_code) {
            $linkPathDir = public_path('p') . '/' . $width . 'x' . $height;
        } else {
            $linkPathDir = null;
        }

        if (!file_exists($miniaturePath)) {
            @mkdir($miniaturePathDir);
            @mkdir($linkPathDir);

            try {
                try {
                    $img = Image::make(Storage::disk($file->disk)->get($file->link));

                    $sHeight = $height;
                    $sWidth = $width;
                    if (!$height) {
                        $height = round($img->height() / abs($img->width() / $width));
                    }
                    if (!$width) {
                        $width = round($img->width() / abs($img->height() / $height));
                    }

                    $img->fit($width, $height)->encode('jpg');
                    $img->save($miniaturePath, 80);
                } catch (\Throwable $e) {}

                $stream = fopen($miniaturePath, 'r+');
                Storage::disk(self::DISK)->writeStream($path, $stream);
                fclose($stream);
                Storage::disk(self::DISK)->setVisibility($path, 'public');
                @unlink($miniaturePath);

                Cache::add('miniature_' . $file->id . "$sWidth-$sHeight", 1, 60);
            } catch (\Throwable $exception) {
                Cache::add('miniature_error_' . $file->id, 1, 60);

                file_put_contents(storage_path('logs/miniature_workers_error.txt'),
                    date('d.m.Y H:i:s') . ' - ' . $exception->getMessage() . "\n", FILE_APPEND);
            }
        }
    }

    public static function checkATime()
    {
        clearstatcache();
        $miniatures = Miniature::where('expire_at', '<', date('Y-m-d H:i:s', time() + 86400))->get();
        foreach ($miniatures as $miniature) {
            $file = File::where('id', '=', $miniature->file_id)->first();
            if ($file) {
                $linkPath = self::getLinkPath($miniature, $file);
                $newExpire = null;
                try {
                    $newExpire = date('Y-m-d H:i:s', fileatime($linkPath) + Miniature::$liveTime);
                } catch (\Throwable $exception) {}
                if ($newExpire && $newExpire > $miniature->expire_at) {
                    $miniature->expire_at = $newExpire;
                    $miniature->save();
                }
            }
        }
    }

    public static function deleteOld()
    {
        $miniatures = Miniature::where('expire_at', '<', date('Y-m-d H:i:s'))->get();
        foreach ($miniatures as $miniature) {
            $miniature->delete();
        }
    }

    /**
     * @param $miniature
     * @param $file
     * @return string
     */
    public static function getLinkPath($miniature, $file)
    {
        return public_path('p') . '/' . $miniature->width . 'x' . $miniature->height . '/' . $file->public_code . '.jpg';
    }

        /**
     * @param int $fileId
     */
    public static function linksDelete(int $fileId)
    {
        $miniatures = Miniature::where('file_id', '=', $fileId)->get();
        foreach ($miniatures as $miniature) {
            /** @var File $file */
            $file = File::where('id', '=', $fileId)->first();
            @unlink(self::getLinkPath($miniature, $file));
        }
    }

    public function delete()
    {
        /** @var File $file */
        $file = File::where('id', '=', $this->file_id)->first();

        $linkDir = public_path('p') . '/' . $this->width . 'x' . $this->height;
        $link = $linkDir . '/' . $file->public_code . '.jpg';
        $miniature = storage_path('disk') . '/' . $this->disk . '/miniatures/' . $this->file_id . '_' . $this->width . '_' . $this->height . '.jpg';

        @unlink($link);
        @unlink($miniature);
        @rmdir($linkDir);

        if (!file_exists($link) && !file_exists($miniature)) {
            return parent::delete();
        }
    }
}
