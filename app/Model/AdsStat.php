<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class AdsStat
 * @package App\Model
 *
 * @property $id
 * @property $code
 * @property $desc
 * @property $views
 * @property $regs
 * @property $created_at
 * @property $updated_at
 *
 * @mixin Builder
 */
class AdsStat extends BaseModel
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'ads_stats';
}
