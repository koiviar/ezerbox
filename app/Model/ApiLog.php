<?php

namespace App\Model;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * Class ApiLogs
 * @package App
 * @property $id
 * @property $user_id
 * @property $token_id
 * @property $request
 * @property $params
 * @property $created_at
 *
 * @mixin Builder
 */
class ApiLog extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'api_logs';

    /**
     * @return mixed
     */
    public function getToken()
    {
        return Cache::remember('token_' . $this->token_id, 60, function() {
            return Token::where('id', '=', $this->token_id)->first()->token ?? null;
        });
    }
}
