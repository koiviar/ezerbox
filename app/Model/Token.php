<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class Token
 * @package App\Model
 *
 * @property $id
 * @property $user_id
 * @property $token
 * @property $desc
 * @property $deleted
 * @property $updated_at
 * @property $created_at
 *
 * @mixin Builder
 */
class Token extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'tokens';

    protected $fillable = [
        'id',
        'user_id',
        'token',
        'created_at',
        'updated_at',
    ];
}
