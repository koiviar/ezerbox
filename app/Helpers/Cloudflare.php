<?php

namespace App\Helpers;

use App\Model\File;
use Curl\Curl;

class Cloudflare
{
    const ZONE = 'd07230a116ec9ed11fc3a47dccfaf0c8';
    const TOKEN = 'mq-RqLgUXD9SevuwaFAp6Ibu-Z-yGN8q899AI_7S';

    public static function flushFileCacheByUrl($code, $extension = null, $public = true)
    {
        if ($public) {
            $urls = [
                'https://ezerbox.com/d/' . $code . $extension,
                'https://s.ezerbox.com/d/' . $code . $extension,
                'https://cdn.ezerbox.com/d/' . $code . $extension,
                'https://ezerbox.com/p/' . $code . $extension,
                'https://s.ezerbox.com/p/' . $code . $extension,
                'https://cdn.ezerbox.com/p/' . $code . $extension,

                'https://ezerbox.com/d/*/' . $code . $extension,
                'https://s.ezerbox.com/d/*/' . $code . $extension,
                'https://cdn.ezerbox.com/d/*/' . $code . $extension,
                'https://ezerbox.com/p/*/' . $code . $extension,
                'https://s.ezerbox.com/p/*/' . $code . $extension,
                'https://cdn.ezerbox.com/p/*/' . $code . $extension,
            ];
        } else {
            $urls = [
                'https://ezerbox.com/d/' . $code,
                'https://s.ezerbox.com/d/' . $code,
                'https://cdn.ezerbox.com/d/' . $code,
                'https://ezerbox.com/p/' . $code,
                'https://s.ezerbox.com/p/' . $code,
                'https://cdn.ezerbox.com/p/' . $code,

                'https://ezerbox.com/d/*/' . $code,
                'https://s.ezerbox.com/d/*/' . $code,
                'https://cdn.ezerbox.com/d/*/' . $code,
                'https://ezerbox.com/p/*/' . $code,
                'https://s.ezerbox.com/p/*/' . $code,
                'https://cdn.ezerbox.com/p/*/' . $code,
            ];
        }

        return self::sendRequest('zones/' . self::ZONE . '/purge_cache', ['files' => $urls]) ? true : false;
    }

    public static function deleteCdnLinkCache($link)
    {
        return self::sendRequest('zones/' . self::ZONE . '/purge_cache', ['files' => [$link]]) ? true : false;
    }

    public static function addDns($subDomain, $ip)
    {
        return self::sendRequest('zones/'. self::ZONE . '/dns_records', [
            'type' => 'A',
            'name' => $subDomain,
            'content' => $ip,
            'ttl' => 120,
        ]);
    }

    public static function removeDns($domain)
    {
        $list = self::sendRequest('zones/' . self::ZONE . '/dns_records', ['name' => $domain], 'GET');
        dd($list);
    }

    protected static function sendRequest($url, $params, $type = 'POST')
    {
        return Retry::runHttp(function () use ($url, $params, $type) {
            $curl = new Curl;
            $curl->setHeader('Authorization', 'Bearer ' . self::TOKEN);
            $curl->setHeader('Content-Type', 'application/json');

            if ($type == 'POST') {
                $response = $curl->post('https://api.cloudflare.com/client/v4/' . $url, json_encode($params));
            } else {
                $response = $curl->get('https://api.cloudflare.com/client/v4/' . $url);
            }

            if (!$response->success) {
                return false;
            }

            return $response;
        });
    }
}

