<?php

namespace App\Helpers\Payment;

use App\Model\Order;
use GuzzleHttp\Client;
use Qiwi\Api\BillPayments;

class Qiwi
{
    const SECRET_KEY = 'eyJ2ZXJzaW9uIjoiUDJQIiwiZGF0YSI6eyJwYXlpbl9tZXJjaGFudF9zaXRlX3VpZCI6ImM2ZmZycy0wMCIsInVzZXJfaWQiOiI3OTgxNzUyMTA4MCIsInNlY3JldCI6IjJmNzM3NjZjYzIxM2UyNWYwNzQ0NjJhNmJkYjc3YzRjNWFhYTg5ZmFkYjUxNWJiOTRlZjIwMjM5YmFlMWVkMjcifX0=';
    const PUBLIC_KEY = '48e7qUxn9T7RyYE1MVZswX1FRSbE6iyCj2gCRwwF3Dnh5XrasNTx3BGPiMsyXQFNKQhvukniQG8RTVhYm3iPpUxE7vWJBo3G5KgyiYFWFi7nJbhgY8UuzaceDDzLC7AB75xyN6BFA2Qsx1jfbArR2ZkzZfbDadqvPKB1fvDagWazAYHXdsKtnCGF11LZ4';

    protected static function getApi()
    {
        return new BillPayments(self::SECRET_KEY);
//        return new Client([
//            'headers' => [
//                'content-type' => 'application/json',
//                'accept' => 'application/json',
//                'Authorization' => 'Bearer ' . self::SECRET_KEY
//            ],
//        ]);
    }

    protected static function getCurl()
    {
        return new Client([
            'headers' => [
                'content-type' => 'application/json',
                'accept' => 'application/json',
                'Authorization' => 'Bearer ' . self::SECRET_KEY
            ],
        ]);
    }

    public static function getForm($orderId, $money, $description = '')
    {
        $billPayments = self::getApi();

        /** @var Order $order */
        $order = Order::where('id', '=', $orderId)->first();

        $params = [
            'publicKey' => self::PUBLIC_KEY,
            'amount' => $money,
            'billId' => $orderId,
            'comment' => 'Fill up balance: €' . $order->eur,
            'lifetime' => date('Y-m-d H:i:s', time() + 3600),
            'successUrl' => route('paymentSuccess', ['payment' => 'qiwi']),
        ];

        /** @var \Qiwi\Api\BillPayments $billPayments */
        return $billPayments->createPaymentForm($params);
    }

    public static function getOrderHash($orderId)
    {
        return hash('crc32', $orderId);
    }

    public function isSuccess($params)
    {
        return $params['unaccepted'] == 'false';
    }

    public static function checkOrderPaid($orderId)
    {
        $curl = self::getCurl();
        $info = $curl->get('https://api.qiwi.com/partner/bill/v1/bills/' . $orderId);
        $info = json_decode($info->getBody()->getContents(), true);

        return $info['status']['value'] == 'PAID';
    }

    public static function cancelOrder($orderId)
    {
        $curl = self::getCurl();
        $curl->post('https://api.qiwi.com/partner/bill/v1/bills/' . $orderId . '/reject');
    }

    public static function checkOrders()
    {
        /** @var Order $order */
        $orders = Order::where('status', '=', 'pending')->get();
        foreach ($orders as $order) {
            try {
                if (\App\Helpers\Payment\Qiwi::checkOrderPaid($order->id)) {
                    $order->status = 'complete';
                    $order->save();
                    continue;
                }
            } catch (\Throwable $e) {}

            if (strtotime($order->created_at) + 3600 < time()) {
                $order->status = 'cancel';
                $order->save();
            }
        }
    }
}
