<?php

namespace App\Helpers\Payment;

class YandexMoney
{

    public static function getForm($orderId, $money, $description = '')
    {
        return '<form method="POST" action="https://yoomoney.ru/quickpay/confirm.xml?lang=' . session('trans') . '"
                class="submitFormToPay">
            <input type="hidden" name="receiver" value="410013955995003">
            <input type="hidden" name="formcomment" value="' . $description . '">
            <input type="hidden" name="short-dest" value="' . $description . '">
            <input type="hidden" name="label" value="' . $orderId . '">
            <input type="hidden" name="quickpay-form" value="small">
            <input type="hidden" name="targets" value="transaction ' . YandexMoney::getOrderHash($orderId) . '">
            <input type="hidden" name="sum" value="' . $money . '" data-type="number">
            <input type="hidden" name="comment" value="' . $description . '">
            <input type="hidden" name="paymentType" value="AC">
            <input type="submit" value="Transfer">
            </form>';
    }

    public static function getOrderHash($orderId)
    {
        return hash('crc32', $orderId);
    }

    public function isSuccess($params)
    {
        return $params['unaccepted'] == 'false';
    }

}
