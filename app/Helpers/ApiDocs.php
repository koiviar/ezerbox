<?php

namespace App\Helpers;

/**
 * Class ApiDocs
 * @package App\Helpers
 */
class ApiDocs
{
    /**
     * @return \array[][][]
     */
    public static function getDocs() : array
    {
        return [
            'disk' => [
                'file' => [
                    route('api.disk.list', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder'
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                [
                                    'type' => 'folder',
                                    'name' => 'test',
                                    'path' => '/my/folder/test'
                                ],
                                [
                                    'type' => 'image',
                                    'name' => 'image.jpg',
                                    'path' => '/my/folder/image.jpg'
                                ]
                            ]
                        ],
                        'desc' => trans('api_docs.disk_file_list')
                    ],
                    route('api.disk.file-upload-link', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder/image.jpg',
                            'link' => 'https://example.com/image-name.jpg',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                'name' => 'image.jpg',
                                'path' => '/my/folder/image.jpg',
                                'size' => 1024,
                                'type' => 'image',
                                'cdn_link' => null,
                                'created_at' => '2021-08-01 12:00:00',
                                'updated_at' => '2021-08-01 12:00:00',
                            ]
                        ],
                        'desc' => trans('api_docs.disk_file_upload_link')
                    ],
                    route('api.disk.file-info', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder/image.jpg',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                'name' => 'image.jpg',
                                'path' => '/my/folder/image.jpg',
                                'size' => 1024,
                                'type' => 'image',
                                'cdn_link' => null,
                                'created_at' => '2021-08-01 12:00:00',
                                'updated_at' => '2021-08-01 12:00:00',
                            ]
                        ],
                        'desc' => trans('api_docs.disk_file_info')
                    ],
                    route('api.disk.file-move', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'from' => '/my/folder/image.jpg',
                            'to' => '/my/folder2/image.jpg',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                'name' => 'image.jpg',
                                'path' => '/my/folder2/image.jpg',
                                'size' => 1024,
                                'type' => 'image',
                                'cdn_link' => null,
                                'created_at' => '2021-08-01 12:00:00',
                                'updated_at' => '2021-08-01 12:00:00',
                            ]
                        ],
                        'desc' => trans('api_docs.disk_file_move')
                    ],
                    route('api.disk.file-rename', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'from' => '/my/folder/image.jpg',
                            'to' => '/my/folder/image-new-name.jpg',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                'name' => 'image-new-name.jpg',
                                'path' => '/my/folder/image-new-name.jpg',
                                'size' => 1024,
                                'type' => 'image',
                                'cdn_link' => null,
                                'created_at' => '2021-08-01 12:00:00',
                                'updated_at' => '2021-08-01 12:00:00',
                            ]
                        ],
                        'desc' => trans('api_docs.disk_file_rename')
                    ],
                    route('api.disk.file-delete', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder/image.jpg',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => []
                        ],
                        'desc' => trans('api_docs.disk_file_delete')
                    ],
                    route('api.disk.file-download', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder/image.jpg',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                'link' => 'https://ezerbox.com/d/df8WhJhdflfdgWyga.jpg'
                            ]
                        ],
                        'desc' => trans('api_docs.disk_file_download')
                    ],
                    route('api.disk.file-cdn-create', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder/image.jpg',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                'link' => 'https://cdn.ezerbox.com/p/j58dkg.jpg'
                            ]
                        ],
                        'desc' => trans('api_docs.disk_file_cdn_create')
                    ],
                    route('api.disk.file-cdn-delete', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder/image.jpg',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => []
                        ],
                        'desc' => trans('api_docs.disk_file_cdn_delete')
                    ],
                ],
                'folder' => [
                    route('api.disk.folder-create', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                'name' => 'folder',
                                'path' => '/my/folder',
                                'created_at' => '2021-08-01 12:00:00',
                                'update_at' => '2021-08-01 12:00:00',
                            ]
                        ],
                        'desc' => trans('api_docs.disk_folder_create')
                    ],
                    route('api.disk.folder-info', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => [
                                'name' => 'folder',
                                'path' => '/my/folder',
                                'created_at' => '2021-08-01 12:00:00',
                                'update_at' => '2021-08-01 12:00:00',
                            ]
                        ],
                        'desc' => trans('api_docs.disk_folder_info')
                    ],
                    route('api.disk.folder-delete', [], false) => [
                        'method' => 'POST',
                        'params' => [
                            'path' => '/my/folder',
                        ],
                        'response' => [
                            'error' => 0,
                            'data' => []
                        ],
                        'desc' => trans('api_docs.disk_folder_delete')
                    ],
                ]
            ]
        ];
    }
}
