<?php

namespace App\Helpers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class JsLang
{
    public static function getJsLang()
    {
        $lang = [];

        $files = Storage::disk('resource')->allFiles('lang/' . session('trans'));

        foreach ($files as $file) {
            $tmp = eval(str_replace('<?php', '', file_get_contents(resource_path($file))));
            $lang = array_merge($lang, $tmp);
        }

        $result = '<script>
                var lang = {
                    lang: {';

        $tmpPhrases = [];
        foreach ($lang as $key => $phrase) {
            if (!is_array($phrase)) {
                $tmpPhrases[] = $key . ':"' . $phrase . '"';
            }
        }

        $result .= implode(", \n", $tmpPhrases);

        $result .= '},
            get: function(key) {
                return lang.lang[key] !== undefined ? lang.lang[key] : key;
            }
                }
           </script>';

        echo($result);
    }
}
