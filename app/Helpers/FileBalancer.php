<?php

namespace App\Helpers;

use App\Model\File;
use App\Model\Server;

class FileBalancer
{
    public static function moveFromInactiveServers()
    {
        $servers = Server::select(['name'])
            ->where('type', '=', Server::TYPE_STORAGE)
            ->get()
            ->keyBy('name')
            ->toArray();

        $servers = array_keys($servers);

        $files = File::whereNotIn('disk', $servers)
            ->limit(1000)
            ->get();

        $bestServer = Server::getBestServer();

        /** @var File $file */
        foreach ($files as $file) {
            $file->moveRealFile($bestServer);
        }
    }
}
