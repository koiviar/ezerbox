<?php

namespace App\Helpers;

use App\Model\BadFile;
use App\Model\File;
use Illuminate\Support\Facades\Storage;

class BadFilesCleaner
{

    public static function dropBadFiles()
    {
        $files = BadFile::get();

        foreach ($files as $file) {
            $filePath = storage_path('app/' . $file->path);

            if (!file_exists($filePath)) {
                $file->delete();
                continue;
            }

            Storage::disk(File::LOCAL_SERVER)->delete($file->path);

            if (!file_exists($filePath)) {
                $file->delete();
            }
        }
    }

}
