<?php

namespace App\Helpers;

use App\Model\Server;
use GuzzleHttp\Exception\GuzzleException;

class HostingApi
{
    const CATEGORY_STORAGE = 2;

    const OS_NFS = 'centos-6-x86_64-storage-filer';
    const OS_UBUNTU = 'ubuntu-16.04-x86_64-storage';

    const PRODUCT_ID_STORAGE = [
        '256' => 119,
        '512' => 120,
        '1024' => 121,
        '2048' => 122,
    ];

    /**
     * @throws GuzzleException
     */
    public static function updateServersStat()
    {
        $servers = Server::all();

        foreach ($servers as $server) {
            if ($server->hosting_id) {
                HostingApi::updateServerInfo($server);
            }
        }
    }

    /**
     * @param Server $server
     * @throws GuzzleException
     */
    public static function updateServerInfo(Server $server)
    {
        $info = HostingApi::getServerInfo($server->hosting_id);
        $service = HostingApi::sendRequest('service/' . $server->hosting_id)['service'];

        $server->status = $service['status'];
        $server->pay_date = $service['next_due'];

        if (isset($info['ip'])) {
            $server->local_ip = $info['additional_ip'][0];
            $server->ip = $info['ip'];
            if (!$server->size) {
                $server->size = $info['disk_limit'] / 1024;
                $server->space_used = $info['disk_usage'] / 1024;
                $server->space_used_p = round($info['disk_usage'] / ($info['disk_limit'] / 100), 2);
            }
            $server->bw_used_p = round(($info['bw_in'] + $info['bw_out']) / ($info['bw_limit'] / 100), 2);
            $server->save();
        }
    }

    /**
     * @param int $hostingId
     * @return mixed
     * @throws GuzzleException
     */
    public static function getServerInfo(int $hostingId)
    {
        return HostingApi::sendRequest('server/' . $hostingId);
    }

    /**
     * @param int $serverId
     * @param string $os
     * @return mixed
     * @throws GuzzleException
     */
    public static function reinstall(int $serverId, string $os)
    {
        return HostingApi::sendRequest('server/' . $serverId . '/reinstall', ['os' => $os], 'POST');
    }

    /**
     * @param int $serverId
     * @return mixed
     * @throws GuzzleException
     */
    public static function reboot(int $serverId)
    {
        return HostingApi::sendRequest('server/' . $serverId . '/reboot', [], 'POST');
    }

    /**
     * @param Server $server
     * @return string
     * @throws GuzzleException
     */
    public static function updatePassword(Server $server): string
    {
        $server->process = Server::PROCESS_PWD;
        $server->save();

        $response = HostingApi::sendRequest('server/' . $server->hosting_id . '/resetpassword', [], 'POST');

        do {
            sleep(1);
            $taskInfo = HostingApi::taskResult($server->hosting_id, $response['task_id']);
        } while ($taskInfo['results'] == '');

        $matches = [];
        preg_match('/this.innerHTML = "(.*?)"\'>/m', $taskInfo['results'], $matches);
        $pass = $matches[1] ?? '';

        $server->pwd = Encoder::encode($pass);
        $server->process = null;
        $server->save();

        return $pass;
    }

    /**
     * @param int $orderNumber
     * @return int
     * @throws GuzzleException
     */
    public static function getServiceIdFromOrderNumber(int $orderNumber): int
    {
        $result = HostingApi::sendRequest('service/order/' . $orderNumber);
        return $result['account_id'];
    }

    /**
     * @param int $productId
     * @return mixed
     * @throws GuzzleException
     */
    public static function orderNew(int $productId)
    {
        return HostingApi::sendRequest('order/' . $productId, [
            'cycle' => 'm',
            'pay_method' => 95,
        ], 'POST');
    }

    /**
     * @param int $serverId
     * @param int $task
     * @return mixed
     * @throws GuzzleException
     */
    public static function taskResult(int $serverId, int $task)
    {
        return HostingApi::sendRequest('server/' . $serverId . '/task/' . $task);
    }

    /**
     * @param int $serverId
     * @return mixed
     * @throws GuzzleException
     */
    public static function cancelServer(int $serverId)
    {
        return HostingApi::sendRequest('service/' . $serverId . '/cancel', [
            'immediate' => false,
            'reason' => 'Not using'
        ], 'POST');
    }

    /**
     * @param $url
     * @param array $params
     * @param string $method
     * @return mixed
     * @throws GuzzleException
     */
    public static function sendRequest($url, $params = [], $method = 'GET')
    {
        $curl = new \GuzzleHttp\Client;

        if ($method == 'GET') {
            $result = $curl->get('https://billing.time4vps.com/api/' . $url, [
                'headers' => [
                    'Authorization' => 'Basic Z2F5ZGlzbWlraGFpbEBnbWFpbC5jb206Mjc3NDEyNDhTZXJ6'
                ]
            ]);
        } else {
            $result = $curl->post('https://billing.time4vps.com/api/' . $url, [
                'headers' => [
                    'Authorization' => 'Basic Z2F5ZGlzbWlraGFpbEBnbWFpbC5jb206Mjc3NDEyNDhTZXJ6'
                ],
                'form_params' => $params
            ]);
        }

        return json_decode($result->getBody()->getContents(), true);
    }
}
