<?php

namespace App\Helpers;

/**
 * Class Retry
 * @package Components
 *
 * Как использовать
 *
 * Retry::runHttp(function($argument1, $argument2) {
 *     $result = doAnything($argument1, $argument2);
 *     if (!$result) {
 *         throw new \Exception('error');
 *     }
 * }, $argument1, $argument2);
 *
 * Код будет выполняться пока не будет успешный результат
 * Либо выкенет ошибку по проходу нескольких попыток согластно установленным правилам
 *
 * При нужде прокинуть аргументы, нужно указать любое требуемое количество
 * аргументов в безымяную функцию, а так же продублировать их как аргументы Retry::runHttp
 */
class Retry
{
    const TYPE_HTTP = 'http';
    const TYPE_MYSQL = 'mysql';

    const WAIT_TYPE_CONSTANT = 'constant';
    const WAIT_TYPE_LINEAR = 'linear';
    const WAIT_TYPE_EXPONENTIAL = 'exponential';

    protected static $strategyList = [
        Retry::TYPE_HTTP => [
            'retry' => 7,
            'strategy' => self::WAIT_TYPE_LINEAR,
            'base_time' => 350,
            'time_cap' => 10000
        ],
        Retry::TYPE_MYSQL => [
            'retry' => 30,
            'strategy' => self::WAIT_TYPE_LINEAR,
            'base_time' => 100,
            'time_cap' => 10000
        ]
    ];

    protected static $function;
    protected static $args;
    protected static $try;
    protected static $strategy;
    protected static $timestart;

    /**
     * @param $function
     * @return mixed
     * @throws \Throwable
     */
    public static function runHttp($function)
    {
        self::$args = self::clearArgs(func_get_args(), 1);
        return self::run($function, Retry::TYPE_HTTP);
    }

    /**
     * @param $function
     * @return mixed
     * @throws \Throwable
     */
    public static function runMysql($function)
    {
        self::$args = self::clearArgs(func_get_args(), 1);
        return self::run($function, Retry::TYPE_MYSQL);
    }

    /**
     * @param $function
     * @param string $type
     * @return mixed
     * @throws \Throwable
     */
    protected static function run($function, string $type)
    {
        self::$strategy = self::$strategyList[$type];
        self::$function = $function;
        self::$args = self::$args ?? self::clearArgs(func_get_args());
        self::$try = 0;
        self::$timestart = microtime(true);

        return self::runProcess();
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    protected static function runProcess()
    {
        while (true) {
            usleep(self::getTimeSleep());
            self::$try++;
            try {
                return call_user_func_array(self::$function, self::$args);
            } catch (\Throwable $e) {
                if (self::is_throw()) {
                    throw $e;
                }
            }
        }
    }

    /**
     * @return bool
     */
    protected static function is_throw() : bool
    {
        if (self::$try > self::$strategy['retry']) {
            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    protected static function getTimeSleep() : int
    {
        $time = 1;
        $base_time = self::$strategy['base_time'];
        switch (self::$strategy['strategy']) {
            case self::WAIT_TYPE_LINEAR:
                $time = self::$try * $base_time;
                break;

            case self::WAIT_TYPE_EXPONENTIAL:
                $time = pow(2, self::$try) * $base_time;
                break;

            default:
                $time = self::$try ? $base_time : 0;
                break;
        }

        $time = self::checkSleep($time);
        $time = self::$try ? $time * 1000 : 1;
        return $time > 0 ? $time : 1;
    }

    /**
     * @param int $time
     * @return int
     */
    protected static function checkSleep(int $time) : int
    {
        $time_to_end = self::$strategy['time_cap'] - ((microtime(true) - self::$timestart) * 1000);

        if ($time_to_end < $time) {
            self::$try = self::$strategy['retry'] + 1;
            $time = $time_to_end;
        }
        return $time;
    }

    /**
     * @param array $args
     * @param int $count
     * @return array
     */
    protected static function clearArgs(array $args, int $count = 2) : array
    {
        for ($i = 0; $i < $count; $i++) {
            unset($args[$i]);
        }
        return $args;
    }
}
