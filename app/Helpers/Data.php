<?php

namespace App\Helpers;

/**
 * Class Data
 * @package App\Helpers
 */
class Data
{
    /**
     * @var array
     */
    private static $data = [];

    /**
     * @var
     */
    private static $instance;

    /**
     * @return Data
     */
    public static function getInstance(): Data
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public static function init()
    {
        self::getInstance();
    }

    /**
     * @param $key
     * @param null $value
     */
    public static function set($key, $value = null)
    {
        if (is_array($value)) {
            $value = array_filter($value);
        }

        self::$data[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function get($key)
    {
        return self::$data[$key] ?? null;
    }

    /**
     * @return array
     */
    public static function getList()
    {
        return self::$data;
    }

    /**
     * @param $key
     */
    public static function forget($key)
    {
        unset(self::$data[$key]);
    }
}
