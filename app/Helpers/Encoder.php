<?php

namespace App\Helpers;

/**
 * Class Encoder
 * @package App\Helpers
 */
class Encoder
{
    protected static $encodeS = [
        'q' => 'N',
        'w' => 'E',
        'e' => '2',
        'r' => 'd',
        't' => '3',
        'y' => 'V',
        'u' => 't',
        'i' => '1',
        'o' => '5',
        'p' => 'f',
        'a' => 'h',
        's' => 'U',
        'd' => 'Q',
        'f' => 'T',
        'g' => 'Z',
        'h' => '0',
        'j' => 'L',
        'k' => 'K',
        'l' => 'i',
        'z' => 'G',
        'x' => 'k',
        'c' => 'u',
        'v' => '4',
        'b' => 'X',
        'n' => 'M',
        'm' => 'b',
        'Q' => '8',
        'W' => 'p',
        'E' => 'l',
        'R' => 'F',
        'T' => 'O',
        'Y' => '6',
        'U' => 'e',
        'I' => 'w',
        'O' => 'J',
        'P' => '9',
        'A' => 'g',
        'S' => 'z',
        'D' => 'r',
        'F' => 'B',
        'G' => '7',
        'H' => 'm',
        'J' => 'S',
        'K' => 'C',
        'L' => 'c',
        'Z' => 'W',
        'X' => 'A',
        'C' => 'o',
        'V' => 'H',
        'B' => 'Y',
        'N' => 'D',
        'M' => 'j',
        '1' => 'I',
        '2' => 'P',
        '3' => 'n',
        '4' => 'a',
        '5' => 'R',
        '6' => 'y',
        '7' => 'v',
        '8' => 'x',
        '9' => 's',
        '0' => 'q'
    ];
    protected static $decodeS = [
        'N' => 'q',
        'E' => 'w',
        '2' => 'e',
        'd' => 'r',
        '3' => 't',
        'V' => 'y',
        't' => 'u',
        '1' => 'i',
        '5' => 'o',
        'f' => 'p',
        'h' => 'a',
        'U' => 's',
        'Q' => 'd',
        'T' => 'f',
        'Z' => 'g',
        '0' => 'h',
        'L' => 'j',
        'K' => 'k',
        'i' => 'l',
        'G' => 'z',
        'k' => 'x',
        'u' => 'c',
        '4' => 'v',
        'X' => 'b',
        'M' => 'n',
        'b' => 'm',
        '8' => 'Q',
        'p' => 'W',
        'l' => 'E',
        'F' => 'R',
        'O' => 'T',
        '6' => 'Y',
        'e' => 'U',
        'w' => 'I',
        'J' => 'O',
        '9' => 'P',
        'g' => 'A',
        'z' => 'S',
        'r' => 'D',
        'B' => 'F',
        '7' => 'G',
        'm' => 'H',
        'S' => 'J',
        'C' => 'K',
        'c' => 'L',
        'W' => 'Z',
        'A' => 'X',
        'o' => 'C',
        'H' => 'V',
        'Y' => 'B',
        'D' => 'N',
        'j' => 'M',
        'I' => '1',
        'P' => '2',
        'n' => '3',
        'a' => '4',
        'R' => '5',
        'y' => '6',
        'v' => '7',
        'x' => '8',
        's' => '9',
        'q' => '0'
    ];

    /**
     * @param string $str
     * @return string
     */
    public static function encode(string $str): string
    {
        $newStr = '';
        for ($i = 0; $i < strlen($str); $i++) {
            $newStr .= self::$encodeS[$str[$i]] ?? $str[$i];
        }
        return Encoder::shuffleE($newStr);
    }

    /**
     * @param string $str
     * @return string
     */
    public static function decode(string $str): string
    {
        $str = Encoder::shuffleD($str);
        $newStr = '';
        for ($i = 0; $i < strlen($str); $i++) {
            $newStr .= self::$decodeS[$str[$i]] ?? $str[$i];
        }
        return $newStr;
    }

    /**
     * @param string $str
     * @return string
     */
    public static function shuffleE(string $str): string
    {
        $newStr = '';
        for ($i = 0; $i < strlen($str) / 2; $i++) {
            if (strlen($str) % 2 == 0) {
                $newStr .= $str[strlen($str) - $i - 1] . $str[$i];
            } else {
                if ($i == floor(strlen($str) / 2)) {
                    $newStr .= $str[$i];
                } else {
                    $newStr .= $str[strlen($str) - $i - 1] . $str[$i];
                }
            }
        }
        return $newStr;
    }

    /**
     * @param string $str
     * @return string
     */
    public static function shuffleD(string $str): string
    {
        $strL = '';
        $strR = '';
        for ($i = 0; $i < strlen($str) / 2; $i++) {
            if (strlen($str) % 2 == 0) {
                $strL .= $str[$i * 2 + 1];
            } else {
                if ($i < floor(strlen($str) / 2)) {
                    $strL .= $str[$i * 2 + 1];
                }
            }

            $strR = $str[$i * 2] . $strR;
        }
        return $strL . $strR;
    }
}
