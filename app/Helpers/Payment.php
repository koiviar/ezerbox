<?php

namespace App\Helpers;

use App\Helpers\Payment\Qiwi;
use App\Helpers\Payment\YandexMoney;
use App\Model\File;
use App\Model\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class Payment
{
    const MIN_BALANCE_TO_OVERLOAD = 0.5;

    const DISK_PRICE = 0.04;
    const NFS_PRICE = [
        256 => 4.99,
        512 => 7.99,
        1024 => 11.99,
        2048 => 19.99,
    ];

    const CURRENCY_EUR = 'eur';
    const CURRENCY_RUB = 'rub';

    protected $type;
    /** @var Order */
    protected $order;
    /** @var Qiwi */
    protected $payment;

    public function __construct($orderId = 0, $type = Qiwi::class)
    {
        $this->type = $type;

        $order = Order::where('id', '=', $orderId)->first();

        if ($order) {
            $this->order = $order;
        }

        $this->payment = new $type;
    }

    public static function getEurPrice($count = 1)
    {
        return self::convertEur($count);
    }

    public static function getDiskPrice($count = 1, $cur = null)
    {
        if ($cur ? $cur == Payment::CURRENCY_EUR : session('trans') == 'en') {
            $price = Payment::DISK_PRICE;
        } else {
            $price = Payment::convertEur(Payment::DISK_PRICE);
            $price = ceil($price * 10) / 10;
        }

        return Payment::roundPrice($price * $count);
    }

    public static function getCurrencyHuman($cur = null)
    {
        if ($cur) {
            return trans($cur);
        } else {
            return session('trans') == 'en' ? trans('eur') : trans('rub');
        }
    }

    public static function getCurrency()
    {
        return session('trans') == 'en' ? 'eur' : 'rub';
    }

    public static function getEurInRub()
    {
        return Cache::remember('EUR-value', 3600, function () {
            $curl = new \GuzzleHttp\Client;
            $response = $curl->get('https://www.cbr-xml-daily.ru/daily_json.js');
            $result = json_decode($response->getBody()->getContents(), true);
            return $result['Valute']['EUR']['Value'];
        });
    }

    public static function convertRub($price)
    {
        return Payment::roundPrice(1 / Payment::getEurInRub() * $price);
    }

    public static function convertEur($price)
    {
        return Payment::roundPrice($price * Payment::getEurInRub());
    }

    /**
     * @param $count
     * @param null $userId
     * @return mixed
     */
    public function createOrder($count, $userId = null)
    {
        $order = new Order;
        $order->type = Order::TYPE_BALANCE;
        $order->user_id = $userId ?? Auth::id();
        $order->currency = Payment::getCurrency();
        $order->eur = $count;
        $order->sum = $count;

        // правки под яндекс (русские платежки)
        $order->sum = Payment::getEurPrice($order->eur);
        $order->currency = Payment::CURRENCY_RUB;

        $order->save();

        $this->order = $order;

        return $order->id;
    }

    public function getForm()
    {
        if ($this->order) {
            return $this->payment->getForm(
                $this->order->id,
                $this->order->sum,
                trans('money.fillUp', ['sum' => $this->order->sum, 'eur' => $this->order->eur]));
        }

        return null;
    }

    public function successAction($params)
    {
        if ($this->payment->isSuccess($params)) {
            $this->order->status = 'complete';
            $this->order->save();
        }
        abort(200);
    }

    public static function roundPrice($price, $dec = 100)
    {
        return (int)($price * $dec) / $dec;
    }

    public static function numberFormat($price)
    {
        return number_format($price, 2, '.', ' ');
    }

    public static function getUserCloudPrice(int $id)
    {
        $usedCalcSpace = File::convertByteToGb(File::getUsedSpace($id, false) - File::getUserAllSpace($id));
        $paySpace = $usedCalcSpace > 0 ? ceil($usedCalcSpace) : 0;
        return round($paySpace * Payment::DISK_PRICE, 4);
    }
}
