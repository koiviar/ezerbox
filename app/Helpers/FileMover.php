<?php

namespace App\Helpers;

use App\Model\DownloadLink;
use App\Model\File;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileMover
 * @package App\Helpers
 */
class FileMover
{
    public static function move($data)
    {
        /** @var File $fileModel */
        $fileModel = File::where('id', '=', $data['id'])->first();

        if (!$fileModel) return;

        // отменяем перенос, если файл уже на месте
        if ($fileModel->disk == $data['disk']) {
            return;
        }

        // удаляем файл в месте назначения, если он там есть
        if (Storage::disk($data['disk'])->exists($fileModel->link)) {
            Storage::disk($data['disk'])->delete($fileModel->link);
        }

        // переносим файл
        $stream = Storage::disk($fileModel->disk)->readStream($fileModel->link);
        Storage::disk($data['disk'])->writeStream($fileModel->link, $stream);

        // проверяем успешность переноса
        if (
            Storage::disk($data['disk'])->exists($fileModel->link) &&
            Storage::disk($data['disk'])->size($fileModel->link) == Storage::disk($fileModel->disk)->size($fileModel->link)
        ) {
            // если перенос успешен, фиксируем и удаляем файл на старом диске
            $oldDisk = $fileModel->disk;

            $fileModel->disk = $data['disk'];
            $fileModel->save();

            Storage::disk($oldDisk)->delete($fileModel->link);

            Cache::forget('download_link_' . $fileModel->id);
        } else {
            // если перенос не удался, проверяем ли есть кусок на новом диске, удаляем и кидаем ошибку
            if (Storage::disk($data['disk'])->exists($fileModel->link)) {
                Storage::disk($data['disk'])->delete($fileModel->link);
            }

            throw new \Exception('File not transfered');
        }
    }
}
