<?php

namespace App\Helpers;

use App\Model\File;
use Arhitector\Yandex\Disk;

class YaDisk
{
    private static $disk;

    private static $instance;

    /**
     * @return YaDisk
     */
    public static function getInstance(): YaDisk
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * YaDisk constructor.
     */
    private function __construct()
    {
        self::$disk = new Disk('AQAAAAAXnGooAAShOi78Xt7OHUiwk1wgnGtEa4o');
    }

    /**
     * @return Disk
     */
    public static function getDisk()
    {
        self::getInstance();
        return self::$disk;
    }

    /**
     * @param string $dir
     * @return mixed
     */
    public static function getList($dir = '/')
    {
        return self::getDisk()->getResource($dir, 1000)->items;
    }

    /**
     * @param $name
     * @return Disk\Resource\Closed
     */
    public static function getItem($name)
    {
        return self::getDisk()->getResource($name);
    }

    /**
     * @param $path
     * @param $localFile
     * @return Disk\Operation|bool
     */
    public static function upload($path, $localFile)
    {
        return self::getDisk()->getResource($path . '/' . last(explode('/', $localFile)))->upload($localFile);
    }

    /**
     * @param $file
     * @return bool
     * @throws \Exception
     */
    public static function delete($file)
    {
        $disk = self::getDisk();
        $resource = $disk->getResource(File::SYSTEM_FILE_FOLDER . '/' . $file->link);

        if ($resource->has()) {
            if (!$resource->delete()) {
                throw new \Exception("Failed delete file from disk");
            }
        }

        $resource = $disk->getResource(File::SYSTEM_FILE_FOLDER . '/' . $file->link);
        if ($resource->has()) {
            throw new \Exception("Failed delete file from disk");
        }

        return true;
    }
}
