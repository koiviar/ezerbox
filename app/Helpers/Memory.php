<?php

namespace App\Helpers;

use App\Model\Server;

/**
 * Class Memory
 * @package App\Helpers
 */
class Memory
{
    public static function updateDisksStat()
    {
        $data = explode("\n", shell_exec('df'));
        $disks = Server::whereIn('type', ['storage', 'main'])->get()->keyBy('name');
        $stores = $disks->toArray();
        foreach ($data as $key => $value) {
            $info = explode(' ', preg_replace('/ {2,}/', ' ', $value));

            if (isset($info[5])  && ($info[5] == '/' || in_array(last(explode('/', $info[5])), array_keys($stores)))) {
                $total = Memory::toGB($info[1]);
                $used = Memory::toGB($info[2]);
                $usedPercent = round($used / ($total / 100), 2);

                /** @var Server $server */
                $server = $info[5] == '/' ? $disks['ezerbox.com'] : $disks[last(explode('/', $info[5]))];
                $server->size = $total;
                $server->space_used = $used;
                $server->space_used_p = $usedPercent;
                $server->save();
            }
        }
    }

    /**
     * @param int $size
     * @return false|float
     */
    public static function toGB(int $size)
    {
        return round($size / 1024 / 1024, 3);
    }
}
