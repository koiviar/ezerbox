<?php

namespace App\Helpers;

use App\Jobs\VPS;

class ServerWorkers
{
    /**
     * @param $server
     * @param $productId
     */
    public static function createNew($server, $productId)
    {
        $data = [];
        $data['server_id'] = $server->id;
        $data['product_id'] = $productId;
        $data['action'] = VPS::ACTION_CREATE;
        dispatch((new VPS($data))->onQueue('med'));
    }

    /**
     * @param int $serverId
     * @param string $os
     */
    public static function reinstallOs(int $serverId, string $os)
    {
        $data = [];
        $data['os'] = $os;
        $data['server_id'] = $serverId;
        $data['action'] = VPS::ACTION_REINSTALL;
        dispatch((new VPS($data))->onQueue('med'));
    }

    /**
     * @param $id
     */
    public static function reloadServer($id)
    {
        $data = [];
        $data['server_id'] = $id;
        $data['action'] = VPS::ACTION_RELOAD;
        dispatch((new VPS($data))->onQueue('med'));
    }
}
