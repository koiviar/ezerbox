<?php

namespace App\Exceptions;

use App\Model\ErrorLog;
use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @param Throwable $exception
     * @throws Throwable
     */
    public function report(Throwable $exception)
    {
        // сохраняем лог в базу для удобной работы с ними в дальнейшем
        if ($this->shouldReport($exception) && !env('APP_DEBUG')) {
            if (!strpos($exception->getMessage(), '1213 Deadlock found when trying to get lock')) {
                \Sentry\init(['dsn' => 'https://f77a690e6c664e08a060aa556aec3c62@o457319.ingest.sentry.io/5453122']);
                \Sentry\captureException($exception);
            }
        }

        parent::report($exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Throwable $exception
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    public function shouldReport(Throwable $e)
    {
        // кастомные исключения, которые не надо логировать
//        if (in_array($e->getMessage(), [
//                'Connection closed by server'
//            ]) ||
//            in_array($e->getCode(), [
//                1203
//            ]) ||
//            strpos($e->getMessage(), 'Could not login with username: admin') === false
//        ) {
//            return false;
//        }

        return parent::shouldReport($e);
    }
}
