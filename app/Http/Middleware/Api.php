<?php

namespace App\Http\Middleware;

use App\Model\ApiLog;
use App\Model\Token;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Api
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $inputToken = null;

        // ищем токен в гет и пост параметрах
        $inputToken = $request->input('token');

        // ищем токен в хедере
        if (!$inputToken) {
            $inputToken = $request->header('token');
        }

        // Авторизация для апи
        /** @var Token $token */
        $token = Token::where('token', '=', $inputToken)->first();
        if ($token) {
            $user = User::where('id', '=', $token->user_id)->first();
            Auth::login($user, false);
        }

        if (!Auth::id()) {
            die(json_encode([
                'error' => 503,
                'message' => 'Not authorized'
            ]));
        }

        $apiLogs = new ApiLog;
        $apiLogs->user_id = $token->user_id;
        $apiLogs->token_id = $token->id;
        $apiLogs->request = $request->path();
        $apiLogs->params = $request->all() ? json_encode($request->all()) : null;
        $apiLogs->save();

        return $next($request);
    }
}
