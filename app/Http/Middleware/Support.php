<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use App\Helpers\Data;

class Support
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // перенаправление на нужный домен
        // if($request->path() != 'bucket/upload' && substr_count($request->root(), '//upload.ezer')) {
        // if($request->method() == 'GET' && substr_count($request->path(), 'bucket') && substr_count($request->root(), '//ezerbox.com') && Auth::check()) {
        //     return redirect(str_finish('https://u.ezerbox.com/', $request->path()));
        // }
        // if($request->method() == 'GET' && !substr_count($request->path(), 'bucket') && substr_count($request->root(), '//u.ezerbox.com')) {
        //     return redirect(str_finish('https://ezerbox.com/', $request->path()));
        // }

        // установка языка, если его нет в сессии
        if (empty(session('trans'))) {
            if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $trans = strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
            } else {
                $trans = 'en';
            }
            session(['trans' => $trans]);
        }

        // установка локали
        app()->setLocale(session('trans'));

        // освежитель онлайн статуса юзера
        if (Auth::user() && Auth::user()->last_activity == []) {
            Auth::user()->last_activity = date('Y-m-d H:i:s', time() - 3600);
        }
        if (Auth::user() && Auth::user()->last_activity < date('Y-m-d H:i:s', time() - 60)) {
            $user = User::where('id', '=', Auth::id())->first();
            if ($user) {
                $user->last_activity = date('Y-m-d H:i:s');
                $user->timestamps = false;
                $user->save();
            }
        }

        // инициализация объекта ханения временной информации
        Data::init();

        return $next($request);
    }
}
