<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminApi
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('token');
        $params = $request->all() ?? [];
        $generatedToken = md5(json_encode(sort($params)) . 'my-awesome/salt');
        if ($token !== $generatedToken) {
            abort(404);
        }

        return $next($request);
    }
}
