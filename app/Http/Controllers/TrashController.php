<?php

namespace App\Http\Controllers;

use App\Model\BaseModel;
use App\Model\Dir;
use App\Model\File;
use App\Model\User;
use App\Helpers\Data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class BucketController
 * @package App\Http\Controllers
 */
class TrashController extends Controller
{
    public function index()
    {
        return view('trash.index', ['zone' => 'trash']);
    }

    public function getList(Request $request)
    {
        $input = $request->all();
        // проверяем папку на наличие
        $folder = Dir::where('id', '=', $input['dir'])->where('deleted', '=', 1)->first();
        if (!$folder) {
            $input['dir'] = 0;
        }

        // получаем список папок
        if (!$input['offset']) {
            $dirs = Dir::where('user_id', '=', Auth::id())
                ->where('deleted', '=', 1)
                ->orderBy('deleted_time', 'ASC');

            if (!$input['dir']) {
                $dirs->where(function ($qin) use ($input) {
                    $qin->where('dir', '=', $input['dir'])
                        ->orWhere('bucket_dir', '=', 1);
                });
            } else {
                $dirs->where('dir', '=', $input['dir']);
            }

            $dirs = $dirs->get();

            $result['dirs'] = (string)view('trash.layouts.dirList', compact('dirs'));
        }

        // получаем список файлов
        $files = File::where('user_id', '=', Auth::id())
            ->skip($input['offset'])->take(20)
            ->where('deleted', '=', 1)
            ->orderBy('deleted_time', 'ASC');

        if (!$input['dir']) {
            $files->where(function ($qin) use ($input) {
                $qin->where('dir', '=', $input['dir'])
                    ->orWhere('bucket_dir', '=', 1);
            });
        } else {
            $files->where('dir', '=', $input['dir']);
        }

        $files = $files->get();

        /** @var \App\Model\File $file */
        foreach ($files as $file) {
            $file->prepareToView();
        }
        $result['files'] = (string)view('trash.layouts.fileList', compact('files'));

        // получаем навигацию для хождения по папкам
        $result['nav'] = File::getBucketNavigation($input['dir']);

        // получаем информацию о содержимом папки
        $result['folderInfo'] = File::getTrashFolderContentCount($input['dir']);

        if (count($files) == 20) {
            $result['drawFileLoader'] = true;
        } else {
            $result['drawFileLoader'] = false;
        }

        // отправляем клиенту
        echo json_encode($result);
    }

    public function deleteEnt(Request $request)
    {
        $input = $request->all();

        // добавление в очередь
        File::deleteEnt($input, false);

        // узнаем доступный размер доступного диска
        $result['used'] = File::getUsedSpace(Auth::id());
        $result['percentUsed'] = File::checkUsedPercent(File::getUsedSpace(Auth::id(), false), File::getUserAllSpace(Auth::id()) / 1024);

        $result['popupSuccess'] = 'Файлы удалены безвозвратно';

        // отдаем клиенту
        echo json_encode($result);
    }

    public function deleteAll()
    {
        File::where('deleted', '=', 1)->where('user_id', '=', Auth::id())->update(['deleted' => 2]);
        Dir::where('deleted', '=', 1)->where('user_id', '=', Auth::id())->update(['deleted' => 2]);

//        Cache::forget('dir_size_0_' . Auth::id() . '_1');

        // узнаем доступный размер доступного диска
        $result['used'] = File::getUsedSpace(Auth::id());
        $result['percentUsed'] = File::checkUsedPercent(File::getUsedSpace(Auth::id(), false), File::getUserAllSpace(Auth::id()) / 1024);

        $result['popupSuccess'] = 'Файлы удалены безвозвратно';

        // отдаем клиенту
        echo json_encode($result);
    }

    public function restoreEnt(Request $request)
    {
        $input = $request->all();

        // добавление в очередь
        File::restoreEnt($input);

        // узнаем доступный размер доступного диска
        $result['used'] = File::getUsedSpace(Auth::id());
        $result['percentUsed'] = File::checkUsedPercent(File::getUsedSpace(Auth::id(), false), File::getUserAllSpace(Auth::id()) / 1024);

        // сообщение с результатом
        if ($msgs = Data::getData('filesRestoreMsg')) {
            $result['popupError'] = implode('<br>', $msgs);
        } else {
            $result['popupSuccess'] = 'Файлы восстановлены';
        }

        // записи, которые не будут удалены из списка
        $result['restoreFailed'] = Data::getData('filesRestoreIds') ?? [];

        // отдаем клиенту
        echo json_encode($result);
    }
}
