<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Dir;
use App\Model\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class FolderController
 * @package App\Http\Controllers\Api
 */
class FolderController extends Controller
{
    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $path = $request->post('path');
        $dirId = File::getDirId($path, true);

        $dir = Dir::where('id', '=', $dirId)->first();
        if ($dir) {
            $this->apiResponse($this->getPreparedFolderInfo($dir));
        } else {
            $this->apiError(1000, 'Folder not created');
        }
    }

    /**
     * @param Request $request
     */
    public function folderInfo(Request $request)
    {
        $path = $request->post('path');
        $dirId = File::getDirId($path);

        $dir = Dir::where('id', '=', $dirId)->first();
        if (!$dir) {
            $this->apiError(1001, 'Folder not found');
        } else {
            $this->apiResponse($this->getPreparedFolderInfo($dir));
        }
    }

    /**
     * @param Request $request
     */
    public function delete(Request $request)
    {
        $path = $request->post('path');
        $dirId = File::getDirId($path);

        $dir = Dir::where('id', '=', $dirId)->first();
        if (!$dir) {
            $this->apiError(1001, 'Folder not found');
        } else {
            File::deleteFolderContent($dir->id, true, true);
            $this->apiResponse();
        }
    }

    /**
     * @param Dir $dir
     * @return array
     */
    protected function getPreparedFolderInfo(Dir $dir) : array
    {
        return [
            'name' => $dir->name,
            'path' => File::getDirPath($dir->id),
            'created_at' => $dir->created_at,
            'update_at' => $dir->updated_at,
        ];
    }
}
