<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Retry;
use App\Http\Controllers\Controller;
use App\Model\Dir;
use App\Model\File;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * Class BucketController
 * @package App\Http\Controllers\Api
 */
class BucketController extends Controller
{
    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        $path = str_replace('//', '/', $request->post('path') . '/');
        $dirId = File::getDirId($path);

        if ($dirId === false) {
            $this->apiError(1001, 'Folder not found');
        }

        $data = [];

        $dirs = Dir::where('user_id', '=', Auth::id())
            ->where('deleted', '=', 0)
            ->where('dir', '=', $dirId)
            ->get();

        /** @var Dir $dir */
        foreach ($dirs as $dir) {
            $data[] = [
                'type' => 'folder',
                'name' => $dir->name,
                'path' => $path . $dir->name,
            ];
        }

        $files = File::where('user_id', '=', Auth::id())
            ->where('deleted', '=', 0)
            ->where('dir', '=', $dirId)
            ->get();

        /** @var File $file */
        foreach ($files as $file) {
            $data[] = [
                'type' => $file->type,
                'name' => $file->name,
                'path' => $path . $file->name,
            ];
        }

        $this->apiResponse($data);
    }

    /**
     * @param Request $request
     */
    public function fileInfo(Request $request)
    {
        $file = File::getFileByPath($request->post('path', ''));
        if (!$file) {
            $this->apiError(1001, 'File not found');
        }

        $this->apiResponse($this->getPreparedFileInfo($file));
    }

    /**
     * @param File $file
     * @return array
     */
    protected function getPreparedFileInfo(File $file) : array
    {
        $data = [
            'name' => $file->name,
            'path' => File::getFilePath($file),
            'size' => $file->size,
            'type' => $file->type,
            'cdn_link' => $file->getCdnLink(),
            'created_at' => date('Y-m-d H:i:s', strtotime($file->created_at)),
            'updated_at' => date('Y-m-d H:i:s',  strtotime($file->updated_at)),
        ];

        if ($file->public_code) {
            $data['public_code'] = $file->public_code;
        }

        return $data;
    }

    /**
     * @param Request $request
     * @throws \Throwable
     */
    public function uploadByLink(Request $request)
    {
        $link = $request->post('link');

        $path = $request->post('path');
        $fileName = last(explode('/', $path));
        $extension = last(explode('.', $fileName));
        $dir = File::getDirId(str_replace($fileName, '', $path), true);

        /** @var User $user */
        $user = User::where('id', '=', Auth::id())->first();

        $file = new File;
        $file->user_id = Auth::id();
        $file->generateCode();
        $file->name = $fileName;
        $file->extension = $extension;
        $file->link = $user->getDir($user->id) . '/' . substr($file->code, 0, 2) . '/' . $file->code . '.' . $file->extension;
        $file->dir = $dir ? Dir::getUserFolder($dir)->id ?? 0 : 0;

        Retry::runHttp(function () use ($file, $link) {
            if (Storage::disk('tmp')->exists($file->link)) {
                Storage::disk('tmp')->delete($file->link);
            }

            if (Storage::disk('tmp')->writeStream($file->link, fopen($link, 'r'))) {

                $file->inspectBaseParam();
                $file->disk = 'tmp';
                $file->save();

                $file->moveRealFile('clo');

                $this->apiResponse($this->getPreparedFileInfo($file));
            }
        });

        $this->apiError(1002, 'Error uploading file by link');
    }

    /**
     * @param Request $request
     */
    public function move(Request $request)
    {
        $from = $request->post('from', '');
        $to = $request->post('to');

        // TODO временный костыль для миграции файлов в новую систему
        if ($request->post('id')) {
            $file = File::where('user_id', '=', Auth::id())->where('id', '=', $request->post('id'))->first();
        } else {
            $file = File::getFileByPath($from);
        }

        if (!$file) {
            $this->apiError(1001, 'File not found');
        }

        $file->name = basename($to);
        $file->dir = File::getDirId(str_replace(basename($to), '', $to), true);
        $file->save();

        $this->apiResponse($this->getPreparedFileInfo($file));
    }

    /**
     * @param Request $request
     */
    public function deleteFile(Request $request)
    {
        // TODO временный костыль для миграции файлов в новую систему
        if ($request->post('id')) {
            $file = File::where('user_id', '=', Auth::id())->where('id', '=', $request->post('id'))->first();
        } else {
            $file = File::getFileByPath($request->post('path', ''));
        }

        if (!$file) {
            $this->apiError(1001, 'File not found');
        }

        $file->softDelete();

        $this->apiResponse();
    }

    public function cdnCreate(Request $request)
    {
        $file = File::getFileByPath($request->post('path', ''));
        if (!$file) {
            $this->apiError(1001, 'File not found');
        }

        if (!$file->public_code) {
            $file->generatePublicCode();
            $file->save();
        }

        $this->apiResponse(['link' => $file->getCdnLink()]);
    }

    public function cdnDelete(Request $request)
    {
        $file = File::getFileByPath($request->post('path', ''));
        if (!$file) {
            $this->apiError(1001, 'File not found');
        }

        if ($file->public_code) {
            $file->public_code = '';
            $file->save();
        }

        $this->apiResponse();
    }

    public function downloadLink(Request $request)
    {
        $file = File::getFileByPath($request->post('path', ''));
        if (!$file) {
            $this->apiError(1001, 'File not found');
        }

        $this->apiResponse(['link' => $file->getDownloadLink()]);
    }
}
