<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function send(Request $request)
    {
        if (!Auth::user()) return redirect('/login');
        $input = $request->all();

        $comment = new Comment($input['data']);
        $comment->user_id = Auth::id();
        $comment->save();

        $comments = new Comment;
        $comments->getCommentsTo($input['data']['zone_id']);
        $view = $comments->getMainView();

        return json_encode($view);
    }

    public function like(Request $request)
    {
        if (!Auth::user()) return redirect('/login');
        $input = $request->all();

        $comment = Comment::where('id', '=', $input['id'])->first();
        $comment->like++;
        $comment->save();

        return json_encode(true);
    }

    public function dislike(Request $request)
    {
        if (!Auth::user()) return redirect('/login');
        $input = $request->all();

        $comment = Comment::where('id', '=', $input['id'])->first();
        $comment->dislike++;
        $comment->save();

        return json_encode(true);
    }

    public function delete(Request $request)
    {
        if (!Auth::user()) return redirect('/login');
        $input = $request->all();

        $comment = Comment::where('id', '=', $input['id'])->first();
        $comment->delete();

        $comments = new Comment;
        $comments->getCommentsTo($input['zone_id']);
        $view = $comments->getMainView();

        return json_encode($view);
    }
}
