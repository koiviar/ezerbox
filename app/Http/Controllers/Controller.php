<?php

namespace App\Http\Controllers;

use App\Model\DownloadLink;
use App\Model\File;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function echoView($data)
    {

    }

    public function echoJson($data)
    {
        die(json_encode($data));
    }

    public function fileProxy(File $file, $url = null)
    {
        // стриминг видео
        if ($file->ex_type == 'video') {
            $start = 0;
            $size = $file->size;
            $end = $size - 1;

            header("Content-Type:" . $file->type);
            header("Content-Length:" . $file->size);

            header("Cache-Control: max-age=31536000, public");
            header("Expires: " . gmdate('D, d M Y H:i:s', time() + 31536000) . ' GMT');
            header("Last-Modified: " . gmdate('D, d M Y H:i:s', strtotime($file->created_at)) . ' GMT');
            header("Accept-Ranges: 0-" . $end);

            $opt = null;

            if (isset($_SERVER['HTTP_RANGE'])) {
                $c_start = $start;
                $c_end = $end;

                list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);

                if ($range == '-') {
                    $c_start = $size - substr($range, 1);
                } else {
                    $range = explode('-', $range);
                    $c_start = $range[0];

                    $c_end = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $c_end;
                }
                $c_end = ($c_end > $end) ? $end : $c_end;
                $start = $c_start;
                $end = $c_end;

                $length = $end - $start + 1;

                header('HTTP/1.1 206 Partial Content');
                header("Content-Length: " . $length);
                header("Content-Range: bytes $start-$end/" . $size);

                $opt = [
                    'http' => [
                        'method' => "GET",
                        'header' => "Range:" . request()->header('Range') . "\r\n"
                    ]
                ];
                $opt = stream_context_create($opt);
            }

            if (in_array($file->disk, FILE::BACKUP_DISKS)) {
                $fileStream = Storage::disk($file->disk)->readStream($file->link);
            } else {
                if ($opt) {
                    $fileStream = fopen($file->getCdnLink() ?? DownloadLink::getLink($file, true), 'r', false, $opt);
                } else {
                    $fileStream = fopen(DownloadLink::getLink($file, true), 'r');
                }
            }


            while (!feof($fileStream)) {
                echo fread($fileStream, 1024);
                flush();
            }
            fclose($fileStream);
            die;

        } // отдаем остальные файлы
        else {
            header("Content-Type:" . $file->type);
            header("Expires: " . gmdate('D, d M Y H:i:s', time() + 31536000) . ' GMT');
            header("Last-Modified: " . gmdate('D, d M Y H:i:s', strtotime($file->created_at)) . ' GMT');

            if (!$url) {
                header("Content-Length:" . $file->size);
                $url = DownloadLink::getLink($file, true);
            }

            $fileStream = fopen($url, 'r');
            while (!feof($fileStream)) {
                echo fread($fileStream, 1024);
                flush();
            }
            fclose($fileStream);
            die;
        }
    }

    public function apiResponse($data = [])
    {
        die(json_encode([
            'error' => 0,
            'data' => $data
        ]));
    }

    public function apiError($errorCode, $errorMsg)
    {
        die(json_encode([
            'error' => $errorCode,
            'msg' => $errorMsg
        ]));
    }
}
