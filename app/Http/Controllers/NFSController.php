<?php

namespace App\Http\Controllers;

use App\Helpers\Encoder;
use App\Helpers\HostingApi;
use App\Helpers\Payment;
use App\Model\Nfs;
use App\Model\Server;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NFSController extends Controller
{
    public function index()
    {
        session(['zone' => 'nfs']);
        if (!Auth::user()) return redirect('/login');
        return view('nfs.index');
    }

    public function list()
    {
        $nfsList = Nfs::where('user_id', '=', Auth::id())->get();

        $listView = (string)view('nfs.list', compact('nfsList'));

        $this->echoJson([
            'list' => $listView
        ]);
    }

    public function create(Request $request)
    {
        if (!isset(HostingApi::PRODUCT_ID_STORAGE[$request->post('space')])) {
            $this->echoJson([
                'error' => 1,
                'msg' => 'Incorrect NFS space value'
            ]);
        }

        if (Auth::user()->money < Payment::NFS_PRICE[$request->post('space')]) {
            $this->echoJson([
                'error' => 1,
                'msg' => 'Not enough money',
                'code' => 99,
                'value' => round(Payment::NFS_PRICE[$request->post('space')] - Auth::user()->money, 2),
            ]);
        }

        $nfs = Nfs::createNfs(Auth::id(), $request->post('space'));

        $this->echoJson([
            'error' => 0,
            'status' => 'ok',
        ]);
    }

    public function delete(Request $request)
    {
        /** @var Nfs $nfs */
        $nfs = Nfs::where('user_id', '=', Auth::id())
            ->where('id', '=', $request->post('id'))
            ->first();

        if ($nfs) {
            $nfs->unbind();
        }

        $this->echoJson([
            'status' => 'ok'
        ]);
    }

    public function getOneView(Request $request)
    {
        $nfs = Nfs::where('user_id', '=', Auth::id())
            ->where('id', '=', $request->post('id'))
            ->first();

        if ($nfs) {
            $item = (string)view('nfs.item', compact('nfs'));
        }

        $this->echoJson([
            'item' => $item ?? ''
        ]);
    }

    public function connectInfo(Request $request)
    {
        /** @var Nfs $nfs */
        $nfs = Nfs::where('user_id', '=', Auth::id())
            ->where('id', '=', $request->post('id'))
            ->first();

        if ($nfs) {
            $this->echoJson([
                'error' => 0,
                'ip' => $nfs->server->ip,
                'host' => strtolower($nfs->server->name . '.ezerbox.com'),
                'pwd' => Encoder::decode($nfs->server->pwd ?? ''),
            ]);
        }

        $this->echoJson([
            'error' => 1,
            'msg' => 'Disk not found',
        ]);
    }

    public function reload(Request $request)
    {
        /** @var Nfs $nfs */
        $nfs = Nfs::where('user_id', '=', Auth::id())
            ->where('id', '=', $request->post('id'))
            ->first();

        if ($nfs) {
            $nfs->reloadServer();
            Server::setProcess($nfs->server_id, Server::PROCESS_RELOAD);
        }

        $this->echoJson([
            'error' => 1,
            'msg' => 'Disk not found',
        ]);
    }
}
