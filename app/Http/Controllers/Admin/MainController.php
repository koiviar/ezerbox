<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\AdsStat;
use App\Model\File;
use App\Model\Miniature;
use App\Model\Server;
use App\Model\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Model\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Class MainController
 * @package App\Http\Controllers\Admin
 */
class MainController extends Controller
{
    /**
     * @param Request $request
     * @return array|false|Application|Factory|View|mixed
     */
    public function index(Request $request)
    {
        $users = User::orderByDesc('created_at')->get();
        $servers = Server::where('type', '!=', 'nfs')->get();
        $ads = AdsStat::all();
        $filesCount = File::count();
        $miniaturesCount = Miniature::count();

        return view('admin.index', [
            'zone' => 'admin',
            'users' => $users,
            'servers' => $servers,
            'ads' => $ads,
            'filesCount' => $filesCount,
            'miniaturesCount' => $miniaturesCount,
        ]);
    }

    public function queue()
    {
        $data = [
            'high' => Queue::size('high'),
            'med' => Queue::size('med'),
            'low' => Queue::size('low'),
            'default' => Queue::size('default'),
        ];
        dd($data);
//        Queue::spy();
//        $connection = 'redis';
//        $default = 'high';
//        var_dump(Queue::getRedis()->connection($connection)->zrange('queues:'.$default.':delayed' ,0, -1));

//For the reserved jobs
//        var_dump(Queue::getRedis()->connection($connection)->zrange('queues:'.$default.':reserved' ,0, -1));
//        print_r(DB::connection('sqlite_main')->table('jobs')->get()->toArray());
    }

    public function adminer()
    {
        @mkdir(public_path('adm'));
        $publicPath = 'adm/' . Str::random(128) . '.php';
//        dd(public_path($publicPath));
//        symlink(base_path('adminer.php'), public_path($publicPath));
//        copy(base_path('adminer.php'), public_path($publicPath));
        return redirect($publicPath);
    }
}
