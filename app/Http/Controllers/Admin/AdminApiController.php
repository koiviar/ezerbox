<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\DownloadLink;
use App\Model\File;
use App\Model\User;
use Illuminate\Http\Request;

/**
 * Class AdminApiController
 * @package App\Http\Controllers\Admin
 */
class AdminApiController extends Controller
{
    public function getFileInfo(Request $request)
    {
        $file = File::where('public_code', '=', $request->post('code'))
            ->where('user_id', '=', (int)$request->post('user_id'))
            ->first();
        return response()->json($file);
    }

    public function getUserByFolder(Request $request)
    {
        $users = User::all();
        foreach ($users as $user) {
            if (strtolower($user->login) == strtolower($request->post('folder'))) {
                return response()->json($user);
            }
        }
        abort(404);
    }

    public function getDownloadLink(Request $request)
    {
        $file = File::where('id', '=', (int)$request->post('id'))
//            ->where('user_id', '=', $request->post('user_id'))
            ->first();
        return response()->json(['link' => DownloadLink::getLink($file)]);
    }
}
