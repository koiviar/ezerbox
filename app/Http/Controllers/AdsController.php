<?php

namespace App\Http\Controllers;

use App\Model\AdsStat;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

/**
 * Class AdsController
 * @package App\Http\Controllers
 */
class AdsController extends Controller
{
    /**
     * @param Request $request
     * @param $adsCode
     * @return Application|RedirectResponse|Redirector
     */
    public function saveCode(Request $request, $adsCode)
    {
        if (!$request->hasCookie('ads_code')) {
            $code = AdsStat::where('code', '=', $adsCode)->first();

            if ($code) {
                $code->views++;
                $code->save();

                return response()->redirectTo('/')
                    ->withCookie(cookie('ads_code', $adsCode, 60 * 24 * 365));
            }
        }

        return redirect('/');
    }
}
