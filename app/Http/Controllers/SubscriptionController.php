<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;
use App\Helpers\Price;
use App\Helpers\Payment\Payeer;

/**
 * Class SubscriptionController
 * @package App\Http\Controllers
 */
class SubscriptionController extends Controller
{
    public function index()
    {
        session(['zone' => 'subscription']);
        if (!Auth::user()) return redirect('/login');
        return view('subscription.index');
    }

    public function createOrder(Request $request)
    {
        $input = $request->all();

        session(['zone' => 'subscription']);
        if (!Auth::user()) return redirect('/login');

        $rawPrice = Price::getRawPrice($input['space'], $input['period'], $input['k']);

        if (!$rawPrice) {
            return json_encode(['success' => false, 'message' => 'Tariff not found']);
        }

        $order = new Order;
        $order->user_id = Auth::id();
        $order->sum = $rawPrice;
        $order->currency = $input['k'];
        $order->status = 'created';
        $order->space = $input['space'];
        $order->period = $input['period'];
        $order->save();

        return json_encode(Payeer::getForm($order->id, $order->sum, $order->currency));
    }
}
