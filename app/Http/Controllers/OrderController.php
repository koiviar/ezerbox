<?php

namespace App\Http\Controllers;

use App\Helpers\Payment;
use App\Model\Nfs;
use Illuminate\Http\Request;
use App\Model\Order AS MOrder;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function createOrder(Request $request)
    {
        $payment = new Payment;
        if ($payment->createOrder($request->post('count'))) {
            $form = $payment->getForm();
            if ($form) {
                echo json_encode(['error' => false, 'form' => $form]);
                die;
            }
        }

        echo json_encode(['error' => true, 'popupError' => 'Create order error!']);
    }

    public function orderDiskPrice(Request $request)
    {
        echo json_encode([
            'price_ru' => Payment::numberFormat(Payment::getDiskPrice($request->post('coins'), Payment::CURRENCY_RUB)),
            'price_en' => Payment::numberFormat(Payment::getDiskPrice($request->post('coins'), Payment::CURRENCY_EUR)),
        ]);
    }

    public function getOrderPrice(Request $request)
    {
        echo json_encode([
            'price_ru' => Payment::numberFormat(Payment::getEurPrice($request->post('count')))
        ]);
    }

    public function getBalanceModal(Request $request)
    {
        $preFill = $request->post('moneyPreFill') ?? 0;
        $money = Auth::user()->getMoney();
        $sum = $cloudPrice = Payment::getUserCloudPrice(Auth::id());
        $nfs = Nfs::where('user_id', '=', Auth::id())->get();

        /** @var Nfs $item */
        foreach ($nfs as $item) {
            $sum += $item->getPrice();
        }

        return $this->echoJson(['modal' => (string)view('order.balanceModal', compact(['money', 'cloudPrice', 'nfs', 'sum', 'preFill']))]);
    }
}
