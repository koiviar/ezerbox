<?php

namespace App\Http\Controllers;

use App\Libs\MiniHash;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

// use Intervention\Image;
// use Intervention\Image\Facades;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    public function changePas(Request $request)
    {
        $input = $request->all();
        $user = User::where('id', '=', Auth::id())->first();
        $user->password = Hash::make($input['pas']);
        $user->save();
        return json_encode(true);
    }

    public function avatarUpload(Request $request)
    {
        if ($request->hasFile('file')) {
            // обрабатываем загруженные файлы
            $file = $request->file('file');

            $path = '/uploads/avatars/';
            $ext = $file->getClientOriginalExtension();
            if (!in_array($ext, ['jpg', 'jpeg', 'png'])) {
                return json_encode(['error' => 'not supported format']);
            }

            // удаляем старые изображения
            if (!strpos(Auth::user()->avatar, 'default-avatar')) {
                Storage::disk('public')->delete(Auth::user()->getAvatar('orig'));
                Storage::disk('public')->delete(Auth::user()->getAvatar('100'));
                Storage::disk('public')->delete(Auth::user()->getAvatar('300'));
            }

            // загружаем файл
            $avatarName = MiniHash::unic('App\Model\User', 'avatar', '%/###.jpg', 10);
            Storage::disk('public')->putFileAs('uploads/avatars', $file, $avatarName . '_orig.' . $ext);

            // конвертируем в jpg
            $img = Image::make(public_path($path) . '/' . $avatarName . '_orig.' . $ext);
            $img->encode('jpg', 75);
            $img->save(public_path('uploads/avatars') . '/' . $avatarName . '_orig.jpg');

            // ресайзим 100px
            $img = Image::make(public_path('uploads/avatars') . '/' . $avatarName . '_orig.' . $ext);
            $img->fit(100)->encode('jpg', 75);
            $img->save(public_path('uploads/avatars') . '/' . $avatarName . '_100.jpg');

            // ресайзим 300px
            $img = Image::make(public_path('uploads/avatars') . '/' . $avatarName . '_orig.' . $ext);
            $img->fit(300)->encode('jpg', 75);
            $img->save(public_path('uploads/avatars') . '/' . $avatarName . '_300.jpg');

            // удаляем изначальный файл
            if ($ext != 'jpg') Storage::disk('public')->delete('avatars/' . $avatarName . '_orig.' . $ext);

            // приписываем его к пользователю, который его загрузил
            $user = User::where('id', '=', Auth::id())->first();
            $user->avatar = $path . $avatarName . '_###.jpg';
            $user->save();
        }

        return json_encode(['path' => $user->avatar]);
    }

    public function deleteAvatar()
    {
        // удаляем старые изображения
        Storage::disk('public')->delete(str_replace(['###', '/uploads/'], ['orig', ''], Auth::user()->avatar));
        Storage::disk('public')->delete(str_replace(['###', '/uploads/'], ['100', ''], Auth::user()->avatar));
        Storage::disk('public')->delete(str_replace(['###', '/uploads/'], ['300', ''], Auth::user()->avatar));
        // меняем привязку в базе
        $user = User::where('id', '=', Auth::id())->first();
        $user->avatar = '/img/default-avatar.jpg';
        $user->save();

        return json_encode(['path' => $user->avatar]);
    }
}
