<?php

namespace App\Http\Controllers;

use App\Helpers\Payment;
use App\Model\DownloadLink;
use App\Libs\MiniHash;
use App\Libs\ViewsIndexer;
use App\Model\Dir;
use App\Model\File;
use App\Model\User;
use App\Model\UserPlan;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Throwable;

/**
 * Class BucketController
 * @package App\Http\Controllers
 */
class BucketController extends Controller
{
    /**
     * @return Redirector|View
     */
    public function index()
    {
        return view('bucket.index2', ['zone' => 'bucket']);
    }

    public function download($code)
    {
        if (!Auth::user()) return redirect('/login');

        /** @var File $file */
        $file = File::where('deleted', '=', 0)
            ->where('user_id', '=', Auth::id())
            ->where('code', '=', $code)
            ->first();

        if (!$file) {
            abort(404);
        }

        $name = $file->getFullFileName();

        return response()->streamDownload(function () use ($file) {
            $fileStream = Storage::disk($file->disk)->readStream($file->link);
            while (!feof($fileStream)) {
                echo fread($fileStream, 1024);
                flush();
            }
            fclose($fileStream);
        }, $name, [
            "Content-Type" => $file->type,
            "Content-Length" => $file->size,
        ]);
    }

    public function uploadChunk()
    {
        if (empty($_FILES) || $_FILES['file']['error']) {
            die('{"OK": 0, "info": "Failed to move uploaded file."}');
        }

        $userFolder = User::getDir(Auth::id());

        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
        $folder = storage_path('app/tmp/' . $userFolder);
        @mkdir($folder, 0777, true);
        $filePath = $folder . '/' . $fileName;

        // Open temp file
        $out = @fopen($filePath . 'part', $chunk == 0 ? "wb" : "ab");
        if ($out) {
            // Read binary input stream and append it to temp file
            $in = @fopen($_FILES['file']['tmp_name'], "rb");

            if ($in) {
                while ($buff = fread($in, 4096))
                    fwrite($out, $buff);
            } else {
                die('{"OK": 0, "info": "Failed to open input stream."}');
            }

            @fclose($in);
            @fclose($out);

            @unlink($_FILES['file']['tmp_name']);
        } else {
            die('{"OK": 0, "info": "Failed to open output stream."}');
        }


        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {

            $fileNameOriginal = $fileName;
            $fileNameA = explode('.', $fileName);

            // завершение загрузки файла
            // генерируем системный код файла
            $fileCode = MiniHash::unic('App\Model\File', 'code', '###', 8);
            // имя файла
            $fileName = $fileCode . '.' . last($fileNameA);
            // генерируем путь файла в системе
            $codeDir = substr($fileCode, 0, 2);
            $fileLink = $userFolder . '/' . $codeDir . '/' . $fileName;

            // создаем папку для файла
            @mkdir(storage_path('app/tmp/' . $userFolder . '/' . $codeDir));

            // сохраняем файл во временной директории
            rename($filePath . 'part', storage_path('app/tmp/' . $fileLink));

            // создаем информацию о файле в базе
            $fileM = new File();
            $fileM->name = $fileNameOriginal;
            $fileM->link = $fileLink;
            $fileM->inspectBaseParam();
            $fileM->user_id = Auth::id();
            $fileM->dir = (int)\request()->header('dir', 0);
            $fileM->code = $fileCode;
            $fileM->disk = 'tmp';
            $fileM->save();

            // добавление в очередь на перемещение в хранилище
            $fileM->moveRealFile();
        }

        die('{"OK": 1, "info": "Upload successful."}');
    }

    /**
     * @param Request $request
     */
    public function getList(Request $request)
    {
        $input = $request->all();
        // проверяем папку на наличие
        $folder = Dir::where('id', '=', (int)$input['dir'])
            ->where('deleted', '=', 0)
            ->where('user_id', '=', Auth::id())
            ->first();
        if (!$folder) {
            $input['dir'] = 0;
        }

        // получаем список папок
        if (!$input['offset']) {
            $dirs = Dir::where('user_id', '=', Auth::id())
                ->where('dir', '=', (int)$input['dir'])
                ->where('deleted', '=', 0)
                ->oldest('name')->get();
            $result['dirs'] = $dirs;
        }

        if ($input['offset']) {
            $page = ceil($input['offset'] / 30) + 1;
        } else {
            $page = 1;
        }
//        $page = $input['offset'] ? ceil($input['offset'] / 30) : 1;
        // получаем список файлов
        $files = File::where('user_id', '=', Auth::id())
            ->where('deleted', '=', 0)
            ->where('dir', '=', (int)$input['dir'])
            ->orderBy('created_at', 'DESC')
//            ->limit(30);
            ->paginate(30, ['*'], 'page', $page);
//        if ($input['offset']) {
//            $files->where('id', '<', $input['offset']);
//        }
        $files = $files->items();

        /** @var File $file */
        foreach ($files as $file) {
            $file->prepareToView();
            $file->toStandardStorage();
        }

        $result['files'] = $files;
        $result['page'] = $page;

        $result['drawFileLoader'] = count($files) == 30;

//        $result = $this->bucketStats($request, $result);

        // получаем навигацию для хождения по папкам
        $result['nav'] = File::getBucketNavigation($request->post('dir'));

        $result['cdn_folder'] = strtolower(Auth::user()->login);

        // отправляем клиенту
        $this->echoJson($result);
    }

    public function dirStat(Request $request)
    {
        $result = $this->bucketStats($request);
        $result['id'] = $request->post('dir');
        $this->echoJson($result);
    }

    /**
     * @param Request $request
     */
    public function createDir(Request $request)
    {
        // создаем категорию
        $dir = new Dir($request->all());
        $dir->user_id = Auth::id();
        $dir->save();

//        $result = $this->bucketStats($request, []);

        // отдаем клиенту на отрисовку
        $this->echoJson(['status' => 'ok']);
    }

    /**
     * @param Request $request
     */
    public function deleteEnt(Request $request)
    {
        $input = $request->all();
        // добавление в очередь
        File::deleteEnt($input);

        // файлы отправлены в корзину
        $result['popupSuccess'] = 'Файлы перемещены в корзину';

//        $result = $this->bucketStats($request, $result);

        // отдаем клиенту
        $this->echoJson($result);
    }

    public function dirsHierarchy()
    {
        $this->echoJson(['dirs' => File::getHierarhyDirList(Auth::id())]);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function rename(Request $request)
    {
        $newName = $request->post('newName');

        // переименовываем сущность
        if ($request->post('type') == 'file') {
            /** @var File $file */
            $file = File::where('id', '=', $request->post('id'))
                ->where('user_id', '=', Auth::id())
                ->first();

            if (strpos($newName, $file->extension) === false) {
                $newName .= $file->extension;
            }

            $file->name = $newName;
            $file->save();

        } else {
            /** @var Dir $dir */
            $dir = Dir::where('id', '=', $request->post('id'))
                ->where('user_id', '=', Auth::id())
                ->first();
            $dir->name = $newName;
            $dir->save();
        }

        // возвращаем флаг удачи или фейл
        $this->echoJson(['name' => $newName]);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function move(Request $request)
    {
        // перемещаем папки или содержимое (поменять id родителя)
        foreach ($request->post('ent') as $item) {
            if ($item['type'] == 'dir') {
                /** @var Dir $dir */
                $dir = Dir::where('id', '=', $item['id'])
                    ->where('user_id', '=', Auth::id())
                    ->first();
                $dir->dir = $request->post('targetDir');
                $dir->save();
            } else {
                /** @var File $file */
                $file = File::where('id', '=', $item['id'])
                    ->where('user_id', '=', Auth::id())
                    ->first();
                $file->dir = $request->post('targetDir');
                $file->save();
            }
        }

//        $result = $this->bucketStats($request);

        // отдаем клиенту
        $this->echoJson(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function getFileInfo(Request $request)
    {
        /** @var File $file */
        $file = File::where('id', '=', $request->post('id'))
            ->where('user_id', '=', Auth::id())
            ->first();
        $file->views += ViewsIndexer::getCount('File', $file->id);
        $file->prepareToView();

        $result['file'] = $file;

        // подготавливаем прямую ссылку на файл
        try {
            $result['file']['download_link'] = DownloadLink::getLink($file);
        } catch (Throwable $exception) {
            $result['file']['download_link'] = false;
        }

        $result['cdn_folder'] = strtolower(Auth::user()->login);

        $this->echoJson($result);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function createPublicLink(Request $request)
    {
        /** @var File $file */
        $file = File::where('id', '=', $request->post('id'))
            ->where('user_id', '=', Auth::id())
            ->first();
        $file->generatePublicCode();
        $file->save();

        $this->echoJson($file);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function deletePublicLink(Request $request)
    {
        /** @var File $file */
        $file = File::where('id', '=', $request->post('id'))
            ->where('user_id', '=', Auth::id())
            ->first();
        $file->public_code = null;
        $file->save();

        $this->echoJson($file);
    }

    /**
     * @param Request $request
     */
    public function checkMoney(Request $request)
    {
        $plan = UserPlan::where('user_id', '=', Auth::id())->first();

        if ($plan->type == 'elastic') {
            if (File::getAvailableSpace(Auth::id()) < $request->post('size') &&
                Auth::user()->money < Payment::MIN_BALANCE_TO_OVERLOAD) {
                $this->echoJson([
                    'error' => 1,
                    'msg' => 'Not enough money',
                    'code' => 99,
                    'data' => [
                        'need' => Payment::MIN_BALANCE_TO_OVERLOAD,
                        'to_fill' => round(Payment::MIN_BALANCE_TO_OVERLOAD - Auth::user()->money, 2),
                    ],
                ]);
            } else {
                $this->echoJson([
                    'error' => 0
                ]);
            }
        } else {
            if (File::getAvailableSpace(Auth::id()) < $request->post('size')) {
                $this->echoJson([
                    'error' => 2,
                    'msg' => 'Not enough space',
                    'data' => [
                        'text' => 'Change plan to get more space'
                    ]
                ]);
            } else {
                $this->echoJson([
                    'error' => 0
                ]);
            }
        }
    }

    /**
     * @param Request $request
     * @param array $result
     * @return array
     */
    protected function bucketStats(Request $request, $result = [])
    {
        // узнаем доступный размер доступного диска
        // подсчет использованого места
        $result['diskInfo'] = User::getUsedSpaceInfo();

        // получаем информацию о содержимом папки
        $result['folderInfo'] = File::getFolderContentCount($request->post('dir'));

        // узнаем доступный размер доступного диска
        $result['used'] = File::getUsedSpace(Auth::id());

//        if ($request->post('dirListLoad') == 'true') {
            // получаем иерархическую структуру папок для перемещения сущностей
//            $result['dirList'] = File::getHierarhyDirList(Auth::id());
//        } else {
//            $result['dirList'] = false;
//        }

        $result['percentUsed'] = File::checkUsedPercent(File::getUsedSpace(Auth::id(), false), File::getUserAllSpace(Auth::id()));

        return $result;
    }
}
