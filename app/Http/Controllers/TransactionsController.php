<?php

namespace App\Http\Controllers;

use App\Model\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionsController extends Controller
{
    public function index()
    {
        session(['zone' => 'transactions']);
        if (!Auth::user()) return redirect('/login');
        $transactions = Transaction::where('user_id', '=', Auth::id())->latest()->limit(50)->get();
        return view('transactions.index', compact('transactions'));
    }
}
