<?php

namespace App\Http\Controllers;

use App\Helpers\ApiDocs;
use App\Model\ApiLog;
use App\Model\Token;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    /**
     * @return array|false|Application|Factory|View|mixed
     */
    public function index()
    {
        $tokens = Token::where('user_id', '=', Auth::id())
            ->where('deleted', '=', 0)
            ->get();

        $logs = ApiLog::where('user_id', '=', Auth::id())
            ->orderByDesc('created_at')
            ->limit(50)
            ->get();

        $requestCount = ApiLog::where('user_id', '=', Auth::id())->count();

        return view('api.index', [
            'zone' => 'api',
            'tokens' => $tokens,
            'logs' => $logs,
            'requestCount' => $requestCount
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createToken(Request $request) : JsonResponse
    {
        $token = new Token;
        $token->user_id = Auth::id();
        $token->token = md5(Auth::id() . '/my-salt/' . microtime(true));
        $token->desc = $request->post('desc');
        $token->save();

        return response()->json(['error' => 0]);
    }

    public function deleteToken(Request $request)
    {
        $token = Token::where('user_id', '=', Auth::id())
            ->where('id', '=', $request->post('id'))
            ->first();

        if ($token) {
            $token->deleted = 1;
            $token->save();
        }

        return response()->json(['error' => 0]);
    }

    public function logs()
    {

    }

    public function docs()
    {
        return view('api.docs', [
            'zone' => 'api',
            'docs' => ApiDocs::getDocs()
        ]);
    }
}
