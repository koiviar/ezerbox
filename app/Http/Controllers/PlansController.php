<?php

namespace App\Http\Controllers;

use App\Model\File;
use App\Model\UserPlan;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class PlansController
 * @package App\Http\Controllers
 */
class PlansController extends Controller
{
    const PLANS = [
        'elastic' => [
            'type' => 'elastic',
            'per_gb' => 0.04,
            'total' => 0,
            'space' => 5,
            'v_space' => '5GB - ∞',
            'desc' => 'example',
        ],
        'lite' => [
            'type' => 'free',
            'per_gb' => 0.035,
            'total' => 5.25,
            'space' => 150,
            'v_space' => '150GB',
            'desc' => 'example',
        ],
        'pro' => [
            'type' => 'free',
            'per_gb' => 0.03,
            'total' => 15,
            'space' => 500,
            'v_space' => '500GB',
            'desc' => 'example',
        ],
        'pro+' => [
            'type' => 'free',
            'per_gb' => 0.025,
            'total' => 50,
            'space' => 2048,
            'v_space' => '2TB',
            'desc' => 'example',
        ],
    ];

    /**
     * @return array|false|Application|Factory|View|mixed
     */
    public function index()
    {
        $plan = UserPlan::where('user_id', '=', Auth::id())->first();

        return view('plans.index', [
            'zone' => 'plans',
            'plans' => self::PLANS,
            'currentPlan' => $plan
        ]);
    }

    public function change(Request $request)
    {
        $plan = self::PLANS[$request->post('plan')] ?? null;
        if ($plan) {
            if ($plan['total'] <= Auth::user()->money) {
                $userPlan = UserPlan::where('user_id', '=', Auth::id())->first();
                $userPlan->plan = $request->post('plan');
                $userPlan->type = $plan['type'];
                $userPlan->price = $plan['type'] == 'elastic' ? $plan['per_gb'] : $plan['total'];
                $userPlan->space = $plan['space'];
                $userPlan->save();

                return response()->json(['error' => 0]);
            } else {
                return response()->json([
                    'error' => 2,
                    'data' => [
                        'need' => $plan['total'],
                        'to_add' => round($plan['total'] - Auth::user()->money, 2)
                    ]
                ]);
            }
        }

        return response()->json(['error' => 1]);
    }
}
