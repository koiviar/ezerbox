<?php

namespace App\Http\Controllers;

use App\Libs\ViewsIndexer;
use App\Model\File;
use App\Model\FileCache;
use App\Model\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PublicFileController extends Controller
{
    public function index()
    {
        session(['zone' => 'publicFile']);

        $files = File::where('public_code', '!=', '')
            ->whereDate('created_at', '>', \Carbon\Carbon::now()->subMonth())
            ->latest('views')
            ->limit(50)
            ->get();
        foreach ($files as $file) {
            $file->prepareToView();
        }

        return view('bucket.publicFileIndex', compact('files'));
    }

    public function search($search)
    {
        session(['zone' => 'publicFile']);

        $files = File::query();
        $files->where('public_code', '!=', '')
            ->latest('views')
            ->limit(50);

        $searchSeparated = explode(' ', $search);
        foreach ($searchSeparated as $word) {
            if ($word == '') continue;
            $files->where('name', 'LIKE', '%' . $word . '%');
        }
        $files = $files->get();

        foreach ($files as $file) {
            $file->prepareToView();
        }

        // эксперементально, подсветка совпадений по поиску
        $bWords = [];
        foreach ($searchSeparated as $key => $word) {
            $bWords[] = '<b>' . $word . '</b>';
        }
        foreach ($files as $key => $file) {
            $file->name = str_ireplace($searchSeparated, $bWords, $file->name);
        }

        return view('bucket.publicFileIndex', compact('files', 'search'));
    }

    public function getFile(Request $request, $public_code)
    {
        session(['zone' => 'publicFile']);

        $file = File::where('public_code', '=', $public_code)->get();
        if (empty($file[0])) return redirect('404');
        $file = $file[0];

        // подсчет просмотров (запись юзеров, раз в час пересчет)
        $file->views += ViewsIndexer::indexUser($request, 'File', $file->id);
        $file->prepareToView();

        $comments = new Comment;
        $comments->getCommentsTo('PublicFile/' . $file->id);
        $commentsView = $comments->getMainView();

        return view('bucket.publicFile', compact('file', 'commentsView'));
    }

    public function download($public_code)
    {
        // достаем файл из основной таблицы файлов
        $file = File::where('public_code', '=', $public_code)->get();
        if (empty($file[0])) return redirect('404');
        $file = $file[0];

        // записываем факт запроса на скачивание
        $file->downloads++;
        $file->save();

        // время жизни временного файла на основном сервере
        $cacheLivevTime = 60 * 60;

        // подготавливаем файл для скачивания, если его нет, то переносим его на основной сервер (временно)
        $downloadFileR = FileCache::where('public_code', '=', $public_code)->get();
        if (empty($downloadFileR[0])) {
            // скачиваем файл, так как его нет в данный момент на основном сервере
            $tmp = explode('.', $file->link);
            $ext = $tmp[1];
            $link = $file->public_code . '.' . $ext;
            $df = Storage::disk($file->server)->get($file->link);
            Storage::disk('lc')->put($link, $df);

            // добавляем файл в базу для индексации
            $downloadFile = new FileCache;
            $downloadFile->public_code = $public_code;
            $downloadFile->link = $link;
            $downloadFile->last_use = time() + $cacheLivevTime;
            $downloadFile->save();
        } else {
            // продлеваем срок жизни файла, так как его запросили
            $downloadFile = $downloadFileR[0];
            $downloadFile->last_use = time() + $cacheLivevTime;
            $downloadFile->save();
        }

        // отдаем файл на скачивание
        return response()->download(storage_path('app/lc') . '/' . $downloadFile->link);
    }
}
