<?php

namespace App\Http\Controllers;

use App\Helpers\Payment;
use App\Helpers\Payment\Qiwi;
use Illuminate\Http\Request;
use App\Model\Order;
use App\Helpers\Price;
use App\Helpers\Payment\Payeer;

/**
 * Class SubscriptionController
 * @package App\Http\Controllers
 */
class PaymentController extends Controller
{

    public function success(Request $request, $payment)
    {
        $input = $request->all();

        if (isset($input['label'])) {
            $payment = new Payment($input['label']);
            $payment->successAction($input);
        } else {
            Qiwi::checkOrders();
        }

        return redirect('/disk');

//        $orderId = $_GET['m_orderid'];
//
//        $order = Order::find($orderId);
//
//        if (!$order) {
//            json_encode(['success' => false, 'msg' => 'order not found']);
//        }
//        if ($order->status == 'success') {
//            json_encode(['success' => true, 'msg' => 'order already payed']);
//        }
//
//        $m_key = md5($orderId.number_format($order->sum, 2).$order->currency);
//        $p_key = md5($orderId.$_GET['m_amount'].$_GET['m_curr']);
//
//        if ($m_key == $p_key) {
//            $order->status = 'success';
//            $order->save();
//        } else {
////            die('*');
//            return redirect('/payment/payeer/fail');
//        }
//
//        return redirect('/subscription');
//        echo json_encode(['success' => true]);
    }

    public function fail(Request $request, $payment)
    {
        echo json_encode(['success' => false]);
    }

    public function status(Request $request, $payment)
    {
        $input = $request->all();

        $orderId = $_REQUEST['m_orderid'];

        $order = Order::where('id', '=', $orderId)->first();

        if (!$order) {
            ob_end_clean();
            exit($orderId . '|error');
        }
        if ($order->status == 'success') {
            ob_end_clean();
            exit($orderId . '|success');
        }

        $m_key = md5($orderId . number_format($order->sum, 2) . $order->currency);
        $p_key = md5($orderId . $_REQUEST['m_amount'] . $_REQUEST['m_curr']);

        if ($m_key == $p_key) {
            $order->status = 'success';
            $order->save();

            ob_end_clean();
            exit($orderId . '|success');
        } else {
            ob_end_clean();
            exit($orderId . '|error');
        }
    }
}
