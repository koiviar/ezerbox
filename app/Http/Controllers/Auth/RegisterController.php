<?php

namespace App\Http\Controllers\Auth;

use App\Model\AdsStat;
use App\Model\UserPlan;
use App\User;
use App\Model\DiskSubscription;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/disk';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'login' => 'required|string|min:4|max:16|regex:/^[\w]+$/i|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data) : User
    {
        $user = new \App\Model\User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->login = $data['login'];
        $user->money = 0.0;
        $user->avatar = '/img/default-avatar.jpg';
        $user->password = Hash::make($data['password']);
        $user->ads_code = Cookie::get('ads_code', null);
        $user->save();

        $code = AdsStat::where('code', '=', Cookie::get('ads_code', null))->first();
        if ($code) {
            $code->regs++;
            $code->save();
        }

        $plan = new UserPlan;
        $plan->user_id = $user->id;
        $plan->plan = 'elastic';
        $plan->type = 'elastic';
        $plan->price = 0.04;
        $plan->space = 5;
        $plan->save();

        return User::where('id', '=', $user->id)->first();
    }
}
