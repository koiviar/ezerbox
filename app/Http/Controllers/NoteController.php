<?php

namespace App\Http\Controllers;

use App\Model\Group;
use App\Model\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public function index()
    {
        return view('note.index', ['zone' => 'note']);
    }

    public function getList(Request $request)
    {
        $input = $request->all();

        // выгрузка заметок
        $operator = $input['group_id'] == -1 ? '!=' : '=';

//        $notes = Note::select('groups.title AS gTitle', 'notes.*')
//            ->leftJoin('groups', 'groups.id', '=', 'notes.group_id')
//            ->where('notes.user_id', '=', Auth::id())
        $notes = Note::where('group_id', $operator, (int)$request->post('group_id'))
            ->where('user_id', '=', Auth::id())
            ->latest('created_at')
            ->get();

        $groupIds = [];
        /** @var Note $note */
        foreach ($notes as $note) {
            $groupIds[] = (int)$note->group_id;
        }

        $groups = Group::whereIn('id', $groupIds)->get()->keyBy('id');
        foreach ($notes as $note) {
            if (isset($groups[$note->group_id])) {
                $note->gTitle = $groups[$note->group_id]->title;
            }
        }

        $data['notes'] = (string)view('note.parts.notes', compact('notes'));

        // выгрузка групп
        $groups = Group::where('user_id', '=', Auth::id())
            ->latest('sort')
            ->get();

        $data['groups'] = (string)view('note.parts.groups', compact('groups'));
        $data['groupOptions'] = (string)view('note.parts.groupOptions', compact('groups'));
        $this->echoJson($data);
    }

    public function create(Request $request)
    {
        $sort = Note::max('sort') + 1;

        /** @var Note $note */
        $note = new Note($request->all());
        $note->content = $request->post('content');
        $note->sort = $sort;
        $note->user_id = Auth::id();
        $note->save();
        $this->echoJson($note->id);
    }

    public function update(Request $request)
    {
        /** @var Note $note */
        $note = Note::where('user_id', '=', Auth::id())
            ->where('id', '=', (int)$request->post('id'))
            ->first();

        if ($note) {
            $note->fill($request->all());
            $note->group_id = (int)$request->post('group_id') ?? null;
            $note->content = $request->post('content');
            $note->save();
            $this->echoJson($note->id);
        } else {
            $this->echoJson(['error' => 1]);
        }
    }

    public function delete(Request $request)
    {
        /** @var Note $note */
        $note = Note::where('user_id', '=', Auth::id())
            ->where('id', '=', (int)$request->post('id'))
            ->first();

        if ($note) {
            $note->delete();
            $this->echoJson(true);
        } else {
            $this->echoJson(['error' => 1]);
        }
    }

    public function getData(Request $request)
    {
        /** @var Note $note */
        $note = Note::where('user_id', '=', Auth::id())
            ->where('id', '=', (int)$request->post('id'))
            ->first();

        if ($note) {
            $result = $note->toArray();
            $result['content'] = $note->content;
            $this->echoJson($result);
        } else {
            $this->echoJson(['error' => 1]);
        }
    }

    public function search(Request $request)
    {
        if ($request->post('search')) {
            $notes = Note::where('title', 'like', '%' . $request->post('search') . '%');
        } else {
            $notes = new Note;
        }
        if ($request->post('group_id') != -1) {
            $notes->where('group_id', $request->post('group_id'));
        }
        $notes = $notes->where('user_id', Auth::id())
            ->orderBy('sort', 'desc')
            ->get();

        $groupIds = [];
        /** @var Note $note */
        foreach ($notes as $note) {
            $groupIds[] = $note->group_id;
        }

        $groups = Group::whereIn('id', $groupIds)->get()->keyBy('id');
        foreach ($notes as $note) {
            if (isset($groups[$note->group_id])) {
                $note->gTitle = $groups[$note->group_id]->title;
            }
        }

        $data['notes'] = (string)view('note.parts.notes', compact('notes'));
//        $notes = Note::select('groups.title AS gTitle', 'notes.*')
//            ->leftJoin('groups', 'groups.id', '=', 'notes.group_id')
//            ->where('notes.user_id', '=', Auth::id())
//            ->where('group_id', $request->post('group_id') == -1 ? '!=' : '=', $request->post('group_id'))
//            ->where('notes.title', 'like', '%' . $request->post('search') . '%')
//            ->latest('notes.created_at')
//            ->get();
//        $data['notes'] = (string)view('note.parts.notes', compact('notes'));

        $this->echoJson($data);
    }

    public function changeNoteSort(Request $request)
    {
        $input = $request->all();
        for ($i = 0; $i < count($input['ids']); $i++) {
            /** @var Note $note */
            $note = Note::where('user_id', '=', Auth::id())
                ->where('id', '=', (int)$input['ids'][$i])
                ->first();

            $note->sort = $input['sort'][$i];
            $note->save();
        }
        $this->echoJson(true);
    }

    // для групп

    public function createGroup(Request $request)
    {
        /** @var Group $group */
        $group = new Group($request->all());
        $group->user_id = Auth::id();
        $group->save();
        $group->sort = $group->id;
        $group->save();
        $this->echoJson(true);
    }

    public function getDataGroup(Request $request)
    {
        /** @var Group $group */
        $group = Group::where('user_id', '=', Auth::id())
            ->where('id', '=', (int)$request->post('id'))
            ->first();

        if ($group) {
            $this->echoJson($group);
        } else {
            $this->echoJson(['error' => 1]);
        }
    }

    public function saveGroup(Request $request)
    {
        /** @var Group $group */
        $group = Group::where('user_id', '=', Auth::id())
            ->where('id', '=', (int)$request->post('id'))
            ->first();

        $group->fill($request->all());
        $group->save();
        $this->echoJson(true);
    }

    public function deleteGroup(Request $request)
    {
        $input = $request->all();
        /** @var Group $group */
        $group = Group::where('user_id', '=', Auth::id())
            ->where('id', '=', (int)$request->post('id'))
            ->first();

        if ($group) {
            $group->delete();
            $this->echoJson(true);
        } else {
            $this->echoJson(['error' => 1]);
        }
    }

    public function changeGroupSort(Request $request)
    {
        $input = $request->all();
        for ($i = 0; $i < count($input['ids']); $i++) {
            /** @var Group $group */
            $group = Group::where('user_id', '=', Auth::id())
                ->where('id', '=', (int)$input['ids'][$i])
                ->first();

            $group->sort = $input['sort'][$i];
            $group->save();
        }
        $this->echoJson(true);
    }
}
