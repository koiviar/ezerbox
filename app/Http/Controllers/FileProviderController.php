<?php

namespace App\Http\Controllers;

use App\Model\Miniature;
use App\Model\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use App\Model\File;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class FileProviderController
 * @package App\Http\Controllers
 */
class FileProviderController extends Controller
{
    /**
     * @param $code
     */
    public function getFile($code)
    {
        $file = File::where('code', '=', $code)->first();

        if (!$file) {
            return abort(404);
        }

        if (!Auth::user() || $file->user_id != Auth::id()) {
            return abort(403);
        }

        if ($file) {
//            $file->toStandardStorage(true);
            $url = $file->disk == 'tmp' ? storage_path('app/tmp/') . $file->link : null;
            $this->fileProxy($file, $url);
        } else {
            return abort(404);
        }
    }

    /**
     * @param $size
     * @param $code
     * @return object|BinaryFileResponse|void
     * @throws \Exception
     */
    public function getFileMini($size, $code)
    {
        set_time_limit(1800);

        /** @var File $file */
        $file = File::where('code', '=', $code)->first();

        if (!$file) {
            return abort(404);
        }

        if (!Auth::user() || $file->user_id != Auth::id()) {
            return abort(403);
        }

        $size = explode('x', str_replace('*', 'x', $size));
        $width = $size[0];
        $height = $size[1] ?? null;

        $link = Miniature::createFile($file->id, $width, $height, true);

        if ($link) {
            $this->fileProxy($file, $link);
        } else {
            $this->ln($file);
            $this->fileProxy($file, storage_path('disk') . '/' . $file->disk . '/' . $file->link);
        }
    }

    /**
     * @param $userCode
     * @param $prefix
     * @param $publicCode
     * @return RedirectResponse|Redirector|void
     */
    public function getPublicFile($userCode, $prefix, $publicCode)
    {
        $tmp = explode('.', $publicCode);
        $publicCode = reset($tmp);

        if (!$publicCode) {
            abort(404);
        }

        $user = User::where('login', '=', 'Ulfan')->first();
        if (!$user) {
            dd(strtolower($userCode));
            abort(404);
        }

        /** @var File $file */
        $file = File::where('public_code', '=', $publicCode)
            ->where('user_id', '=', $user->id)
            ->where('deleted', '=', 0)->first();

        if (!$file) {
            abort(404);
        }

//        if ($file->disk == 'tmp') {
//
//        }
        $url = $file->disk == 'tmp' ? storage_path('app/tmp/') . $file->link : null;
        $this->fileProxy($file, $url);
//        $file->toStandardStorage(true);
//        $this->ln($file);
//        return redirect('https://cdn.ezerbox.com/p/' . $file->public_code . $file->extension);
    }

    /**
     * @param $size
     * @param $publicCode
     * @return RedirectResponse|Redirector|void
     */
    public function getPublicFileMini($size, $publicCode)
    {
        set_time_limit(1800);

        $tmp = explode('.', $publicCode);
        $publicCode = reset($tmp);

        /** @var File $file */
        $file = File::where('public_code', '=', $publicCode)->where('deleted', '=', 0)->first();

        if (!$file) {
            return abort(404);
        }

        $size = explode('x', str_replace('*', 'x', $size));
        $width = $size[0];
        $height = $size[1] ?? null;

        $link = Miniature::createFile($file->id, $width, $height);
        if ($link) {
            if (file_exists(public_path('p/' . $link))) {
                return redirect('https://cdn.ezerbox.com/p/' . $link);
            } else {
                abort(404);
            }
        } else {
            $this->ln($file);
            if (file_exists($this->lnPAth($file))) {
                return redirect('https://cdn.ezerbox.com/p/' . $file->public_code . $file->extension);
            } else {
                abort(404);
            }
        }
    }

    protected function ln(File $file)
    {
        @mkdir(public_path('p'));
        exec('ln -s "' . storage_path('disk') . '/' . $file->disk . '/' . $file->link . '" "' . $this->lnPAth($file) . '"');
    }

    protected function lnPAth(File $file)
    {
        return public_path('p/' . $file->public_code . $file->extension);
    }
}
