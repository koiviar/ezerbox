<?php

namespace App\Http\Controllers;

use App\Helpers\Payment;
use App\Model\DiskSubscription;
use App\Model\DiskUsageStat;
use App\Model\File;
use App\Model\MoneyOps;
use App\Model\Nfs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $data = [];

        // сбор информации об использовании облачного диска
        $data['disk']['files_count'] = File::where('user_id', '=', Auth::id())
            ->where('deleted', '=', 0)
            ->count();

        $data['disk']['spaceUsed'] = File::getUsedSpace(Auth::id(), true);
        $usedCalcSpace = File::convertByteToGb(File::getUsedSpace(Auth::id(), false) - File::getUserAllSpace(Auth::id()));
        $data['disk']['spaceUsedToCalc'] = $usedCalcSpace > 0 ? ceil($usedCalcSpace) : 0;

        /** @var DiskUsageStat $stats */
        $stats = DiskUsageStat::where('user_id', '=', Auth::id())
            ->where('date', '=', date('Y-m-d 00:00:00', strtotime("-1 day")))
            ->first();

        $newCount = $data['disk']['files_count'];
        $oldCount = $stats->info['count'] ?? $newCount;

        if ($oldCount) {
            $data['disk']['percentFiles'] = round(($newCount - $oldCount) / ($oldCount / 100), 2);
        } else {
            $data['disk']['percentFiles'] = 0;
        }

        $newSize = File::getUsedSpace(Auth::id(), false);
        $oldSize = $stats->info['size'] ?? $newSize;
        if ($oldSize) {
            $data['disk']['percentSize'] = round(($newSize - $oldSize) / ($oldSize / 100), 2);
        } else {
            $data['disk']['percentSize'] = 0;
        }

        // подсчет баланса
        $data['money']['disk'] = round($data['disk']['spaceUsedToCalc'] * Payment::DISK_PRICE, 4);

        $nfs = Nfs::where('user_id', '=', Auth::id())->get();
        $data['money']['nfs'] = 0;
        /** @var Nfs $item */
        foreach ($nfs as $item) {
            $data['money']['nfs'] += Payment::NFS_PRICE[$item->server->size];
        }
        $data['money']['nfs'] = round($data['money']['nfs'], 4);
        $data['money']['per_month'] = round($data['money']['disk'] + $data['money']['nfs'], 4);

        // подписки
        $data['sub'] = DiskSubscription::where('user_id', '=', Auth::id())->get();

        // получение информации о движениях баланса
        $data['money_up'] = MoneyOps::where('user_id', '=', Auth::id())
            ->where('dir', '=', '+')
            ->orderByDesc('id')
            ->limit(10)
            ->get();
        $data['money_down'] = MoneyOps::where('user_id', '=', Auth::id())
            ->where('dir', '=', '-')
            ->orderByDesc('id')
            ->limit(10)
            ->get();

        return view('dashboard.index', [
            'zone' => 'dashboard',
            'data' => $data,
        ]);
    }
}
