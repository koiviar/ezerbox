<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFileProviderCache extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->create('file_provider_caches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('file_code', 15)->nullable();
            $table->timestamp('expire_date')->nullable();
            $table->tinyInteger('miniature')->default(0);
            $table->char('miniature_code', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->dropIfExists('file_provider_caches');
    }
}
