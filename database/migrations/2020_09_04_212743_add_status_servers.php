<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusServers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->string('hosting_id')->nullable();
            $table->string('status')->nullable();
            $table->string('using_status')->default('used');
            $table->dateTime('pay_date')->nullable();
        });
    }
}
