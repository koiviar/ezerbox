<?php

use App\Model\BaseModel as BM;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IndexesUpgrade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('api_logs', function (Blueprint $table) {
            try {
                $table->dropIndex('api_logs_created_at_index');
            } catch (\Throwable $e) {}
        });

        Schema::table('files', function (Blueprint $table) {
            try {
                $table->dropIndex('files_id_user_id_index');
            } catch (\Throwable $e) {}
        });

        Schema::table('files', function (Blueprint $table) {
            try {
                $table->dropIndex('files_id_user_id_dir_index');
            } catch (\Throwable $e) {}
        });

        Schema::table('files', function (Blueprint $table) {
            try {
                $table->index(['code'], 'code');
            } catch (\Throwable $e) {}
            try {
                $table->index(['user_id', 'dir'], 'user_id__dir');
            } catch (\Throwable $e) {}
            try {
                $table->index(['name'], 'name');
            } catch (\Throwable $e) {}
        });


        Schema::table('dirs', function (Blueprint $table) {
            try {
                $table->dropIndex('dirs_id_user_id_index');
            } catch (\Throwable $e) {}
        });
        Schema::table('dirs', function (Blueprint $table) {
            try {
                $table->dropIndex('dirs_id_user_id_dir_index');
            } catch (\Throwable $e) {}
        });
        Schema::table('dirs', function (Blueprint $table) {
            try {
                $table->dropIndex('dirs_user_id_index');
            } catch (\Throwable $e) {}
        });

        Schema::table('dirs', function (Blueprint $table) {
            try {
                $table->index('user_id', 'user_id');
            } catch (\Throwable $e) {}
            try {
                $table->index(['user_id', 'dir'], 'user_id__dir');
            } catch (\Throwable $e) {}
        });

        Schema::table('file_provider_caches', function (Blueprint $table) {
            try {
                $table->index('file_id', 'file_id');
            } catch (\Throwable $e) {}
            try {
                $table->index(['miniature_code'], 'miniature_code');
            } catch (\Throwable $e) {}
        });

        Schema::table('download_links', function (Blueprint $table) {
            try {
                $table->dropIndex('download_links_server_file_id_index');
            } catch (\Throwable $e) {}
        });

        Schema::table('download_links', function (Blueprint $table) {
            try {
                $table->index('file_id', 'file_id');
            } catch (\Throwable $e) {}
            try {
                $table->index(['server'], 'server');
            } catch (\Throwable $e) {}
            try {
                $table->index(['file_id', 'server'], 'file_id__server');
            } catch (\Throwable $e) {}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
