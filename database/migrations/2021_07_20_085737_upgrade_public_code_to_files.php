<?php

use App\Model\BaseModel;
use App\Model\File;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpgradePublicCodeToFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->string('public_code', 12)->nullable()->default(null)->change();
        });

//        File::where('public_code', '=', '')->update(['public_code' => null]);
//
//        try {
//            $pdo = new \PDO("sqlite:" . database_path('db/files.sqlite'));
//            $q = $pdo->prepare('VACUUM');
//            $q->execute();
//        } catch (\Throwable $exception) {}
    }
}
