<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePublicFileProviderCaches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_main')->create('public_file_provider_caches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('file_code', 15)->nullable();
            $table->timestamp('expire_date')->nullable();
            $table->tinyInteger('miniature')->default(0);
            $table->char('miniature_code', 15)->nullable();
            $table->char('extension', 64)->default('');
            $table->integer('views')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_main')->dropIfExists('public_file_provider_caches');
    }
}
