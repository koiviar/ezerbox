<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalStatusInfoToFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_files')->table('files', function (Blueprint $table) {
            $table->smallInteger('deleted')->default('0');
            $table->integer('deleted_time')->nullable();
        });
    }
}
