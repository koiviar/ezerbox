<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_files')->table('files', function (Blueprint $table) {
            $table->index(['user_id']);
            $table->index(['id', 'user_id']);
            $table->index(['id', 'user_id', 'dir']);
        });

        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_dirs')->table('dirs', function (Blueprint $table) {
            $table->index(['user_id']);
            $table->index(['id', 'user_id']);
            $table->index(['id', 'user_id', 'dir']);
        });

        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_notes')->table('notes', function (Blueprint $table) {
            $table->index(['user_id']);
            $table->index(['id', 'user_id']);
            $table->index(['id', 'user_id', 'group_id']);
        });

        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_groups')->table('groups', function (Blueprint $table) {
            $table->index(['user_id']);
            $table->index(['id', 'user_id']);
        });

        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_download_links')->table('download_links', function (Blueprint $table) {
            $table->index(['server', 'file_id']);
        });
    }
}
