<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBucketInfoToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_main')->table('users', function (Blueprint $table) {
            $table->integer('free_space')->default(500);
            $table->integer('pay_space')->default(0);
            $table->double('money', 15, 2)->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_main')->table('users', function (Blueprint $table) {
            //
        });
    }
}
