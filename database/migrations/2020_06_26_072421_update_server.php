<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->integer('space_used')->default(0);
            $table->integer('space_used_p')->default(0);
            $table->integer('bw_used_p')->default(0);
        });
    }
}
