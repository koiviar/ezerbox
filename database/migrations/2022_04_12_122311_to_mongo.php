<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class toMongo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $part = 10000;
/*
        $count = \App\Model\Old\FileOld::count();
        $all = ceil($count / $part);
        for ($i = 1; $i <= $all; $i++) {
            echo "$i/$all\n";
            $data = \App\Model\Old\FileOld::offset(($i - 1) * $part)->limit($part)->get();
            if ($data->count()) {
                foreach ($data as $item) {
                    $item->user_id = (int)$item->user_id;
                    $item->dir = (int)$item->dir;
                    $item->deleted = (int)$item->deleted;
                    $item->bucket_dir = (int)$item->bucket_dir;
                    $item->size = (int)$item->size;
                }
                \App\Model\File::insert($data->toArray());
            }
        }

        $count = \App\Model\Old\DirOld::count();
        $all = ceil($count / $part);
        for ($i = 1; $i <= $all; $i++) {
            echo "$i/$all\n";
            $data = \App\Model\Old\DirOld::offset(($i - 1) * $part)->limit($part)->get();
            if ($data->count()) {
                foreach ($data as $item) {
                    $item->user_id = (int)$item->user_id;
                    $item->dir = (int)$item->dir;
                    $item->deleted = (int)$item->deleted;
                    $item->bucket_dir = (int)$item->bucket_dir;
                }
                \App\Model\Dir::insert($data->toArray());
            }
        }

        $data = \App\Model\Old\UserOld::all();
        if ($data->count()) {
            \App\Model\User::insert($data->toArray());
        }

        $data = \App\Model\Old\AdsStatOld::all();
        if ($data->count()) {
            \App\Model\AdsStat::insert($data->toArray());
        }

        $data = \App\Model\Old\BadUserOld::all();
        if ($data->count()) {
            \App\Model\BadUser::insert($data->toArray());
        }

        $data = \App\Model\Old\CommentOld::all();
        if ($data->count()) {
            \App\Model\Comment::insert($data->toArray());
        }

        $data = \App\Model\Old\DiskSubscriptionOld::all();
        if ($data->count()) {
            \App\Model\DiskSubscription::insert($data->toArray());
        }

        $data = \App\Model\Old\DiskUsageStatOld::all();
        if ($data->count()) {
            foreach ($data as $item) {
                $item->user_id = (int)$item->user_id;
            }
            \App\Model\DiskUsageStat::insert($data->toArray());
        }

        $data = \App\Model\Old\DownloadLinkOld::all();
        if ($data->count()) {
            \App\Model\DownloadLink::insert($data->toArray());
        }

        $data = \App\Model\Old\ErrorLogOld::all();
        if ($data->count()) {
            \App\Model\ErrorLog::insert($data->toArray());
        }*/

        $data = \App\Model\Old\FileProviderCacheOld::all();
        if ($data->count()) {
            \App\Model\FileProviderCache::insert($data->toArray());
        }

        $data = \App\Model\Old\GroupOld::all();
        if ($data->count()) {
            foreach ($data as $item) {
                $item->user_id = (int)$item->user_id;
                $item->group = (int)$item->group;
            }
            \App\Model\Group::insert($data->toArray());
        }

        $data = \App\Model\Old\MiniatureOld::all();
        if ($data->count()) {
            \App\Model\Miniature::insert($data->toArray());
        }

        $data = \App\Model\Old\MoneyOpsOld::all();
        if ($data->count()) {
            foreach ($data as $item) {
                $item->user_id = (int)$item->user_id;
            }
            \App\Model\MoneyOps::insert($data->toArray());
        }

        $data = \App\Model\Old\NoteOld::all();
        if ($data->count()) {
            foreach ($data as $item) {
                $item->user_id = (int)$item->user_id;
                $item->group = (int)$item->group;
            }
            \App\Model\Note::insert($data->toArray());
        }

        $data = \App\Model\Old\OrderOld::all();
        if ($data->count()) {
            foreach ($data as $item) {
                $item->user_id = (int)$item->user_id;
            }
            \App\Model\Order::insert($data->toArray());
        }

        $data = \App\Model\Old\ServerOld::all();
        if ($data->count()) {
            \App\Model\Server::insert($data->toArray());
        }

        $data = \App\Model\Old\SpaceUsageOld::all();
        if ($data->count()) {
            foreach ($data as $item) {
                $item->user_id = (int)$item->user_id;
            }
            \App\Model\SpaceUsage::insert($data->toArray());
        }

        $data = \App\Model\Old\TokenOld::all();
        if ($data->count()) {
            foreach ($data as $item) {
                $item->user_id = (int)$item->user_id;
                $item->deleted = (int)$item->deleted;
            }
            \App\Model\Token::insert($data->toArray());
        }

        $data = \App\Model\Old\TransactionOld::all();
        if ($data->count()) {
            \App\Model\Transaction::insert($data->toArray());
        }

        $data = \App\Model\Old\UserPlanOld::all();
        if ($data->count()) {
            foreach ($data as $item) {
                $item->user_id = (int)$item->user_id;
            }
            \App\Model\UserPlan::insert($data->toArray());
        }

        $data = \App\Model\Old\UseServiceOld::all();
        if ($data->count()) {
            \App\Model\UseService::insert($data->toArray());
        }

        $data = \App\Model\Old\VpsUsageOld::all();
        if ($data->count()) {
            \App\Model\VpsUsage::insert($data->toArray());
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
