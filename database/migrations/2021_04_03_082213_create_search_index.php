<?php

use App\Model\BaseModel as DB;
use App\Model\File;
use App\Model\Note;
use App\Model\Dir;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->text('search_title')->nullable();
        });

//        $count = Note::whereNull('search_title')->count();
//        for ($i = 0; $i <= $count; $i += 1000) {
//            $notes = Note::whereNull('search_title')->offset($i)->limit(1000)->get();
//            /** @var Note $file */
//            foreach ($notes as $file) {
//                $file->search_title = mb_strtolower($file->title);
//                $file->save();
//            }
//        }

        Schema::table('dirs', function (Blueprint $table) {
            $table->text('search_title')->nullable();
        });

//        $count = Dir::whereNull('search_title')->count();
//        for ($i = 0; $i <= $count; $i += 1000) {
//            $dirs = Dir::whereNull('search_title')->offset($i)->limit(1000)->get();
//            /** @var Dir $dir */
//            foreach ($dirs as $file) {
//                $file->search_title = mb_strtolower($file->name);
//                $file->save();
//            }
//        }

        Schema::table('files', function (Blueprint $table) {
            $table->text('search_title')->nullable();
        });

//        $count = File::whereNull('search_title')->count();
//        for ($i = 0; $i <= $count; $i += 1000) {
//            $files = File::whereNull('search_title')->offset($i)->limit(1000)->get();
//            /** @var File $file */
//            foreach ($files as $file) {
//                $file->search_title = mb_strtolower($file->name);
//                $file->save();
//            }
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->dropColumn('search_title');
        });

        Schema::table('dirs', function (Blueprint $table) {
            $table->dropColumn('search_title');
        });

        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('search_title');
        });
    }
}
