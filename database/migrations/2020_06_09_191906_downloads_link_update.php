<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DownloadsLinkUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_download_links')->table('download_links', function (Blueprint $table) {
            $table->string('static', 1)->nullable();
            $table->integer('file_id')->nullable();
        });
    }
}
