<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLinkToFileProviderCaches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->table('file_provider_caches', function (Blueprint $table) {
            $table->text('link')->nullable();
            $table->char('disk', 8)->nullable();
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->table('file_provider_caches', function (Blueprint $table) {
            $table->dropColumn('views');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->table('file_provider_caches', function (Blueprint $table) {
            $table->dropColumn('link');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->table('file_provider_caches', function (Blueprint $table) {
            $table->dropColumn('disk');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->table('file_provider_caches', function (Blueprint $table) {
            $table->integer('views')->default(0)->after('extension');
        });
    }
}
