<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoneyOp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_money_ops')->create('money_ops', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->double('sum', 15, 4);
            $table->char('cur', 3)->default('eur');
            $table->char('dir', 1)->default('-');
            $table->text('info')->nullable();
            $table->timestamps();
        });
    }
}
