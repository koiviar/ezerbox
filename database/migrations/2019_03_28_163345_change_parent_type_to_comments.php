<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeParentTypeToComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_comments')->table('comments', function (Blueprint $table) {
            $table->integer('user_id')->default(0)->change();
            $table->integer('parent')->default(0)->change();
            $table->integer('like')->default(0)->change();
            $table->integer('dislike')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_comments')->table('comments', function (Blueprint $table) {
            //
        });
    }
}
