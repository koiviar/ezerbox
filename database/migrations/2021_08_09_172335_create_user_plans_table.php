<?php

use App\Model\BaseModel;
use App\Model\User;
use App\Model\UserPlan;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dbFile = database_path('db/user_plans.sqlite');
        if (!file_exists($dbFile)) {
            file_put_contents($dbFile, '');
        }

        Schema::create('user_plans', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->char('plan');
            $table->char('type');
            $table->integer('space');
            $table->double('price');
            $table->timestamps();
        });

//        $users = User::all();
//        foreach ($users as $user) {
//            $plan = UserPlan::where('user_id', '=', $user->id)->first();
//            if (!$plan) {
//                $plan = new UserPlan;
//                $plan->user_id = $user->id;
//                $plan->plan = 'elastic';
//                $plan->type = 'elastic';
//                $plan->price = 0.04;
//                $plan->space = 5;
//                $plan->save();
//            }
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_plans');
    }
}
