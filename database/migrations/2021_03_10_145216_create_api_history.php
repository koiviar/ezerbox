<?php

use App\Model\BaseModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!file_exists(database_path('db/api_logs.sqlite'))) {
            @mkdir(database_path('db'));
            $f = fopen(database_path('db/api_logs.sqlite'), 'w');
            fclose($f);
        }

        Schema::create('api_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->index();
            $table->integer('token_id')->index();
            $table->text('request');
            $table->text('params')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(BaseModel::getCN('api_logs'))->dropIfExists('api_logs');
    }
}
