<?php

use App\Model\BaseModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!file_exists(database_path('db/ads_stats.sqlite'))) {
            @mkdir(database_path('db'));
            $f = fopen(database_path('db/ads_stats.sqlite'), 'w');
            fclose($f);
        }

        Schema::create('ads_stats', function (Blueprint $table) {
            $table->id();
            $table->char('code', 8);
            $table->char('desc', 255)->nullable();
            $table->integer('views')->default(0);
            $table->integer('regs')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_stats');
        @unlink(database_path('db/ads_stats.sqlite'));
    }
}
