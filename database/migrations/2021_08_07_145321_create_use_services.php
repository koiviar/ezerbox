<?php

use App\Model\BaseModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUseServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dbFile = database_path('db/use_services.sqlite');
        if (!file_exists($dbFile)) {
            file_put_contents($dbFile, '');
        }

        Schema::create('use_services', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->char('type', 64);
            $table->double('price', 11, 2)->default(0);
            $table->text('info')->nullable();
            $table->smallInteger('billed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('use_services');
    }
}
