<?php

use App\Model\BaseModel as BD;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMiniature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!file_exists(database_path('db/miniatures.sqlite'))) {
            @mkdir(database_path('db'));
            $f = fopen(database_path('db/miniatures.sqlite'), 'w');
            fclose($f);
        }

        Schema::create('miniatures', function (Blueprint $table) {
            $table->id();
            $table->integer('file_id')->index('file_id');
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->char('disk', 8);
            $table->timestamp('expire_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('miniatures');
    }
}
