<?php

use App\Model\FileProviderCache;
use App\Model\PublicFileProviderCache;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFileProviderCache extends Migration
{
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->table('file_provider_caches', function (Blueprint $table) {
            $table->integer('file_id')->nullable();
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_file_provider_caches')->table('file_provider_caches', function (Blueprint $table) {
            $table->dropColumn('file_code');
        });
    }
}
