<?php

use App\Model\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserMoneyChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_main')->table('users', function (Blueprint $table) {
            $table->double('money_2', 15, 4)->default(0);
        });

//        $users = User::all();
//
//        foreach ($users as $user) {
//            $user->money_2 = $user->money;
//            $user->save();
//        }

        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_main')->table('users', function (Blueprint $table) {
            $table->dropColumn('money');
        });

        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_main')->table('users', function (Blueprint $table) {
            $table->renameColumn('money_2', 'money');
        });
    }
}
