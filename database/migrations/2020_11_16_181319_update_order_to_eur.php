<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderToEur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_orders')->table('orders', function (Blueprint $table) {
            $table->dropColumn('space');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_orders')->table('orders', function (Blueprint $table) {
            $table->dropColumn('period');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_orders')->table('orders', function (Blueprint $table) {
            $table->dropColumn('coins');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_orders')->table('orders', function (Blueprint $table) {
            $table->dropColumn('comment');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_orders')->table('orders', function (Blueprint $table) {
            $table->double('eur', 10, 2)->nullable();
        });
    }
}
