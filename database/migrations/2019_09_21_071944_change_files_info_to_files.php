<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFilesInfoToFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_files')->table('files', function (Blueprint $table) {
            $table->char('real_dir', 15)->default('');
            $table->char('code', 15)->default('');
        });
    }
}
