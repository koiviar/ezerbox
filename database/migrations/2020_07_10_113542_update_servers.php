<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->dropColumn('space_used');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->dropColumn('space_used_p');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->dropColumn('bw_used_p');
        });

        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->double('space_used', 15, 3)->default(0);
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->double('space_used_p', 15, 2)->default(0);
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_servers')->table('servers', function (Blueprint $table) {
            $table->double('bw_used_p', 15, 2)->default(0);
        });

//        \App\Helpers\HostingApi::updateServersStat();
    }
}
