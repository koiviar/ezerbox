<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoreIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_files')->table('files', function (Blueprint $table) {
            $table->index(['user_id', 'dir'], 'user_id_dir');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_files')->table('files', function (Blueprint $table) {
            $table->index(['user_id', 'deleted'], 'user_id_deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_files')->table('files', function (Blueprint $table) {
            $table->dropIndex('user_id_dir');
        });
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_files')->table('files', function (Blueprint $table) {
            $table->dropIndex('user_id_deleted');
        });
    }
}
