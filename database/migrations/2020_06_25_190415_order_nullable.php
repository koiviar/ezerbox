<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION', 'mongo') == 'mongo' ? 'mongo' : 'sqlite_orders')->table('orders', function (Blueprint $table) {
            $table->integer('space')->nullable()->default(null)->change();
            $table->integer('period')->nullable()->default(null)->change();
            $table->integer('coins')->nullable()->default(null)->change();
        });
    }
}
