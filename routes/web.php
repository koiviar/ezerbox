<?php

use App\Model\ApiLog;
use App\Model\BaseModel;
use App\Model\DiskSubscription;
use App\Model\DiskUsageStat;
use App\Model\DownloadLink;
use App\Model\File;
use App\Model\Dir;
use App\Model\FileProviderCache;
use App\Model\Group;
use App\Model\Miniature;
use App\Model\MoneyOps;
use App\Model\Nfs;
use App\Model\Note;
use App\Model\Old\UserOld;
use App\Model\Order;
use App\Model\Server;
use App\Model\SpaceUsage;
use App\Model\Token;
use App\Model\Transaction;
use App\Model\User;
use App\Model\VpsUsage;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use Google\Cloud\Vision\V1\Feature\Type;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Google\Cloud\Vision\V1\Likelihood;
use Illuminate\Support\Facades\Storage;

Route::group(['middleware' => 'support'], function () {
    Route::get('/home', function () {
        return redirect('/disk');
    });

    Route::get('/login', function () {
        return redirect('/');
    });

    Route::get('/', function () {
        session(['zone' => 'index']);
        Auth::logout();
        return view('landing.main');
    });

    // смена языка
    Route::get('/changeLang/{lang}', function($lang) {
        session(['trans' => $lang]);
        return 'true';
    });

    // обработка платежей
    Route::any('/payment/{payment}/success', 'PaymentController@success')->name('paymentSuccess');
    Route::any('/payment/{payment}/fail', 'PaymentController@fail');
    Route::any('/payment/{payment}/status', 'PaymentController@status');

    Route::get('/ads/{adsCode}', 'AdsController@saveCode')->name('ads.saveCode');

    // публичные страницы
    Route::get('/api/manage/docs', 'ApiController@docs')->name('apiManage.docs');

    Auth::routes();

    // защита от неавторизированныз запросов
    Route::middleware(['auth'])->group(function () {
        Route::get('/csrf-token', function () {
            return response()->json(['token' => csrf_token()]);
        });

        // dashboard
        Route::get('/dash', 'DashboardController@index');

        // bill
        Route::get('/bill', 'BillController@index')->name('bill.index');

        Route::get('/plans', 'PlansController@index')->name('plans');
        Route::post('/plans/change', 'PlansController@change')->name('plans.change');

        // обработка запросов связанных с файловым менеджером
        Route::get('/disk', 'BucketController@index');
        Route::get('/disk/download/{code}', 'BucketController@download');
        Route::post('/disk/upload-chunk', 'BucketController@uploadChunk');
        Route::post('/disk/check-money', 'BucketController@checkMoney');
        Route::post('/disk/getList', 'BucketController@getList');
        Route::post('/disk/createDir', 'BucketController@createDir');
        Route::post('/disk/delete', 'BucketController@deleteEnt');
        Route::post('/disk/rename', 'BucketController@rename');
        Route::post('/disk/move', 'BucketController@move');
        Route::post('/disk/getFileInfo', 'BucketController@getFileInfo');
        Route::post('/disk/createPublicLink', 'BucketController@createPublicLink');
        Route::post('/disk/deletePublicLink', 'BucketController@deletePublicLink');
        Route::post('/disk/dirsHierarchy', 'BucketController@dirsHierarchy')->name('disk.dirsHierarchy');
        Route::post('/disk/dirStat', 'BucketController@dirStat')->name('disk.dirStat');

        // для работы с NFS дисками
        Route::get('/nfs', 'NFSController@index');
        Route::post('/nfs/list', 'NFSController@list');
        Route::post('/nfs/getOneView', 'NFSController@getOneView');
        Route::post('/nfs/create', 'NFSController@create');
        Route::post('/nfs/delete', 'NFSController@delete');
        Route::post('/nfs/reload', 'NFSController@reload');
        Route::post('/nfs/connect-info', 'NFSController@connectInfo');
        Route::post('/nfs/info', 'NFSController@info');

        // обработка запросов с мусорной корзины
        Route::get('/trash', 'TrashController@index');
        Route::post('/trash/getList', 'TrashController@getList');
        Route::post('/trash/deleteEnt', 'TrashController@deleteEnt');
        Route::post('/trash/deleteAll', 'TrashController@deleteAll');
        Route::post('/trash/restoreEnt', 'TrashController@restoreEnt');

        // для работы с заметками
        Route::get('/note', 'NoteController@index');
        Route::post('/note/getList', 'NoteController@getList');
        Route::post('/note/create', 'NoteController@create');
        Route::post('/note/update', 'NoteController@update');
        Route::post('/note/delete', 'NoteController@delete');
        Route::post('/note/getData', 'NoteController@getData');
        Route::post('/note/search', 'NoteController@search');
        Route::post('/note/changeNoteSort', 'NoteController@changeNoteSort');

        // для групп
        Route::post('/note/createGroup', 'NoteController@createGroup');
        Route::post('/note/getDataGroup', 'NoteController@getDataGroup');
        Route::post('/note/saveGroup', 'NoteController@saveGroup');
        Route::post('/note/deleteGroup', 'NoteController@deleteGroup');
        Route::post('/note/changeGroupSort', 'NoteController@changeGroupSort');

        // публичные файлы
        Route::get('/shared', 'PublicFileController@index');
        Route::get('/shared/download/{public_id}', 'PublicFileController@download');
        Route::get('/shared/search/{search}', 'PublicFileController@search');
        Route::get('/shared/{public_id}', 'PublicFileController@getFile');

        // для обработки запросов связанных с пользователем
        Route::post('/user/changePas', 'UserController@changePas');
        Route::post('/user/avatarUpload', 'UserController@avatarUpload');
        Route::post('/user/deleteAvatar', 'UserController@deleteAvatar');

        // страница с операциями с балансом
        Route::get('/transactions', 'TransactionsController@index');

        // страница с тарифами
        Route::get('/subscription', 'SubscriptionController@index');

        // заказы
        Route::post('/order/createOrder', 'OrderController@createOrder');
        Route::post('/order/getOrderPrice', 'OrderController@getOrderPrice');
        Route::post('/order/getBalanceModal', 'OrderController@getBalanceModal');

        // API управление
        Route::get('/api/manage', 'ApiController@index')->name('apiManage.index');
        Route::get('/api/manage/logs', 'ApiController@logs')->name('apiManage.logs');
        Route::post('/api/manage/token/create', 'ApiController@createToken')->name('apiManage.createToken');
        Route::post('/api/manage/token/delete', 'ApiController@deleteToken')->name('apiManage.deleteToken');
    });
});

// API
Route::group(['middleware' => 'api'], function () {

    // работа с файлами
    Route::post('/api/disk/list', 'Api\BucketController@list')->name('api.disk.list');
    Route::post('/api/disk/file/info', 'Api\BucketController@fileInfo')->name('api.disk.file-info');
    Route::post('/api/disk/file/upload', 'Api\BucketController@upload'); // TODO
    Route::post('/api/disk/file/upload/link', 'Api\BucketController@uploadByLink')->name('api.disk.file-upload-link');
    Route::post('/api/disk/file/move', 'Api\BucketController@move')->name('api.disk.file-move');
    Route::post('/api/disk/file/rename', 'Api\BucketController@move')->name('api.disk.file-rename');
    Route::post('/api/disk/file/delete', 'Api\BucketController@deleteFile')->name('api.disk.file-delete');
    Route::post('/api/disk/file/cdn/create', 'Api\BucketController@cdnCreate')->name('api.disk.file-cdn-create');
    Route::post('/api/disk/file/cdn/delete', 'Api\BucketController@cdnDelete')->name('api.disk.file-cdn-delete');
    Route::post('/api/disk/file/download-link', 'Api\BucketController@downloadLink')->name('api.disk.file-download');

    // работа с папками
    Route::post('/api/disk/folder/info', 'Api\FolderController@folderInfo')->name('api.disk.folder-info');
    Route::post('/api/disk/folder/search', 'Api\FolderController@list'); // TODO
    Route::post('/api/disk/folder/create', 'Api\FolderController@create')->name('api.disk.folder-create');
    Route::post('/api/disk/folder/change', 'Api\FolderController@list'); // TODO
    Route::post('/api/disk/folder/delete', 'Api\FolderController@delete')->name('api.disk.folder-delete');

    // заметки
    Route::get('/api/note/list', 'Api\BucketController@list'); // TODO
    Route::get('/api/note/{note}', 'Api\BucketController@list'); // TODO
    Route::get('/api/note/search', 'Api\BucketController@list'); // TODO
    Route::post('/api/note/create', 'Api\BucketController@list'); // TODO
    Route::post('/api/note/edit', 'Api\BucketController@list'); // TODO
    Route::post('/api/note/delete', 'Api\BucketController@list'); // TODO

    // группы заметок
    Route::get('/api/note/group/list', 'Api\BucketController@list'); // TODO
    Route::get('/api/note/group/{id}', 'Api\BucketController@list'); // TODO
    Route::post('/api/note/group/create', 'Api\BucketController@list'); // TODO
    Route::post('/api/note/group/edit', 'Api\BucketController@list'); // TODO
    Route::post('/api/note/group/delete', 'Api\BucketController@list'); // TODO

});

// прямой доступ к файлам
Route::group(['middleware' => 'file_provider'], function () {
	// приватный защищенный файл
    Route::get('/d/{code}', 'FileProviderController@getFile');
    Route::get('/d/{size}/{code}/', 'FileProviderController@getFileMini');
});

Route::group(['middleware' => 'cdn'], function () {
    // ссылка для всех
    Route::get('/p/{user}/{prefix}/{code}', 'FileProviderController@getPublicFile');
    Route::get('/p/{user}/{size}/{prefix}/{code}', 'FileProviderController@getPublicFileMini');
});

Route::group(['middleware' => 'admin'], function() {
   Route::get('/ad-zone/', 'Admin\MainController@index');
   Route::get('/ad-zone/queue', 'Admin\MainController@queue');
   Route::get('/ad-zone/adminer', 'Admin\MainController@adminer');
});

Route::group(['middleware' => 'adminApi'], function() {
    Route::post('/ad-api/get/user', 'Admin\AdminApiController@getUserByFolder');
    Route::post('/ad-api/get/file', 'Admin\AdminApiController@getFileInfo');
    Route::post('/ad-api/get/download', 'Admin\AdminApiController@getDownloadLink');
});

Route::group(['middleware' => 'admin'], function() {
    Route::get('/laravel/test', function (Request $request) {

//        if (\Illuminate\Support\Facades\Auth::id() != 1) {
//            abort(404);
//        }

        dd(ApiLog::where('created_at', '<', date('d.m.Y H:i:s', time() - 86400 * 3))->count());
        die;

//        $users = User::get();
        $users = UserOld::get();
        User::insert($users->toArray());
//        $tmp = new User;
//        $tmp->setConnection('mongo');
//        $tmp
//        foreach ($users as $user) {
//            $tmp = new User($users->toArray());
//            $tmp->setConnection('mongo');
////            $tmp->save();
//            dd($tmp);
//        }
        die;

//        $stream = Storage::disk('public')->readStream('/_720_2147420833.mp4');
//        Storage::disk('wasabi')->writeStream('/_720_2147420833.mp4', $stream);
//        $stream = fopen(public_path('d/test.png'), 'r+');
//        die;

//        $stream = Storage::disk('public')->readStream('3lERhxRiUQ4.jpg');
//        return response()->streamDownload(function () use ($stream) {
//            $stream;
//        }, null, ['Content-Type' => 'image/jpeg']);
//        die;
//        return Storage::disk('public')->response('/_720_2147420833.mp4', null, \request()->header());
//        return Storage::disk('public')->response('/_720_2147420833.mp4', null, \request()->header());
//        return Storage::disk('public')->response('/3lERhxRiUQ4.jpg', null, [
//            'Content-Type' => 'image/jpeg'
//        ]);

//        dd(Storage::disk('wasabi')->files('/'));
//        dd(Storage::disk('scale')->url('/3lERhxRiUQ4.jpg'));
        die(Storage::disk('wasabi')->temporaryUrl('/_720_2147420833.mp4', now()->addMinutes(5)));
//        dd(Storage::disk('scale')->getVisibility('/3lERhxRiUQ4.jpg'));

//        echo( Storage::disk('co1')->url('test.jpg') );
//        echo '<br>' . ( Storage::disk('co1')->temporaryUrl('test.jpg', now()->addMinutes(5)) );
//        echo file_get_contents(Storage::disk('co1')->temporaryUrl('test.jpg', 120));

        die;
        $data = explode("\n", shell_exec('df'));
        foreach ($data as $key => $value) {
            $info = explode(' ', preg_replace('/ {2,}/', ' ', $value));

            if (isset($info[5])  && ($info[5] == '/')) {

//                dd([
//                    $info,
//                    $info[1] * 0.85,
//                    $info[2]
//                ]);
                $usedPercent = round($info[2] / ($info[1] / 100), 2);
                if ($usedPercent > 85) {
//                    dd($info[1] / 1024 / 1024);
//                    dd($info[1]);
                    $need = $info[1] * 0.85;
                    dd($need / 1024 / 1024);
                }
//                dd($usedPercent);

            }
        }

//        file_put_contents('/mnt/HC_Volume_15430872/test.txt', 'testus');

//        $logs = ApiLog::where('token_id', '=', 3)
//            ->where('created_at', '>=', date('Y-m-d H:i:s', time() - 86400))
//            ->where('request', '=', 'api/disk/folder/delete')
//            ->orderByDesc('id')
//            ->get();
//
//        foreach ($logs as $log) {
//            $client = new Client([
//                'base_uri' => 'https://ezerbox.com',
//                'headers' => ['token' => 'a3e5eb9282e4fa6d2aa368de48174dea']]);
//
//            $client->post('api/disk/folder/delete', ['form_params' => json_decode($log->params)]);
//        }



        set_time_limit(3600 * 24);
    });
});
