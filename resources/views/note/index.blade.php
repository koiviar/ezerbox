@extends('main')

@section('style')
	{!! Css::get('/css/zone/note.css') !!}
@stop

@section('content')
	<div class="zonePreloader bg-dark-regular">
		<div>
			<i class="fa fa-cog fa-spin"></i>
			<div>{{ trans('main.loading') }}</div>
		</div>
	</div>

	<div class="main-container-padding">
		<div class="row">
			<div class="col-sm-3 br-h">
				<div class="mt-s-15 mb-3">
					<div class="content-header" style="width:calc(100% - 47px);display:inline-block;">{{ trans('note.groups') }}</div>
					<button class="btn btn-primary newGroup" data-toggle="modal" data-target="#groupModal"><i class="fas fa-plus"></i></button>

					{{-- подгрузка групп скриптом --}}
					<div class="groupListSection"></div>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="">
					<div class="row">
						<div class="col-sm-2">
							<div class="content-header">{{ trans('note.notes') }}</div>
						</div>
						<div class="col-sm-5">
							<input class="form-control search" placeholder="Поиск" type="search" style="height: 36px;">
						</div>
						<div class="col-sm-5">
							<button class="btn btn-primary newNote"><i class="fas fa-plus"></i> {{ trans('note.addNote') }}</button>
						</div>
					</div>

					{{-- подгрузка заметок скриптом --}}
					<div class="noteListSection"></div>
				</div>
			</div>
		</div>
	</div>

	{!! view('note.modals.group'); !!}

	{!! view('note.modals.note'); !!}

@stop

@section('scripts')
	{!! Js::get('/vendor/tinymce/tinymce.min.js') !!}
	{!! Js::get('/js/zone/note.js') !!}
@stop
