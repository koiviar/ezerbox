@forelse($notes as $note)
	<div class="row note" data-id="{{ $note->id }}" data-sort="{{ $note->sort }}">
		<div class="col-sm-8 col-lg-5">
			<i class="fas fa-arrows-alt"></i>&nbsp;&nbsp;
			{{ $note->title }}
		</div>
		<div class="col-sm-4" style="text-align: right">
			{{ $note->gTitle }}
		</div>
{{--		<div class="col-sm-4 text-right">--}}
{{--			{{ $note->created_at }}--}}
{{--		</div>--}}
	</div>
@empty
	<div class="row note" style="padding: 5px 15px">
		{{ trans('note.noteNotExists') }}
	</div>
@endforelse