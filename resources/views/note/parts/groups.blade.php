<div class="group notSorte" data-id="-1">
	{{ trans('main.all') }}
</div>
@foreach($groups as $group)
	<div class="group" data-id="{{ $group->id }}" data-sort="{{ $group->sort }}">
		<i class="fas fa-arrows-alt"></i>&nbsp;&nbsp;
		{{ $group->title }} 
		<span class="editGroup fl-right"><i class="fas fa-pencil-alt"></i></span>
	</div>
@endforeach