<option value="0">{{ trans('main.all') }}</option>
@foreach($groups as $group)
	<option value="{{ $group->id }}">{{ $group->title }}</option>
@endforeach