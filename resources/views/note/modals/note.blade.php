{{-- редактирование заметки --}}
<div class="modal fade" id="noteModal" data-type="create">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
	    			<h5 class="modal-title" id="exampleModalLongTitle">{{ trans('note.note') }}</h5>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	        		</button>
	      		</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<label>
							<input type="text" name="title" class="form-control" placeholder="{{ trans('note.noteTitle') }}">
						</label>
					</div>
					<div class="col-sm-6">
						<label>
							<span>{{ trans('note.group') }}:</span>
							<select class="form-control line-input noteGroup">

							</select>
						</label>
					</div>
				</div>
				<label>
					<textarea class="form-control note-content-area" contenteditable="true" placeholder="{{ trans('note.note') }}" name="content" style="height:400px"></textarea>
				</label>
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-danger deleteNote" style="position:absolute;left:10px;"><i class="fas fa-trash"></i> {{ trans('main.delete') }}</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('main.close') }}</button>
                <button type="button" class="btn btn-primary createUpdateNote"><i class="fas fa-save"></i> {{ trans('main.save') }}</button>
{{--				<span class="note-ok-status">--}}
{{--					<i class="far fa-check-circle"></i> {{ trans('note.saved') }}--}}
{{--				</span>--}}
{{--				<span class="note-fail-status" style="display:none;">--}}
{{--					<i class="far fa-clock"></i> {{ trans('note.notSaved') }}--}}
{{--				</span>--}}
			</div>
		</div>
	</div>
</div>
