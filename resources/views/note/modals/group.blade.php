{{-- добавление новой группы --}}
<div class="modal fade" id="groupModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			    <h4 class="modal-title" id="myModalLabel">{{ trans('note.modalTitleAddGroup') }}</h4>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span>
	        	</button>
			</div>
			<div class="modal-body">
				<input type="text" name="title" class="form-control" placeholder="{{ trans('note.nameGroup') }}">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success createGroup"><i class="fas fa-save"></i> {{ trans('main.create') }}</button>
			</div>
		</div>
	</div>
</div>
{{-- редактирование группы --}}
<div class="modal fade" id="editGroup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			    <h4 class="modal-title" id="myModalLabel">{{ trans('note.modalTitleEditGroup') }}</h4>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span>
	        	</button>
			</div>
			<div class="modal-body">
				<input type="text" name="title" class="form-control" placeholder="{{ trans('note.nameGroup') }}">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger deleteGroup" style="float:left"><i class="fas fa-trash"></i> {{ trans('main.delete') }}</button>
				<button type="button" class="btn btn-success saveGroup"><i class="fas fa-save"></i> {{ trans('main.save') }}</button>
			</div>
		</div>
	</div>
</div>