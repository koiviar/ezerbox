@extends('main')

@section('style')
{{--    {!! Css::get('/css/zone/nfs.css') !!}--}}
@stop

@section('content')

    <div class="white-rounded mb-4">
        <table class="table mb-5">
            <thead>
                <tr>
                    <td>Сервер</td>
                    <td>Объем</td>
                    <td>IP</td>
                    <td>Исп трафик</td>
                </tr>
            </thead>
            @foreach($servers as $server)
                <tr>
                    <td>{{ $server->name }}</td>
                    <td>{{ $server->space_used . ' GB / ' . $server->size . ' GB (' . $server->space_used_p . ' %)' }}</td>
                    <td>{{ $server->ip }}</td>
                    <td>{{ $server->bw_used_p }}%</td>
                </tr>
            @endforeach
        </table>

        <p>
            Файлов: {{ number_format($filesCount) }}
        </p>
        <p>
            Миниатюр: {{ number_format($miniaturesCount) }}
        </p>

        <table class="table mb-5">
            <thead>
            <tr>
                <td>Код</td>
                <td>Описание</td>
                <td>Заходы/Рег</td>
                <td>Изменено</td>
            </tr>
            </thead>
            @foreach($ads as $ad)
                <tr>
                    <td>{{ $ad->code }}</td>
                    <td>{{ $ad->desc }}</td>
                    <td>{{ $ad->views . ' / ' . $ad->regs }}</td>
                    <td>{{ $ad->updated_at }}</td>
                </tr>
            @endforeach
        </table>

        <table class="table">
            <thead>
                <tr>
                    <td>Логин</td>
                    <td>Email</td>
                    <td>Имя</td>
                    <td>Баланс</td>
                    <td>Папка</td>
                    <td>Активность</td>
                    <td>Регистрация</td>
                </tr>
            </thead>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->login }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->money }}</td>
                    <td>{{ $user->folder }}</td>
                    <td>{{ $user->last_activity }}</td>
                    <td>{{ $user->created_at }}</td>
                </tr>
            @endforeach
        </table>
    </div>

@stop

@section('scripts')
{{--    {!! Js::get('/js/zone/nfs.js') !!}--}}
@stop
