@extends('main')

@section('style')
	{!! Css::get('/css/zone/transactions.css') !!}
@stop

@section('content')
	<div class="main-container-padding">
		<h3>{{ trans('transactions.pageTitle') }}</h3>

		{{-- <iframe src="http://www.free-kassa.ru/merchant/forms.php?gen_form=1&m=132579&default-sum=100&button-text=Оплатить&type=v3&id=401432"  width="590" height="320" frameBorder="0" target="_parent" ></iframe> --}}

		<table class="table">
			{!! view('transactions.layouts.tableContent', compact('transactions')); !!}
		</table>
	</div>
@stop

@section('scripts')
	{!! Js::get('/js/zone/transactions.js') !!}
@stop
