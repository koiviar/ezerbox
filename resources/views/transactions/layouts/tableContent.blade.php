@forelse($transactions as $transaction)
	<tr>
		<td>{{ trans('transactions.'.$transaction->type, ['comment' => $transaction->comment]) }}</td>
		<td style="color:{{ $transaction->direct == '-' ? 'red' : 'green' }}">{{ $transaction->direct }}{{ $transaction->money }}</td>
		<td style="text-align: right;" title="{{ date('H:i d.m.Y', $transaction->created_at->timestamp) }}">{{ $transaction->created_at->diffForHumans() }}</td>
	</tr>
@empty
	<tr><td>{{ trans('transactions.transactionsNotExists') }}</td></tr>
@endforelse