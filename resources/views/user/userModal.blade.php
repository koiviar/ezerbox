{{-- модальное окно аккаунта юзера --}}
<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
    		<div class="modal-header">
    			<h5 class="modal-title" id="exampleModalLongTitle">{{ trans('user.modalTitleAccountInfo') }}</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
      			@if(Auth::user())
	      			<div class="row">
	      				<div class="col-sm-4">
	      					<img src="{{ str_replace('###', '300', Auth::user()->avatar) }}" class="avatar avatar-300">
                  <label for="avatarUploader" class="btn btn-outline-primary mb-0 uploaderLabel uploadAvatarBtn">{{ trans('bucket.upload') }}</label>
                  <i class="fa fa-trash btn btn-outline-primary deleteAvatar deleteAvatarBtn"></i>
	      				</div>
	      				<div class="col-sm-8">
	      					<div>{{ trans('user.name') }}: {{ Auth::user()->name }}</div>
                  <div>{{ trans('user.email') }}: {{ Auth::user()->email }}</div>
                  <div>
                    <a href="javascript:void(0);" class="link openChangePass">{{ trans('user.changePass') }}</a>
                    <form class="changePassFields" style="display:none;">
                      <input type="password" class="pas1 form-control" style="margin-bottom: 5px;" placeholder="{{ trans('user.pass') }}">
                      <input type="password" class="pas2 form-control" style="margin-bottom: 5px;" placeholder="{{ trans('user.pass2') }}">
                      <button class="btn btn-success savePas"><i class="fas fa-save"></i> {{ trans('user.changePass') }}</button>
                    </form>
                  </div>
	      				</div>
	      			</div>
	      		@else

	      		@endif
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('main.close') }}</button>
      		</div>
    	</div>
  	</div>
</div>
{{-- модальное окно аккаунта юзера конец --}}

{{-- форма загрузчика аватарок --}}
  <form method="POST" action="/user/avatarUpload" enctype="multipart/form-data" class="invisible avatarUploadForm" style="height: 0;width: 0;position: fixed;">
      @csrf
      <input type="file" name="file" id="avatarUploader">
      <input type="submit" name="send" value="Upload">
    </form>
  {{-- форма загрузчика аватарок конец --}}

{!! Js::get('/js/zone/user.js') !!}
