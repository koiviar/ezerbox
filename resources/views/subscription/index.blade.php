@extends('main')

@section('style')
	{!! Css::get('/css/zone/subscription.css') !!}
@stop

@section('content')
	<div class="main-container-padding">
		{{-- <h3>{{ trans('transactions.pageTitle') }}</h3> --}}
		<h2 style="text-align: center;">Тарифы</h2>

		<br>

		<?php
			$availableSpace = \App\Model\User::getAvailableSpaceHuman();

			$subs = \App\Model\DiskSubscription::where('user_id', '=', Auth::id())->orderBy('id', 'DESC')->get();
		?>

		<div class="row justify-content-center">

            <div class="col-sm-5">
                <div class="white-rounded text-center">
                    <h2>Диск по требованию</h2>
                    <p>
                        Внутренняя валюта сервиса, используетя для оплаты услуг<br>
                        Будет тратится в размере 1 единицы за 1 Gb в месяц, при первышении лимита по диску
                    </p>
                    <div class="buy-btn" data-toggle="modal" data-target="#buyCoinsModal">
                        <span class="price-monthly">
                            {{ Payment::getDiskPrice(1) . ' ' . Payment::getCurrencyHuman() }} / 1 Gb в месяц
                        </span>
                        <span class="price">
                            {{ Payment::getDiskPrice(1) . ' ' . Payment::getCurrencyHuman() }} / 1 Gb в месяц
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-sm-5">
                <div class="white-rounded text-center">
                    <h2>Фиксированный диск</h2>
                    <p>
                        Вы резервируете себе выбранное количество диска на выбранный период<br>
                        Зарезервированный диск стоит дешевле, выгоден при длитильном и объемном использовании
                    </p>
                    <div class="buy-btn"  data-toggle="modal" data-target="#buSubscriptionModal">
                        <span class="price-monthly">
                            {{ Payment::getDiskPrice(1) . ' ' . Payment::getCurrencyHuman() }} / 1 Gb в месяц
                        </span>
                        <span class="price">
                            {{ Payment::getDiskPrice(1) . ' ' . Payment::getCurrencyHuman() }} / 1 Gb в месяц
                        </span>
                    </div>
                </div>
            </div>

		</div>

		<br>
		<br>

		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="white-rounded">
					<h3 class="text-center">Доступное место: {{ $availableSpace }}</h3>

					<br>

					<div style="overflow-x: auto">
						<table class="table">
							<tbody>
								@foreach ($subs as $sub)
									<tr>
										<th>{{ trans($sub->comment) }}</th>
										<th>{{ $sub->forever ? 'навсегда' : 'до '.date('d.m.Y', strtotime($sub->expire_date)) }}</th>
										<th>{{ App\Model\File::humanSize($sub->space * 1024 * 1024 * 1024) }}</th>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>

    {{-- Модалка покупки монет --}}
    <div class="modal fade" id="buyCoinsModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Покупка EzerCoin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Покупка внутренней валюты для оплаты услуг</p>
                    <input class="form-control coin-input number" min="1" max="3000" step="1" value="25"
                        style="border-width: 0 0 1px 0;width: auto;display: inline-block"> =

                    @if (session('trans') == 'en')
                        <span class="coin-price-en">{{ Payment::getDiskPrice(25) }}</span> {{ Payment::getCurrencyHuman() }}
                        (<span class="coin-price-ru">{{ Payment::getDiskPrice(25, Payment::CURRENCY_RUB) }}</span> {{ Payment::getCurrencyHuman(Payment::CURRENCY_RUB) }})
                    @else
                        <span class="coin-price-ru">{{ Payment::getDiskPrice(25) }}</span> {{ Payment::getCurrencyHuman() }}
                    @endif

                    <hr>
                    <h6>Оплата</h6>
                    <button class="btn btn-success buy-button"
                            style="margin-bottom: 15px;"
                            data-operator="yandex"
                            data-operator-type="card"
                            data-type="coin">
                        Яндекс деньги (Банковская карта)</button>
                    <button class="btn btn-success buy-button"
                            data-operator="yandex"
                            data-operator-type="pocket"
                            data-type="coin">
                        Яндекс деньги (Кошелек)</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('main.close') }}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Модалка покупки монет конец --}}

    {{-- Модалка покупки подписки --}}
    <div class="modal fade" id="buSubscriptionModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Покупка расширения диска</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label>Продолжительность</label>
                    <br>
                    <button class="btn btn-outline-primary btn-period" data-period="1">1 мес</button>
                    <button class="btn btn-outline-primary btn-period btn-primary active" data-period="3">3 мес</button>
                    <button class="btn btn-outline-primary btn-period" data-period="6">6 мес</button>
                    <button class="btn btn-outline-primary btn-period" data-period="12">12 мес</button>

                    <br>
                    <hr>
                    <label>Объем</label>
                    <br>
                    <button class="btn btn-outline-primary btn-space" data-space="5">5 GB</button>
                    <button class="btn btn-outline-primary btn-space btn-primary active" data-space="25">25 GB</button>
                    <button class="btn btn-outline-primary btn-space" data-space="100">100 GB</button>
                    <button class="btn btn-outline-primary btn-space" data-space="250">250 GB</button>

                    <hr>
                    <span>Цена: </span>
                    @if (session('trans') == 'en')
                        <span class="subscription-price-en">{{ Payment::getDiskPrice(3, 25) }}</span> {{ Payment::getCurrencyHuman() }}
                        (<span class="subscription-price-ru">{{ Payment::getDiskPrice(3, 25, Payment::CURRENCY_RUB) }}</span> {{ Payment::getCurrencyHuman(Payment::CURRENCY_RUB) }})
                    @else
                        <span class="subscription-price-ru">{{ Payment::getDiskPrice(3, 25) }}</span> {{ Payment::getCurrencyHuman() }}
                    @endif

                    <hr>
                    <h6>Оплата</h6>
                    <button class="btn btn-success buy-button"
                            style="margin-bottom: 15px;"
                            data-operator="yandex"
                            data-operator-type="card"
                            data-type="subscription">
                        Яндекс деньги (Банковская карта)</button>
                    <button class="btn btn-success buy-button"
                            data-operator="yandex"
                            data-operator-type="pocket"
                            data-type="subscription">
                        Яндекс деньги (Кошелек)</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('main.close') }}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Модалка покупки подписки конец --}}
@stop

@section('scripts')
	 {!! Js::get('/js/zone/subscription.js') !!}
@stop
