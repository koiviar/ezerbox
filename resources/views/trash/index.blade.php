@extends('main')

@section('style')
    {!! Css::get('/css/zone/trash.css') !!}
@endsection

@section('content')
    <div class="zonePreloader bg-dark-regular">
        <div>
            <i class="fa fa-cog fa-spin"></i>
            <div>{{ trans('main.loading') }}</div>
        </div>
    </div>

    <div class="main-container-padding white-rounded">

        {{-- кнопки управления --}}
        <div class="files-controll">
            {{-- файл не выбран --}}
{{--            <div class="files-not-selected">--}}
{{--                <label for="uploader" class="btn btn-outline-primary mb-0 uploaderLabel uploader-button">--}}
{{--                    <i class="fas fa-upload"></i> {{ trans('bucket.upload') }}</label>--}}
{{--                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#createDirModal">--}}
{{--                    <i class="fas fa-plus"></i> <a class="hideTextBtn">{{ trans('bucket.creteDir') }}</a></button>--}}
{{--            </div>--}}
{{--            --}}{{-- файл выбран --}}
{{--            <div class="files-selected" style="display:none">--}}
{{--                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#moveEntModal">--}}
{{--                    <i class="fas fa-arrows-alt"></i> <a class="hideTextBtn">{{ trans('bucket.move') }}</a></button>--}}
{{--                <button type="button" class="btn btn-outline-primary renameEnt">--}}
{{--                    <i class="fas fa-pen"></i> <a class="hideTextBtn">{{ trans('bucket.rename') }}</a></button>--}}
{{--                <button type="button" class="btn btn-outline-primary deleteSelected">--}}
{{--                    <i class="fas fa-trash"></i> <a class="hideTextBtn">{{ trans('main.delete') }}</a></button>--}}
{{--                <button type="button" class="btn btn-outline-primary unselect-files">--}}
{{--                    <i class="fas fa-times"></i></button>--}}
{{--            </div>--}}

            <button type="button" class="btn btn-outline-primary deleteSelected">
                <i class="fas fa-trash"></i> <a class="hideTextBtn">{{ trans('bucket.delete') }}</a></button>
            <button type="button" class="btn btn-outline-primary restoreSelected">
                <i class="fas fa-trash-restore"></i> <a class="hideTextBtn">{{ trans('bucket.restore') }}</a></button>
            <button type="button" class="btn btn-outline-secondary deleteAll fl-right" style="margin-right: 0;">
                <i class="fas fa-dumpster"></i> <a class="hideTextBtn">{{ trans('bucket.deleteAll') }}</a></button>

            {{-- навигация по папкам --}}
            <nav aria-label="breadcrumb" class="nav-breadcrumb">
                <ol class="breadcrumb dirNavigation"></ol>
            </nav>
        </div>

        {{-- таблица с файлами --}}
        <div class="overflow-auto">
            <table class="table">
                <thead>
                <th style="width:20px;">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input select-all" id="cc-dir-file-all">
                        <label class="custom-control-label" for="cc-dir-file-all"></label>
                    </div>
                </th>
                <th>{{ trans('bucket.name') }}</th>
                <th>{{ trans('bucket.size') }}</th>
                <th>{{ trans('bucket.type') }}</th>
                <th>{{ trans('bucket.deletedTime') }}</th>
                <th></th>
                </thead>
                <tbody class="files"></tbody>
            </table>
        </div>
{{--        <div class="files row"></div>--}}

        <div class="dirInfoBar bg-dark-regular">
            {{ trans('bucket.folders') }}: <span class="foldersB">0</span> /
            {{ trans('bucket.files') }}: <span class="filesB">0</span> /
            {{ trans('bucket.size') }}: <span class="sizeB">0</span>
        </div>

    </div>

@endsection

@section('scripts')
    {!! Js::get('/js/zone/trash.js') !!}
@endsection
