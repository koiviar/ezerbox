@foreach($files as $file)
	<tr data-id="{{ $file->id }}" data-type="file" class="element-item-parent">
		<td>
			<div class="custom-control custom-checkbox">
			 	<input type="checkbox" class="custom-control-input fileOperationChecbox" id="cc-file-{{ $file->id }}">
			 	<label class="custom-control-label" for="cc-file-{{ $file->id }}"></label>
			</div>
		</td>
		<td data-id="{{ $file->id }}" data-type="file" class="element-item element-file">
			<span class="entName getFileInfo">
				@if($file->ex_type == 'image')
					<?php $srcPrefix = $_SERVER['SERVER_NAME'] == 'ezerbox.com' ? '//s.ezerbox.com/d/' : '/d/'; ?>
					<i class="far fa-image typeIcon"></i>
{{--					<img src="{{ $srcPrefix.'24x24/'.$file->code }}">--}}
				@else
					<i class="far fa-file typeIcon"></i>
				@endif

				<span class="name">{{ $file->name }}</span>
			</span>
		</td>
		<td>{{ $file->size }}</td>
		<td>{{ $file->ex_typeH }}</td>
		<td>{{ \Carbon\Carbon::now()->diffForHumans(date('Y-m-d H:i:s', strtotime($file->deleted_time) + 86400 * 7)) }} удаления</td>
		<td>
			<div class="float-right">
				<i class="fas fa-trash-restore restoreFile" data-id="{{ $file->id }}"></i>&nbsp;&nbsp;
				<i class="fas fa-trash deleteFile" data-id="{{ $file->id }}"></i>
			</div>
		</td>
	</tr>
@endforeach
