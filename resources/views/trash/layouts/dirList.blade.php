@foreach($dirs as $dir)
	<tr data-id="{{ $dir->id }}" data-type="dir" class="element-item-parent">
		<td>
			<div class="custom-control custom-checkbox">
				<input type="checkbox" class="custom-control-input fileOperationChecbox" id="cc-file-{{ $dir->id }}">
				<label class="custom-control-label" for="cc-file-{{ $dir->id }}"></label>
			</div>
		</td>
		<td data-id="{{ $dir->id }}" data-type="dir" class="element-item">
			<span class="entName getFileInfo"><i class="far fa-folder typeIcon"></i> <span class="name">{{ $dir->name }}</span></span>
		</td>
		<td></td>
		<td></td>
{{--		<td>{{ strtotime($dir->deleted_time) + 86400 * 7 }}</td>--}}
		<td>{{ \Carbon\Carbon::now()->diffForHumans(date('Y-m-d H:i:s', strtotime($dir->deleted_time) + 86400 * 7)) }} удаления</td>
		<td>
			<div class="float-right">
				<i class="fas fa-trash-restore restoreFile" data-id="{{ $dir->id }}"></i>&nbsp;&nbsp;
				<i class="fas fa-trash deleteFile" data-id="{{ $dir->id }}"></i>
			</div>
		</td>
	</tr>
@endforeach
