@extends('main')

@section('style')
    {!! Css::get('/css/zone/plans.css') !!}
@stop

@section('content')

    <div class="plans-page">
        <h2 class="text-center mb-4">{{ trans('plans.plans') }}</h2>
        <div class="row plans">
            @foreach($plans as $title => $plan)
                <div class="col plan {{ $currentPlan['plan'] == $title ? 'active' : '' }}" data-plan="{{ $title }}">
                    <div class="title">{{ $title }}</div>
                    <div class="price">€{{ $plan['total'] }}<span class="sub-info"> / {{ trans('plans.per_month') }}</span></div>
                    <div class="price-per-gb">€{{ $plan['per_gb'] }}<span class="sub-info"> / {{ trans('plans.month_gb') }}</span></div>
                    <div class="space mt-4"><i class="fas fa-hdd"></i> {{ $plan['v_space'] }}</div>
                </div>
            @endforeach
        </div>

        <h3 class="text-center mt-5">{{ trans('plans.current_plan') . ': ' . strtoupper($currentPlan['plan']) }}</h3>

        <div class="used-info text-center mt-4">
            <?php $diskInfo = \App\Model\User::getUsedSpaceInfo(); ?>
            <div class="text">
                <span class="usedSpace">{{ strtoupper($diskInfo['used']) }}</span>
                /
                <span class="spaceAvailable">{{ strtoupper($diskInfo['spaceAvailable']) }}</span>
            </div>

            <div class="progress m-b-30" style="height: 20px;">
                <div class="progress-bar" role="progressbar" style="width: {{ $diskInfo['percentUsed'] }}%;"
                     aria-valuenow="{{ $diskInfo['percentUsed'] }}" aria-valuemin="0" aria-valuemax="100">{{ $diskInfo['percentUsed'] }}%</div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        $('.plan').click(function () {
            let plan = $(this).data('plan');
            Swal.fire({
                title: 'Change plan to ' + plan + '?',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!',
                cancelButtonText: lang.get('cancel')
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'POST',
                        url:'/plans/change',
                        data: {
                            plan: plan
                        },
                        dataType:'json',
                        success: function(data) {
                            if (data.error === 0) {
                                location.reload();
                            } else if (data.error === 1) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Plan changing error',
                                    showConfirmButton: true
                                });
                            } else if (data.error === 2) {
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Not enough money!',
                                    html: 'To change plan you need money for 1 month<br><b>Please fill up balance on €' + data.data.to_add + ' or more</b>',
                                    showConfirmButton: true
                                });
                            }
                            // Swal.fire({
                            //     icon: 'success',
                            //     title: 'Your NFS has been deleted',
                            //     showConfirmButton: true,
                            //     timer: 2500
                            // });

                        }
                    });
                }
            });
        });
    </script>
    {{--    {!! Js::get('/js/zone/nfs.js') !!}--}}
@stop
