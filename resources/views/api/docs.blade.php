@extends('main')

@section('style')
    {!! Css::get('/css/zone/api.css') !!}

    <style>
        a.anchor {
            display: block;
            position: relative;
            top: -200px;
            visibility: hidden;
        }
        .method-block {
            position: relative;
        }
        .api .anchor {
            position: absolute;
            top: -100px;
        }
        .method-block .url {
            border: 0;
            border-radius: 0.25rem 0.25rem 0 0;
        }
        .nav-block {
            position: fixed;
            top: 100px;
            overflow-y: auto;
            max-height: calc(100vh - 100px);
        }
        @media (max-width: 767px) {
            .nav-block {
                position: inherit;
                top: 0;
                max-height: inherit;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-container-padding white-rounded api">

        <div class="tab-docs">
            <div class="row">
                <div class="col-md-3 mb-3">
                    <nav class="nav-block navbar pt-0" id="navbar-api">
                        <nav class="nav nav-pills flex-column">
                        @foreach($docs as $blockTitle => $block)
                            <h3 class="nav-link" href="#{{ $blockTitle }}">{{ trans('api_docs.' . $blockTitle) }}</h3>
                            @foreach($block as $titleSubBlock => $subBlock)
                                <a class="nav-link" href="#{{ $titleSubBlock }}"><h5>{{ trans('api_docs.' . $titleSubBlock) }}</h5></a>
                                    <nav class="nav nav-pills flex-column">
                                        @foreach($subBlock as $link => $method)
                                            <a class="nav-link" href="#{{ $link }}">{{ $link }}</a>
                                        @endforeach
                                    </nav>
                            @endforeach
                        @endforeach
                        </nav>
                    </nav>
                </div>
                <div class="col-md-9">
                    <div data-spy="scroll" data-target="#navbar-api" data-offset="0">
                        @foreach($docs as $blockTitle => $block)
                            <div class="position-relative">
                                <div class="anchor" id="{{ $blockTitle }}"></div>
                                <h2 class="mb-3">{{ trans('api_docs.' . $blockTitle) }}</h2>
                            </div>
                            @foreach($block as $titleSubBlock => $subBlock)
                                <div class="position-relative">
                                    <div class="anchor" id="{{ $titleSubBlock }}"></div>
                                    <h3 class="mt-4">{{ trans('api_docs.' . $titleSubBlock) }}</h3>
                                </div>
                                @foreach($subBlock as $link => $method)
                                    <div class="method-block card mb-3">
                                        <div class="anchor" id="{{ $link }}"></div>
                                        <div class="alert alert-primary m-0 url"><b>{{ $method['method'] . ' ' . $link }}</b></div>
                                        @if ($method['params'])
                                            <div class="params p-3">
                                                <h5 class="mb-3">{{ trans('api_docs.params') }}</h5>
                                                @foreach($method['params'] as $key => $param)
                                                    <div>{{ $key . ' = ' . $param }}</div>
                                                @endforeach
                                            </div>
                                        @endif
                                        <div class="desc pl-3 pr-3 pb-3">
                                            {{ $method['desc'] }}
                                        </div>
                                        <pre style="margin-bottom: -42px;">
                                            <code class="language-php" style="padding: 0 15px">
$client = new \GuzzleHttp\Client();
$response = $client->post('https://api.ezerbox.com{{ $link }}', [
    'headers' => [
        'token' => 'test'
    ],
@if ($method['params'])
    'form_params' => [
@foreach($method['params'] as $key => $param)
        '{{ $key }}' => '{{ $param }}',
@endforeach
    ]
@endif
]);

// {{ trans('api_docs.response') }}
[
@foreach($method['response'] as $key => $value)
@if (is_array($value) && empty($value))
    '{{ $key }}' => []
@elseif (is_array($value))
    '{{ $key }}' => [
@foreach($value as $inKey => $inValue)
@if (is_array($inValue) && empty($inValue))
        '{{ $inKey }}' => []
@elseif (is_array($inValue))
        '{{ $inKey }}' => [
@foreach($inValue as $in2Key => $in2Value)
            '{{ $in2Key }}' => '{{ $in2Value }}',
@endforeach
        ]
@else
        '{{ $inKey }}' => '{{ $inValue }}',
@endif
@endforeach
    ]
@else
    '{{ $key }}' => '{{ $value }}',
@endif
@endforeach
]
                                            </code>
                                        </pre>
                                    </div>
                                @endforeach
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    {!! Js::get('/js/zone/api.js') !!}

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.9/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.9/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
@endsection
