@extends('main')

@section('style')
    {!! Css::get('/css/zone/api.css') !!}
@endsection

@section('content')
    <div class="main-container-padding white-rounded">

        <a class="link mb-3" style="display: block" href="{{ route('apiManage.docs') }}">{{ trans('api.docs') }}</a>

        <div class="tab-keys">
            <h3>
                {{ trans('api.tokens') }}
                <button class="btn btn-primary float-right mr-0 createToken">{{ trans('api.create_token') }}</button>
            </h3>
            <table class="table">
                <thead>
                <th>{{ trans('api.token') }}</th>
                <th>{{ trans('api.desc') }}</th>
                <th class="text-right">{{ trans('api.actions') }}</th>
                </thead>
                <tbody>
                @foreach($tokens as $token)
                    <tr>
                        <td>{{ $token->token }}</td>
                        <td>{{ $token->desc }}</td>
                        <td class="text-right"><i class="fa fa-trash cursor-pointer deleteToken" data-id="{{ $token->id }}"></i></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <h3 class="mt-5">
                {{ trans('api.logs') }}
                <span class="float-right">{{ trans('api.request_count') . ': ' . number_format($requestCount, 0, '.', ' ') }}</span>
            </h3>
            <table class="table">
                <thead>
                <th>{{ trans('api.token') }}</th>
                <th>{{ trans('api.request') }}</th>
                <th>{{ trans('api.params') }}</th>
                <th class="text-right">{{ trans('api.time') }}</th>
                </thead>
                <tbody>
                @foreach($logs as $log)
                    <tr>
                        <td>{{ $log->getToken() }}</td>
                        <td>{{ '/' . $log->request }}</td>
                        <td>
                            @foreach(json_decode($log->params) as $key => $param)
                                {{ $key . ' = ' . $param }} <br>
                            @endforeach
                        </td>
                        <td class="text-right">{{ $log->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection

@section('scripts')
    {!! Js::get('/js/zone/api.js') !!}
@endsection
