<div class="modal fade show" id="balanceModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ trans('main.balance') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center money mb-3">
                    <i class="fas fa-euro-sign"></i> {{ $money }}
                </div>
                <div class="costs">
                    <table class="table">
                        <tr>
                            <td>Cloud storage</td>
                            <td>- €{{ $cloudPrice }}</td>
                        </tr>
                        @foreach($nfs as $item)
                            <tr>
                                <td>NFS storage ({{ $item->server->name }})</td>
                                <td>- €{{ $item->getPrice() }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>Итого в месяц</td>
                            <td>- €{{ $sum }}</td>
                        </tr>
                    </table>
                </div>
                <div class="fill-up text-center">
                    <input class="form-control eur-money" value="{{ $preFill }}" type="number"><i class="fas fa-euro-sign"></i>
                    <span class="rub-money-block">(<span class="rub-money">{{ Payment::getEurPrice($preFill) }}</span> Rub)</span>
                    <button class="btn btn-primary w-100 mt-2 checkout">Пополнить</button>

{{--------------------------------------------------------------------------------------------------------------}}

{{--
{{--                    <div id="smart-button-container">--}}
{{--                        <div style="text-align: center"><label for="description"> </label><input type="text" name="descriptionInput" id="description" maxlength="127" value=""></div>--}}
{{--                        <p id="descriptionError" style="visibility: hidden; color:red; text-align: center;">Please enter a description</p>--}}
{{--                        <div style="text-align: center"><label for="amount"> </label><input name="amountInput" type="number" id="amount" value="" ><span> EUR</span></div>--}}
{{--                        <p id="priceLabelError" style="visibility: hidden; color:red; text-align: center;">Please enter a price</p>--}}
{{--                        <div id="invoiceidDiv" style="text-align: center; display: none;"><label for="invoiceid"> </label><input name="invoiceid" maxlength="127" type="text" id="invoiceid" value="" ></div>--}}
{{--                        <p id="invoiceidError" style="visibility: hidden; color:red; text-align: center;">Please enter an Invoice ID</p>--}}
{{--                        <div style="text-align: center; margin-top: 0.625rem;" id="paypal-button-container"></div>--}}
{{--                    </div>--}}
{{--                    <script--}}
{{--                            src="https://www.paypal.com/sdk/js?client-id=YOUR_CLIENT_ID">--}}
{{--                    <script src="https://www.paypal.com/sdk/js?client-id=sb&currency=EUR" data-sdk-integration-source="button-factory"></script>--}}
{{--                    <script>--}}
{{--                        function initPayPalButton() {--}}
{{--                            var description = document.querySelector('#smart-button-container #description');--}}
{{--                            var amount = document.querySelector('#smart-button-container #amount');--}}
{{--                            var descriptionError = document.querySelector('#smart-button-container #descriptionError');--}}
{{--                            var priceError = document.querySelector('#smart-button-container #priceLabelError');--}}
{{--                            var invoiceid = document.querySelector('#smart-button-container #invoiceid');--}}
{{--                            var invoiceidError = document.querySelector('#smart-button-container #invoiceidError');--}}
{{--                            var invoiceidDiv = document.querySelector('#smart-button-container #invoiceidDiv');--}}

{{--                            var elArr = [description, amount];--}}

{{--                            if (invoiceidDiv.firstChild.innerHTML.length > 1) {--}}
{{--                                invoiceidDiv.style.display = "block";--}}
{{--                            }--}}

{{--                            var purchase_units = [];--}}
{{--                            purchase_units[0] = {};--}}
{{--                            purchase_units[0].amount = {};--}}

{{--                            function validate(event) {--}}
{{--                                return event.value.length > 0;--}}
{{--                            }--}}

{{--                            paypal.Buttons({--}}
{{--                                style: {--}}
{{--                                    color: 'blue',--}}
{{--                                    shape: 'rect',--}}
{{--                                    label: 'paypal',--}}
{{--                                    layout: 'vertical',--}}

{{--                                },--}}

{{--                                onInit: function (data, actions) {--}}
{{--                                    actions.disable();--}}

{{--                                    if(invoiceidDiv.style.display === "block") {--}}
{{--                                        elArr.push(invoiceid);--}}
{{--                                    }--}}

{{--                                    elArr.forEach(function (item) {--}}
{{--                                        item.addEventListener('keyup', function (event) {--}}
{{--                                            var result = elArr.every(validate);--}}
{{--                                            if (result) {--}}
{{--                                                actions.enable();--}}
{{--                                            } else {--}}
{{--                                                actions.disable();--}}
{{--                                            }--}}
{{--                                        });--}}
{{--                                    });--}}
{{--                                },--}}

{{--                                onClick: function () {--}}
{{--                                    if (description.value.length < 1) {--}}
{{--                                        descriptionError.style.visibility = "visible";--}}
{{--                                    } else {--}}
{{--                                        descriptionError.style.visibility = "hidden";--}}
{{--                                    }--}}

{{--                                    if (amount.value.length < 1) {--}}
{{--                                        priceError.style.visibility = "visible";--}}
{{--                                    } else {--}}
{{--                                        priceError.style.visibility = "hidden";--}}
{{--                                    }--}}

{{--                                    if (invoiceid.value.length < 1 && invoiceidDiv.style.display === "block") {--}}
{{--                                        invoiceidError.style.visibility = "visible";--}}
{{--                                    } else {--}}
{{--                                        invoiceidError.style.visibility = "hidden";--}}
{{--                                    }--}}

{{--                                    purchase_units[0].description = description.value;--}}
{{--                                    purchase_units[0].amount.value = amount.value;--}}

{{--                                    if(invoiceid.value !== '') {--}}
{{--                                        purchase_units[0].invoice_id = invoiceid.value;--}}
{{--                                    }--}}
{{--                                },--}}

{{--                                createOrder: function (data, actions) {--}}
{{--                                    return actions.order.create({--}}
{{--                                        purchase_units: purchase_units,--}}
{{--                                    });--}}
{{--                                },--}}

{{--                                onApprove: function (data, actions) {--}}
{{--                                    return actions.order.capture().then(function (orderData) {--}}

{{--                                        // Full available details--}}
{{--                                        console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));--}}

{{--                                        // Show a success message within this page, e.g.--}}
{{--                                        const element = document.getElementById('paypal-button-container');--}}
{{--                                        element.innerHTML = '';--}}
{{--                                        element.innerHTML = '<h3>Thank you for your payment!</h3>';--}}

{{--                                        // Or go to another URL:  actions.redirect('thank_you.html');--}}

{{--                                    });--}}
{{--                                },--}}

{{--                                onError: function (err) {--}}
{{--                                    console.log(err);--}}
{{--                                }--}}
{{--                            }).render('#paypal-button-container');--}}
{{--                        }--}}
{{--                        initPayPalButton();--}}
{{--                    </script>--}}
                </div>
            </div>
        </div>
    </div>
</div>
