<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>EzerBox - Live</title>

    {!! Css::get('https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css') !!}
    {!! Css::get('/css/main.css') !!}
    {!! Css::get('https://use.fontawesome.com/releases/v5.15.1/css/all.css') !!}
    {{-- <link rel="shortcut icon" href="/public/img/logoWeb.png" type="image/png"> --}}

{{--    <meta name="viewport" content="width=420">--}}
{{--     <meta name="viewport" content="width=device-width, height=device-height,  initial-scale=1.0, user-scalable=no;user-scalable=0;"/>--}}
     <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0"/>

    @yield('style')

    {{-- стили для юзера --}}
    {!! Css::get('/css/zone/user.css') !!}

</head>
<body class="{{ Cookie::get('sideBarHide') }} {{ !Auth::user() ? 'AuthHide' : '' }}">
<nav class="navbar header">
    @if(Auth::user())
        <div class="form-inline">
            <span class="sideBarMenuActivator"><i class="fas fa-bars"></i></span>
            {{-- <a class="navbar-brand" href="/">EzerBox</a> --}}
        </div>
    @endif

    {{-- то что справа в хеадере --}}
    @if(Auth::user())

        <div class="form-inline">
            {{-- <a class="userCashField" href="/transactions">
                <span>{{ trans('user.cash') }}:</span>
                <span class="userCash userCashStyle">{{ Auth::user()->money }}</span>
                <span class="userCash userCashStyle">EzerCoin</span>
            </a> --}}

            <button class="btn btn-default openUserInfo" data-toggle="modal" data-target="#userProfileModal">
                <img src="{{ Auth::user()->getAvatar('100') }}" class="avatar-100">
                <span class="name">{{ Auth::user()->name }}</span>
            </button>
            <a class="header-logout" href="javascript:void(0);"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit()">{{ trans('main.logOut') }}</a>
        </div>

        {{-- для выхода --}}
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
    @else

        <div class="form-inline">
            <div class="logo">
                <a class="" href="/">EzerBox</a>
            </div>
        </div>
        <div class="form-inline">
            <a href="/login" class="header-login">{{ trans('main.login') }}</a>
        </div>
    @endif


</nav>

@if(Auth::user())
    <div class="list-group side-bar">

        <div class="logo">
            <a class="" href="/">EzerBox</a>
        </div>

        <ul class="navigation">
{{--            <li class="{{ $zone == 'dashboard' ? 'active' : '' }}">--}}
{{--                <a href="/dash">--}}
{{--                    <i class="fas fa-chart-line"></i>{{ trans('main.tabDashboard') }}</a>--}}
{{--            </li>--}}
            <li class="{{ $zone == 'bucket' ? 'active' : '' }}">
                <a href="/disk">
                    <i class="fas fa-cloud"></i>{{ trans('main.tabBucket') }}</a>
            </li>
            <li class="{{ $zone == 'api' ? 'active' : '' }}">
                <a href="/api/manage">
                    <i class="fas fa-code"></i>{{ trans('main.tabApi') }}</a>
            </li>
{{--            <li class="{{ $zone == 'nfs' ? 'active' : '' }}">--}}
{{--                <a href="/nfs">--}}
{{--                    <i class="far fa-hdd"></i>{{ trans('main.tabNFS') }}</a>--}}
{{--            </li>--}}
            <li class="{{ $zone == 'note' ? 'active' : '' }}">
                <a href="/note">
                    <i class="far fa-clipboard"></i>{{ trans('main.tabNote') }}</a>
            </li>
            {{-- <li class="{{ $zone == 'publicFile' ? 'active' : '' }}">
                <a href="/publicFile">
                    <i class="fas fa-copy"></i>{{ trans('main.tabPublicFiles') }}</a>
            </li> --}}
{{--            <li class="{{ $zone == 'subscription' ? 'active' : '' }}">--}}
{{--                <a href="/subscription">--}}
{{--                    <i class="fas fa-cubes"></i>{{ trans('main.tabMoreSpace') }}</a>--}}
{{--            </li>--}}
            <li class="{{ $zone == 'trash' ? 'active' : '' }}">
                <a href="/trash">
                    <i class="fas fa-trash"></i>{{ trans('main.tabTrash') }}</a>
            </li>
        </ul>


        {{-- User::getUsedSpaceInfo() --}}

        @if (Auth::user())


                <?php $diskInfo = \App\Model\User::getUsedSpaceInfo(); ?>
                <div class="float-right moreSpace">
                    <a href="javascript:void(0)" class="showBalanceModal">
                        <div class="user-money" title="{{ trans('t.money') }}">
                            <i class="fas fa-euro-sign"></i>
                            {{ Auth::user()->getMoney() }}
                        </div>
                    </a>

                    <a href="{{ route('plans') }}">
                        <div class="text-info">
                            <span class="usedSpace">{{ $diskInfo['used'] }}</span>
                            /
                            <span class="spaceAvailable">{{ $diskInfo['spaceAvailable'] }}</span>
                        </div>

                        <div class="progress m-b-30" style="height: 10px;">
                            <div class="progress-bar" role="progressbar" style="width: {{ $diskInfo['percentUsed'] }}%;"
                                 aria-valuenow="{{ $diskInfo['percentUsed'] }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </a>
                </div>

        @endif


        <div class="changeLang">
            <div class="elem {{ App::getLocale() == 'ru' ? 'activeLang' : '' }}" data-val="ru">Язык - Русский</div>
            <div class="elem {{ App::getLocale() == 'en' ? 'activeLang' : '' }}" data-val="en">Language - English</div>
        </div>
    </div>
@endif

<div class="main-container">

    @yield('content')

</div>



{!! Js::get('https://code.jquery.com/jquery-3.3.1.min.js') !!}
{!! Js::get('/vendor/js/jquery-ui.min.js') !!}
{!! Js::get('/vendor/sweet-alert-2/sa.js') !!}
{{-- https://github.com/js-cookie/js-cookie --}}
{!! Js::get('https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js') !!}
{!! Js::get('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js') !!}
{!! Js::get('https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js') !!}

{!! JsLang::getJsLang() !!}

{!! Js::get('/js/main.js') !!}
{!! Js::get('/js/zone/order.js') !!}

@yield('scripts')

{!! view('user.userModal') !!}


{{-- счетчик для просмотра статистики --}}
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(53038876, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/53038876" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
