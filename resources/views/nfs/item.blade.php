<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4 mb-3 nfs" data-id="{{ $nfs->id }}">
    <div class="card m-0">
        <div class="card-header">{{ $nfs->server->name . ' ' . $nfs->label }} <i class="fa fa-trash-alt fl-right delete-nfs"></i></div>
        <div class="card-body pt-0">
            <div class="box-control">
                <button class="btn connect"><i class="fas fa-link"></i> Подключиться</button>
                <button class="btn reload mr-0"><i class="fas fa-sync-alt"></i> Перезапустить</button>
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">Status <span class="fl-right status">{{ $nfs->server->process ? $nfs->server->process : $nfs->server->status }}</span></li>
                <li class="list-group-item">IP <span class="fl-right">{{ $nfs->server->ip }}</span></li>
                <li class="list-group-item">Host <span class="fl-right">{{ mb_strtolower($nfs->server->name) . '.ezerbox.com' }}</span></li>
            </ul>
        </div>
        <div class="card-body pt-0">
            <p>Ресурсы</p>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Disk <span class="fl-right">{{ $nfs->server->space_used . 'GB / ' . $nfs->server->size }}GB</span></li>
{{--                <li class="list-group-item">Bw <span class="fl-right">{{ $nfs->server->bw_used_p }}%</span></li>--}}
            </ul>
        </div>
    </div>
</div>
