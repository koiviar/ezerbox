@extends('main')

@section('style')
    {!! Css::get('/css/zone/nfs.css') !!}
@stop

@section('content')

    <div class="zonePreloader bg-dark-regular">
        <div>
            <i class="fa fa-cog fa-spin"></i>
            <div>{{ trans('main.loading') }}</div>
        </div>
    </div>

    <div class="white-rounded mb-4">
        <div class="input-group" style="max-width: 300px;">
            <select class="custom-select border-primary" id="nfs-size">
                @foreach(\App\Model\Server::STORAGE_SIZES as $size)
                    <option value="{{ $size }}">{{ $size }} GB - {{ \App\Helpers\Payment::NFS_PRICE[$size] }} €</option>
                @endforeach
            </select>
            <div class="input-group-append">
                <button class="btn btn-outline-primary add-nfs"><i class="fa fa-plus"></i> Добавить</button>
            </div>
        </div>
    </div>

    <div class="nfs-list row"></div>

    <div class="modal fade show" id="connect-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Подключение к диску</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="info">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">IP <span class="fl-right ip"></span></li>
                            <li class="list-group-item">Host <span class="fl-right host"></span></li>
                            <li class="list-group-item">User <span class="fl-right">root</span></li>
                            <li class="list-group-item">Password <span class="fl-right pwd"></span></li>
                        </ul>
                    </div>
                    <div class="p-2 mt-3">
                        <h4 class="mb-4">Способы подключения</h4>
                        <h5>Windows</h5>
                        <p class="windows-connect">
                            Подключение внешнего сетевого диска для Windows
                            Используйте стандартное приложение проводник и нажав на иконку вашего компьютера правой кнопкой,
                            выберите пункт добавить сетевой диск<br>
                            Путь к диску: \\{ip или host}\storage
                        </p>
                        <h5>Linux</h5>
                        <p class="linux-connect">
                            Подключение внешнего сетевого диска для Linux
                        </p>
                        <h5>FTP</h5>
                        <p class="ftp-connect">
                            Подключайтесь через удобный вам FTP-Менеджер или любым иным удобным вам способом
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    {!! Js::get('/js/zone/nfs.js') !!}
@stop
