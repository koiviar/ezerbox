<li class="breadcrumb-item"><span class="dirNav link" data-id="0">{{ trans('bucket.bucket') }}</span></li>
@foreach($result as $item)
	<li class="breadcrumb-item"><span class="dirNav link" data-id="{{ $item['id'] }}">{{ $item['name'] }}</span></li>
@endforeach