@foreach($dirs as $dir)
	<div class="col pr-0 element-item-parent">
		<div class="element-item element-dir" data-id="{{ $dir->id }}" data-type="dir">
			<div class="image">
				<i class="far fa-folder-open"></i>
			</div>

			<span class="name">{{ $dir->name }}</span>
		</div>
	</div>
@endforeach
