{{-- @foreach($files as $file)
	<tr data-id="{{ $file->id }}" data-type="file">
		<td>
			<div class="custom-control custom-checkbox">
			 	<input type="checkbox" class="custom-control-input fileOperationChecbox" id="cc-file-{{ $file->id }}">
			 	<label class="custom-control-label" for="cc-file-{{ $file->id }}"></label>
			</div>
		</td>
		<td><span class="entName getFileInfo link-dc"><i class="far fa-file typeIcon"></i> <span class="name">{{ $file->name }}</span></span></td>
		<td>{{ $file->size }}</td>
		<td>{{ $file->ex_typeH }}</td>
		<td>{{ $file->created_atH }}</td>
		<td>
			<div class="float-right">
				<i class="fas fa-pencil-alt renameEnt" data-id="{{ $file->id }}"></i>
				<i class="fas fa-trash deleteFile" data-id="{{ $file->id }}"></i>
			</div>
		</td>
	</tr>
@endforeach
 --}}
@foreach($files as $file)
	<div class="col pr-0 element-item-parent">
		<div class="element-item element-file" data-id="{{ $file->id }}" data-code="{{ $file->code }}" data-type="file">
			<div class="image">
				@if($file->ex_type == 'image')
					<?php $srcPrefix = $_SERVER['SERVER_NAME'] == 'ezerbox.com' ? '//ezerbox.com/d/' : '/d/'; ?>
					<i class="fas fa-spinner fa-spin image-pre-loader"></i>
					<div class="image-preview" style="background-image: url({{ $srcPrefix . '255x140/' . $file->code }})"></div>
				@else
					{!! $file->getIcon() !!}
				@endif
			</div>

			<span class="name">{{ $file->name }}</span>
		</div>
	</div>
@endforeach
