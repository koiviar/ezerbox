@extends('main')

@section('style')
	{!! Css::get('/css/zone/bucket.css') !!}
@endsection

@section('content')
	<div class="zonePreloader bg-dark-regular">
		<div>
			<i class="fa fa-cog fa-spin"></i>
			<div>{{ trans('main.loading') }}</div>
		</div>
	</div>

	<div class="main-container-padding white-rounded">

		{{-- кнопки управления --}}
		<div class="files-controll">
			{{-- файл не выбран --}}
			<div class="files-not-selected">
				<button class="btn btn-primary mb-0 uploaderLabel uploader-button" id="browse">
					<i class="fas fa-upload"></i> {{ trans('bucket.upload') }}</button>
				<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#createDirModal">
					<i class="fas fa-plus"></i> <a class="hideTextBtn">{{ trans('bucket.creteDir') }}</a></button>
			</div>
			{{-- файл выбран --}}
			<div class="files-selected" style="display:none">
				<a class="btn btn-primary download-button" target="_blank" href="/bucket/download">
					<i class="fas fa-download"></i> {{ trans('bucket.download') }}</a>
                <button class="btn btn-outline-primary" data-toggle="modal" data-target="#moveEntModal">
					<i class="fas fa-arrows-alt"></i> <a class="hideTextBtn">{{ trans('bucket.move') }}</a></button>
                <button type="button" class="btn btn-outline-primary renameEnt">
                    <i class="fas fa-pen"></i> <a class="hideTextBtn">{{ trans('bucket.rename') }}</a></button>
				<button type="button" class="btn btn-outline-primary deleteSelected">
					<i class="fas fa-trash"></i> <a class="hideTextBtn">{{ trans('main.delete') }}</a></button>
				<button type="button" class="btn btn-outline-primary unselect-files">
					<i class="fas fa-times"></i></button>
			</div>

			{{-- навигация по папкам --}}
			<nav aria-label="breadcrumb" class="nav-breadcrumb">
				<ol class="breadcrumb dirNavigation"></ol>
			</nav>
		</div>

		{{-- таблица с файлами --}}
		{{-- <table class="table">
			<thead>
				<th style="width:20px;">
					<div class="custom-control custom-checkbox">
					 	<input type="checkbox" class="custom-control-input select-all" id="cc-dir-file-all">
					 	<label class="custom-control-label" for="cc-dir-file-all"></label>
					</div>
				</th>
				<th>{{ trans('bucket.name') }}</th>
				<th>{{ trans('bucket.size') }}</th>
				<th>{{ trans('bucket.type') }}</th>
				<th>{{ trans('bucket.createdAt') }}</th>
				<th></th>
			</thead>
			<tbody class="files"></tbody>
		</table> --}}
		<div class="files row mr-0"></div>

		{{-- информация о содержимом папки --}}
		<div class="dirInfoBar bg-dark-regular">
			{{ trans('bucket.folders') }}: <span class="foldersB">0</span> /
			{{ trans('bucket.files') }}: <span class="filesB">0</span> /
			{{ trans('bucket.size') }}: <span class="sizeB">0</span>
		</div>

	</div>

	{{-- модалка для создания папки --}}
	<div class="modal fade" id="createDirModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	    		<div class="modal-header">
	    			<h5 class="modal-title" id="exampleModalLongTitle">{{ trans('bucket.modalCreateDir') }}</h5>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	        		</button>
	      		</div>
	      		<div class="modal-body">
	        		<input class="form-control createDirName">
	      		</div>
	      		<div class="modal-footer">
	        		<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('main.cancel') }}</button>
	        		<button type="button" class="btn btn-primary createDir">
	        			<i class="fas fa-save"></i> {{ trans('main.create') }}</button>
	      		</div>
	    	</div>
	  	</div>
	</div>
	{{-- конец модалки для создания папки --}}

	{{-- модалка для ошибок --}}
	<div class="modal fade" id="uploaderErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
	    	<div class="modal-content">
	    		<div class="modal-header bg-warning">
	    			<h5 class="modal-title" id="exampleModalLongTitle">{{ trans('bucket.modalUploadError') }}</h5>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	        		</button>
	      		</div>
	      		<div class="modal-body errorContent">

	      		</div>
	      		<div class="modal-footer">
	        		<button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('main.close') }}</button>
	      		</div>
	    	</div>
	  	</div>
	</div>
	{{-- модалка для ошибок конец --}}

	{{-- подалка переименовывания сущности --}}
	<div class="modal fade" id="renameEntModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	    		<div class="modal-header">
	    			<h5 class="modal-title" id="exampleModalLongTitle">{{ trans('bucket.modalRename') }}</h5>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	        		</button>
	      		</div>
	      		<div class="modal-body">
	        		<input type="text" class="renameEntInput form-control">
	      		</div>
	      		<div class="modal-footer">
	        		<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('main.close') }}</button>
	        		<button type="button" class="btn btn-primary renameEntStart" data-dismiss="modal">
	        			<i class="fas fa-save"></i> {{ trans('bucket.rename') }}</button>
	      		</div>
	    	</div>
	  	</div>
	</div>
	{{-- подалка переименовывания сущности конец --}}

	{{-- подалка перемещения сущности --}}
	<div class="modal fade" id="moveEntModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	    		<div class="modal-header">
	    			<h5 class="modal-title" id="exampleModalLongTitle">{{ trans('bucket.modalMove') }}</h5>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	        		</button>
	      		</div>
	      		<div class="modal-body dirListSelect">

	      		</div>
	      		<div class="modal-footer">
	        		<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('main.close') }}</button>
	        		<button type="button" class="btn btn-primary moveEntStart" data-dismiss="modal">
	        			<i class="fas fa-save"></i> {{ trans('bucket.move') }}</button>
	      		</div>
	    	</div>
	  	</div>
	</div>
	{{-- подалка перемещения сущности конец --}}

	{{-- окно с подробной информацией о файле --}}
	<div class="modal fade" id="fileInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
	    	<div class="modal-content">
	      		<div class="modal-body">
	      			<div class="infoContent" style="display: none">
	      				<div class="row">
	      					<div class="col-sm-9">
	      						<div class="imgPreview">
	      							<i class="fa fa-cog fa-spin image-preloader"></i>
	      							<img src="#">
	      							<video src="#"></video>
	      							<div class="arrow leftArrow"><i class="fas fa-angle-left"></i></div>
	      							<div class="arrow rightArrow"><i class="fas fa-angle-right"></i></div>
	      						</div>
	      					</div>
	      					<div class="col-sm-3">
  								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  						  			<span aria-hidden="true">&times;</span>
  								</button>
			      				<div class="fileTitle">{{ trans('main.loading') }}</div>
			      				<div class="fileInfo">
			      					<div class="row">
			      						{{--  --}}
			      						<div class="field-label">{{ trans('bucket.type') }}</div>
			      						<span class="type">{{ trans('main.loading') }}</span>
			      						{{--  --}}
			      						<div class="field-label">{{ trans('bucket.size') }}</div>
			      						<span class="size">{{ trans('main.loading') }}</span>
			      						{{--  --}}
			      						<div class="field-label">{{ trans('bucket.directLink') }} ({{ trans('main.tmp') }})</div>
			      						<span class="linkView">{{ trans('main.loading') }}</span>
			      							<a class="link" href="#" target="_blank"><i class="fas fa-external-link-alt"></i></a>
			      						{{--  --}}
			      						<div class="field-label">{{ trans('bucket.timeUpload') }}</div>
			      						<span class="createdAt">{{ trans('main.loading') }}</span>
			      						{{--  --}}
			      						<div class="field-label">{{ trans('bucket.timeChange') }}</div>
			      						<span class="updatedAt">{{ trans('main.loading') }}</span>
			      					</div>
			      				</div>
			      				<div style="border-top: 1px dashed #dadce0;"></div>
			      				<div class="fileInfo">
				      				<div class="row">
				      					{{--  --}}
				      					<button class="createPublicLink btn btn-outline-primary" style="margin: 5px 0 -5px;">
				      						<i class="fa fa-plus"></i> {{ trans('bucket.createPublickLink') }}</button>
				      					<button class="deletePublicLink btn btn-outline-primary" style="margin: 5px 0 -5px;">
				      						<i class="fa fa-trash"></i> {{ trans('bucket.deletePublickLink') }}</button>
				      					{{-- <span class="createPublicLink link">{{ trans('bucket.createPublickLink') }}</span>
				      					<span class="deletePublicLink"><i class="fa fa-trash"></i></span> --}}
				      					<div class="publicInfoBlock">
					      					{{--  --}}
				      						{{-- <div class="field-label">{{ trans('bucket.publickLink') }}</div> --}}
			      							{{-- <a class="publicLink" target="_blank" href="#">{{ trans('main.loading') }}</a> --}}
			      							{{--  --}}
			      							<div class="field-label">{{ trans('bucket.publickDirectLink') }}</div>
			      							<a class="publicDirectLink" target="_blank" href="#">{{ trans('main.loading') }}</a>
					      					{{--  --}}
				      						{{-- <div class="field-label">{{ trans('bucket.views') }}</div> --}}
				      						{{-- <span class="views">{{ trans('main.loading') }}</span> --}}
					      					{{--  --}}
				      						{{-- <div class="field-label">{{ trans('bucket.downloads') }}</div> --}}
				      						{{-- <span class="downloads">{{ trans('main.loading') }}</span> --}}
				      					</div>
				      				</div>
			      				</div>
	      					</div>
	      				</div>


	      			</div>
	      		</div>
	      		{{-- <div class="modal-footer">
	        		<button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('main.close') }}</button>
	      		</div> --}}
	    	</div>
	  	</div>
	</div>
	{{-- окно с подробной информацией о файле конец --}}

    {{-- окно с файлами на загрузку --}}
    <div class="uploader-files-list" id="filelist" style="display:none;"></div>
    {{-- окно с файлами на загрузку конец --}}
    <div id="dropPrepare"></div>
@endsection

@section('scripts')
    {!! Js::get('/vendor/plupload-3.1.2/js/plupload.full.min.js') !!}
    {!! Js::get('/js/zone/bucket.js') !!}
@endsection
