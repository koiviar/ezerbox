@extends('main')

@section('style')
	{!! Css::get('/css/zone/bucket.css') !!}
@stop

@section('content')

	<div class="main-container-padding">
		{{-- <input class="form-control searchPublicFiles"> --}}
		<div class="input-group mb-3">
			<input type="text" class="form-control searchPublicFiles" placeholder="Фраза для поиска" value="{{ isset($search) ? $search : '' }}">
			<div class="input-group-append">
				<button class="btn btn-outline-primary searchPublicFilesButton" type="button"
					style="border-width: 1px 1px 1px 0 !important;border-color:#dadce0;">Поиск</button>
				@if(isset($search))
					<button class="btn btn-outline-primary searchPublicFilesButtonCancel" type="button"
						style="border-width: 1px 1px 1px 0 !important;border-color:#dadce0;">Отмена</button>
				@endif
			</div>
		</div>

		<style type="text/css">
			td {
				height: 50px;
				vertical-align: middle !important;
			}
		</style>

		<br>

		@if(isset($search))
			<h4>Результать по фразе "{{ $search }}"</h4>
		@else
			<h4>Самые популярные файлы за последний месяц</h4>
		@endif

		<table class="table">
			<thead>
				<th></th>
				<th>Название</th>
				<th style="text-align: right;">Размер</th>
				<th style="text-align: right;"><i class="fa fa-eye"></i></th>
				<th style="text-align: right;"><i class="fa fa-download"></i></th>
				<th style="text-align: right;">Время загрузки</th>
			</thead>
			@forelse($files as $file)
				<tr>
					<td class="imagePreview" style="width: 64px;text-align: center;">
						@if($file->ex_type)
							<img src="//{{ $file->link }}" style="max-height: 40px;max-width: 40px;">
						@else
							<i class="fas fa-file icon" style="font-size: 26px"></i>
						@endif
					</td>
					<td><a href="/publicFile/{{ $file->public_id }}">{!! $file->name !!}</a></td>
					<td style="text-align: right;">{{ $file->size }}</td>
					<td style="text-align: right;">{{ $file->views }}</td>
					<td style="text-align: right;">{{ $file->downloads }}</td>
					<td style="text-align: right;">{{ $file->created_atH }}</td>
				</tr>
			@empty
			@endforelse
		</table>


	{{-- {{ $files }} --}}
	</div>

@stop

@section('scripts')
	{!! Js::get('/js/zone/publicFile.js') !!}
@stop
