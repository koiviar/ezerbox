@extends('main')

@section('style')
	{!! Css::get('/css/zone/bucket.css') !!}
	{!! Css::get('/css/zone/comments.css') !!}
@stop

@section('content')

	{{-- {{ $file }} --}}
	<div class="main-container-padding">
		<div class="input-group mb-3">
			<input type="text" class="form-control searchPublicFiles" placeholder="Фраза для поиска" value="{{ isset($search) ? $search : '' }}">
			<div class="input-group-append">
				<button class="btn btn-outline-primary searchPublicFilesButton" type="button"
					style="border-width: 1px 1px 1px 0 !important;border-color:#dadce0;">Поиск</button>
				@if(isset($search))
					<button class="btn btn-outline-primary searchPublicFilesButtonCancel" type="button"
						style="border-width: 1px 1px 1px 0 !important;border-color:#dadce0;">Отмена</button>
				@endif
			</div>
		</div>

		<div class="row publicFileRow">
			<div class="col-sm-8 filePreviewRow">
				@if ($file->ex_type == 'image')
					<img src="//{{ $file->link }}" class="mainImage">
				@else
					<i class="fas fa-file mainIcon"></i>
				@endif
			</div>
			<div class="col-sm-4">
				<h3>{{ $file->name }}</h3>
				<br>
				<div class="fileInfo">
					<div class="row">
						<div class="col-sm-4"><div class="dashed">{{ trans('bucket.type') }}</div></div>
						<div class="col-sm-8"><span class="type">{{ $file->ex_typeH }}</span></div>
					</div>
					<div class="row">
						<div class="col-sm-4"><div class="dashed">{{ trans('bucket.size') }}</div></div>
						<div class="col-sm-8"><span class="size">{{ $file->size }}</span></div>
					</div>
					<div class="row">
						<div class="col-sm-4"><div class="dashed">{{ trans('bucket.timeUpload') }}</div></div>
						<div class="col-sm-8"><span class="createdAt">{{ $file->created_atH }}</span></div>
					</div>
					<div class="row">
						<div class="col-sm-4"><div class="dashed">{{ trans('bucket.views') }}</div></div>
						<div class="col-sm-8"><span class="views">{{ $file->views }}</span></div>
	  				</div>
	  				<div class="row">
						<div class="col-sm-4"><div class="dashed">{{ trans('bucket.downloads') }}</div></div>
						<div class="col-sm-8"><span class="downloads">{{ $file->downloads }}</span></div>
	  				</div>
				</div>

				<br>
				<br>

				<a class="btn btn-success downloadButton" href="/publicFile/download/{{ $file->public_id }}" target="_blank" data-id="{{ $file->public_id }}">
					<i class="fas fa-download"></i> {{ trans('bucket.download') }}
				</a>
			</div>
		</div>

		<br>

		<h5>Комментарии</h5>
		<br>
		<div class="row">
			<div class="col-sm-9">
				{!! $commentsView !!}
			</div>
			<div class="col-sm-3"></div>
		</div>


	</div>

@stop

@section('scripts')
	{!! Js::get('/js/zone/publicFile.js') !!}
	{!! Js::get('/js/zone/comments.js') !!}
@stop
