@extends('main')

@section('style')
	{!! Css::get('/css/zone/bucket.css') !!}
@endsection

@section('content')
    <button class="btn btn-primary mb-0 uploaderLabel uploader-button" id="browse2" style="display: none">
        <i class="fas fa-upload"></i> {{ trans('bucket.upload') }}</button>
    <div id="bucket">
        <div class="zonePreloader bg-dark-regular" v-if="loader">
            <div>
                <i class="fa fa-cog fa-spin"></i>
                <div>{{ trans('main.loading') }}</div>
            </div>
        </div>

        <div class="main-container-padding white-rounded" @dragover="dragover" @dragleave="dragleave">
            {{-- кнопки управления --}}
            <div class="files-controll">
                {{-- файл не выбран --}}
                <div class="files-not-selected" :class="{ hide: fileControl }">
                    <label class="btn btn-primary mb-0 uploaderLabel uploader-button" id="browse" for="browse2">
                        <i class="fas fa-upload"></i> {{ trans('bucket.upload') }}</label>
                    <button type="button" class="btn btn-outline-primary" @click="createDir">
                        <i class="fas fa-plus"></i> <a class="hideTextBtn">{{ trans('bucket.creteDir') }}</a></button>
                </div>
                {{-- файл выбран --}}
                <div class="files-selected" :class="{ hide: !fileControl }">
                    <a class="btn btn-primary download-button" target="_blank" :href="downloadFileLink" :class="{ hide: !showDownload }" download>
                        <i class="fas fa-download"></i> {{ trans('bucket.download') }}</a>
                    <button class="btn btn-outline-primary moveFiles" @click="moveFiles">
                        <i class="fas fa-arrows-alt"></i> <a class="hideTextBtn">{{ trans('bucket.move') }}</a></button>
                    <button type="button" class="btn btn-outline-primary renameEnt" :class="{ hide: !showRename }" @click="rename">
                        <i class="fas fa-pen"></i> <a class="hideTextBtn">{{ trans('bucket.rename') }}</a></button>
                    <button type="button" class="btn btn-outline-primary deleteSelected" @click="deleteEnt">
                        <i class="fas fa-trash"></i> <a class="hideTextBtn">{{ trans('main.delete') }}</a></button>
                    <button type="button" class="btn btn-outline-primary" @click="unselectAll">
                        <i class="fas fa-times"></i></button>
                </div>

                {{-- навигация по папкам --}}
                <nav aria-label="breadcrumb" class="nav-breadcrumb">
                    <ol class="breadcrumb dirNavigation">
                        <li class="breadcrumb-item"><span class="dirNav link" @click="getList(false, 0)">{{ trans('bucket.bucket') }}</span></li>
                        <li class="breadcrumb-item" v-for="item in path"><span class="dirNav link" @click="getList(false, item.id)">@{{ item.name }}</span></li>
                    </ol>
                </nav>
            </div>

            <div class="files">
                <div v-if="dirs.length > 0">
                    {{ trans('bucket.title_dirs') }}
                    <div class="dir mr-3">
                        <div class="row w100-15-s">
                            <div class="col-lg-2 pr-0 element-item-parent" v-for="dir in dirs">
                                <div class="element-item element-dir" :class="{ active: dir.isActive }"
                                     v-on:click="select($event, dir)" @dblclick="open(dir)"
                                     data-type="dir" :id="'folder-' + dir.id">
                                    {{--                                <div class="image">--}}
                                    {{--                                    <i class="far fa-folder-open"></i>--}}
                                    {{--                                </div>--}}

                                    <span class="name">@{{ dir.name }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-if="files.length > 0">
                    {{ trans('bucket.title_files') }}
                    <div class="file mr-3">
                        <div class="row w100-15-s">
                            <div class="col-lg-2 pr-0 element-item-parent" v-for="file in files">
                                <div class="file-anchor" :id="'file-' + file.id"></div>
                                <div class="element-item element-file" :class="{ active: file.isActive }"
                                     v-on:click="select($event, file)" @dblclick="open(file)"
                                     data-code="@{{ file.code }}" data-type="file">
                                    <div class="image">
                                        <div v-if="file.ex_type === 'image'">
{{--                                            <img :src="file.preview" style="max-width: 100%">--}}
                                            <i class="fas fa-spinner fa-spin image-pre-loader"></i>
{{--                                            <div class="image-preview" style="background-image: url({{ $srcPrefix . '255x140/' . $file->code }})"></div>--}}
                                            <div class="image-preview" :style="{ 'background-image': 'url(' + file.preview + ')' }"></div>
                                        </div>
                                        <div v-if="file.ex_type !== 'image'">
                                            <i :class="file.icon"></i>
                                        </div>
                                    </div>

                                    <span class="name">@{{ file.name }}</span>
                                </div>
                            </div>
                            <div class="col-lg-2 pr-0 element-item-parent" id="loadMoreFilesRef" v-if="loadMoreFilesTrigger">
                                <div class="element-item element-dir" data-id="5" data-type="dir">
                                    <div class="image">
                                        <i class="fa fa-cog fa-spin"></i>
                                    </div>
                                    <span class="name">{{ trans('main.loading') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- информация о содержимом папки --}}
            <div class="dirInfoBar bg-dark-regular">
                {{ trans('bucket.folders') }}: <span class="foldersB">@{{ stat.dirs }}</span> /
                {{ trans('bucket.files') }}: <span class="filesB">@{{ stat.files }}</span> /
                {{ trans('bucket.size') }}: <span class="sizeB">@{{ stat.size }}</span>
            </div>

        </div>

        {{-- окно с подробной информацией о файле --}}
        <div class="modal fade" id="fileInfoModal" v-if="openFile" :class="{ show: openFile }" :style="{ display: modalFileShow }">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="infoContent">
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="imgPreview">
                                        <i class="fa fa-cog fa-spin image-preloader"
                                           v-if="openFile.ex_type === 'image' || openFile.ex_type === 'video'"></i>
                                        <div v-if="openFile.ex_type === 'image'" style="z-index: 1">
                                            <img :src="openFile.download_link">
                                        </div>
                                        <div v-if="openFile.ex_type === 'video'" style="z-index: 5">
                                            <video :src="openFile.download_link" controls></video>
                                        </div>
                                        <div v-if="openFile.ex_type !== 'image' && openFile.ex_type !== 'video'">
                                            <i :class="openFile.icon" style="font-size: 80px;"></i>
                                        </div>
                                        <div class="arrow leftArrow" @click="leftArrow"><i class="fas fa-angle-left"></i></div>
                                        <div class="arrow rightArrow" @click="rightArrow"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                                <div class="col-sm-3 fileInfoBlock">
                                    <button type="button" class="close" @click="closeModal">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="fileTitle">@{{ openFile.name }}</div>
                                    <div class="fileInfo">
                                        <div class="row">
                                            {{--  --}}
                                            <div class="field-label">{{ trans('bucket.type') }}</div>
                                            <span class="type">@{{ openFile.type }}</span>
                                            {{--  --}}
                                            <div class="field-label">{{ trans('bucket.size') }}</div>
                                            <span class="size">@{{ openFile.size }}</span>
                                            {{--  --}}
                                            <div class="field-label">{{ trans('bucket.directLink') }} ({{ trans('main.tmp') }})</div>
                                            <span class="linkView" v-if="!openFile.download_link">{{ trans('main.loading') }}</span>
                                            <a class="link" :href="openFile.download_link" target="_blank"
                                               v-if="openFile.download_link" style="overflow: hidden">
                                                @{{ openFile.download_link }} <i class="fas fa-external-link-alt"></i></a>
                                            {{--  --}}
                                            <div class="field-label">{{ trans('bucket.timeUpload') }}</div>
                                            <span class="createdAt">@{{ openFile.created_atH }}</span>
                                            {{--  --}}
{{--                                            <div class="field-label">{{ trans('bucket.timeChange') }}</div>--}}
{{--                                            <span class="updatedAt">{{ trans('main.loading') }}</span>--}}
                                        </div>
                                    </div>
                                    <div style="border-top: 1px dashed #dadce0;"></div>
                                    <div class="fileInfo">
                                        <div class="row">
                                            <div class="field-label">{{ trans('bucket.publickDirectLink') }}</div>
                                            {{--  --}}
                                            <button class="createPublicLink btn btn-outline-primary" style="margin: 5px 0 -5px;"
                                                    v-if="!openFile.public_code" @click="createCDN">
                                                <i class="fa fa-plus"></i> {{ trans('bucket.createPublickLink') }}</button>
                                            <button class="deletePublicLink btn btn-outline-primary" style="margin: 5px 0 -5px;"
                                                    v-if="openFile.public_code" @click="deleteCDN">
                                                <i class="fa fa-trash"></i> {{ trans('bucket.deletePublickLink') }}</button>
                                            {{-- <span class="createPublicLink link">{{ trans('bucket.createPublickLink') }}</span>
                                            <span class="deletePublicLink"><i class="fa fa-trash"></i></span> --}}
                                            <div class="publicInfoBlock" v-if="openFile.public_code">
                                                {{--  --}}
                                                {{-- <div class="field-label">{{ trans('bucket.publickLink') }}</div> --}}
                                                {{-- <a class="publicLink" target="_blank" href="#">{{ trans('main.loading') }}</a> --}}
                                                {{--  --}}
                                                <a class="publicDirectLink" target="_blank" :href="'https://' + openFile.publicLink">@{{ openFile.publicLink }}</a>
                                                {{--  --}}
                                                {{-- <div class="field-label">{{ trans('bucket.views') }}</div> --}}
                                                {{-- <span class="views">{{ trans('main.loading') }}</span> --}}
                                                {{--  --}}
                                                {{-- <div class="field-label">{{ trans('bucket.downloads') }}</div> --}}
                                                {{-- <span class="downloads">{{ trans('main.loading') }}</span> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- окно с подробной информацией о файле конец --}}

        {{-- окно с файлами на загрузку --}}
        <div class="uploader-files-list" id="filelist" v-if="Object.keys(uploader.list).length > 0">
            <div class="list">
                <div id="file.id" v-for="file in uploader.list">@{{ file.name }}
                    <span style="float: right"> &nbsp;&nbsp;&nbsp(@{{ sizeToHuman(file.size) }})</span>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" :class="file.bg" :style="{ width: file.percent }">@{{ file.percent }}</div>
                    </div>
                </div>
            </div>
            <div id="total-progress">Total <span style="float: right"> &nbsp;&nbsp;@{{ uploader.uploadedFiles + '/' + uploader.allFiles }}
                    &nbsp;(@{{ sizeToHuman(uploader.allSize) }})</span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" :style="{ width: uploader.percent }">@{{ uploader.percent }}</div>
                </div>
            </div>
        </div>
        {{-- окно с файлами на загрузку конец --}}
        <div class="dropPrepareLabel" id="dropPrepare" :class="uploader.dropZone" @dragover="dragover" @dragleave="dragleave" @drop="drop"></div>
    </div>
{{--    <div id="dropPrepare"></div>--}}
@endsection

@section('scripts')
    {!! Js::get('/vendor/plupload-3.1.2/js/plupload.full.min.js') !!}
    {!! Js::get('https://unpkg.com/vue@next') !!}
{{--    {!! Js::get('https://cdn.jsdelivr.net/npm/vue-plupload@2.2.3/index.min.js') !!}--}}
    {!! Js::get('https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.4/axios.min.js') !!}
    {!! Js::get('/js/zone/bucket2.js') !!}
@endsection
