@extends('main')

@section('style')
    {!! Css::get('/css/zone/dashboard.css') !!}
@stop

@section('content')

    <div class="row card-deck mb-3">
        <div class="card bs-s-0 mt-s-15">
            <h5 class="card-header">{{ trans('dash.dashBalance') }}</h5>
            <div class="card-body">
                <div class="money mb-3 showBalanceModal">
                    <i class="fas fa-euro-sign"></i>
                    {{ round(Auth::user()->money, 2) }}
                </div>
                <div>
                    <div class="w-25 text-primary d-inline-block">Cloud</div>
                    € {{ $data['money']['disk'] }} / {{ trans('main.month') }}
                </div>
{{--                <div>--}}
{{--                    <div class="w-25 text-primary d-inline-block">NFS</div>--}}
{{--                    € {{ $data['money']['nfs'] }} / месяц--}}
{{--                </div>--}}
            </div>
        </div>

        <div class="card bs-s-0">
            <h5 class="card-header">{{ trans('dash.dashDiskUsage') }}</h5>
            <div class="card-body">
                <div>
                    {{ trans('bucket.files') }} - {{ number_format($data['disk']['files_count']) }}
                    @if($data['disk']['percentFiles'] > 0)
                        <span class="text-success">+{{ $data['disk']['percentFiles'] }}%</span>
                    @elseif($data['disk']['percentFiles'] < 0)
                        <span class="text-danger">{{ $data['disk']['percentFiles'] }}%</span>
                    @endif
                </div>
                <div class="mb-3">
                    {{ trans('bucket.size') }} - {{ $data['disk']['spaceUsed'] }}
                    @if($data['disk']['percentSize'] > 0)
                        <span class="text-success">+{{ $data['disk']['percentSize'] }}%</span>
                    @elseif($data['disk']['percentFiles'] < 0)
                        <span class="text-danger">{{ $data['disk']['percentSize'] }}%</span>
                    @endif
                </div>
                <div>{{ trans('main.price') }} - € {{ round(Payment::DISK_PRICE * $data['disk']['spaceUsedToCalc'] / 30, 4) }} / {{ trans('main.day') }}</div>
            </div>
        </div>

        <div class="card bs-s-0">
            <h5 class="card-header">{{ trans('dash.dashSub') }}</h5>
            <div class="card-body">
                @foreach($data['sub'] as $sub)
                    <div class="sub mb-2">
                        <div class="title">{{ trans($sub->comment) }}</div>
                        <div class="float-right">{{ $sub->forever ? trans('main.forever') : trans('main.until') . $sub->expire_date}}</div>
                        <div>{{ trans('bucket.size') }} - {{ $sub->space }}GB</div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row card-deck">
        <div class="card bs-s-0">
            <h5 class="card-header">{{ trans('dash.dashFillUp') }}</h5>
            <div class="card-body">
                <table class="table hide-s">
                    <thead>
                        <th class="border-top-0 pt-0">{{ trans('main.date') }}</th>
                        <th class="border-top-0 pt-0">{{ trans('main.description') }}</th>
                        <th class="border-top-0 pt-0">{{ trans('main.amount') }}</th>
                    </thead>
                    <tbody>
                        @forelse($data['money_up'] as $up)
                            <tr>
                                <td>{{ date('d/m/Y', strtotime($up->created_at)) }}</td>
                                <td>{{ $up->getPreparedText() }}</td>
                                <td>€ {{ $up->sum }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">{{ trans('dash.noOpFillUp') }}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="hide-b">
                    @forelse($data['money_up'] as $up)
                        <div class="mb-3">
                            <div>{{ $up->getPreparedText() }}</div>
                            <b class="fl-right">€ {{ $up->sum }}</b>
                            {{ date('d/m/Y', strtotime($up->created_at)) }}
                        </div>
                    @empty
                        {{ trans('dash.noOpFillUp') }}
                    @endforelse
                </div>
            </div>
        </div>

        <div class="card bs-s-0">
            <h5 class="card-header">{{ trans('dash.dashFillDown') }}</h5>
            <div class="card-body">
                <table class="table hide-s">
                    <thead>
                        <th class="border-top-0 pt-0">{{ trans('main.date') }}</th>
                        <th class="border-top-0 pt-0">{{ trans('main.description') }}</th>
                        <th class="border-top-0 pt-0">{{ trans('main.amount') }}</th>
                    </thead>
                    <tbody>
                    @forelse($data['money_down'] as $down)
                        <tr>
                            <td>{{ date('d/m/Y', strtotime($down->created_at)) }}</td>
                            <td>{{ $down->getPreparedText() }}</td>
                            <td>€ {{ $down->sum }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">{{ trans('dash.noOpFillDown') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <div class="hide-b">
                    @forelse($data['money_down'] as $down)
                        <div class="mb-3">
                            <div>{{ $down->getPreparedText() }}</div>
                            <b class="fl-right">€ {{ $down->sum }}</b>
                            {{ date('d/m/Y', strtotime($down->created_at)) }}
                        </div>
                    @empty
                        {{ trans('dash.noOpFillDown') }}
                    @endforelse
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
{{--    {!! Js::get('/js/zone/nfs.js') !!}--}}
@stop
