<div class="media comment" data-id="{{ $comment->id }}">
	<img src="{{ str_replace('###', '100', $comment->avatar) }}" 
		class="mr-3 {{ (Auth::user() && Auth::id() == $comment->user_id) ? 'avatar-100' : '' }}" style="max-width: 50px">
	<div class="media-body">
		<h5 class="mt-0">{{ $comment->name }} <span class="time">{{ date('H:i d.m.y', $comment->created_at->timestamp) }}</span></h5>
		<p class="message">{{ $comment->message }}</p>
		<div class="placeToCommentForm"></div>

		@if(Auth::user())
			<div class="actionList">
				<span class="action answer">Ответить</span>
				@if(Auth::id() == $comment->user_id)
					<span class="action delete">Удалить</span>
				@endif
			</div>
		@endif

		@forelse($comment->child as $child)
			{!! view('comments.comment', ['comment' => $child]) !!}
		@empty
		@endforelse

	</div>
</div>