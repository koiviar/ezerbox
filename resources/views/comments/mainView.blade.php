<div class="commentsArea" data-zone="{{ $zone }}">

	@forelse($comments as $comment)
		{!! view('comments.comment', ['comment' => $comment]) !!}
	@empty
	@endforelse

	<div class="sendCommentForm main">
		<textarea class="form-control message"></textarea>
		<button class="btn btn-primary sendComment" style="margin-top: 5px;">Отправить</button>
	</div>

</div>