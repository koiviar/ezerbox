<head>
    {!! Css::get('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css') !!}
    {!! App\Libs\Css::get('/css/main.css') !!}
    {!! App\Libs\Css::get('/css/landing/main.css') !!}

    <meta name="viewport" content="width=420">
</head>
<body class="authHide">

    <section class="section-1" style="min-height: 100vh">
        <div class="container pt-100px">
{{--            <nav class="navbar header">--}}
{{--                @if(Auth::user())--}}
{{--                    <div class="form-inline">--}}
{{--                        <span class="sideBarMenuActivator"><i class="fas fa-bars"></i></span>--}}
{{--                         <a class="navbar-brand" href="/">EzerBox</a>--}}
{{--                    </div>--}}
{{--                @endif--}}

{{--                 то что справа в хеадере--}}
{{--                @if(Auth::user())--}}

{{--                    <div class="form-inline">--}}
{{--                         <a class="userCashField" href="/transactions">--}}
{{--                            <span>{{ trans('user.cash') }}:</span>--}}
{{--                            <span class="userCash userCashStyle">{{ Auth::user()->money }}</span>--}}
{{--                            <span class="userCash userCashStyle">EzerCoin</span>--}}
{{--                        </a>--}}

{{--                        <button class="btn btn-default openUserInfo" data-toggle="modal" data-target="#userProfileModal">--}}
{{--                            <img src="{{ str_replace('###', '100', Auth::user()->avatar) }}" class="avatar-100">--}}
{{--                            <span class="name">{{ Auth::user()->name }}</span>--}}
{{--                        </button>--}}
{{--                        <a class="header-logout" href="javascript:void(0);"--}}
{{--                           onclick="event.preventDefault(); document.getElementById('logout-form').submit()">{{ trans('main.logOut') }}</a>--}}
{{--                    </div>--}}

{{--                     для выхода--}}
{{--                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>--}}
{{--                @else--}}

{{--                    <div class="form-inline">--}}
{{--                        <div class="logo">--}}
{{--                            <a class="" href="/">EzerBox</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="form-inline">--}}
{{--                        <a href="/login" class="header-login">{{ trans('main.login') }}</a>--}}
{{--                    </div>--}}
{{--                @endif--}}


{{--            </nav>--}}

            <div class="cbp-af-header">
                <div class="container">
                    <nav class="row">
                        <div id="logo">
                            <a href="/">Ezerbox</a>
                        </div>
                        <ul id="nav">
{{--                            <li><a href="#s3">Contact</a></li>--}}
{{--                            <li><a href="#s2">Pricing</a></li>--}}
                            <li><a href="https://stats.uptimerobot.com/1p37NUpPjE" target="_blank">Live monitor</a></li>
                            <li><a href="{{ route('apiManage.docs') }}">Api</a></li>
                        </ul>
                    </nav>
                </div>

            </div>

            <div class="row vh-100">
                <div class="col-lg-7 col-md-6 col-sm-12 d-flex align-items-center no-flex">
                    <div class="huge-title">
                        <h2 class="mb-5">Elastic cloud storage, pay only for you use</h2>
                        <div class="main-info">
                            <span class="text-light">
                                Price calculating for each GB over free limit. Cost is 0.04 € per GB<br>
                                Calculating only real using GB
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12 d-flex align-items-center">
                    <div class="login-form">
                        <div class="login-nav">
                            <div class="item active" data-target="login">{{ trans('user.loginA') }}</div>
                            <div class="item" data-target="register">{{ trans('user.registerA') }}</div>
                        </div>
                        <div class="login p-2">
                            <form method="POST" action="{{ route('login') }}" class="m-0 login">
                                @csrf

                                <div class="mb-2 margin">
                                    <input id="login"
                                           type="login"
                                           class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}"
                                           name="login"
                                           placeholder="{{ trans('user.loginName') }}"
                                           value="{{ old('login') }}" required autofocus>

                                    @if ($errors->has('login'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('login') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <div class="mb-3 margin">
                                    <input id="password"
                                           type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           placeholder="{{ trans('user.pass') }}"
                                           name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <div class="form-group row hideText">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" checked {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ trans('user.remember') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary w-100">
                                    {{ trans('user.loginA') }}
                                </button>

                                {{-- TODO доделать восстановление пароля --}}
{{--                                @if (Route::has('password.request'))--}}
{{--                                    <div class="text-center">--}}
{{--                                        <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                            {{ trans('user.forgotPass') }}--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                @endif--}}

                            </form>

                            <form method="POST" action="{{ route('register') }}" class="m-0 register" style="display:none;">
                                @csrf

                                <div class="mb-2 margin">
                                    <input id="name"
                                           type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} save"
                                           name="name"
                                           value="{{ old('name') }}"
                                           placeholder="{{ trans('user.name') }}"
                                           required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="mb-2 margin">
                                    <input id="login"
                                           type="login"
                                           class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }} save"
                                           name="login"
                                           value="{{ old('login') }}"
                                           placeholder="{{ trans('user.loginName') }}"
                                           required>

                                    @if ($errors->has('login'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('login') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="mb-2 margin">
                                    <input id="email"
                                           type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} save"
                                           name="email"
                                           value="{{ old('email') }}"
                                           placeholder="{{ trans('user.email') }}"
                                           required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="mb-2 margin">
                                    <input id="password"
                                           type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password"
                                           placeholder="{{ trans('user.pass') }}"
                                           required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="mb-3 margin">
                                    <input id="password-confirm"
                                           type="password"
                                           class="form-control"
                                           name="password_confirmation"
                                           placeholder="{{ trans('user.pass2') }}"
                                           required>

                                </div>
                                <button type="submit" class="btn btn-primary w-100">
                                    {{ trans('user.registerA') }}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="contact text-right">
            <div class="container">
                <a href="mailto:support@ezerbox.com">support@ezerbox.com</a>
            </div>
        </div>

    </section>
{{--    <section class="section-2" id="s2">--}}
{{--        <div class="container">--}}
{{--            <div class="twelve columns article centered make-it-appear-top animated fadeInDown">--}}

{{--                <h3 class="text-center">Pricing</h3>--}}
{{--                <br>--}}
{{--                <h2 class="text-center">0.04 € per GB</h2>--}}
{{--                <br>--}}
{{--                <p class="text-center">Price calculating for each GB over free limit, 0.04 €</p>--}}
{{--                <p class="text-center">Calculating only real using GB per hour</p>--}}

{{--            </div>--}}
{{--        </div>--}}

{{--    </section>--}}

{{--    <section class="section-3" id="s3">--}}
{{--        <div class="container">--}}
{{--            <div class="twelve columns article centered make-it-appear-top animated fadeInDown">--}}

{{--                <h3 class="text-center">Contact</h3>--}}

{{--                <p class="text-center">If you have questions, write a message to <a href="mailto:ezerbox@yandex.ru">ezerbox@yandex.ru</a></p>--}}


{{--            </div>--}}
{{--        </div>--}}

{{--    </section>--}}

    <!-- Preload the Whole Page -->

</body>

{!! Js::get('https://code.jquery.com/jquery-3.3.1.min.js') !!}

<script>
    // if (location.hash) {
    //     target = location.hash.replace('#', '');
    //     $('.login-nav .item').removeClass('active');
    //     $('.login-nav .item[data-target=' + target + ']').addClass('active');
    //     $('.login-form form').hide();
    //     $('.login-form form.' + target).show();
    //     $('.login-form form input').val('');
    // }

    if (sessionStorage.getItem('reg.tab') === 'register') {
        let target = sessionStorage.getItem('reg.tab');
        $('.login-nav .item').removeClass('active');
        $('.login-nav .item[data-target=' + target + ']').addClass('active');
        $('.login-form form').hide();
        $('.login-form form.' + target).show();

        $('.save').each(function () {
            $(this).val(sessionStorage.getItem('reg.input_' + $(this).attr('name')));
        });
    } else {
        sessionStorage.setItem('reg.tab', 'login');
    }

    $('.save').keyup(function () {
        sessionStorage.setItem('reg.input_' + $(this).attr('name'), $(this).val());
    });

    $('.login-nav .item').click(function () {
        $('.login-nav .item').removeClass('active');
        $(this).addClass('active');
        $('.login-form form').hide();
        $('.login-form form.' + $(this).data('target')).show();
        $('.login-form form input').val('');

        sessionStorage.setItem('reg.tab', $(this).data('target'));
        // location.hash = $(this).data('target');
    });

    setVhSize();
    window.addEventListener('resize', () => {
        setVhSize();
    });

    function setVhSize() {
        var vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    }
</script>

{{-- счетчик для просмотра статистики --}}
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(53038876, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/53038876" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
