<?php

return [
    'plans' => 'Plans',
    'per_month' => 'per month',
    'month_gb' => 'per GB',
    'current_plan' => 'Current plan',
];
