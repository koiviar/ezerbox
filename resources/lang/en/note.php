<?php

return [
	'groups' => 'Groups',
	'notes' => 'Notes',
	'addNote' => 'Add note',
	'modalTitleAddGroup' => 'Create group',
	'modalTitleEditGroup' => 'Edit group',
	'nameGroup' => 'Group Name',
	'group' => 'Group',
	'note' => 'Remark',
	'noteTitle' => 'Note Title',
	'noteNotExists' => 'There are no notes',
	'saved' => 'Saved',
    'notSaved' => 'Not saved',
];