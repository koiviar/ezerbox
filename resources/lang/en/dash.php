<?php

return [
    'dashBalance' => 'Balance',
    'dashDiskUsage' => 'Cloud storage',
    'dashSub' => 'Available limit',
    'dashFillUp' => 'Fill up balance',
    'dashFillDown' => 'Balance write-off',
    'noOpFillUp' => 'No fill up balance operations',
    'noOpFillDown' => 'No balance write-off operations',
];
