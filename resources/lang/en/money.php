<?php

return [
    'getMoneyFromDisk' => 'Pay for cloud storage: :hours GB/hours (€:price)',
    'getMoneyFromNFS' => 'Pay for :server: :hours hours (€:price)',
    'fillUp' => 'Fill up balance €:eur',
];
