<?php

return [
    'params' => 'Parameters',
    'disk' => 'Disk',
    'file' => 'Files',
    'folder' => 'Folders',
    'response' => 'Response',

    'disk_file_list' => 'Get a list of files and folders at the specified path',
    'disk_file_upload_link' => 'Upload a file from an available web link. Folders are created automatically',
    'disk_file_info' => 'Getting information about a file',
    'disk_file_move' => 'Move file to another folder or rename. Folders are created automatically',
    'disk_file_rename' => 'Move file to another folder or rename',
    'disk_file_delete' => 'Delete file',
    'disk_file_download' => 'Getting a temporary link to download a file',
    'disk_file_cdn_create' => 'Create permanent cdn link to file',
    'disk_file_cdn_delete' => 'Deleting cdn link',

    'disk_folder_create' => 'Create a folder at the specified path',
    'disk_folder_info' => 'Get information about a folder',
    'disk_folder_delete' => 'Delete folder',
];
