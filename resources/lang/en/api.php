<?php

return [
    'token' => 'Token',
    'tokens' => 'Tokens',
    'desc' => 'Description',
    'actions' => 'Actions',
    'create_token' => 'Create token',
    'logs' => 'History',
    'request' => 'Request',
    'params' => 'Params',
    'time' => 'Time',
    'request_count' => 'Requests for month',
    'are_you_sure' => 'Are you sure?',
    'token_delete_warning' => 'Token will be deleted',
    'token_delete_confirm' => 'Yes, delete token',
    'token_create_title' => 'Create api token',
    'token_desc_enter' => 'Enter description of your token',
    'docs' => 'Documentation',
];
