<?php

return [
	'modalTitleAccountInfo' => 'My account',
	'name' => 'Name',
	'email' => 'Email',
	'cash' => 'Balance',
	'changePass' => 'Change Password',
	'pass' => 'Password',
	'pass2' => 'Repeat password',
	'login' => 'Login',
	'register' => 'Register',
	'remember' => 'Remember me',
	'forgotPass' => 'Forgot password?',
	'loginA' => 'Login',
	'registerA' => 'Register',
	'loginName' => 'Username',
];