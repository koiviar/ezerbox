<?php

return [
    'token' => 'Токен',
    'tokens' => 'Токены',
    'desc' => 'Описание',
    'actions' => 'Действия',
    'create_token' => 'Создать токен',
    'logs' => 'История запросов',
    'request' => 'Запрос',
    'params' => 'Параметры',
    'time' => 'Время',
    'request_count' => 'Запросов за месяц',
    'are_you_sure' => 'Вы уверены?',
    'token_delete_warning' => 'Токен будет удален',
    'token_delete_confirm' => 'Да, удалить токен',
    'token_create_title' => 'Создание API токена',
    'token_desc_enter' => 'Напишите короткое описание токена',
    'docs' => 'Документация',
];
