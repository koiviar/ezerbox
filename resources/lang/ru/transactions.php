<?php

return [
    'pageTitle' => 'Операции с балансом',
    'transactionsNotExists' => 'Операции с балансом не происходило',
    'bucketPay' => 'Оплата облачного хранилища',
    'viewsInvoice' => 'Выплата за :comment просмотров',
];