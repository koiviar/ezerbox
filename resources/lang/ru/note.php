<?php

return [
    'groups' => 'Группы',
    'notes' => 'Заметки',
    'addNote' => 'Добавить заметку',
    'modalTitleAddGroup' => 'Создание группы',
    'modalTitleEditGroup' => 'Редактирование группы',
    'nameGroup' => 'Название группы',
    'group' => 'Группа',
    'note' => 'Заметка',
    'noteTitle' => 'Заголовок заметки',
    'noteNotExists' => 'Заметки отсутствуют',
    'saved' => 'Сохранено',
    'notSaved' => 'Не сохранено',
];