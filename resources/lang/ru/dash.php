<?php

return [
    'dashBalance' => 'Баланс',
    'dashDiskUsage' => 'Облачный диск',
    'dashSub' => 'Доступное место',
    'dashFillUp' => 'Пополнения',
    'dashFillDown' => 'Списания',
    'noOpFillUp' => 'Нет операций пополнения',
    'noOpFillDown' => 'Нет операций списания',
];
