<?php

return [
    'apply' => 'Применить',
    'cancel' => 'Отмена',
    'close' => 'Закрыть',
    'create' => 'Создать',
    'delete' => 'Удалить',
    'loading' => 'Загрузка...',
    'tabBucket' => 'Облачный диск',
    'tabApi' => 'Api',
    'tabNote' => 'Заметки',
    'tabPublicFiles' => 'Общие файлы',
    'tabMoreSpace' => 'Доступное место',
    'tabNFS' => 'Сетевой диск NFS',
    'tabTrash' => 'Корзина',
    'tabDashboard' => 'Статистика',
    'save' => 'Сохранить',
    'all' => 'Все',
    'logOut' => 'Выйти',
    'login' => 'Войти',
    'notRegisterRequest' => 'Не зарегистрированы?',
    'register' => 'Регистрация',
    'tabPost' => 'Лента новостей',
    'tabChat' => 'Чаты',
    'comments' => 'Комментариии',
    'processing' => 'Обработка',
    'balance' => 'Баланс',
    'day' => 'день',
    'month' => 'месяц',
    'forever' => 'навсегда',
    'until' => 'до',
    'date' => 'дата',
    'description' => 'описание',
    'amount' => 'сумма',
    'price' => 'Цена',
    'tmp' => 'временная',
];
