<?php

return [
    'getMoneyFromDisk' => 'Плата за облачный диск: :hours GB/час (€:price)',
    'getMoneyFromNFS' => 'Плата за NFS :server: :hours час (€:price)',
    'fillUp' => 'Пополнение баланса на €:eur',
];
