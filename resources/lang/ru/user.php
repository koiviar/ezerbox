<?php

return [
    'modalTitleAccountInfo' => 'Мой аккаунт',
    'name' => 'Имя',
    'email' => 'Email',
    'cash' => 'Баланс',
    'changePass' => 'Изменить пароль',
    'pass' => 'Пароль',
    'pass2' => 'Повторите пароль',
    'login' => 'Вход',
    'register' => 'Регистрация',
    'remember' => 'Запомнить меня',
    'forgotPass' => 'Забыли пароль?',
    'loginA' => 'Вход',
    'registerA' => 'Регистрация',
    'loginName' => 'Логин',
];
