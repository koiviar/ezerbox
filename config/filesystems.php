<?php

$scaleBuckets = 1;
$scale = [];
for ($i = 1; $i <= $scaleBuckets; $i++) {
    $scale["ez$i"] = [
        'driver' => 's3',
        'key' => 'SCWSFW7K1FWDQVFHHRXB',
        'secret' => 'd852893e-b259-40ae-8efc-f5740ad58880',
        'bucket' => "ez$i",
        'endpoint' => 'https://s3.nl-ams.scw.cloud',
        'region' => 'nl-ams',
    ];
}

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => array_merge([

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'tmp' => [
            'driver' => 'local',
            'root' => storage_path('app/tmp'),
        ],

	    'provider' => [
            'driver' => 'local',
            'root' => storage_path('app/provider_folder'),
        ],

        'storage' => [
            'driver' => 'local',
            'root' => storage_path(),
        ],

        'pp' => [
            'driver' => 'local',
            'root' => public_path('p'),
            'url' => env('APP_URL').'/p',
            'visibility' => 'public',
        ],

        'public' => [
            'driver' => 'local',
            'root' => public_path(),
            'url' => env('APP_URL'),
            'visibility' => 'public',
        ],

        'resource' => [
            'driver' => 'local',
            'root' => resource_path(),
            'url' => env('APP_URL'),
        ],

        // scale
        'miniatures' => [
            'driver' => 's3',
            'key' => 'SCWSFW7K1FWDQVFHHRXB',
            'secret' => 'd852893e-b259-40ae-8efc-f5740ad58880',
            'bucket' => 'miniatures',
            'endpoint' => 'https://s3.nl-ams.scw.cloud',
            'region' => 'nl-ams',

            'cache' => [
                'store' => env('CACHE_DRIVER', 'memcached'),
                'expire' => 3600,
                'prefix' => 'co1',
            ],
        ],

        'wasabi' => [
            'driver' => 's3',
            'key' => '7D3D1CJVGGEC3DH7FJST',
            'secret' => 'TBbBPD4cyJSlsBpjrnECOtf75FAyeWXk7SOhM965',
            'bucket' => 'ezer',
            'endpoint' => 'https://s3.eu-central-1.wasabisys.com',
            'region' => 'eu-central-1',
            'throw' => true
        ],

        'ms1' => [
            'driver' => 's3',
            'key' => 's3DBKasZq0KvzXgL',
            'secret' => 'sDpa8P27RCKPu0Y9YBYw4j80yBe9pcxD',
            'bucket' => 'ms1',
//            'bucket_endpoint' => true,
            'use_path_style_endpoint' => true,
            'endpoint' => 'https://m1.storage.ezerbox.com:9000/ms1',
            'region' => 'moscow',
            'throw' => true
        ],

        'icedrive' => [
            'driver'     => 'webdav',
            'baseUri'    => 'https://webdav.icedrive.io',
            'userName'   => 'gaydismikhail@gmail.com',
            'password'   => 'E5B6_7AmbFFe897ff4',
            //'pathPrefix' => 'backups', // optional
        ],

        'co1' => [
            'driver' => 's3',

            'key' => 'KkIsbWkBXOqj02DN27lt',
            'secret' => 'PzXAKeu5TxvSuw9QzxGG_G8KC41vlQ9W4Lp9RPb-',
            'bucket' => 'co1',
            'endpoint' => 'https://storage.yandexcloud.net',
            'region' => 'ru-central1',

            // Other Disk Options...

            'cache' => [
                'store' => env('CACHE_DRIVER', 'memcached'),
                'expire' => 3600,
                'prefix' => 'co1',
            ],
        ],

        // clo cloud
        'clo' => [
            'driver' => 's3',

            'key' => 'CZ7F01ZIMZQSLZ9HQHVM',
            'secret' => '0NMJ2EBJeCdu9EetsX7zj1o33pkWnBIgezsYpUS5',
            'bucket' => 'ezerbox',
            'use_path_style_endpoint' => true,
            'endpoint' => 'https://storage.clo.ru/ezerbox',
            'region' => 'us-east-1',

            // Other Disk Options...
            'cache' => [
                'store' => env('CACHE_DRIVER', 'memcached'),
                'expire' => 3600,
                'prefix' => 'clo',
            ],
        ],

        'b2' => [
            'driver'         => 'b2',
            'accountId'      => 'be41eaa7a817',
            'applicationKey' => '002b186486a94025ee92df175f720a46735a825b0f',
            'bucketName'     => 'ezerbox',
            'bucketId'       => '1b9ea431be4a4a477ab80117', //optional
        ],

        // ftp сервера
        'testus' => [
            'driver' => 'sftp',
            'host'     => '176.223.128.51',
            'username' => 'ftpuser',
            'password' => '4872ezer',
            'root'     => '/home/ftpuser/ftp',
            'timeout'  => 7200,
        ],

        'nd' => [
            'driver' => 'sftp',
            'host'     => '176.223.128.51',
            'username' => 'admin',
            'password' => '27741248Kk',
            'root'     => '/home/admin/web/ftp',
            'timeout'  => 7200,
        ],
        'nd_p' => [
            'driver' => 'sftp',
            'host'     => '176.223.128.51',
            'username' => 'admin',
            'password' => '27741248Kk',
            'root'     => '/home/admin/web/nd.ezerbox.com/public_html',
            'timeout'  => 7200,
        ],

        'n1' => [
            'driver' => 'sftp',
            'host'     => env('SERVER_IP_N1', 'n1.ezerbox.com'),
            'username' => 'root',
            'password' => '7e7RE4Y0Ryrr',
            'root'     => '/var/www/ftp',
            'timeout'  => 7200,
        ],
        'n1_p' => [
            'driver' => 'sftp',
            'host'     => env('SERVER_IP_N1', 'n1.ezerbox.com'),
            'username' => 'root',
            'password' => '7e7RE4Y0Ryrr',
            'root'     => '/var/www/html',
            'timeout'  => 7200,
        ],
    ], $scale),

];
