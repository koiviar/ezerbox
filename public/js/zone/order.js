var order = {

    init: function() {

        $('body').on('click', '.showBalanceModal', function () {
            order.showBalanceModal();
        });

        $('body').on('keyup', '.fill-up .eur-money', function () {
            order.getOrderPrice($(this).val());
        });

        $('body').on('click', '.checkout', function () {
            let data = {};
            data.count = $('.fill-up .eur-money').val();

            order.createOrder(data, $(this));
        });
    },

    getOrderPrice: function(count) {
        $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/order/getOrderPrice',
            data: {
                count: count
            },
            dataType:'json',
            success:function(data) {
                $('.fill-up .rub-money').html(data.price_ru);
            }
        });
    },

    createOrder: function (data, button) {
        main.lockButton(button);
        $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/order/createOrder',
            data: data,
            dataType:'json',
            success:function(data) {
                location.href = data.form;
                // $('body').append(data.form);
                // $('.submitFormToPay').submit();
                // main.unlockButton(button);
            }
        });
    },

    showBalanceModal: function (moneyPreFill = 4.99) {
        $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/order/getBalanceModal',
            data: {
                moneyPreFill: moneyPreFill
            },
            dataType:'json',
            success:function(data) {
                $('#balanceModal').remove();
                $('body').append(data.modal);
                $('#balanceModal').modal('show');
            }
        });
    }

};

order.init();
