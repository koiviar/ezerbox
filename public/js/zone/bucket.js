var bucket = {

	dir: 0,
	fileId: 0,
	selectedType: '',
	selectedId: 0,

	ctrlActive: false,
	shiftActive: false,

	timeOffset: 0,

    uploader: undefined,

    ajax: undefined,

	init: function() {

		var dt = new Date();
     	bucket.timeOffset = dt.getTimezoneOffset() / 60;

		if (Cookies.get('bucketDir')) {
			bucket.dir = Cookies.get('bucketDir');
		}

        bucket.uploaderInit();

		$('body').on('click', '.createDir', function() {
			bucket.createDir();
		});

		// удаляем папку кликнув по ней
		$('body').on('click', '.deleteDir', function() {
			if(!confirm('Удалить?')) return false;
			ent = {};
			ent[0] = {};
			ent[0]['id'] = $(this).data('id');
			ent[0]['type'] = 'dir';
			bucket.deleteEnt(ent);
		});

		// удаляем файл кликнув по нему
		$('body').on('click', '.deleteFile', function() {
			if(!confirm('Удалить?')) return false;
			ent = {};
			ent[0] = {};
			ent[0]['id'] = $(this).data('id');
			ent[0]['type'] = 'file';
			bucket.deleteEnt(ent);
		});

		$('body').on('click', '.dirLink', function() {
			bucket.dir = $(this).closest('tr').data('id');
			bucket.getList();
		});

		$('body').on('click', '.dirNav', function() {
			bucket.dir = $(this).data('id');
			bucket.getList();
		});

		$('body').on('click', '.select-all', function() {
			if($(this).prop('checked')) {
				$('.table .files .custom-checkbox input').prop('checked', true);
			} else {
				$('.table .files .custom-checkbox input').prop('checked', false);
			}
		});

		$('body').on('click', '.deleteSelected', function() {
			// if(!confirm('Удалить отмеченное?')) return false;
			ent = {};
			i = 0
			$('.element-item.active').each(function() {
				ent[i] = {};
				ent[i]['id'] = $(this).data('id');
				ent[i]['type'] = $(this).data('type');
				i++;
			});
			main.lockButton('.deleteSelected');
			bucket.deleteEnt(ent);
		});

		$('body').on('click', '.renameEnt', function() {
            $('.element-item.active').each(function() {
                id = $(this).data('id');
                type = $(this).data('type');
                name = $(this).find('.name').text();
            });
			// id = $(this).closest('tr').data('id');
			// type = $(this).closest('tr').data('type');
			$('#renameEntModal').modal('show').data('id', id).data('type', type);
			$('.renameEntInput').val(name);
		});

		$('body').on('click', '.renameEntStart', function() {
			bucket.rename();
		});

		$('body').on('click', '.moveEntStart', function() {
			ent = {};
			i = 0
			// $('.table tr').each(function() {
			// 	if($(this).find('.custom-checkbox input').prop('checked')) {
			// 		ent[i] = {};
			// 		ent[i]['id'] = $(this).data('id');
			// 		ent[i]['type'] = $(this).data('type');
			// 		i++;
			// 	}
			// });
			$('.element-item.active').each(function() {
				// if($(this).find('.custom-checkbox input').prop('checked')) {
					ent[i] = {};
					ent[i]['id'] = $(this).data('id');
					ent[i]['type'] = $(this).data('type');
					i++;
				// }
			});
			bucket.move(ent);
		});

		$('body').on('click', '.getFileInfo', function() {
			bucket.rowIndex = $('.getFileInfo').index(this);
			bucket.getFileInfo($(this).closest('tr').data('id'));
		});

		//появление дропзоны
		$(document).on('drag dragstart dragover dragenter', function(e) {
			$('#dropPrepare').addClass('dropPrepare');
			// $('.mg-admin-html #add-product-wrapper .img-dropzone').show()
		});

		//исчезновение дропзоны
		$(document).on('dragend drop mouseleave mouseout', function(e) {
			$('#dropPrepare').delay(1000).removeClass('dropPrepare');
			// $('.mg-admin-html #add-product-wrapper .img-dropzone').delay(1000).hide();
		});

		$('body').on('click', '.imgPreview .leftArrow, .imgPreview .rightArrow', function() {
			bucket.getFileInfo($(this).data('id'));
		});

		$('body').on('click', '.createPublicLink', function() {
			bucket.createPublicLink();
		});

		$('body').on('click', '.deletePublicLink', function() {
			bucket.deletePublicLink();
		});

		$('body').on('click', '.files .element-item', function() {
			if ($(this).hasClass('active')) {
				if ($(this).data('type') == 'dir') {
					bucket.dir = $(this).data('id');
					Cookies.set('bucketDir', bucket.dir, {expires: 1/24});
					bucket.getList();
				} else {
					bucket.rowIndex = $('.element-file').index(this);
					bucket.getFileInfo($(this).data('id'));
				}
			} else {
				if (!bucket.ctrlActive && !bucket.shiftActive) {
					$('.files .element-item').removeClass('active');
				}

				$(this).addClass('active');

				setTimeout(function() {
					var count = $('.files .element-item.active').length - 2;
					if (bucket.shiftActive) {
						var active = false;
						$('.files .element-item').each(function(index) {
							if ($(this).hasClass('active')) {
								if (active) {
									if (count > 0) {
										count--;
									} else {
										active = false;
									}
								} else {
									active = true;
								}
							}

							if (active) {
								$(this).addClass('active');
							}
						});
					}
				}, 0);

				bucket.selectedType = $(this).data('type');
				bucket.selectedId = $(this).data('id');
			}

			bucket.renderFilesControll();

		});

		document.addEventListener('keydown', function(event) {
			if (event.code == 'ControlLeft' || event.code == 'ControlRight') {
				bucket.ctrlActive = true;
			}

			if (event.code == 'ShiftLeft' || event.code == 'ShiftRight') {
				bucket.shiftActive = true;
			}

			if (event.code == 'Escape') {
				$('.files .element-item').removeClass('active');
				bucket.renderFilesControll();
			}
		});

		document.addEventListener('keyup', function(event) {
			if (event.code == 'ControlLeft' || event.code == 'ControlRight') {
				bucket.ctrlActive = false;
			}

			if (event.code == 'ShiftLeft' || event.code == 'ShiftRight') {
				bucket.shiftActive = false;
			}
		});

		$('body').on('click', '.files .unselect-zone, .unselect-files', function() {
			$('.files .element-item').removeClass('active');
			bucket.renderFilesControll();
		});

		$(window).resize(function() {
			bucket.resizeUnselectedZone();
		});

		setInterval(function() {
			trigger = $('.loadMoreFilesTrigger');

			if(
				trigger.offset() &&
				main.isTopEdgeVisible(trigger) &&
				trigger.data('lastChecked') != 'true'
			) {
				trigger.data('lastChecked', 'true');
				bucket.getList(false, $('.element-file').length);
			}
		}, 1000);

		bucket.getList(true);

        // адаптивное расположение кнопок управления
        bucket.fixTopButtons();
        $(document).on('scroll', function() {
            bucket.fixTopButtons();
        });
        $('.files').resize(function() {
            bucket.fixTopButtons();
        });

		// адаптивное расположение информации о папке внизу рабочей области
		bucket.fixFolderInfoPosition();
		$(document).on('scroll', function() {
            bucket.fixFolderInfoPosition();
        });
        $('.files').resize(function() {
            bucket.resizeUnselectedZone();
        });

        $('body').on('click', '.element-item', function () {
            if ($(this).data('type') === 'file' && $('.element-item.active').length === 1) {
                $('.download-button').show().attr('href', '/disk/download/' + $(this).data('code'));
            } else {
                $('.download-button').hide();
            }
        });

        // следим за состоянием окна модалки, чтобы убрать контент, когда больше не нужен
        setInterval(function () {
            if (!$('#fileInfoModal .modal-content').is(':visible')) {
                bucket.clearFileInfoModal();
            }
        }, 1000);

        // $('.files img').bind('load', function() {
        //     bucket.loadGalleryImage();
        // });
	},

    loadGalleryImage: function() {
        $('.files img').each(function () {
            if (!$(this).attr('src')) {
                $(this).attr('src', $(this).data('src'));
                return false;
            }
        });
    },

    fixTopButtons: function() {
	    var selectorButtons = '.files-controll';
        var offset = '15px';
	    if ($(window).scrollTop() > 0) {
            $(selectorButtons).addClass('fixed');
            if ($('body').hasClass('sideBarHide')) {
                $(selectorButtons).css('left', offset);
            } else {
                $(selectorButtons).css('left', '265px');
            }
            $('.files').css('padding-top', $('.files-controll').innerHeight() - 15);
        } else {
            $('.files').css('padding-top', 0);
            $(selectorButtons).removeClass('fixed').css('left', '15px');
        }
    },

    fixFolderInfoPosition: function() {
	    var selectorDir = '.dirInfoBar';
	    var offset = '15px';
	    if (!main.isBottomEdgeVisible('.main-container-padding')) {
            $(selectorDir).css('position', 'fixed').css('bottom', 0).css('right', offset);
            if ($('body').hasClass('sideBarHide')) {
                $(selectorDir).css('left', offset);
            } else {
                $(selectorDir).css('left', '265px');
            }
        } else {
            $(selectorDir).css('position', 'absolute').css('bottom', 0).css('left', '15px').css('right', '15px');
        }
    },

    renderFilesControll: function() {
	    if ($('.element-item.active').length) {
            $('.files-not-selected').hide();
            $('.files-selected').show();

            if ($('.element-item.active').length == 1) {
                $('.renameEnt').show();
            } else {
                $('.renameEnt').hide();
            }
        } else {
            $('.files-not-selected').show();
            $('.files-selected').hide();
        }
    },

	resizeUnselectedZone: function() {
		height = $('body').height() - $('.files-controll').height() - 39 - 36 - 15 - 15 - 70;
		$('.unselect-zone').css('height', height + 'px');
	},

	createPublicLink: function() {
		main.lockButton('.createPublicLink');
		if (bucket.ajax != undefined) bucket.ajax.abort();
        bucket.ajax = $.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/disk/createPublicLink',
			data:{
				'id':bucket.fileId
			},
			dataType:'json',
			success:function(data) {
				main.unlockButton('.createPublicLink');

				bucket.renderPublicLinkInfo(data);
			}
		});
	},

	deletePublicLink: function() {
		main.lockButton('.deletePublicLink');
		if (bucket.ajax != undefined) bucket.ajax.abort();
        bucket.ajax = $.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/disk/deletePublicLink',
			data:{
				'id':bucket.fileId
			},
			dataType:'json',
			success:function(data) {
				main.unlockButton('.deletePublicLink');

				bucket.renderPublicLinkInfo(data);
			}
		});
	},

	getFileInfo: function(id) {
		bucket.fileId = id;

		$('#fileInfoModal').modal('show');
		// $('#fileInfoModal .preloader').show();
		// $('#fileInfoModal .infoContent').hide();

        bucket.clearFileInfoModal();

		if (bucket.ajax != undefined) bucket.ajax.abort();
        bucket.ajax = $.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/disk/getFileInfo',
			data:{
				'id':id,
			},
			dataType:'json',
			success:function(response) {
				var data = response.file;

				$('#fileInfoModal .preloader').hide();
				$('#fileInfoModal .infoContent').show();

				var link = '//' + location.host + '/d/' + data.code;
				var linkView = location.host + '/d/' + data.code;

				if (location.host == 'ezerbox.com') {
					link = '//s.' + location.host + '/d/' + data.code;
					linkView = 's.' + location.host + '/d/' + data.code;
				}

				if (data.download_link) {
                    $('#fileInfoModal .link').attr('href', data.download_link).show();
                    $('#fileInfoModal .linkView').text('ezerbox.com/...');
                } else {
                    $('#fileInfoModal .link').hide();
                    $('#fileInfoModal .linkView').text(lang.get('processing'));
                }

				$('#fileInfoModal .fileTitle').text(data.name);
				$('#fileInfoModal .size').text(data.size);
				$('#fileInfoModal .type').text(data.ex_typeH);
				$('#fileInfoModal .createdAt').text(data.created_atH);
				$('#fileInfoModal .updatedAt').text(data.updated_atH);

				$('#fileInfoModal .views').text(data.views);
				$('#fileInfoModal .downloads').text(data.downloads);

				bucket.renderPublicLinkInfo(data);

                if (data.ex_type == 'image') {
                    $('#fileInfoModal .imgPreview img, #fileInfoModal .imgPreview .icon').remove();
                    $('#fileInfoModal .imgPreview').append('<img>');
                    // $('#fileInfoModal .imgPreview img').show().attr('src', link);
                    $('#fileInfoModal .imgPreview img').show().attr('src', data.download_link);
                    $('#fileInfoModal .image-preloader').show();
                    $('#fileInfoModal .imgPreview video').hide();
                } else if (data.ex_type == 'video') {
                    $('#fileInfoModal .imgPreview video, #fileInfoModal .imgPreview .icon').remove();
                    $('#fileInfoModal .imgPreview').append('<video></video>');
                    // $('#fileInfoModal .imgPreview video').show().attr('src', link).prop('controls', true);
                    $('#fileInfoModal .imgPreview video').show().attr('src',  data.download_link).prop('controls', true);
                    $('#fileInfoModal .image-preloader').show();
                    $('#fileInfoModal .imgPreview img').hide();
                } else {
                    $('#fileInfoModal .imgPreview img, #fileInfoModal .imgPreview .icon').remove();
                    $('#fileInfoModal .imgPreview video, #fileInfoModal .imgPreview .icon').remove();
                    $('#fileInfoModal .imgPreview').append('<i class="fas fa-file icon"></i>');
                    $('#fileInfoModal .image-preloader').hide();
                }

				// для стрелочек перелистывания вайлов
				if(response.left) {
					$('.imgPreview .leftArrow').show().data('id', response.left);
				} else {
					$('.imgPreview .leftArrow').hide();
				}
				if(response.right) {
					$('.imgPreview .rightArrow').show().data('id', response.right);
				} else {
					$('.imgPreview .rightArrow').hide();
				}
			}
		});
	},

	move: function(ent) {
		main.lockButton('.moveEntStart');
		if (bucket.ajax != undefined) bucket.ajax.abort();
        bucket.ajax = $.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/disk/move',
			data:{
				'targetDir':$('#moveEntModal .selectDirMove').val(),
				'ent':ent,
                'dir':bucket.dir,
			},
			dataType:'json',
			success:function(data) {
				main.unlockButton('.moveEntStart');
				// bucket.getList();
                for(i = 0; i < Object.keys(ent).length; i++) {
                    $('.element-item[data-id=' + ent[i]['id'] + ']').closest('.element-item-parent').remove();
                    // $('tr[data-type='+ent[i]['type']+'][data-id='+ent[i]['id']+']').detach();
                }
                bucket.renderFilesControll();
                bucket.renderBucketStats(data);
			}
		});
	},

	rename: function(ent) {
		id = $('#renameEntModal').data('id');
		type = $('#renameEntModal').data('type');
		newName = $('.renameEntInput').val();
		main.lockButton('.renameEntStart');
		if (bucket.ajax != undefined) bucket.ajax.abort();
        bucket.ajax = $.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/disk/rename',
			data:{
				'id':id,
				'type':type,
				'newName':newName,
			},
			dataType:'json',
			success:function(data) {
				$('#renameEntModal').modal('hide');
				// $('tr[data-type='+type+'][data-id='+id+'] .entName .name').text(newName);
                $('.element-item.active').each(function() {
                    $(this).find('.name').text(newName);
                });
				main.unlockButton('.renameEntStart');
			}
		});
	},

	deleteEnt: function(ent) {
		if (bucket.ajax != undefined) bucket.ajax.abort();
        bucket.ajax = $.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/disk/deleteEnt',
			data:{
				'ent':ent,
                'dir':bucket.dir,
                'dirListLoad':true,
			},
			dataType:'json',
			success:function(data) {
				for(i = 0; i < Object.keys(ent).length; i++) {
					$('.element-item[data-id=' + ent[i]['id'] + ']').closest('.element-item-parent').remove();
					// $('tr[data-type='+ent[i]['type']+'][data-id='+ent[i]['id']+']').detach();
				}
				$('.usedSpace').html(data.used);
				$('.spaceAvailable').html(data.spaceAailable);
				main.unlockButton('.deleteSelected');
				// bucket.getList(true);
				bucket.renderFilesControll();
                bucket.renderBucketStats(data);
			}
		});
	},

	createDir: function() {
		main.lockButton('.createDir');
		if (bucket.ajax != undefined) bucket.ajax.abort();
        bucket.ajax = $.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/disk/createDir',
			data:{
				'name':$('.createDirName').val(),
				'dir':bucket.dir,
                'dirListLoad':true,
			},
			dataType:'json',
			success:function(data) {
				$('.element-dir').parent().detach();
				$('.files').prepend(data.dirs);
				$('#createDirModal').modal('hide');
				main.unlockButton('.createDir');

				$('.unselect-zone').remove();
				$('.files').prepend('<div class="unselect-zone"></div>');
				bucket.resizeUnselectedZone();
                bucket.renderBucketStats(data);
			}
		});
	},

	getList: function(dirListLoad = false, filesOffset = 0) {
	    // обновляем сохраненную директорию в куках
        Cookies.set('bucketDir', bucket.dir, {expires: 1/24});
        bucket.uploaderSetDir();

		data = {};
		data['dir'] = bucket.dir;
		data['dirListLoad'] = dirListLoad;
		data['offset'] = filesOffset;
		if (bucket.ajax != undefined) bucket.ajax.abort();
        bucket.ajax = $.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/disk/getList',
			data:data,
			dataType:'json',
			success:function(data) {
				if (filesOffset === 0) {
					$('.files').html(data.dirs);
					$('.files').append(data.files);
				} else {
					$('.loadMoreFilesTrigger').remove();
					$('.files').append(data.files);
				}

				$('.files').prepend('<div class="unselect-zone"></div>');
				bucket.resizeUnselectedZone();
				bucket.renderBucketStats(data);

				$('.select-all').prop('checked', false);

				$('.zonePreloader').hide();

				if (data.drawFileLoader) {
					$('.files').append('<div class="col-lg-2 pr-0 element-item-parent loadMoreFilesTrigger">' +
							'<div class="element-item element-dir" data-id="5" data-type="dir">' +
								'<div class="image">' +
									'<i class="fa fa-cog fa-spin"></i>' +
								'</div>' +
								'<span class="name">Loading</span>' +
							'</div>' +
						'</div><span class=""></span>');
				}

				bucket.renderFilesControll();
				bucket.fixFolderInfoPosition();

				// перезагрузка изображения в случае фейла
				$('.element-item img').bind('error', function() {
					console.log($(this).attr('src'));
					var img = $(this);
					setTimeout(function () {
                        var src = img.attr('src').split('?')[0] + '?' + Date.now();
						img.attr('src', src);
					}, 1000);
				});

				// показ изображений после прогрузки
				$('.element-item img').bind('load', function() {
					$(this).show();
				});

                // $('.files img').bind('load', function() {
                //     bucket.loadGalleryImage();
                // });
                //
                // bucket.loadGalleryImage();
			},
            error:function () {
                setTimeout(function () {
                    bucket.getList(dirListLoad, filesOffset);
                }, 3000);
            }
		});
	},

    renderBucketStats: function(data) {
        $('.usedSpace').html(data.diskInfo.used);
        $('.spaceAvailable').html(data.diskInfo.spaceAvailable);
        $('.moreSpace .progress-bar').css('width', data.diskInfo.percentUsed + '%');
        $('.dirNavigation').html(data.nav);

        $('.dirInfoBar .foldersB').html(data.folderInfo.dirs);
        $('.dirInfoBar .filesB').html(data.folderInfo.files);
        $('.dirInfoBar .sizeB').html(data.folderInfo.size);

        if(data.dirList) $('.dirListSelect').html(data.dirList);

        // if(data.percentUsed > 90) {
        //     $('.moreSpace .progress-bar').addClass('bg-danger').removeClass('bg-warning');
        // } else if(data.percentUsed > 75) {
        //     $('.moreSpace .progress-bar').removeClass('bg-danger').addClass('bg-warning');
        // } else {
        //     $('.moreSpace .progress-bar').removeClass('bg-danger').removeClass('bg-warning');
        // }
    },

	renderPublicLinkInfo: function(data) {
		if(data.public_code != '' && data.public_code != null) {
			var link = location.host + '/shared/' + data.public_code;
			var directLink = 'cdn.' + location.host + '/p/' + data.public_code + data.extension;

			$('#fileInfoModal .publicLink').text(link).attr('href', '//' + link).show();
			$('#fileInfoModal .publicDirectLink').text(directLink).attr('href', '//' + directLink).show();

			$('#fileInfoModal .publicInfoBlock').show();

			$('#fileInfoModal .deletePublicLink').show();
			$('#fileInfoModal .createPublicLink').hide();
		} else {
			$('#fileInfoModal .publicInfoBlock').hide();

			$('#fileInfoModal .deletePublicLink').hide();
			$('#fileInfoModal .createPublicLink').show();
		}
	},

    uploaderInit: function () {
        // загрузчик файлов
        bucket.uploader = new plupload.Uploader();
        bucket.uploaderSetDir();
        bucket.uploader.init();
        bucket.uploader.bind('FilesAdded', function(up, files) {
            bucket.allSize = 0;
            bucket.allSizeNow = 0;

            var html = '<div class="list">';
            plupload.each(files, function(file) {
                bucket.allSize += file.size;
                html += '<div id="' + file.id + '">' + file.name +
                    '<span style="float: right"> &nbsp;&nbsp;&nbsp;(' + plupload.formatSize(file.size) + ')</span>' +
                    '<div class="progress">' +
                    '<div class="progress-bar" role="progressbar" style="width: 0;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>' +
                    '</div></div>';
            });

            // общий загрузка прогресс
            bucket.allSizeNow = bucket.allSize;
            html += '</div><div id="total-progress">Total ' +
                '<span style="float: right"> &nbsp;&nbsp;&nbsp;(' + plupload.formatSize(bucket.allSize) + ')</span>' +
                '<div class="progress">' +
                '<div class="progress-bar" role="progressbar" style="width: 0;" data-max="' + bucket.allSize + '">0%</div>' +
                '</div></div>';
            $('#filelist').html(html);

            // проверка баланса
            if (bucket.ajax != undefined) bucket.ajax.abort();
            bucket.ajax = $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url: '/disk/check-money',
                data: {
                    size: bucket.allSize
                },
                dataType: 'json',
                success: function (data) {
                    if (data.error === 0) {

                        $('.uploader-files-list').show();
                        bucket.uploader.start();
                        bucket.uploadInProgress = true;

                    } else {
                        plupload.each(files, function(file) {
                            bucket.uploader.removeFile(file);
                        });

                        if (data.error === 1) {
                            Swal.fire({
                                title: 'Not enough money!',
                                text: "To use over limits storage, your balance must be more than " + data.data.need + ' €',
                                icon: 'error',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Add money'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    order.showBalanceModal(data.data.to_fill);
                                }
                            });
                        } else if (data.error === 2) {
                            Swal.fire({
                                title: data.msg,
                                text: data.data.text,
                                icon: 'error',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Change plan'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.href = '/plans';
                                }
                            });
                        }

                    }
                }
            });
        });
        bucket.uploader.bind('FileUploaded', function(up, file) {
            $('#' + file.id + ' .progress-bar').addClass('bg-success');

            bucket.allSizeNow -= file.size;
            let percent = (bucket.allSize - bucket.allSizeNow) / (bucket.allSize / 100);
            percent = (Math.round(percent * 100)) / 100;
            $('#total-progress .progress-bar').css('width', percent + '%').html(percent + '%');
        });
        bucket.uploader.bind('UploadProgress', function(up, file) {
            $('#' + file.id + ' .progress-bar').css('width', file.percent + '%').html(file.percent + '%');
        });
        bucket.uploader.bind('UploadComplete', function () {
            $('#total-progress .progress-bar').css('width', 100 + '%').html(100 + '%').addClass('bg-success');

            bucket.uploadInProgress = false;
            bucket.uploader._options.headers.dir = bucket.dir;
            setTimeout(function () {
                $('.uploader-files-list').hide();
            }, 3000);
            bucket.getList();
        });
    },

    uploaderSetDir: function () {
        if (!bucket.uploadInProgress) {
            bucket.uploader.setOptions({
                browse_button: 'browse', // this can be an id of a DOM element or the DOM element itself
                url: '/disk/upload-chunk',
                chunk_size: '5000kb',
                max_retries: 10,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'dir': bucket.dir
                },
                drop_element: 'dropPrepare'
            });
        }
    },

    clearFileInfoModal: function () {
        $('#fileInfoModal .imgPreview img, #fileInfoModal .imgPreview .icon').remove();
        $('#fileInfoModal .imgPreview video, #fileInfoModal .imgPreview .icon').remove();
        $('#fileInfoModal .image-preloader').show();
    },
}

bucket.init();
