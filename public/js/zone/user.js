var user = {

	init: function() {

		$('body').on('click', '.openChangePass', function() {
			$(this).hide();
			$('.changePassFields').show();
		});

		$('body').on('click', '.savePas', function(e) {
			e.preventDefault();
			user.savePas();
		});

		$('body').on('click', '.deleteAvatar', function(e) {
			if(!confirm('Delete?')) return false;
			user.deleteAvatar();
		});

		$('body').on('change', '#avatarUploader', function() {
			$("form.avatarUploadForm").submit();
		});

		// загрузчик файлов
		$('form.avatarUploadForm').submit(function(e) { // capture submit
			e.preventDefault();
			var fd = new FormData(this); // XXX: Neex AJAX2

			// You could show a loading image for example...
			// main.lockButton('.uploaderLabel', 'Загрузка');
			console.log('start');

			$.ajax({
				headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				url: $(this).attr('action'),
				xhr: function() { // custom xhr (is the best)
					// $('.uploader-progress').removeClass('invisible');

				 	var xhr = new XMLHttpRequest();
				 	var total = 0;

				 	// Get the total size of files
				 	$.each(document.getElementById('avatarUploader').files, function(i, file) {
				 	       total += file.size;
				 	});

				 	// Called when upload progress changes. xhr2
				 	xhr.upload.addEventListener("progress", function(evt) {
				 		// show progress like example
				 		var loaded = (evt.loaded / total).toFixed(2)*100; // percent
				 		// }
				 	}, false);

				 	return xhr;
				},
				type: 'POST',
				processData: false,
				contentType: false,
				data: fd,
				dataType:'json',
				success: function(data) {
					console.log('ok');
					user.setAvatars(data.path);
				},
				error: function() {
					console.log('fail');
				}
			});
		});

	},

	setAvatars: function(path) {
		$('.avatar-100').attr('src', path.replace('###', '100'));
		$('.avatar-300').attr('src', path.replace('###', '300'));
	},

	deleteAvatar: function() {
		$.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/user/deleteAvatar',
			data:{},
			dataType:'json',
			success:function(data) {
				user.setAvatars(data.path);
			}
		});
	},

	savePas: function() {
		if(($('.pas1').val() != $('.pas2').val()) || ($('.pas1').val() == '')) {
			$('.pas1, .pas2').addClass('inputError');
			return false;
		} else {
			$('.pas1, .pas2').removeClass('inputError');
		}
		main.lockButton('.savePas');

		$.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/user/changePas',
			data:{
				'pas':$('.pas1').val()
			},
			dataType:'json',
			success:function(data) {
				$('.openChangePass').show();
				$('.changePassFields').hide();
				main.unlockButton('.savePas');
			}
		});
	}

}

user.init();