let nfs = {

	init: function() {

        nfs.getList();

        $('body').on('click', '.add-nfs', function () {
            nfs.addNfs($('#nfs-size').val(), $('#nfs-size option:selected').text());
        });

        $('body').on('click', '.delete-nfs', function () {
            nfs.deleteNfs($(this).closest('.nfs').data('id'));
        });

        $('body').on('click', '.box-control .connect', function () {
            nfs.connect($(this).closest('.nfs').data('id'));
        });

        $('body').on('click', '.box-control .reload', function () {
            nfs.reload($(this).closest('.nfs').data('id'));
        });

        setInterval(function () {
            $('.nfs').each(function () {
                if($(this).find('.status').text() !== 'Active') {
                    nfs.updateOne($(this).data('id'));
                }
            });
        }, 5000);
	},

    connect: function(id) {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'POST',
            url: '/nfs/connect-info',
            data: {
                id: id
            },
            dataType:'json',
            success: function(data) {
                $('#connect-modal .info .ip').text(data.ip);
                $('#connect-modal .info .host').text(data.host);
                $('#connect-modal .info .pwd').text(data.pwd);

                $('#connect-modal').modal('show');
            }
        });
    },

    reload: function(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Server will not be able to a few minutes!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, reboot my server!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: '/nfs/reload',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function (data) {
                        nfs.updateOne(id);
                    }
                });
            }
        });
    },

    deleteNfs: function(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:'/nfs/delete',
                    data: {
                        id: id
                    },
                    dataType:'json',
                    success: function(data) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Your NFS has been deleted',
                            showConfirmButton: true,
                            timer: 2500
                        });

                        nfs.getList();
                    }
                });
            }
        });
    },

    addNfs: function(size, text) {
        Swal.fire({
            title: 'NFS ' + text,
            text: "Confirm this action",
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, by it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: '/nfs/create',
                    data: {
                        space: size
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.error === 0) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Your NFS will be added in a few minutes',
                                showConfirmButton: true,
                                timer: 2500
                            });

                            nfs.getList();
                        } else {
                            if (data.code === 99) {
                                Swal.fire({
                                    title: 'Not enough money!',
                                    text: "To by this server, your balance must be more than " + data.value + ' €',
                                    icon: 'error',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Add money'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        order.showBalanceModal(data.value);
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    },

    getList: function() {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/nfs/list',
            data: {},
            dataType:'json',
            success:function(data) {
                $('.nfs-list').html(data.list);

                $('.zonePreloader').remove();
            }
        });
    },

    updateOne: function (id) {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/nfs/getOneView',
            data: {
                id: id
            },
            dataType:'json',
            success:function(data) {
                $('.nfs[data-id=' + id + ']').replaceWith(data.item);
            }
        });
    }
};

nfs.init();
