var publicFile = {

	init: function() {

		$('.searchPublicFiles').keypress(function (e) {
			if (e.which == 13) {
				$('.searchPublicFilesButton').click();
				return false;    //<---- Add this line
			}
		});

		$('body').on('click', '.searchPublicFilesButton', function() {
			searchFrase = $('.searchPublicFiles').val();
			searchFrase = searchFrase.split('/').join(' ');
			searchFrase = searchFrase.split('?').join(' ');
			searchFrase = searchFrase.split('&').join(' ');
			if(searchFrase == '') {
				location.href = '/publicFile';
			} else {
				location.href = '/publicFile/search/' + searchFrase;
			}
		});

		$('body').on('click', '.searchPublicFilesButtonCancel', function() {
			location.href = '/publicFile';
		});

	},

}

publicFile.init();