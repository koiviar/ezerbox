var trash = {

    dir: 0,
    fileId: 0,
    selectedType: '',
    selectedId: 0,

    ctrlActive: false,
    shiftActive: false,

    timeOffset: 0,

    init: function() {

        var dt = new Date();
        trash.timeOffset = dt.getTimezoneOffset() / 60;

        // удаляем файл кликнув по нему
        $('body').on('click', '.deleteFile', function() {
            // if(!confirm('Удалить?')) return false;
            ent = {};
            ent[0] = {};
            ent[0]['id'] = $(this).data('id');
            ent[0]['type'] = $(this).closest('.element-item-parent').data('type');
            trash.deleteEnt(ent);
        });

        $('body').on('click', '.restoreFile', function() {
            // if(!confirm('Удалить?')) return false;
            ent = {};
            ent[0] = {};
            ent[0]['id'] = $(this).data('id');
            ent[0]['type'] = $(this).closest('.element-item-parent').data('type');
            trash.restoreEnt(ent);
        });

        // $('body').on('click', '.dirLink', function() {
        //     trash.dir = $(this).closest('tr').data('id');
        //     trash.getList();
        // });

        $('body').on('click', '.dirNav', function() {
            trash.dir = $(this).data('id');
            trash.getList();
        });

        $('body').on('click', '.select-all', function() {
            if($(this).prop('checked')) {
                $('.table .files .custom-checkbox input').prop('checked', true);
            } else {
                $('.table .files .custom-checkbox input').prop('checked', false);
            }
        });

        $('body').on('click', '.deleteSelected', function() {
            // if(!confirm('Удалить отмеченное?')) return false;
            ent = {};
            i = 0
            $('.element-item.active').each(function() {
                ent[i] = {};
                ent[i]['id'] = $(this).data('id');
                ent[i]['type'] = $(this).data('type');
                i++;
            });
            main.lockButton('.deleteSelected');
            trash.deleteEnt(ent);
        });

        $('body').on('click', '.deleteAll', function() {
            // if(!confirm('Удалить отмеченное?')) return false;
            main.lockButton('.deleteAll');
            trash.deleteAll();
        });

        $('body').on('click', '.restoreSelected', function() {
            // if(!confirm('Удалить отмеченное?')) return false;
            ent = {};
            i = 0
            $('.element-item.active').each(function() {
                ent[i] = {};
                ent[i]['id'] = $(this).data('id');
                ent[i]['type'] = $(this).data('type');
                i++;
            });
            main.lockButton('.restoreSelected');
            trash.restoreEnt(ent);
        });


        $('body').on('click', '.imgPreview .leftArrow, .imgPreview .rightArrow', function() {
            trash.getFileInfo($(this).data('id'));
        });

        $('body').on('click', '.files .element-item', function() {
            if ($(this).hasClass('active') || $(this).prop('tagName') == 'TD') {
                if ($(this).data('type') == 'dir') {
                    trash.dir = $(this).data('id');
                    trash.getList();
                } else {
                    trash.rowIndex = $('.element-file').index(this);
                    trash.getFileInfo($(this).data('id'));
                }
            } else {
                if (!trash.ctrlActive && !trash.shiftActive) {
                    $('.files .element-item').removeClass('active');
                }

                $(this).addClass('active');

                setTimeout(function() {
                    var count = $('.files .element-item.active').length - 2;
                    if (trash.shiftActive) {
                        var active = false;
                        $('.files .element-item').each(function(index) {
                            if ($(this).hasClass('active')) {
                                if (active) {
                                    if (count > 0) {
                                        count--;
                                    } else {
                                        active = false;
                                    }
                                } else {
                                    active = true;
                                }
                            }

                            if (active) {
                                $(this).addClass('active');
                            }
                        });
                    }
                }, 0);

                trash.selectedType = $(this).data('type');
                trash.selectedId = $(this).data('id');
            }

            trash.renderFilesControll();

        });

        $('body').on('change', 'table .custom-control-input', function() {
            $('.files tr').each(function (index) {
                if ($(this).find('.fileOperationChecbox').prop('checked')) {
                    $(this).find('.element-item').addClass('active');
                } else {
                    $(this).find('.element-item').removeClass('active');
                }
            });
        });

        document.addEventListener('keydown', function(event) {
            if (event.code == 'ControlLeft' || event.code == 'ControlRight') {
                trash.ctrlActive = true;
            }

            if (event.code == 'ShiftLeft' || event.code == 'ShiftRight') {
                trash.shiftActive = true;
            }

            if (event.code == 'Escape') {
                $('.files .element-item').removeClass('active');
                trash.renderFilesControll();
            }
        });

        document.addEventListener('keyup', function(event) {
            if (event.code == 'ControlLeft' || event.code == 'ControlRight') {
                trash.ctrlActive = false;
            }

            if (event.code == 'ShiftLeft' || event.code == 'ShiftRight') {
                trash.shiftActive = false;
            }
        });

        $('body').on('click', '.files .unselect-zone, .unselect-files', function() {
            $('.files .element-item').removeClass('active');
            trash.renderFilesControll();
        });

        $(window).resize(function() {
            trash.resizeUnselectedZone();
        });

        setInterval(function() {
            trigger = $('.loadMoreFilesTrigger');

            if(
                trigger.offset() &&
                main.isTopEdgeVisible(trigger) &&
                trigger.data('lastChecked') != 'true'
            ) {
                trigger.data('lastChecked', 'true');
                trash.getList(false, $('.element-file').length);
            }
        }, 1000);

        trash.getList(true);

        trash.fixFolderInfoPosition();
        $(document).on('scroll', function() {
            trash.fixFolderInfoPosition();
        });
        $('.files').resize(function() {
            trash.resizeUnselectedZone();
        });

    },

    fixFolderInfoPosition: function() {
        var selectorDir = '.dirInfoBar';
        if (!main.isBottomEdgeVisible('.main-container-padding')) {
            $(selectorDir).css('position', 'fixed').css('bottom', 0).css('right', '15px');
            if ($('body').hasClass('sideBarHide')) {
                $(selectorDir).css('left', '15px');
            } else {
                $(selectorDir).css('left', '265px');
            }
        } else {
            $(selectorDir).css('position', 'absolute').css('bottom', 0).css('left', '15px').css('right', '15px');
        }
    },

    renderFilesControll: function() {
        if ($('.element-item.active').length) {
            $('.files-not-selected').hide();
            $('.files-selected').show();

            if ($('.element-item.active').length == 1) {
                $('.renameEnt').show();
            } else {
                $('.renameEnt').hide();
            }
        } else {
            $('.files-not-selected').show();
            $('.files-selected').hide();
        }
    },

    resizeUnselectedZone: function() {
        height = $('body').height() - $('.files-controll').height() - 39 - 36 - 15 - 15 - 70;
        $('.unselect-zone').css('height', height + 'px');
    },

    getFileInfo: function(id) {
        trash.fileId = id;

        $('#fileInfoModal').modal('show');
        // $('#fileInfoModal .preloader').show();
        // $('#fileInfoModal .infoContent').hide();

        $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/bucket/getFileInfo',
            data:{
                'id':id,
            },
            dataType:'json',
            success:function(response) {
                var data = response.file;

                $('#fileInfoModal .preloader').hide();
                $('#fileInfoModal .infoContent').show();

                var link = '//' + location.host + '/d/' + data.code;
                var linkView = location.host + '/d/' + data.code;

                if (location.host == 'ezerbox.com') {
                    link = '//s.' + location.host + '/d/' + data.code;
                    linkView = 's.' + location.host + '/d/' + data.code;
                }


                $('#fileInfoModal .fileTitle').text(data.name);
                $('#fileInfoModal .size').text(data.size);
                $('#fileInfoModal .link').attr('href', link);
                $('#fileInfoModal .linkView').text(linkView);
                $('#fileInfoModal .type').text(data.ex_typeH);
                $('#fileInfoModal .createdAt').text(data.created_atH);
                $('#fileInfoModal .updatedAt').text(data.updated_atH);

                $('#fileInfoModal .views').text(data.views);
                $('#fileInfoModal .downloads').text(data.downloads);

                trash.renderPublicLinkInfo(data);

                if (data.ex_type == 'image') {
                    $('#fileInfoModal .imgPreview img, #fileInfoModal .imgPreview .icon').remove();
                    $('#fileInfoModal .imgPreview').append('<img>');
                    $('#fileInfoModal .imgPreview img').show().attr('src', link);
                    $('#fileInfoModal .image-preloader').show();
                    $('#fileInfoModal .imgPreview video').hide();
                } else if (data.ex_type == 'video') {
                    $('#fileInfoModal .imgPreview video, #fileInfoModal .imgPreview .icon').remove();
                    $('#fileInfoModal .imgPreview').append('<video></video>');
                    $('#fileInfoModal .imgPreview video').show().attr('src', link).prop('controls', true);
                    $('#fileInfoModal .image-preloader').show();
                    $('#fileInfoModal .imgPreview img').hide();
                } else {
                    $('#fileInfoModal .imgPreview img, #fileInfoModal .imgPreview .icon').remove();
                    $('#fileInfoModal .imgPreview video, #fileInfoModal .imgPreview .icon').remove();
                    $('#fileInfoModal .imgPreview').append('<i class="fas fa-file icon"></i>');
                    $('#fileInfoModal .image-preloader').hide();
                }

                // для стрелочек перелистывания вайлов
                if(response.left) {
                    $('.imgPreview .leftArrow').show().data('id', response.left);
                } else {
                    $('.imgPreview .leftArrow').hide();
                }
                if(response.right) {
                    $('.imgPreview .rightArrow').show().data('id', response.right);
                } else {
                    $('.imgPreview .rightArrow').hide();
                }
            }
        });
    },

    deleteEnt: function(ent) {
        $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/trash/deleteEnt',
            data:{
                'ent':ent,
            },
            dataType:'json',
            success:function(data) {
                for(i = 0; i < Object.keys(ent).length; i++) {
                    $('.element-item[data-id=' + ent[i]['id'] + ']').closest('.element-item-parent').remove();
                    // $('tr[data-type='+ent[i]['type']+'][data-id='+ent[i]['id']+']').detach();
                }
                $('.usedSpace').html(data.used);
                $('.spaceAvailable').html(data.spaceAailable);

                main.unlockButton('.deleteSelected');
                main.unlockButton('.deleteAll');
                // trash.getList(true);
                trash.renderFilesControll();
            },
            error: function () {
                main.popupMsg('Please select the files to delete', false);
                main.unlockButton('.deleteSelected');
            }
        });
    },

    deleteAll: function(ent) {
        $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/trash/deleteAll',
            data:{
                'ent':ent,
            },
            dataType:'json',
            success:function(data) {
                $('.element-item').closest('.element-item-parent').remove();

                // $('.usedSpace').html(data.used);
                // $('.spaceAvailable').html(data.spaceAailable);

                main.unlockButton('.deleteSelected');
                main.unlockButton('.deleteAll');
                // trash.getList(true);
                trash.renderFilesControll();
            },
            error: function () {
                main.popupMsg('Please select the files to delete', false);
                main.unlockButton('.deleteSelected');
            }
        });
    },

    restoreEnt: function(ent) {
        $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/trash/restoreEnt',
            data:{
                'ent':ent,
            },
            dataType:'json',
            success:function(data) {
                for(i = 0; i < Object.keys(ent).length; i++) {
                    if (data.restoreFailed.indexOf(ent[i]['id']) == -1) {
                        $('.element-item[data-id=' + ent[i]['id'] + ']').closest('.element-item-parent').remove();
                    }
                    // $('tr[data-type='+ent[i]['type']+'][data-id='+ent[i]['id']+']').detach();
                }

                $('.usedSpace').html(data.used);
                $('.spaceAvailable').html(data.spaceAailable);

                main.unlockButton('.restoreSelected');
                // trash.getList(true);
                trash.renderFilesControll();
            },
            error: function () {
                main.popupMsg('Please select the files to restore', false);
                main.unlockButton('.restoreSelected');
            }
        });
    },

    getList: function(dirsListLoad = false, $filesOffset = 0) {
        data = {};
        data['dir'] = trash.dir;
        data['dirListLoad'] = dirsListLoad;
        data['offset'] = $filesOffset;
        $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'/trash/getList',
            data:data,
            dataType:'json',
            success:function(data) {
                if ($filesOffset == 0) {
                    $('.files').html(data.dirs);
                    $('.files').append(data.files);
                } else {
                    $('.loadMoreFilesTrigger').remove();
                    $('.files').append(data.files);
                }

                trash.resizeUnselectedZone();

                $('.dirNavigation').html(data.nav);

                $('.dirInfoBar .foldersB').html(data.folderInfo.dirs);
                $('.dirInfoBar .filesB').html(data.folderInfo.files);
                $('.dirInfoBar .sizeB').html(data.folderInfo.size);

                if(data.dirList) $('.dirListSelect').html(data.dirList);

                if(data.percentUsed > 90) {
                    $('.usedSpacePercent .progress-bar').addClass('bg-danger').removeClass('bg-warning');
                } else if(data.percentUsed > 75) {
                    $('.usedSpacePercent .progress-bar').removeClass('bg-danger').addClass('bg-warning');
                } else {
                    $('.usedSpacePercent .progress-bar').removeClass('bg-danger').removeClass('bg-warning');
                }

                $('.select-all').prop('checked', false);

                $('.zonePreloader').hide();

                if (data.drawFileLoader) {
                    $('.files').append('<tr class="col-lg-2 element-item-parent loadMoreFilesTrigger">' +
                        '<td colspan="6" style="text-align: center">' +
                        '<i class="fa fa-cog fa-spin"></i>' +
                        '<span class="name">Loading</span>' +
                        '</td>' +
                        '</tr>');
                }

                trash.renderFilesControll();
                trash.fixFolderInfoPosition();

                // перезагрузка изображения в случае фейла
                $('.element-item img').bind('error', function() {
                    console.log($(this).attr('src'));
                    var img = $(this);
                    setTimeout(function () {
                        var src = img.attr('src').split('?')[0] + '?' + Date.now();
                        img.attr('src', src);
                    }, 1000);
                });

                // показ изображений после прогрузки
                $('.element-item img').bind('load', function() {
                    $(this).show();
                });
            }
        });
    },
}

trash.init();
