var comments = {

	init: function() {

		$('body').on('click', '.commentsArea .sendComment', function() {
			comments.send($(this).closest('.sendCommentForm'));
		});

		$('body').on('click', '.commentsArea .action.answer', function(e) {
			comments.prepareForm($(this).closest('.comment'));
		});

		$('body').on('click', '.commentsArea .action.delete', function() {
			if(!confirm('Delete?')) return false;
			comments.delete($(this).closest('.comment'));
		});

		$('body').on('click', '.commentsArea .likeComment', function() {
			comments.like($(this).closest('.comment').data('id'));
		});

		$('body').on('click', '.commentsArea .dislikeComment', function() {
			comments.dislike($(this).closest('.comment').data('id'));
		});

		// $('body').on('keypress', '.sendCommentForm .message', (function (e) {
		$(document).keypress(function(e) {
			if (e.target.className == 'form-control message' && e.which == 13) {
				comments.send($(e.target).closest('.sendCommentForm'));
				return false;    //<---- Add this line
			}
		});

	},

	prepareForm: function(comment) {
		comment.closest('.commentsArea').find('.sendCommentForm').not('.main').detach();
		comment.find('.placeToCommentForm:first').append(comment.closest('.commentsArea').find('.sendCommentForm.main').clone());
		comment.find('.sendCommentForm').removeClass('main').find('.message').val('');
	},

	send: function(form) {
		data = {};
		data['message'] = form.find('.message').val();
		data['parent'] = form.closest('.comment').data('id') == undefined ? 0 : form.closest('.comment').data('id');
		data['zone_id'] = form.closest('.commentsArea').data('zone');
		$.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/comment/send',
			data:{
				'data':data
			},
			dataType:'json',
			success:function(data) {
				$('.commentsArea').replaceWith(data);
			}
		});
	},

	delete: function(comment) {
		$.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/comment/delete',
			data:{
				'id':comment.data('id'),
				'zone_id':comment.closest('.commentsArea').data('zone')
			},
			dataType:'json',
			success:function(data) {
				$('.commentsArea').replaceWith(data);
			}
		});
	},

	like: function(id) {
		$.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/comment/like',
			data:{
				'id':id
			},
			dataType:'json',
			success:function(data) {

			}
		});
	},

	dislike: function(ic_id) {
		$.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type:'POST',
			url:'/comment/dislike',
			data:{
				'id':id
			},
			dataType:'json',
			success:function(data) {

			}
		});
	},

}

comments.init();