var api = {

    init: function () {
        $('.createToken').click(function () {
            api.createToken();
        });

        $('.deleteToken').click(function () {
            api.deleteToken($(this).data('id'));
        });
    },

    createToken: function () {
        let desc = '';
        Swal.fire({
            title: lang.get('token_create_title'),
            input: 'text',
            text: lang.get('token_desc_enter'),
            inputValue: desc,
            showCancelButton: true,
            cancelButtonText: lang.get('cancel')
        }).then((result) => {
            if (result.isConfirmed) {
                main.lockButton('.createToken');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: '/api/manage/token/create',
                    data: {
                        desc: result.value
                    },
                    dataType: 'json',
                    success: function (data) {
                        // main.unlockButton('.createToken');
                        location.reload();
                    }
                });
            }
            console.log(result);
        });
    },

    deleteToken: function (id) {
        Swal.fire({
            title: lang.get('are_you_sure'),
            text: lang.get('token_delete_warning'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: lang.get('token_delete_confirm'),
            cancelButtonText: lang.get('cancel')
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: '/api/manage/token/delete',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });
    },
}

api.init();
