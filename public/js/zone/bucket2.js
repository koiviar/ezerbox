const bucket = {
    data() {
        return {
            fileControl: false,
            showDownload: false,
            showRename: false,
            openFile: {},
            modalFileShow: 'none',
            path: {},
            selected: {},
            stat: {
                dirs: 0,
                files: 0,
                size: 0
            },
            uploader: {
                dropZone: '',
                dropZoneFlag: false,
                inProgress: false,
                allSize: 0,
                allSizeNow: 0,
                list: {},
                ajax: null,
                percent: 0,
                allFiles: 0,
                uploadedFiles: 0,
            },
            loader: true,
            dir: 0,
            dirs: [],
            files: [],
            loadMoreFilesTrigger: false,
            dirsHierarchy: [],
            lastDir: -1,
            openFileIndex: -1,
            listLoading: false,
            images: {},
            cdnFolder: null,
            downloadFileLink: '#'
        }
    },

    created: function () {
        history.pushState('', document.title, window.location.pathname);

        this.getToken(true);

        this.uploaderInit();

        document.addEventListener('keydown', this.keyWatch);

        setInterval(() => {
            this.loadMoreFiles();
        }, 1000);
    },

    methods: {
        getToken(start = false) {
            axios.get('/csrf-token').then((response) => {
                this.token = response.data.token;
                if (start) {
                    this.getList(false, sessionStorage.getItem('dirId'));
                }
            });
        },

        loadMoreFiles() {
            if (!this.loader && this.loadMoreFilesTrigger) {
                let top = document.getElementById("loadMoreFilesRef").offsetTop;
                if (main.isTopEdgeVisible('ne nuzno', top)) {
                    this.getList(false, this.dir, this.files.length);
                }
            }
        },

        // запросы
        getList(onlyDirs = false, id = 0, offset = 0) {
            if (this.listLoading) {
                return false;
            }

            sessionStorage.setItem('dirId', id);
            this.dir = id;
            this.uploaderSetDir();

            if (!offset) {
                this.loader = true;
                this.unselectAll();
            }

            this.listLoading = true;

            axios.post('/disk/getList', {
                dir: this.dir,
                offset: offset
            }, {
                headers: {'X-CSRF-TOKEN': this.token}
            }).then((response) => {
                this.cdnFolder = response.data.cdn_folder;

                this.listLoading = false;
                if (!onlyDirs) {
                    if (!offset) {
                        this.files = [];
                    }
                    let file;
                    let host = '' + location.host;
                    for (let i = 0; i < response.data.files.length; i++) {
                        file = response.data.files[i];
                        this.files.push(file);

                        // if (file.ex_type === 'image') {
                        //     this.images[file.id] = new Image();
                        //     this.images[file.id].addEventListener("load", function() {
                                // console.log(1);
                            // }, false);

                            // this.images[file.id].src = '//' + host + '/d/255x140/' + file.code;
                            // console.log(host);
                            // console.log('//' + host + '/d/255x140/' + file.code);
                        // }
                    }
                }
                // console.log(this.images);
                this.path = response.data.nav;

                if (response.data.dirs !== undefined) {
                    this.dirs = [];
                    for (let i = 0; i < response.data.dirs.length; i++) {
                        this.dirs.push(response.data.dirs[i]);
                    }
                }

                if (offset === 0) {
                    this.dirStat();
                }

                // this.stat.dirs = response.data.folderInfo.dirs ? response.data.folderInfo.dirs : {};
                // this.stat.size = response.data.folderInfo.size;

                this.loadMoreFilesTrigger = response.data.drawFileLoader;

                this.loader = false;
            }).catch(function (error) {
                this.getToken();

                this.listLoading = false;
                this.getList(onlyDirs, id, offset);
            });
        },

        dirStat() {
            axios.post('/disk/dirStat', {  dir: this.dir }, {
                headers: {'X-CSRF-TOKEN': this.token}
            }).then((response) => {
                if (this.dir === response.data.id) {
                    this.stat.size = response.data.folderInfo.size;
                    this.stat.files = response.data.folderInfo.files;
                    this.stat.dirs = response.data.folderInfo.dirs;
                }
            }).catch(function (error) {
                this.getToken();
            });
        },

        removeActiveItems() {
            var i = 0;
            while (i < this.dirs.length) {
                if (this.dirs[i].isActive) {
                    this.dirs.splice(i, 1);
                } else {
                    ++i;
                }
            }

            var i = 0;
            while (i < this.files.length) {
                if (this.files[i].isActive) {
                    this.files.splice(i, 1);
                } else {
                    ++i;
                }
            }
        },

        getSelectedFileIndex() {
            for (let i = 0; i < this.files.length; i++) {
                if (this.files[i].isActive) {
                    return i;
                }
            }
            return false;
        },

        getSelectedFile() {
            for (let i = 0; i < this.dirs.length; i++) {
                if (this.dirs[i].isActive) {
                    return this.dirs[i];
                }
            }
            for (let i = 0; i < this.files.length; i++) {
                if (this.files[i].isActive) {
                    return this.files[i];
                }
            }
            return false;
        },

        moveFiles() {
            main.unlockButton('.moveFiles');

            const inputOptions = new Promise((resolve) => {
                axios.post('/disk/dirsHierarchy', {}, {
                    headers: {'X-CSRF-TOKEN': this.token}
                }).then((response) => {
                    this.dirsHierarchy = response.data.dirs;

                    let options = [];
                    for (let i = 0; i < this.dirsHierarchy.length; i++) {
                        options.push(this.dirsHierarchy[i].name);
                    }

                    resolve(options);
                }).catch(function (error) {
                    this.getToken();
                });
            })

            let ent = [];
            for (let i = 0; i < this.dirs.length; i++) {
                if (this.dirs[i].isActive) {
                    ent.push({id: this.dirs[i].id, type: 'dir'});
                }
            }
            for (let i = 0; i < this.files.length; i++) {
                if (this.files[i].isActive) {
                    ent.push({id: this.files[i].id, type: 'file'});
                }
            }

            Swal.fire({
                title: lang.get('move'),
                showCancelButton: true,
                cancelButtonText: lang.get('cancel'),
                input: 'select',
                inputOptions: inputOptions
            }).then((result) => {
                if (result.isConfirmed) {
                    main.lockButton('.rename');

                    let id = this.dirsHierarchy[result.value].id;

                    axios.post('/disk/move', { ent: ent, dir: this.dir, targetDir: id }, {
                        headers: {'X-CSRF-TOKEN': this.token}
                    }).then((response) => {
                        this.removeActiveItems();

                        main.unlockButton('.moveFiles');
                    }).catch(function (error) {
                        this.getToken();
                    });
                }
            });

            // let targetDir = 0;
            //

        },

        rename() {
            let file = this.getSelectedFile();
            let name = file.name;
            Swal.fire({
                title: lang.get('rename'),
                input: 'text',
                inputValue: name,
                showCancelButton: true,
                cancelButtonText: lang.get('cancel')
            }).then((result) => {
                if (result.isConfirmed) {
                    main.lockButton('.rename');

                    axios.post('/disk/rename', {
                        id: file.id,
                        type: file.type === undefined ? 'dir' : 'file',
                        newName: result.value
                    }, {
                        headers: {'X-CSRF-TOKEN': this.token}
                    }).then((response) => {
                        file.name = response.data.name;
                        main.unlockButton('.rename');
                    }).catch(function (error) {
                        this.getToken();
                    });
                }
            });
        },

        createDir() {
            let name = '';
            Swal.fire({
                title: lang.get('create_dir'),
                input: 'text',
                text: lang.get('dir_name'),
                inputValue: name,
                showCancelButton: true,
                cancelButtonText: lang.get('cancel')
            }).then((result) => {
                if (result.isConfirmed) {
                    main.lockButton('.createDir');

                    this.loader = true;
                    axios.post('/disk/createDir', {
                        dir: this.dir,
                        name: result.value
                    }, {
                        headers: {'X-CSRF-TOKEN': this.token}
                    }).then((response) => {
                        this.getList(true, this.dir);

                        main.unlockButton('.createDir');
                    }).catch(function (error) {
                        this.getToken();
                    });
                }
            });
        },

        deleteEnt() {
            main.unlockButton('.deleteSelected');

            let ent = [];
            for (let i = 0; i < this.dirs.length; i++) {
                if (this.dirs[i].isActive) {
                    ent.push({id: this.dirs[i].id, type: 'dir'});
                }
            }
            for (let i = 0; i < this.files.length; i++) {
                if (this.files[i].isActive) {
                    ent.push({id: this.files[i].id, type: 'file'});
                }
            }

            axios.post('/disk/delete', {
                ent: ent,
                dir: this.dir,
                dirListLoad: false
            }, {
                headers: {'X-CSRF-TOKEN': this.token}
            }).then((response) => {
                this.removeActiveItems();
                this.unselectAll();

                main.unlockButton('.deleteSelected');
            }).catch(function (error) {
                this.getToken();
            });
        },

        // клики и прочее
        changeControl() {
            if (this.selected.length > 0) {
                this.fileControl = true;
            } else {
                this.fileControl = false;
            }
            if (this.selected.length === 1) {
                this.showDownload = this.selected[0].type !== undefined;
                this.showRename = true;
            } else {
                this.showDownload = false;
                this.showRename = false;
            }
        },

        select(e, file) {
            if (e.ctrlKey) {
                this.ctrlClick(file);
                this.changeControl();
                return false;
            } else if (e.shiftKey) {
                this.shiftClick(file);
                this.changeControl();
                return false;
            } else if (e.dblClick) {
                this.open(file);
                return false;
            }

            this.unselectAll();
            file.isActive = true;
            this.downloadFileLink = file.download_link;
            console.log(file);
            console.log(this.downloadFileLink);
            this.selected = [file];
            this.changeControl();
        },

        ctrlClick(file) {
            file.isActive = true;
            this.selected.push(file);
        },

        shiftClick(file) {
            console.log('shift');
        },

        open(file) {
            // return false;
            if (!file.type) {
                this.unselectAll();
                this.getList(false, file.id);
            } else {
                for (let i = 0; i < this.files.length; i++) {
                    if (this.files[i].isActive) {
                        this.openFileIndex = i;
                        break;
                    }
                }

                location.hash = 'file-' + file.id;

                if (this.openFile) {
                    this.openFile = null;
                }

                this.openFile = file;
                this.openFile.publicLink = null;
                this.modalFileShow = 'block';

                if (this.openFile.public_code) {
                    this.openFile.publicLink = this.getCdnLink(file.public_code, file.extension);
                }

                axios.post('/disk/getFileInfo', {
                    id: this.openFile.id
                }, {
                    headers: {'X-CSRF-TOKEN': this.token}
                }).then((response) => {
                    let index = this.getSelectedFileIndex();
                    if (index) {
                        if (this.files[index].id === response.data.file.id) {
                            this.files[index] = response.data.file;
                            this.openFile = this.files[index];
                        }
                    }

                    this.openFile.isActive = true;

                    if (this.openFile.public_code) {
                        this.cdnFolder = response.data.cdn_folder;
                        this.openFile.publicLink = this.getCdnLink(response.data.file.public_code, response.data.file.extension);
                        this.openFile.public_code = response.data.file.public_code;
                    }
                }).catch(function (error) {
                    this.getToken();
                });
            }
        },

        getCdnLink(code, extension) {
            return 'cdn.' + location.host + '/p/' + this.cdnFolder + '/' + code.substr(0, 2) + '/' + code + extension;
        },

        leftArrow() {
            if (this.openFileIndex > 0) {
                this.files[this.openFileIndex].isActive = false;
                this.openFileIndex--;
                this.files[this.openFileIndex].isActive = true;
                this.open(this.files[this.openFileIndex]);
            }
        },

        rightArrow() {
            if (this.openFileIndex + 1 < this.files.length) {
                this.files[this.openFileIndex].isActive = false;
                this.openFileIndex++;
                this.files[this.openFileIndex].isActive = true;
                this.open(this.files[this.openFileIndex]);
            }
        },

        unselectAll() {
            this.fileControl = false;
            this.selected = [];

            for (let i = 0; i < this.dirs.length; i++) {
                this.dirs[i].isActive = false;
            }
            for (let i = 0; i < this.files.length; i++) {
                this.files[i].isActive = false;
            }
            this.changeControl();
            this.downloadFile = '#';
        },

        closeModal() {
            this.openFile = {};
            this.modalFileShow = 'none';
            history.pushState('', document.title, window.location.pathname);
        },

        keyWatch(event) {
            if (event.code === 'Escape') {
                if (this.modalFileShow === 'block') {
                    this.closeModal();
                } else {
                    this.unselectAll();
                }
                this.changeControl();
            }
            if (event.code == 'ArrowLeft') {
                if (this.modalFileShow === 'block') {
                    this.leftArrow();
                }
            }
            if (event.code == 'ArrowRight') {
                if (this.modalFileShow === 'block') {
                    this.rightArrow();
                }
            }
            // console.log(event.code);
        },

        createCDN() {
            main.lockButton('.createPublicLink');
            axios.post('/disk/createPublicLink', {
                id: this.openFile.id
            }, {
                headers: {'X-CSRF-TOKEN': this.token}
            }).then((response) => {
                main.unlockButton('.createPublicLink');
                this.openFile.publicLink = this.getCdnLink(response.data.public_code, response.data.extension);
                this.openFile.public_code = response.data.public_code;
                let file = this.getSelectedFile();
                file = response.data;
            }).catch(function (error) {
                this.getToken();
            });
        },

        deleteCDN() {
            main.lockButton('.deletePublicLink');
            axios.post('/disk/deletePublicLink', {
                id: this.openFile.id
            }, {
                headers: {'X-CSRF-TOKEN': this.token}
            }).then((response) => {
                main.unlockButton('.deletePublicLink');
                this.openFile.publicLink = null;
                this.openFile.public_code = null;
            }).catch(function (error) {
                this.getToken();
            });
        },

        // загрузчик
        uploaderInit() {
            // загрузчик файлов
            // Uploader = new plupload.Uploader();
            this.uploaderSetDir();
            // Uploader.init();
            Uploader.bind('FilesAdded', (up, files) => {
                this.uploader.allFiles = 0;
                this.uploader.uploadedFiles = 0;
                this.uploader.allSize = 0;
                this.uploader.allSizeNow = 0;
                this.uploader.list = {};

                plupload.each(files, (file) => {
                    this.uploader.allSize += file.size;
                    this.uploader.list[file.id] = {
                        name: file.name,
                        size: file.size,
                        percent: 0
                    };
                    this.uploader.allFiles++;
                });

                // общий загрузка прогресс
                this.uploader.allSizeNow = this.uploader.allSize;

                // проверка баланса
                if (this.uploader.ajax != null) this.uploader.ajax.abort();
                this.uploader.ajax = $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: '/disk/check-money',
                    data: {
                        size: bucket.allSize
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.error === 0) {

                            $('.uploader-files-list').show();
                            Uploader.start();
                            bucket.uploadInProgress = true;

                        } else {
                            plupload.each(files, function(file) {
                                Uploader.removeFile(file);
                            });

                            if (data.error === 1) {
                                Swal.fire({
                                    title: 'Not enough money!',
                                    text: "To use over limits storage, your balance must be more than " + data.data.need + ' €',
                                    icon: 'error',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Add money'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        order.showBalanceModal(data.data.to_fill);
                                    }
                                });
                            } else if (data.error === 2) {
                                Swal.fire({
                                    title: data.msg,
                                    text: data.data.text,
                                    icon: 'error',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Change plan'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.href = '/plans';
                                    }
                                });
                            }

                        }
                    }
                });
            });
            Uploader.bind('FileUploaded', (up, file) => {
                // $('#' + file.id + ' .progress-bar').addClass('bg-success');

                this.uploader.list[file.id].percent = '100%';
                this.uploader.list[file.id].bg = 'bg-success';
                this.uploader.uploadedFiles++;

                this.uploader.allSizeNow -= file.size;
                let percent = (this.uploader.allSize - this.uploader.allSizeNow) / (this.uploader.allSize / 100);
                percent = (Math.round(percent * 100)) / 100;
                this.uploader.percent = percent + '%';
                // $('#total-progress .progress-bar').css('width', percent + '%').html(percent + '%');
            });
            Uploader.bind('UploadProgress', (up, file) => {
                this.uploader.list[file.id].percent = file.percent + '%';
                // $('#' + file.id + ' .progress-bar').css('width', file.percent + '%').html(file.percent + '%');
            });
            Uploader.bind('UploadComplete', () => {
                this.uploader.percent = '100%';
                // $('#total-progress .progress-bar').css('width', 100 + '%').html(100 + '%').addClass('bg-success');

                this.uploader.uploadInProgress = false;
                Uploader._options.headers.dir = this.dir;
                setTimeout(() => {
                    this.uploader.list = {};
                    this.uploader.percent = '0%';
                }, 3000);
                this.getList(false, this.dir);
            });
        },

        uploaderSetDir() {
            setTimeout(() => {
                if (!this.uploader.uploadInProgress) {
                    Uploader.setOptions({
                        //browse_button: 'browse2', // this can be an id of a DOM element or the DOM element itself
                        url: '/disk/upload-chunk',
                        chunk_size: '5000kb',
                        max_retries: 10,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'dir': this.dir
                        },
                        drop_element: 'dropPrepare'
                    });
                }
            }, 500);
        },

        sizeToHuman(size) {
            if (size / 1024 < 1024) {
                return Math.round((size / 1024) * 100) / 100 + ' Kb';
            } else if (size / 1024 / 1024 < 1024) {
                return Math.round((size / 1024 / 1024) * 100) / 100 + ' Mb';
            } else {
                return Math.round((size / 1024 / 1024 / 1024) * 100) / 100 + ' Gb';
            }
        },

        dragover(event) {
            event.preventDefault();
            // Add some visual fluff to show the user can drop its files
            this.uploader.dropZone = 'show';
            this.uploader.dropZoneFlag = true;
        },
        dragleave(event) {
            // Clean up
            this.uploader.dropZoneFlag = false;
            setTimeout(() => {
                if (!this.uploader.dropZoneFlag) {
                    this.uploader.dropZone = '';
                }
            }, 500);

        },
        drop(event) {
            this.uploader.dropZoneFlag = false;
            setTimeout(() => {
                if (!this.uploader.dropZoneFlag) {
                    this.uploader.dropZone = '';
                }
            }, 500);
        }
    }
}

let Uploader = new plupload.Uploader({
    browse_button: 'browse2'
});
setTimeout(() => {
    Uploader.init();
}, 2000);


// var VuePluload = require("vue-plupload");
// Vue.use('vue-plupload');

Vue.createApp(bucket).mount('#bucket');
