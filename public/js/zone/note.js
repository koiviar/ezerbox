$(document).ready(function() {

	var note = {

		group: -1,
		searchInput: '',
		noteSortList: [],
		groupSortList: [],
		inited: false,
		saveTimer: -1,
		saveStatus: true,
		editor: undefined,
		contentToCheck: '',
		timeToSave: 12,

		init: function() {

			note.inited = true;

			note.getList();

			note.editor = tinymce.init({
			    selector: '#noteModal [name=content]'
			});

			$('body').on('click', '.createUpdateNote', function() {
				note.createUpdateNote();
			});

			$('body').on('click', '.newNote', function() {
				$('#noteModal').data('type', 'create');
				$('#noteModal').modal('show');
				note.clearModal();
				groupId = note.group == -1 ? 0 : note.group;
				$('#noteModal .noteGroup').val(groupId);
			});

			$('body').on('click', '.note', function() {
				$('#noteModal').data('type', 'update');
				$('#noteModal').modal('show');
				note.clearModal();
				note.getDataNote($(this).data('id'));
			});

			$('body').on('click', '.deleteNote', function() {
				if (!confirm('Удалить заметку?')) return false;
				note.deleteNote($('#noteModal').data('id'));
			});

			//

			$('body').on('click', '.createGroup', function() {
				note.createGroup();
			});

			$('body').on('click', '.editGroup', function() {
				$('#editGroup').modal('show');
				$('#editGroup input').val('');
				note.getDataGroup($(this).parents('.group').data('id'));
			});

			$('body').on('click', '.saveGroup', function() {
				note.saveGroup();
			});

			$('body').on('click', '.deleteGroup', function() {
				if (!confirm('Удалить группу?')) return false;
				note.deleteGroup($('#editGroup').data('id'));
			});

			$('body').on('click', '.group', function() {
				$('.group').removeClass('active');
				$(this).addClass('active');
				note.group = $(this).data('id');
				note.getList();
			});

			setInterval(function() {
				if (note.searchInput != $('.search').val()) {
					note.searchInput = $('.search').val();
					note.search();
				}
			}, 1000);

			// для сохранения автоматического
			// setInterval(function() {
			// 	note.saveTimer--;
			// 	if (note.saveTimer == 0) {
			// 		note.createUpdateNote();
			// 	}
			// 	// check editor state
			// 	content = tinymce.activeEditor.getContent();
			// 	if (note.contentToCheck != content) {
			// 		note.contentToCheck = content;
			// 		note.saveStatus = false;
			// 		note.saveTimer = note.timeToSave;
			// 	}
			// 	// отображение статуса сохранения
			// 	note.renderStatusInfo();
			// }, 100);

			$('body').on('keyup', '#noteModal [name=content], #noteModal [name=title]', function() {
				note.saveStatus = false;
				note.saveTimer = note.timeToSave;

				$('.note-ok-status').hide();
				$('.note-fail-status').show();
			});

			$('body').on('change', '#noteModal .noteGroup', function() {
				note.saveStatus = false;
				note.saveTimer = note.timeToSave;

				$('.note-ok-status').hide();
				$('.note-fail-status').show();
			});

		},

		renderStatusInfo: function() {
			if (note.saveStatus) {
				$('.note-ok-status').show();
				$('.note-fail-status').hide();
			} else {
				$('.note-ok-status').hide();
				$('.note-fail-status').show();
			}
		},

		changeNoteSort: function() {
			ids = [];
			$('.noteListSection .note').each(function() { ids.push($(this).data('id')); });
			data = {};
			data['ids'] = ids;
			data['sort'] = note.noteSortList;
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/changeNoteSort',
				data: data,
				dataType: 'json',
				success: function(data) {
					// note.getList();
				}
			});
		},

		changeGroupSort: function() {
			ids = [];
			$('.groupListSection .group:not(.notSorte)').each(function() { ids.push($(this).data('id')); });
			data = {};
			data['ids'] = ids;
			data['sort'] = note.groupSortList;
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/changeGroupSort',
				data: data,
				dataType: 'json',
				success: function(data) {
					// note.getList();
				}
			});
		},

		sortableInit: function() {
			// для заметок
			note.noteSortList = [];
			$('.noteListSection .note').each(function() {
				note.noteSortList.push($(this).data('sort'));
			});
			$('.noteListSection').sortable({
	    		placeholder: "ui-note-highlight",
	    		delay: 100,
	    		stop: function() { note.changeNoteSort() },
		    });
		    // для групп
    		note.groupSortList = [];
    		$('.groupListSection .group:not(.notSorte)').each(function() {
    			note.groupSortList.push($(this).data('sort'));
    		});
    		$('.groupListSection').sortable({
        		placeholder: "ui-note-highlight",
        		delay: 100,
        		items: "> div:not(.notSorte)",
        		stop: function() { note.changeGroupSort() },
    	    });
		},

		search: function() {
			data = {};
			data['group_id'] = note.group;
			data['search'] = note.searchInput;
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/search',
				data: data,
				dataType: 'json',
				success: function(data) {
					$('.noteListSection').html(data.notes);
					note.sortableInit();
				}
			});
		},

		deleteGroup: function(id) {
			$('#editGroup').modal('hide');
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/deleteGroup',
				data: {
				    'id': id
                },
				dataType: 'json',
				success: function(data) {
					note.getList();
				}
			});
		},

		saveGroup: function() {
			if ($('#editGroup [name=title]').val() == '') {
				$('#editGroup [name=title]').focus().css('border-color', 'red');
				return false;
			} else {
				$('#editGroup [name=title]').css('border-color', '#ccc');
			}
			data = {};
			data['title'] = $('#editGroup [name=title]').val();
			data['id'] = $('#editGroup').data('id');
			$('#editGroup').modal('hide');
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/saveGroup',
				data: data,
				dataType: 'json',
				success: function(data) {
					note.getList();
				}
			});
		},

		getDataGroup: function(id) {
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/getDataGroup',
				data: {'id': id},
				dataType: 'json',
				success: function(data) {
					$('#editGroup').data('id', data.id);
					$('#editGroup [name=title]').val(data.title);
				}
			});
		},

		createGroup: function() {
			data = {};
			data['title'] = $('#groupModal [name=title]').val();
			$('#groupModal').modal('hide');
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/createGroup',
				data: data,
				dataType: 'json',
				success: function(data) {
					note.getList();
				}
			});
		},

		//

		deleteNote: function(id) {
			$('#noteModal').modal('hide');
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/delete',
				data: {'id': id},
				dataType: 'json',
				success: function(data) {
					note.getList();
				}
			});
		},

		clearModal: function() {
			$('#noteModal input, #noteModal textarea').val('');
			$('#noteModal').data('id', '');
			tinymce.activeEditor.setContent('');
		},

		getDataNote: function(id) {
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/getData',
				data: {
				    'id': id
                },
				dataType: 'json',
				success: function(data) {
					$('#noteModal').data('id', data.id);
					$('#noteModal [name=title]').val(data.title);
					// $('#noteModal [name=content]').val(data.content);
					$('#noteModal .noteGroup').val(data.group_id);
					if (data.content) {
						tinymce.activeEditor.setContent(data.content);
						note.contentToCheck = data.content;
					} else {
						tinymce.activeEditor.setContent('');
						note.contentToCheck = '';
					}
					note.saveStatus = true;
					note.renderStatusInfo();
				}
			});
		},

		getList: function() {
			data = {};
			data['group_id'] = note.group;
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/getList',
				data: data,
				dataType: 'json',
				success: function(data) {
					savedGroup = undefined;
					if ($('#noteModal .noteGroup').is(':visible')) {
						savedGroup = $('#noteModal .noteGroup').val();
					}

					$('.noteListSection').html(data.notes);
					$('.groupListSection').html(data.groups);
					$('.noteGroup').html(data.groupOptions);
					$('.group[data-id='+note.group+']').addClass('active');
					note.sortableInit();

					if (savedGroup) {
						$('#noteModal .noteGroup [value="' + savedGroup + '"]').attr('selected', 'selected');
					}

					$('.zonePreloader').hide();
				}
			});
		},

		createUpdateNote: function() {
			if ($('#noteModal [name=title]').val() == '') {
				$('#noteModal [name=title]').focus().css('border-color', 'red');
				return false;
			} else {
				$('#noteModal [name=title]').css('border-color', '#ccc');
			}
			data = {};
			data['title'] = $('#noteModal [name=title]').val();
			// data['content'] = $('#noteModal [name=content]').val();
			data['group_id'] = $('#noteModal .noteGroup').val();
			data['content'] = tinymce.activeEditor.getContent();
			if ($('#noteModal').data('type') == 'create') {
				type = 'create';
			} else {
				type = 'update';
				data['id'] = $('#noteModal').data('id');
			}
			// $('#noteModal').modal('hide');
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: 'POST',
				url: '/note/' + type,
				data: data,
				dataType: 'json',
				success: function(data) {
					note.getList();
					note.saveStatus = true;
					$('#noteModal').data('id', data);
					$('#noteModal').data('type', 'update')

                    $('#noteModal').modal('hide');
				}
			});
		},

	};

	if (!note.inited) note.init();

});
