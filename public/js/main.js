var main = {

	init: function() {

		$(document).ajaxError(function(event, jqxhr, settings, exception) {
		    if (jqxhr.status === 419  && jqxhr.responseJSON.message === 'CSRF token mismatch.') {
		        location.reload();
		    }

		    if(jqxhr.responseJSON !== undefined && jqxhr.responseJSON.exception !== undefined) {
		    	console.error(
		    		'Ошибка - '+jqxhr.responseJSON.exception+"\n\n"+
		    		'Файл - '+jqxhr.responseJSON.file+"\n\n"+
		    		'Строка - '+jqxhr.responseJSON.line+"\n\n"+
		    		'Сообщение ошибки - '+jqxhr.responseJSON.message);

		    	main.unlockAjaxButtons();
		    }
		});

		$(document).ajaxSuccess(function(event, jqxhr, settings) {
			var msgS, msgE;

			if (msgS = jqxhr.responseJSON.popupSuccess) {
				main.popupMsg(msgS);
			}

			if (msgE = jqxhr.responseJSON.popupError) {
				main.popupMsg(msgE, false);
			}

		});

		// смена языка
		$('body').on('click', '.changeLang .elem' , function() {
			$.ajax({
				type:'GET',
				url:'/changeLang/'+$(this).data('val'),
				success:function(data) {
					location.reload();
				}
			});
		});

		// сайдба скрываем или нет
		$('body').on('click', '.sideBarMenuActivator' , function() {
			if($('body').hasClass('sideBarHide')) {
				$('body').removeClass('sideBarHide');
			} else {
				$('body').addClass('sideBarHide');
			}
		});

		// для цифровых инпутов
        $('body').on('input', '.number', function () {
            var value = $(this).val();

            if (value !== '') {
                value = +value.match(/\d+/g)[0];
            }

            if (value > +$(this).attr('max')) {
                value = $(this).attr('max');
            }

            if (value < +$(this).attr('min')) {
                value = $(this).attr('min');
            }

            $(this).val(value);
        });

		main.showHideSideBar();
		$(window).resize(function() {
			main.showHideSideBar();
		});

		main.setVhSize();
		window.addEventListener('resize', () => {
			main.setVhSize();
		});

	},

	popupMsg: function(msg, success = true) {
		var object = $('<div class="alert alert-' + (success ? 'success' : 'danger') + ' popupFixed">' + msg + '</div>').appendTo('body');

		object.css('left', (window.innerWidth / 2 - object.innerWidth() / 2) + 'px')

		setInterval(function () {
			object.addClass('show');
		}, 0);

		setTimeout(function () {
			if (object) object.remove();
		}, 4000);
	},

	setVhSize: function() {
		var vh = window.innerHeight * 0.01;
			document.documentElement.style.setProperty('--vh', `${vh}px`);
	},

	showHideSideBar: function() {
		if (window.innerWidth > 850) {
			$('body').removeClass('sideBarHide');
			Cookies.remove('sideBarHide');
		} else {
			$('body').addClass('sideBarHide');
			Cookies.set('sideBarHide', 'sideBarHide');
		}
	},

	lockButton: function(selector, text = 'обработка', $ajax = true) {
		textOnBtn = $(selector).html();
		$(selector).html('<span class="hideText">' + textOnBtn + '</span>'+
			'<span class="workText"><i class="fas fa-cog fa-spin"></i> ' + text + '</span>').prop('disabled', true);
        $(selector).addClass('ajaxButtonLoading');
	},

	unlockButton: function(selector) {
		$(selector).html($(selector).find('.hideText').html()).prop('disabled', false);
	},

    unlockAjaxButtons() {
	    $('.ajaxButtonLoading').each(function () {
	        main.unlockButton('.' + $(this).attr('class').split(' ').join('.'));
        })
    },

	isVisible: function(elem) {
	    var docViewTop = $(window).scrollTop();
	    var docViewBottom = docViewTop + window.innerHeight;

	    var elemTop = $(elem).offset().top;
	    var elemBottom = elemTop + $(elem).outerHeight();
		var margin = $(elem).css('margin-bottom');
		if (margin != 'undefined') {
			elemBottom -= margin.replace('px', '');
		}

	    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	},

	isTopEdgeVisible: function(elem, top = null) {
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + window.innerHeight;

        if (top) {
            var elemTop = top;
        } else {
            var elemTop = $(elem).offset().top;
        }

	    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
	},

    isBottomEdgeVisible: function(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + window.innerHeight;

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).outerHeight();
		var margin = $(elem).css('margin-bottom');
        if (margin != 'undefined') {
			elemBottom -= margin.replace('px', '');
		}

        return ((elemBottom <= docViewBottom) && (elemBottom >= docViewTop));
    }

};

main.init();
